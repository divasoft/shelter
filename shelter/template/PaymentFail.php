<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<h1><?=$arMessages["PaymentFail"]["title"][MESSAGE_LANG];?></h1>
<p><?=$arMessages["PaymentFail"]["text"][MESSAGE_LANG];?></p>
<script>setTimeout(function () {
   window.location.href = "<?=DivasoftShelter::ORIGINAL_SITE ?>";
}, 2000);</script>