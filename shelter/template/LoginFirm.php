<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<form action="" method="post">
<input type="hidden" name="ucs_checkcode" value="<?=$arResult['captcha'];?>" />
<h1><?=$arMessages["LoginFirm"]["head"][MESSAGE_LANG];?></h1>
<div <?if ($arResult['mess'] != ''){?>class="ucs_textcolor"<?}?>><?=$arResult['mess'];?></div>
<table class="ucs_add_guest">
	<tr>
		<td><?=$arMessages["LoginFirm"]["login"][MESSAGE_LANG];?></td>
		<td><input type="text" name="login" class="ucs_width_150" value="<?=protection($_POST['login']);?>"></td>
	</tr>
	<tr>
		<td valign="top"><?=$arMessages["LoginFirm"]["pass"][MESSAGE_LANG];?></td>
		<td><input type="password" name="pass" class="ucs_width_150" value=""></td>
	</tr>
	<tr>
		<td colspan="2" align="center" style="padding-top:5px;"><input type="submit" class="ucs_submit" value="<?=$arMessages["LoginFirm"]["submit"][MESSAGE_LANG];?>" /></td>
	</tr>
</table>
</form>