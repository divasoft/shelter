<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<form action="" method="post">
<h1><?=$arMessages["AboutUser"]["head"][MESSAGE_LANG];?></h1>
<? if ($arResult['mess'] != '') { ?>
<p class="ucs_textcolor"><?=$arResult['mess'];?></p>
<? } ?>
<table class="ucs_add_guest ucs_ptable">	
	<tr>
		<td><?=$arMessages["AboutUser"]["email"][MESSAGE_LANG];?><span>*</span></td>
		<td><input type="text" name="email" value="<?=$GUEST['email'];?>" class="ucs_width_250"></td>
	</tr>
	<tr>
		<td><?=$arMessages["AboutUser"]["name"][MESSAGE_LANG];?><span>*</span></td>
		<td><input type="text" name="name" value="<?=$GUEST['name'];?>" class="ucs_width_250"></td>
	</tr>
	<tr>
		<td><?=$arMessages["AboutUser"]["name1"][MESSAGE_LANG];?><span>*</span></td>
		<td><input type="text" name="name1" value="<?=$GUEST['name1'];?>" class="ucs_width_250"></td>
	</tr>
	<tr>
		<td><?=$arMessages["AboutUser"]["name2"][MESSAGE_LANG];?></td>
		<td><input type="text" name="name2" value="<?=$GUEST['name2'];?>" class="ucs_width_250"></td>
	</tr>
	<tr>
		<td><?=$arMessages["AboutUser"]["phone"][MESSAGE_LANG];?><span>*</span></td>
		<td><input type="text" name="phone" value="<?=$GUEST['phones'];?>" class="ucs_width_250"><?if (FORMAT_PHONE){?><div class="ucs_fphone"><?=$arMessages["Reserv"]["formatphone"][MESSAGE_LANG];?></div><?}?></td>
	</tr>	
	<tr>
		<td><?=$arMessages["AboutUser"]["aboutpass"][MESSAGE_LANG];?></td>
		<td><input type="password" name="pass" class="ucs_width_250" value=""><div><font><?=$arMessages["AboutUser"]["aboutpassinfo"][MESSAGE_LANG];?></font></div></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" class="ucs_submit" value="<?=$arMessages["AboutUser"]["submit"][MESSAGE_LANG];?>" /></td>
	</tr>
</table>
</form>