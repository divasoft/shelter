﻿<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<h1><?=$arMessages["Rules"]["head"][MESSAGE_LANG];?></h1>

<? if ($arResult['lang'] == "RU") { ?>
<p><strong>Как отменить бронирование</strong><br>
  Чтобы отменить заказ, используйте ссылку в своем электронном подтверждении бронирования<br>
  Порядок проживания в отеле<br>
  Размещение детей и предоставление дополнительных кроватей<br>
  При размещении одного ребёнка младше 2 лет на детской кроватке проживание ему предоставляется бесплатно .<br>
  При размещении одного ребёнка в возрасте от 2 до 12 лет на дополнительной кровати взимается 1000 RUB с человека за ночь.<br>
  При размещении одного ребёнка старшего возраста или взрослого на дополнительной кровати взимается 1000 RUB с человека за ночь.<br>
  Максимальное количество дополнительных кроватей/детских кроваток в номере - 1.
</p>
<p><strong>Отмена бронирования</strong><br>
В случае отмены или изменения бронирования в срок до 2 суток до даты заезда штраф не взимается В случае отмены или изменения бронирования позднее этого срока или в случае незаезда взимается 100 процентов от стоимости первой ночи проживания<br>
  Предоплата: Залог не взимается.<br>
Налоги и сборы за обслуживание: Налог в размере 7 % входит в стоимость. городской налог не применяется. сбор за обслуживание не применяется.</p>
<? }elseif($arResult['lang'] == "EN") { ?>
<p><strong>How to cancel a booking</strong><br>
  To cancel an order, use the link in your confirmation email<br>
  Hotel Policies<br>
  Children and extra beds<br>
  One child under 2 years cot stay free.<br>
  One child from 2 to 12 years is charged RUB 1000 per night per person.<br>
  One older child or adult is charged RUB 1000 per night per person.<br>
Maximum capacity of extra beds / cots in the room - 1.</p>
<p><strong>Cancellation</strong><br>
If canceled or modified up to 2 days before date of arrival, no fee will be charged If canceled or modified later or in case of no-show will be charged 100 percent of the first night<br>
  Prepayment: No deposit will be charged.<br>
Taxes and service charges: Tax of 7% is included. A city tax is not applicable. A service charge is not applicable.</p>
<? } ?>