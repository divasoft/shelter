<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<h1><?=$arMessages["PaymentSuccess"]["title"][MESSAGE_LANG];?></h1>
<p><?=$arMessages["PaymentSuccess"]["text"][MESSAGE_LANG];?></p>
<script>setTimeout(function () {
   window.location.href = "<?=DivasoftShelter::ORIGINAL_SITE ?>";
}, 2000);</script>