<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<div class="ucs_aboutreserv_rate">
<? for($i = 0; $i < $arResult['AboutReserv']['col']; $i++) { ?>
<div class="ucs_aboutreserv_rate_info"><?
	if ($arResult['AboutReserv']['col'] > 1) { ?>
	<div class="ucs_aboutreserv_rate_block"><?=$arMessages["Reserv"]["info"][MESSAGE_LANG];?><?=($i+1);?></div>
	<? } ?>
	
	<? if (sizeof($_SESSION['Shelter']['Data']['HotelList']) > 1) {?>
	<div><?=$arMessages["Reserv"]["hotel"][MESSAGE_LANG];?>: <span><?=$_SESSION['Shelter']['Data']['HotelList'][$arResult['AboutReserv']['hotel']];?></span></div>
	<? } ?>
	<div><?=$arMessages["Reserv"]["from"][MESSAGE_LANG];?>: <span><?=date('d.m.Y', $arResult['AboutReserv']['request_in'][$i]).' '.$arResult['AboutReserv']['request_in_time'][$i][0].':'.$arResult['AboutReserv']['request_in_time'][$i][1];?></span></div>
	<div><?=$arMessages["Reserv"]["to"][MESSAGE_LANG];?>: <span><?=date('d.m.Y', $arResult['AboutReserv']['request_out'][$i]).' '.$arResult['AboutReserv']['request_out_time'][$i][0].':'.$arResult['AboutReserv']['request_out_time'][$i][1];?></span></div>
	<div class="ucs_aboutreserv_rate_div"><?=$arMessages["Reserv"]["typenumber"][MESSAGE_LANG];?>: <span><?=$arResult['AboutReserv']['typenumber'][$i].' ('.$arResult['AboutReserv']['colnumbers'][$i].' '.$arMessages["Reserv"]["colnumbers_short"][MESSAGE_LANG].')';?></span></div>
	<div><?=$arMessages["Reserv"]["tarif"][MESSAGE_LANG];?>: <span><?=$arResult['AboutReserv']['tarif'][$i];?></span></div>
	
	<? if (sizeof($arResult['AboutReserv']['packageService'][$i]) > 0) { 
		?><div class="ucs_aboutreserv_list"><?=$arMessages["Reserv"]["services"][MESSAGE_LANG];?>: <?
		for($j = 0; $j < sizeof($arResult['AboutReserv']['packageService'][$i]); $j++){
			?><div><?=($j+1);?>. <span><?=$arResult['AboutReserv']['packageService'][$i][$j]['name'];?></span></div><?
		}
		?></div><?
	}?>
	
	<div class="ucs_aboutreserv_rate_div">	
		<? foreach($arResult['AboutReserv']['guest'][$i] as $type_place=>$info){
			$p = 0;
			?><div class="ucs_ar_left"><?=$_SESSION['Shelter']['Data']['RoomFactor'][$arResult['AboutReserv']['hotel']][$arResult['AboutReserv']['roomkind'][$i]][$type_place]['name'];?>: <?
			foreach($info as $type_guest=>$guests){
				if (SHOW_PEOPLE_INFO) {
					for($j = 0; $j < sizeof($guests); $j++){
						if ($p > 0) echo ', ';
						?><span><?=$guests[$j]['fio'];?><?if (SHOW_PEOPLE_BIRTHDAY){?> (<?=date('d.m.Y', $guests[$j]['birth']);?>)<?}?></span><?
						$p++;
					}
				}else{
					if ($p > 0) echo ', ';
					?><span><?=sizeof($guests).' x '.$_SESSION['Shelter']['Data']['TypeGuest'][$arResult['AboutReserv']['hotel']][$type_guest];?></span><?
					$p++;
				}
			}
			?></div><?
		}?>
	</div>
	
	<? if (sizeof($arResult['AboutReserv']['operation'][$i]) > 0) { 
		?><div class="ucs_aboutreserv_rate_div ucs_aboutreserv_list"><?=$arMessages["Reserv"]["operation"][MESSAGE_LANG];?>: <?
		for($j = 0; $j < sizeof($arResult['AboutReserv']['operation'][$i]); $j++){
			?><div><?=($j+1);?>. <span><?=$arResult['AboutReserv']['operation'][$i][$j]['name'];?></span> (<?=$arResult['AboutReserv']['operation'][$i][$j]['quant'];?> <?=$arMessages["Reserv"]["colnumbers_short"][MESSAGE_LANG];?> x <?=$arResult['AboutReserv']['operation'][$i][$j]['cost'];?> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?>)</div><?
		}
		?></div><?
	}
	
	if ($arResult['AboutReserv']['contract'] > 0){
		if (authFirm()){
			$arContractList = getContractsListForDebtor();
			$contract_use = '';
			foreach($arContractList as $id_c=>$name_c){
				if ($id_c == $arResult['AboutReserv']['contract']) $contract_use = $name_c;
			}
			if ($contract_use != ''){
				?><div class="ucs_aboutreserv_rate_div"><?=$arMessages["Debtor"]["contracts_use"][MESSAGE_LANG];?>: <span><?=$contract_use;?></span></div><?
			}		
		}
	}
	
	if ($arResult['AboutReserv']['commission'][$i] > 0){
		?><div class="ucs_aboutreserv_rate_div"><?=$arMessages["commission"]["name"][MESSAGE_LANG];?>: <span><?=toCost($arResult['AboutReserv']['commission'][$i]);?> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></span></div><?
	}
	
	if ($arResult['AboutReserv']['discount'][$i] < 0){
		?><div class="ucs_aboutreserv_rate_div"><?=$arMessages["discount"]["sum"][MESSAGE_LANG];?>: <span><?=toCost($arResult['AboutReserv']['discount'][$i]);?> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></span></div><?
	}
		
	if (($arResult['AboutReserv']['col'] > 1) || ($arResult['AboutReserv']['cost'][$i] > 0)) { ?>	
	<div class="ucs_aboutreserv_rate_div ucs_aboutreserv_rate_sum"><?=$arMessages["Reserv"]["cost"][MESSAGE_LANG];?>: <span><?=toCost($arResult['AboutReserv']['cost'][$i]);?> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></span>
	<?if ($arResult['AboutReserv']['discount'][$i] < 0){?> <font><?=toCost($arResult['AboutReserv']['fullcost'][$i]);?> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></font><?}?>
	</div>	
	<? } ?>
</div><?
}
?>
</div>

<? if ($arResult['AboutReserv']['payment'] != '') { ?>
<div class="ucs_aboutreserv_info">
	<div><?=$arMessages["Reserv"]["pay"][MESSAGE_LANG];?>: <span><?=$arResult['AboutReserv']['payment'];?></span></div>
</div>
<? } ?>