<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<?
// STANDART
if (!$arResult['ajax']) {	
	?>		
	<link href="<?=SCRIPT_URL;?>js/tip.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?=SCRIPT_URL;?>js/ui/jquery-ui-1.8.15.custom.css" />
	<script type="text/javascript" src="<?=SCRIPT_URL;?>js/ui/jquery.ui.core"></script>
	<script type="text/javascript" src="<?=SCRIPT_URL;?>js/ui/jquery.ui.widget"></script>
	<script type="text/javascript" src="<?=SCRIPT_URL;?>js/ui/jquery.ui.datepicker.js"></script>
	
	<? if (EARLY_TIME) { ?>
	<link rel="stylesheet" type="text/css" media="screen" href="<?=SCRIPT_URL;?>js/uitime/ui-timepicker.css" />
	<script type="text/javascript" src="<?=SCRIPT_URL;?>js/uitime/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="<?=SCRIPT_URL;?>js/uitime/jquery-ui.min-reservation.js"></script>
	<script type="text/javascript" src="<?=SCRIPT_URL;?>js/uitime/jquery-ui-timepicker-addon.js"></script>	
	<? } ?>
	
	
	<h1><?=$arMessages["Reserv"]["personal"][MESSAGE_LANG];?></h1>	
	<input type="hidden" id="ucs_hotelid" value="<?=$arResult['hotel'];?>" />
	<input type="hidden" id="ucs_reservation_id" value="<?=$arResult['reservation_id'];?>" />		
	
	<? getReservForm(); ?>
		
	<table class="ucs_reserv_forms<? if (!(checkAuthUser())) echo ' ucs_none';?>" id="ucs_reservinfo">
	<? if (sizeof($arResult['type_pay']) > 0) { ?>
	<tr>
		<td><?=$arMessages["Reserv"]["paytext"][MESSAGE_LANG];?> <select id="ucs_pay">				
			<? foreach($arResult['type_pay'] as $key=>$val) {
				$s = '';
				if ($arResult['payment'] == $key) $s = ' selected="selected"';
				?>
				<option value="<?=$key;?>"<?=$s;?>><?=$val;?></option>
			<? } ?>
		</select></td>			
	</tr>
	<? } ?>
	<tr>
		<td><?=$arMessages["Reserv"]["commenttext"][MESSAGE_LANG];?><textarea id="ucs_comment" class="ucs_textarea"><?=$arResult['info'];?></textarea><div><span class="ucs_reserv_infolen"><?=$arResult['commenttextlen'];?></span>(<span id="ucs_reserv_infolen_of"><?=$arResult['infoLen'];?></span> <?=$arMessages["Reserv"]["commenttextof"][MESSAGE_LANG];?> <?=RESERV_INFO_LEN;?>)</div></td>
	</tr>
	</table>
		
	<div class="ucs_mainblock"><div class="ucs_block">
<? 
}
// STANDART
?>
<script  type="text/javascript" src="<?=SCRIPT_URL;?>js/jtip.js"></script>
<input type="hidden" id="ucs_checkcode" value="<?=$arResult['captcha'];?>" />
<?
 
// ������
for($i = 0; $i < $arResult['col']; $i++) {?>
<div class="ucs_race">
	<input type="hidden" class="ucs_inquiry_id" id="ucs_inquiry_id<?=$i;?>" value="<?=$arResult['inquiry_id'][$i];?>" />
	<input type="hidden" class="ucs_claim_id" id="ucs_claim_id<?=$i;?>" value="<?=$arResult['claim_id'][$i];?>" />
	<?
	$color_colnumbers = '';
	if ($arResult[$i]['color']) $color_colnumbers = ' ucs_elemcolor';
	
	if (RESERV_EDIT_INFO) {
		include(PATH_TEMPLATE_INC.'ReservationRates.php');
	}else{
		include(PATH_TEMPLATE_INC.'ReservationNoRates.php');
	}
	
	include(PATH_TEMPLATE_INC.'ReservationGuest.php');
	
	if ((sizeof($arResult['packageServiceList']) > 0) && (sizeof($arResult['operation']) > 0)) { ?>
	<div class="ucs_dop_info">	
	<table>
	<tr>
		<td valign="top" width="41%"><?include(PATH_TEMPLATE_INC.'ReservationPackageServices.php');?></td>
		<td width="2%" style="border-right:1px #ccc solid;">&nbsp;</td>
		<td valign="top" width="57%"><?include(PATH_TEMPLATE_INC.'ReservationServices.php');?></td>
	</tr>
	</table>
	</div>
	<? }elseif ((sizeof($arResult['packageServiceList']) > 0) || (sizeof($arResult['operation']) > 0)){ ?>
	<div class="ucs_dop_info">	
		<?include(PATH_TEMPLATE_INC.'ReservationPackageServices.php');?>
		<?include(PATH_TEMPLATE_INC.'ReservationServices.php');?>
	</div>	
	<? } ?>
	
	<? if ($arResult['no_show_price'][$i]){?>
	<div class="ucs_cost_rate<?if ($arResult['col'] < 2){?> ucs_none<?}?>">
		<?=$arMessages["Reserv"]["cost"][MESSAGE_LANG];?>: <span id="ucs_cost_rate<?=$i;?>"><?=toCost($arResult['arCost'][$i]);?></span> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?>
		<? if ($arResult['arFullCost'][$i] > 0){?> <font><span id="ucs_fullcost_rate<?=$i;?>"><?=toCost($arResult['arFullCost'][$i]);?></span> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></font><?}?>		
	</div>
	<? } ?>
</div>	
<? } ?>

<? if ($arResult['send_data']) {?>
	<? include_once(PATH_TEMPLATE_INC.'Discount.php');	?>
<? } ?>	

<? if ($arResult['no_show_price_total']){?>
<div class="ucs_zag"><?=$arMessages["Reserv"]["total"][MESSAGE_LANG];?>: <span id="ucs_cost"><?=toCost($arResult['cost']);?></span> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?>
	<?if ($arResult['fullcost'] > 0){?> 
		<font><span id="ucs_fullcost"><?=toCost($arResult['fullcost']);?></span> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></font>
	<?}?>	
</div>
<? } ?>

<? if (((!USER_USE) || (checkAuthUser())) && $arResult['captcha_reserv']) { ?>
<table class="ucs_capcha_reserv">
<tr>
	<td><?=$arMessages["CreateUser"]["countenter"][MESSAGE_LANG];?><span>*</span><br /><img src="<?=PATH_CAPTCHA;?>captcha.php?width=100&height=35&characters=5&use=<?=time();?>" id="ucs_cph" /></td>
	<td><input type="text" id="ucs_send_count" autocomplete="off" value="" maxlength="7" /></td>
</tr>
</table>
<? } // ���� ��������� ���� ?>

<? if (RESERV_RULES_USE && $arResult['send_data']) { ?>
<table class="ucs_capcha_reserv">
<tr>
	<td><input type="checkbox" id="rules" class="ucs_checkbox" value="1" onclick="rule_confirm(this)" /></td>
	<td><a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_RESERV_RULES), true);?>" target="_blank"><?=$arMessages["Reserv"]["rules"][MESSAGE_LANG];?></a></td>
</tr>
</table>
<? } // ����������� ������������� ������� ?>

<p <?if ($arResult['mess'] != ''){?>class="ucs_textcolor"<?}?> id="ucs_error"><?=$arResult['mess'];?></p>

<p id="ucs_reserv_button">
	<? if (RESERV_ADD_RATES && ($arResult['type_user'] - $arResult['col'] > 0)) { ?>
	<input type="button" class="ucs_submit" onclick="countsum(1, 1)" value="<?=$arMessages["Reserv"]["addrate"][MESSAGE_LANG];?>">
	<? } ?>

	<? if ($arResult['send_data']) { ?>
	<input type="submit" class="ucs_submit<? if (RESERV_RULES_USE) {?> ucs_none<?} ?>" onclick="reserv()" id="ucs_reserv_but" value="<?=$arMessages["Reserv"]["submit"][MESSAGE_LANG];?>"<? if (!$arResult['send_data']) echo ' class="ucs_none"'?>>
	<? } ?>
</p>

<script type="text/javascript">
	
	$(function() {
		<? for($i = 0; $i < $arResult['col']; $i++) { ?>
			$("#ucs_datepickerfrom<?=$i;?>").datepicker({ minDate: 0 });		
			$("#ucs_datepickerto<?=$i;?>").datepicker({ minDate: 0 });			
			
			<? if (EARLY_TIME) { ?>			
			$('#ucs_timepickerfrom<?=$i;?>').timepicker();
			$('#ucs_timepickerto<?=$i;?>').timepicker();				
			<? } ?>
			
			$('.ucs_tarifs').change(function(){
				tarif_i = $(this).attr('name');
				id = $(this).val();
				
				name = tarif_i+"_"+id;
				$('.'+tarif_i).hide();
				$('#'+name).show();
				
				setCost();
			});
		<? } ?>
	});
	
	var arTarif = [
	<?
		for($i = 0; $i < $arResult['col']; $i++) 
			for($j = 0; $j < sizeof($arResult['tarif_array'][$i]); $j++) { 				
				echo '['.$i.','.$arResult['room'][$i].','.$arResult['tarif_array'][$i][$j]['id'].','.$arResult['tarif_array'][$i][$j]['cost'].','.$arResult['tarif_array'][$i][$j]['fullcost'].']';  
				if (!(($i == ($arResult['col'] - 1)) && ($j == (sizeof($arResult['tarif_array'][$i]) - 1)))) echo ',';
			} 	
	?>
	];	
	
	$(function(){
		$('#ucs_comment').change(function(){checkLenInfo();});
		$('#ucs_comment').keyup(function(){checkLenInfo();});
		
		
		<? if (sizeof($arResult['packageServiceList']) > 0) { ?>
		$('.ucs_pservices_check').click(function(){setCheckPackageService(0, 0);});
		$('.ucs_pservices_check_clear').click(function(){
			info = $(this).attr('id').split('_');			
			setCheckPackageService(1, info[1]);
		});
		<? } ?>
		
		<? if (sizeof($arResult['operation']) > 0) { ?>
		$('.ucs_operation_check').click(function(){setCost();});
		$('.ucs_operation_check_clear').click(function(){
			info = $(this).attr('id').split('_');			
			$('.ucs_operation_check_clear_'+info[1]).attr('checked',false);
			$('.ucs_operation_quant_clear_'+info[1]).val(1);
			setCost();
		});
		<? } ?>
	});
	
	<?if ((isset($_GET['edit'])) && ($arResult['contract'] > 0)){?>
		countsum(0, 1);
	<? } ?>
	
</script>

<?

// STANDART
if (!$arResult['ajax']) { ?>
</div>
</div>	

<? 
	if ((sizeof($arResult['show_creditcard_payment']) > 0) && CREDITCARD_USE) {
	?><script type="text/javascript">
	
	var arPayment = [<?	
		for($i = 0; $i < sizeof($arResult['show_creditcard_payment']); $i++) echo $arResult['show_creditcard_payment'][$i].','	
	?>];
	
	$(function() {		
		$('#ucs_pay').change(function(){			
			pay = $(this).val();
			$('.ucs_creditcard_reserv_show input').removeClass('ucs_nocheck');
			show = false;
			for(i = 0; i < arPayment.length; i++)
				if (pay == arPayment[i]) show = true;
				
			if (show) {
				$('.ucs_creditcard_reserv_show').removeClass('ucs_none');
				$('.ucs_creditcard_reserv_show input').removeClass('ucs_nocheck');
			}else{
				$('.ucs_creditcard_reserv_show').addClass('ucs_none');
				$('.ucs_creditcard_reserv_show input').addClass('ucs_nocheck');
			}
		});
	});
	
	</script>
	<?
	}
?>
<? } 
// STANDART
?>