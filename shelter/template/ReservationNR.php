<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<h1><?if (sizeof($arResult['info']) > 0) echo $_SESSION['Shelter']['Data']['HotelList'][$arResult['AboutReserv']['hotel']].'. ';?><?=$arMessages["ReservationNR"]["head"][MESSAGE_LANG];?></h1>
<? if ($arResult['mess'] != '') { ?>
<p class="ucs_textcolor"><?=$arResult['mess'];?></p>
<? } ?>

<? if ($arResult['correct']) { ?>
	<div class="ucs_aboutreserv_info_block">
		<div class="ucs_aboutreserv_rate_div"><?=$arMessages["ReservationNR"]["aboutreserv"][MESSAGE_LANG];?>: <span><?=$arResult['info']['code'];?></span></div>
		<div class="ucs_aboutreserv_rate_div"><?=$arMessages["ReservationNR"]["from"][MESSAGE_LANG];?>: <span><?=$arResult['info']['arrival'];?></span></div>
		<div class="ucs_aboutreserv_rate_div"><?=$arMessages["ReservationNR"]["to"][MESSAGE_LANG];?>: <span><?=$arResult['info']['departure'];?></span></div>
		<? if ($arResult['AboutReserv']['col'] == 1){?>
			<? $i = 0; ?>
			<div class="ucs_aboutreserv_rate_div"><?=$arMessages["Reserv"]["typenumber"][MESSAGE_LANG];?>: <span><?=$arResult['AboutReserv']['typenumber'][$i].' ('.$arResult['AboutReserv']['colnumbers'][$i].' '.$arMessages["Reserv"]["colnumbers_short"][MESSAGE_LANG].')';?></span></div>
			<div class="ucs_aboutreserv_rate_div"><?=$arMessages["Reserv"]["tarif"][MESSAGE_LANG];?>: <span><?=$arResult['AboutReserv']['tarif'][$i];?></span></div>
			<? foreach($arResult['AboutReserv']['guest'][$i] as $type_place=>$info){
				$p = 0;
				?><div class="ucs_aboutreserv_rate_div"><?=$_SESSION['Shelter']['Data']['RoomFactor'][$arResult['AboutReserv']['hotel']][$arResult['AboutReserv']['roomkind'][$i]][$type_place]['name'];?>: <?
				foreach($info as $type_guest=>$guests){
					if (SHOW_PEOPLE_INFO) {
						for($j = 0; $j < sizeof($guests); $j++){
							if ($p > 0) echo ', ';
							?><span><?=$guests[$j]['fio'];?><?if (SHOW_PEOPLE_BIRTHDAY){?> (<?=date('d.m.Y', $guests[$j]['birth']);?>)<?}?></span><?
							$p++;
						}
					}else{
						if ($p > 0) echo ', ';
						?><span><?=sizeof($guests).' x '.$_SESSION['Shelter']['Data']['TypeGuest'][$arResult['AboutReserv']['hotel']][$type_guest];?></span><?
						$p++;
					}
				}
				?></div><?
			}?>
			<? if (sizeof($arResult['AboutReserv']['packageService'][$i]) > 0) { 
				?><div class="ucs_aboutreserv_rate_div ucs_aboutreserv_list"><?=$arMessages["Reserv"]["services"][MESSAGE_LANG];?>: <?
				for($j = 0; $j < sizeof($arResult['AboutReserv']['packageService'][$i]); $j++){
					?><div><?=($j+1);?>. <span><?=$arResult['AboutReserv']['packageService'][$i][$j]['name'];?></span></div><?
				}
				?></div><?
			}?>
			<? if (sizeof($arResult['AboutReserv']['operation'][$i]) > 0) {?>
				<div class="ucs_aboutreserv_rate_div ucs_aboutreserv_list"><?=$arMessages["Reserv"]["operation"][MESSAGE_LANG];?>: <?
				for($j = 0; $j < sizeof($arResult['AboutReserv']['operation'][$i]); $j++){
					?><div><?=($j+1);?>. <span><?=$arResult['AboutReserv']['operation'][$i][$j]['name'];?></span> (<?=$arResult['AboutReserv']['operation'][$i][$j]['quant'];?> <?=$arMessages["Reserv"]["colnumbers_short"][MESSAGE_LANG];?> x <?=$arResult['AboutReserv']['operation'][$i][$j]['cost'];?> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?>)</div><?
				}
				?></div><?
			}
		} ?>
		<div class="ucs_aboutreserv_rate_div"><?=$arMessages["ReservationNR"]["reservation"][MESSAGE_LANG];?>: <span><?=$arMessages["status_reservation"][$arResult['info']['status']][MESSAGE_LANG];?></span></div>
		<div class="ucs_aboutreserv_rate_div"><?=$arMessages["ReservationNR"]["status"][MESSAGE_LANG];?>: <span><?=$arResult['info']['pay'];?></span></div>
		<div class="ucs_aboutreserv_rate_div"><?=$arMessages["ReservationNR"]["cost"][MESSAGE_LANG];?>: <span><?=toCost($arResult['info']['summa']);?> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></span></div>
	</div>
	
	<? if ($arResult['AboutReserv']['col'] > 1){?>
	<div class="ucs_links_color"></div>
	<div class="ucs_aboutreserv_info_block_rates">
		<h1><?=$arMessages["ReservationNR"]["headrates"][MESSAGE_LANG];?></h1>
		<div class="ucs_aboutreserv_info_block_abouts">
			<? include_once(PATH_TEMPLATE.PAGE_ABOUTRESERV.'.php'); ?>	
		</div>
	</div>
	<? } ?>
	
	<? if (!$arResult['info']['payment']) { ?>
		<? if (PAYMENT) { ?>
			<div class="ucs_links_color"></div>
			<? include_once(PATH_TEMPLATE.PAGE_RESERVPAY.'.php'); ?>	
		<? } ?>
		
		<? if ($arResult['info']['deleteAllow']) { ?>
		<div class="ucs_links_color"></div>
		<h1><?=$arMessages["ReservationNR"]["cancel"][MESSAGE_LANG];?></h1>
		<p><?=$arMessages["ReservationNR"]["delete"][MESSAGE_LANG];?></p>
		<form action="" method="post">
		<table cellspacing="0" class="ucs_reservdelete">
		<tr>
			<td><img src="<?=PATH_CAPTCHA;?>captcha.php?width=100&height=35&characters=5" /></td>
			<td align="center"><input type="text" name="count" autocomplete="off" value="" maxlength="7" /></td>
			<td align="center"><input type="submit" class="ucs_submit" value="<?=$arMessages["ReservationNR"]["submit"][MESSAGE_LANG];?>" /></td>
		</tr>
		</table>
		</form>
		<? } ?>
	<? } ?>
<? } ?>