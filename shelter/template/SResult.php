<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); 

$room_id = DivasoftShelter::getRoomId(); // DIVASOFT 
if($room_id>0 && !array_key_exists($room_id, $arResult['roomInfo'])) {
    $msg = "Выбранный номер на заданное время - забронирован";
    $arResult['mess'].=(($arResult['mess']=="")?"":"<br/>").$msg;
    //DivasoftShelter::clearRoomId();
}

?>
<link href="<?=SCRIPT_URL;?>js/tip.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/jtip.js"></script>

<h1><?=$arMessages["Result"]["head"][MESSAGE_LANG];?></h1>

<? if ($arResult['mess'] != ''){ ?><p class="ucs_textcolor"><?=$arResult['mess'];?></p><?}?>
<? if ($arResult['exist']) {?>
	<? if (sizeof($arResult['currency_show']) > 0) { ?>
	<div class="ucs_current_list">
		<div id="ucs_change_current_before"><?=$arMessages["Result"]["current_load"][MESSAGE_LANG];?></div>
		<div id="ucs_change_current_after" class="ucs_none">
			<?=$arMessages["Result"]["current"][MESSAGE_LANG];?> <select id="ucs_current_list" onchange="current_show(this.value)"></select>
		</div>
	</div>
	<? } ?>
	
	<? if ($arResult['numb_rooms'] > 1) { ?>
		<div class="ucs_message ucs_textcolor"><?=$arMessages["Result"]["prices"][MESSAGE_LANG];?></div>
	<? } ?>

	<table cellpadding="0" cellspacing="0" class="ucs_sresult_table">
	<? 
	foreach($arResult['roomkinds'] as $i=>$room) { 
		$roomlink = '<span title="'.$room['name'].'">'.$room['name'].'</span>';
		foreach($LINK_ROOM as $key=>$val) {
			if ($val[1] != '') $val[1] = 'href="'.$val[1].'"';
			if ($room['code'] == $key) $roomlink = '<a '.$val[1].' rel="'.NUMBER_URL.CODE_LANG.'/'.$val[0].'" class="jTip" id="six'.$key.'" name="'.protection($room['name']).'">'.$room['name'].'</a>';
		}
		
		$pRoom = '';
		if (sizeof($arResult['roomInfo'][$room['id']]) > 0){
			foreach ($arResult['roomInfo'][$room['id']] as $f1=>$arTypeNumb){
				if ($pRoom != '') $pRoom .= '<br />';
				if ($arTypeNumb['quant'] > 0)
					$pRoom .= $arTypeNumb['name'].' - '.$arTypeNumb['quant'].' '.$arMessages["Result"]["pnumb"][MESSAGE_LANG];					
			}				
		}
		
		if ($i == 0) {
		?>
		<tr>
			<td><?=$arMessages["Result"]["name_room"][MESSAGE_LANG];?></td>
			<td><?=$arMessages["Result"]["name_price"][MESSAGE_LANG];?><?if(FIND_NUMBER_USE){?> (<?=$arMessages["Result"]["price_room"][MESSAGE_LANG];?>)<?}?></td>
			<td><?=$arMessages["Result"]["name_choose"][MESSAGE_LANG];?></td>
		</tr>
		<?
		}
		?>
		
		<tr id="room_<?=$room['id']?>" <?if($room_id==$room['id']):?> class="active"<?endif;?>>
			<td class="ucs_nameresult"><?=$roomlink;?><? if (SHOW_NUMBER) echo ' <font>('.$room['minNEW'].' '.$arMessages["Result"]["pnumb"][MESSAGE_LANG].')</font>';?><div><?=$pRoom;?></div></td>
			<td width="50%">
			<? $first = false; ?>					
				<table cellpadding="0" cellspacing="0" class="ucs_sresult_table_bn">
				<? foreach($room['ratesNEW'] as $id=>$price) { 
				$linkrate = $room['ratesInfo'][$id]['name'];
				foreach($LINK_RATE as $key=>$val) {					
					if ($val[1] != '') $val[1] = 'href="'.$val[1].'"';
					if ($room['ratesInfo'][$id]['code'] == $key) $linkrate = '<a '.$val[1].' rel="'.RATE_URL.CODE_LANG.'/'.$val[0].'" class="jTip jRate" id="six'.$key.$id.'" name="'.protection($room['ratesInfo'][$id]['name']).'">'.$room['ratesInfo'][$id]['name'].'</a>';
				}
				?>
				<tr>
					<td <? if (sizeof($room['ratesNEW']) == 1){?>class="ucs_none"<?}?> width="10px"><input type="radio" id="tarif<?=$id;?>" name="tarif<?=$room['id'];?>" value="<?=$id;?>"<? if (!$first) {?> checked="checked"<?} ;?> class="ucs_input_check" /></td>
					<td <? if (sizeof($room['ratesNEW']) == 1){?>colspan="2"<?}?> class="prices"><label for="tarif<?=$id;?>"><?=$linkrate;?> (<b><span class="new_price"><?=toCost($price, 1);?></span> <span class="new_price_code"><?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></span></b>) <?getDescriptionFactors($room['id'], $id);?></label> <input type="hidden" class="old_price" value="<?=$price;?>" /></td>
				</tr>	
				<? 
				$first = true;
				} ?>
				<? if (isset($room['ratespossibleNEW'])){?>
				<tr><td colspan="2" class="ucs_ratepossible prices"><span><?=$arMessages["Result"]["possible"][MESSAGE_LANG];?>:</span><?
						foreach($room['ratespossibleNEW'] as $id_pos=>$arPos) { 
							?><div><?=$arPos['name'];?> <b><span class="new_price"><?=toCost($arPos['sum'], 1);?></span> <span class="new_price_code"><?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></span></b> (<?if ($arPos['rules']['days1'] > 0){?><?=$arMessages["Result"]["from"][MESSAGE_LANG];?> <?=$arPos['rules']['days1'];?><?}?><? if (($arPos['rules']['days1'] > 0) && ($arPos['rules']['days2'] > 0)){?> <?}?><?if ($arPos['rules']['days2'] > 0){?><?=$arMessages["Result"]["to"][MESSAGE_LANG];?> <?=$arPos['rules']['days2'];?><?}?> <?=$arMessages["Result"]["duraction"][MESSAGE_LANG];?>)<input type="hidden" class="old_price" value="<?=$price;?>" /></div><?
						}?>
				</td></tr>
				<? } ?>
			</table></td>	
			<td align="center" width="100px">
				<? if (sizeof($room['ratesNEW']) > 0){?>
                <form action="<?=change_address(Array(MODULE_SHEDULE=>PAGE_RESERV), true);?>" method="POST">	
				<input type="hidden" name="res_request_id" value="<?=$room['id'];?>" />
				<input type="hidden" id="res_request_in" name="res_request_in" value="<?=$arResult['request_in'];?>" />
				<input type="hidden" id="res_request_out" name="res_request_out" value="<?=$arResult['request_out'];?>" />
				<input type="submit" value="<?=$arMessages["Result"]["submit"][MESSAGE_LANG];?>" class="ucs_submit" />
                </form>
				<? } ?>
			</td>				
		</tr>
        
	<? } ?>	
	</table>
        <?if($room_id>0): // DIVASOFT?>
        <script>
            $(function(){
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#room_<?=$room_id?>").offset().top
                }, 1000);
            });
        </script>
        <?endif;?>
	<? if (sizeof($arResult['currency_show']) > 0) { ?>
		<script type="text/javascript">change_current();</script>
	<? } 
}else
	echo $arMessages["Result"]["nofindrooms"][MESSAGE_LANG];
?>