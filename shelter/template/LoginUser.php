<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<form action="" method="post">
<input type="hidden" name="ucs_checkcode" value="<?=$arResult['captcha'];?>" />
<h1><?=$arMessages["LoginUser"]["head"][MESSAGE_LANG];?></h1>
<div <?if ($arResult['mess'] != ''){?>class="ucs_textcolor"<?}?>><?=$arResult['mess'];?></div>
<table class="ucs_add_guest">
	<tr>
		<td><?=$arMessages["LoginUser"]["login"][MESSAGE_LANG];?></td>
		<td><input type="text" name="login" class="ucs_width_150" value="<?=protection($_POST['login']);?>"></td>
	</tr>
	<tr>
		<td valign="top"><?=$arMessages["LoginUser"]["pass"][MESSAGE_LANG];?></td>
		<td><input type="password" name="pass" class="ucs_width_150" value=""></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td align="right" style="height:30px;"><a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_FORGETPASSWORD), true);?>"><?=$arMessages["LoginUser"]["forget"][MESSAGE_LANG];?></a></td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" class="ucs_submit" value="<?=$arMessages["LoginUser"]["submit"][MESSAGE_LANG];?>" /></td>
	</tr>
</table>
</form>