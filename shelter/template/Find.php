<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<? if (sizeof($arResult['hotel']) > 1) { ?>
<div class="ucs_hotelselect">
<select onchange="changeHotel(this.value)">
<? foreach($arResult['hotel'] as $id=>$val) { ?>
	<?
	$s = '';
	if ($arResult['Shotel'] == $id) $s = ' selected="selected"';
	?>
	<option value="<?=$id;?>"<?=$s;?>><?=$val;?></option>
<? } ?>
</select>
</div>
<? } ?>

<form action="" method="post">
<h1><?=$arMessages["Find"]["head"][MESSAGE_LANG];?></h1>
<table class="ucs_add_guest ucs_findform">
	<tr>
		<td valign="top"><?=$arMessages["Find"]["from"][MESSAGE_LANG];?></td>
		<td>			
			<? if (EARLY_TIME){?><input type="text" readonly="" id="ucs_timepicker_from" name="request_time_from" class="ucs_timepicker" value="<?=getHourSet(FROM_HOUR);?>:<?=getMinutesSet(FROM_HOUR);?>"><?}?>
			<input type="text" readonly="" id="ucs_request_in" name="request_in" onchange="check_request_out()" value="<?=$date_from;?>">
		</td>
	</tr>
	<tr>
		<td valign="top"><?=$arMessages["Find"]["to"][MESSAGE_LANG];?></td>
		<td>			
			<? if (EARLY_TIME){?><input type="text" readonly="" id="ucs_timepicker_to" name="request_time_to" class="ucs_timepicker" value="<?=getHourSet(TO_HOUR);?>:<?=getMinutesSet(TO_HOUR);?>"><?}?>
			<input type="text" readonly="" id="ucs_request_out" name="request_out" value="<?=$date_to;?>">
		</td>
	</tr>
	<? if (FIND_NUMBER_USE) { ?>
	<tr>
		<td valign="top"><?=$arMessages["Find"]["colnumber"][MESSAGE_LANG];?></td>
		<td>
			<input type="text" name="request_colnumber" value="<?=$arResult['NumbRoom'];?>" class="ucs_width_50">
		</td>
	</tr>
	<? } ?>		
	<tr <? if (!FIND_PEOPLE_USE){?>class="ucs_none"<?}?>>
		<td>&nbsp;</td>
		<td>
			<table>
				<? foreach($arResult['TypeGuest'] as $id=>$name){
				$col = protection($arResult['NumbGuest'][$id]);
				if ($col == 0) $col = '';				
				?>
				<tr>
					<td><?=$name;?>:</td>
					<td>
						<input type="text" name="request_guest_<?=$id;?>" value="<?=$col;?>" class="ucs_width_50">
					</td>
				</tr>
				<? } ?>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" class="ucs_submit" value="<?=$arMessages["Find"]["submit"][MESSAGE_LANG];?>" /></td>
	</tr>
</table>
</form>
<p>&nbsp;</p>
<link rel="stylesheet" type="text/css" media="screen" href="<?=SCRIPT_URL;?>js/ui/jquery-ui-1.8.15.custom.css" />
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/ui/jquery.ui.core"></script>
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/ui/jquery.ui.widget"></script>
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">	
	$(function() {
		$("#ucs_request_in").datepicker({ minDate: 0 });		
		$("#ucs_request_out").datepicker({ minDate: +1 });
	});
</script>

<? if (EARLY_TIME) { ?>
<link rel="stylesheet" type="text/css" media="screen" href="<?=SCRIPT_URL;?>js/uitime/ui-timepicker.css" />
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/uitime/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/uitime/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/uitime/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript">	
	$(function() {
		$('#ucs_timepicker_from').timepicker();
		$('#ucs_timepicker_to').timepicker();
	});
</script>
<? } ?>	

<script type="text/javascript">	
	function changeHotel(id){
		var link = '<?=change_address(Array(MODULE_SHEDULE=>PAGE_FIND, 'hotel'=>'#ID'), true);?>';
		link = link.replace('#ID', id);
		window.location.href = link;
	}	
</script>
