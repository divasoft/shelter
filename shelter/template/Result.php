<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<link href="<?=SCRIPT_URL;?>js/tip.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/jtip.js"></script>

<h1><?=$arMessages["Result"]["head"][MESSAGE_LANG];?></h1>
<? if($arResult['exist']){?>
	<? if ($arResult['mess'] != ''){ ?><p class="ucs_textcolor"><?=$arResult['mess'];?></p><?}?>
	<p><?=$arMessages["Result"]["choose"][MESSAGE_LANG].$arResult['max'];?></p>
	<div class="ucs_find_result_div">
	<table class="ucs_find_result">
		<tr>
			<td></td>
			<?php for ($i=$arResult['result_request_in']; $i<=($arResult['result_request_out']); ++$i) { ?>
			<td class="ucs_nameresult"><?=date('d.m',toIdate($i));?></td>
			<?php } ?>
		</tr>
		<?php foreach ($arResult['roomkinds'] as $roomkind) { ?>
			<? if ($roomkind['name'] != '') { 
				$link = '<span title="'.$roomkind['name'].'">'.$roomkind['name'].'</span>';
				foreach($LINK_ROOM as $key=>$val) {
					if ($val[1] != '') $val[1] = 'href="'.$val[1].'"';
					if ($roomkind['code'] == $key) $link = '<a '.$val[1].' rel="'.NUMBER_URL.CODE_LANG.'/'.$val[0].'" class="jTip" id="ucs_six'.$key.'" name="'.protection($roomkind['name']).'">'.$roomkind['name'].'</a>';
				}	
				
				$pRoom = '';
				if (sizeof($arResult['roomInfo'][$roomkind['id']]) > 0){
					foreach ($arResult['roomInfo'][$roomkind['id']] as $f1=>$arTypeNumb){
						if ($pRoom != '') $pRoom .= '<br />';
						if ($arTypeNumb['quant'] > 0)
							$pRoom .= $arTypeNumb['name'].' - '.$arTypeNumb['quant'].' '.$arMessages["Result"]["pnumb"][MESSAGE_LANG];					
					}				
				}
			?>
			<tr>
				<td class="ucs_nameresult"><?=$link;?><div><?=$pRoom;?></div></td>
			<?php 
			for ($i=$arResult['result_request_in']; $i<=($arResult['result_request_out']); $i++) { 
				if (!isset($roomkind['dates'][$i])) $roomkind['dates'][$i] = 0;
				$col = $roomkind['dates'][$i]; 
							
				$class = 'ucs_cursor ucs_room'.$roomkind['id'].' ';
				$text = '';
				
				if (!isset($col) || 0 == $col) $class .= 'ucs_norooms';
					elseif ($col<NUMBERS) $class .= 'ucs_lessrooms';
						else $class .= 'ucs_manyrooms';
				$click = ' onclick="choose_room(\''.$i.'\', \''.($i+1).'\', \''.$roomkind['id'].'\')"';
				
				if ($col < 1) {
					$col = '&nbsp;';
					//$click = '';
				}
				
				if (!SHOW_NUMBER) $col = '&nbsp;';			
					else $text = $roomkind['dates'][$i].' '.$arMessages["Result"]["pnumb"][MESSAGE_LANG];
					
				?><td <?=$click;?> id="ucs_room_<?=$i;?>_<?=$roomkind['id'];?>" class="<?=$class;?>" title="<?=$text;?>"><?=$col;?></td><?
			} ?>
			</tr>
			<? } ?>
		<?php } ?>
	</table>
	</div>
	<table class="ucs_find_result_history">
		<tr>
			<td><?=$arMessages["Result"]["manyrooms"][MESSAGE_LANG].' '.NUMBERS;?></th>
			<td class="ucs_manyrooms" width="20"></td>
		</tr>
		<tr>
			<td><?=$arMessages["Result"]["lessrooms"][MESSAGE_LANG].' '.NUMBERS;?></th>
			<td class="ucs_lessrooms" width="20"></td>
		</tr>
		<tr>
			<td><?=$arMessages["Result"]["norooms"][MESSAGE_LANG];?></th>
			<td class="ucs_norooms" width="20"></td>
		</tr>
	</table>

	<p>
		<form action="<?=change_address(Array(MODULE_SHEDULE=>PAGE_RESERV), true);?>" method="POST">		
			<input type="hidden" id="ucs_res_request_in" name="res_request_in" value="" />
			<input type="hidden" id="ucs_res_request_out" name="res_request_out" value="" />
			<input type="hidden" id="ucs_res_request_id" name="res_request_id" value="" />
			<input type="submit" id="ucs_res_sub" value="<?=$arMessages["Result"]["submit"][MESSAGE_LANG];?>" class="ucs_submit ucs_none" />
		</form>
	<p>
	<input type="hidden" id="ucs_res_reserv" value="<?=$arResult['max'];?>" />
<?}
else
	echo $arMessages["Result"]["nofindrooms"][MESSAGE_LANG];?>