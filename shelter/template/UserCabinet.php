<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<h1><?=$arMessages["UserCabinet"]["head"][MESSAGE_LANG];?></h1>
<? if ($arResult['mess'] != '') { ?>
<p class="ucs_textcolor"><?=$arResult['mess'];?></p>
<? } ?>

<? if (($GUEST['type'] == 1) && (sizeof($arResult['reserves']) > 0)) if (file_exists(PATH_TEMPLATE_INC.'FirmCabinet.php')) include_once(PATH_TEMPLATE_INC.'FirmCabinet.php'); ?>

<? if (sizeof($arResult['reserves']) > 0) { ?>
	<table cellspacing="0" class="ucs_usercabinet">
	<? foreach ($arResult['reserves'] as $id=>$res) { ?>
	<tr id="<?=$res['id'];?>_<?=$res['contact'];?>_<?=$res['phone'];?>_<?=str_replace('"', '', $res['hotelname']);?>_<?=toIdate($res['arrival'], 'd.m.Y');?>_<?=$arResult['reserves'][$id]['PAID'];?>_<?=$res['status'];?>" class="ucs_reservation">
		<td width="80px" align="center">#<span class="ucs_number<?if(($arResult['reserves'][$id]['REMOVE']) || (!$arResult['reserves'][$id]['onlinereserv'])){?> ucs_reserv_remove<?}?>" <?if((!$arResult['reserves'][$id]['REMOVE']) && ($arResult['reserves'][$id]['onlinereserv'])){?>onclick="about_reserv(<?=$res['id'];?>)"<?}?>><?=$res['code'];?></span></td>
		<td width="25%">
			<div class="ucs_phone"><?=$res['phone'];?></div>
			<div class="ucs_contact"><?=$res['contact'];?></div>
			<div class="ucs_contact"><?=$res['email'];?></div>
			<? if (!$arResult['reserves'][$id]['onlinereserv']){?>
			<div align="left"><?=$arMessages["UserCabinet"]["createinshelter"][MESSAGE_LANG];?></div>
			<? } ?>
		</td>
		<td width="25%">
			<div class="ucs_hotel"><?=$res['hotelname'];?></div>
			<div><?=$arMessages["UserCabinet"]["from"][MESSAGE_LANG];?>: <?=toIdate($res['arrival'], 'd.m.Y H:i');?></div>
			<div><?=$arMessages["UserCabinet"]["to"][MESSAGE_LANG];?>: <?=toIdate($res['departure'], 'd.m.Y H:i');?></div>
		</td>
		<td width="25%">
			<div class="ucs_summ"><?=toCost($res['summa']).' '.$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></div>
			<?if($res['COMMISSION'] > 0){?>
			<div class="ucs_status"><?=$arMessages["commission"]["name"][MESSAGE_LANG].': '.toCost($res['COMMISSION']).' '.$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></div>
			<? } ?>
			<div class="ucs_status"><?=$arMessages["status_reservation"][$res['status']][MESSAGE_LANG];?></div>
			<div class="ucs_status"><? if ($arResult['reserves'][$id]['PAID'] == 2) {?>
				<?=$arMessages["UserCabinet"]["paid"][MESSAGE_LANG];?>
				<? if ($arResult['reserves'][$id]['COST'] < 0) echo ' '.$res['balance'].' '.$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?>				
			<? }elseif ($arResult['reserves'][$id]['PAID'] == 1){ ?>
				<?=$arMessages["UserCabinet"]["paid"][MESSAGE_LANG].' '.$res['balance'].' '.$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?>
			<? }else{ ?>
				<span class="ucs_color"><?=$arMessages["UserCabinet"]["nopaid"][MESSAGE_LANG];?></span>
			<? } ?></div>
		</td>
		<td width="80px">
			<? if ($arResult['reserves'][$id]['PAY']) { ?>
				<a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_RESERVPAY, 'id'=>$res['id']), true);?>"><?=$arMessages["UserCabinet"]["pay"][MESSAGE_LANG];?></a>
			<? } ?>
			
			<? if ($arResult['reserves'][$id]['EDIT']) { ?>
				<a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_RESERV, 'edit'=>$res['id']), true);?>"><?=$arMessages["UserCabinet"]["edit"][MESSAGE_LANG];?></a>
			<? } ?>
			
			<? if ($arResult['reserves'][$id]['DELETE']) { ?>
				<div class="ucs_link" onclick="delReserv('<?=change_address(Array(MODULE_SHEDULE=>PAGE_RESERVDELETE, 'id'=>$res['id']), true);?>', '<?=$arMessages["UserCabinet"]["mess"][MESSAGE_LANG];?>')"><?=$arMessages["UserCabinet"]["delete"][MESSAGE_LANG];?></div> 
			<? } ?>
			
			<? if ((!$arResult['reserves'][$id]['PAY']) && (!$arResult['reserves'][$id]['EDIT']) && (!$arResult['reserves'][$id]['DELETE']) && (!$arResult['reserves'][$id]['REMOVE'])) { ?>
				<div class="ucs_link" onclick="about_reserv(<?=$res['id'];?>)"><?=$arMessages["UserCabinet"]["noaction"][MESSAGE_LANG];?></div>
			<? }elseif(($arResult['reserves'][$id]['REMOVE']) && ($GUEST['type'] == 1)){?>
				<?=$arMessages["UserCabinet"]["del_status"][MESSAGE_LANG];?>
			<? } ?>		
		</td>
	</tr>
	<tr>
		<td colspan="5" id="ucs_load_block_<?=$res['id'];?>" class="ucs_none ucs_informs"><img src="<?=PATH_IMG;?>load.gif" /></td>
	</tr>
	<? } ?>
	</table><div class="ucs_usercabinet_block"></div>
<? }else{ ?>
	<p><?=$arMessages["UserCabinet"]["empty"][MESSAGE_LANG];?>
<? } ?>