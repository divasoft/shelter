<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<link href="<?=SCRIPT_URL;?>style.css" rel="stylesheet" type="text/css" />
<script  type="text/javascript" src="<?=SCRIPT_URL;?>js/jquery.js"></script>
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/check.js"></script>
<script type="text/javascript">
	var time = <?=MODULE_CHECK_CONNECT;?>;	
	var path_file = '<?=SCRIPT_URL?>';
	var path_url = '<?=change_address(Array('connect_ok'=>1));?>';	
</script>

<div class="ucs_connect_block">
	<div class="ucs_check_connect"><?=$arMessages["CheckConnect"]["connect"][MESSAGE_LANG];?><p><img src="<?=PATH_IMG;?>load.gif" alt="" /></p></div>
	<div class="ucs_no_connect ucs_none"><?=$arMessages["CheckConnect"]["no_connect"][MESSAGE_LANG];?></div>
</div>