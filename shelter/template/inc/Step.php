<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<? 

function getDubPage($key, $namePage){
	if (($key == $namePage) || (($namePage == PAGE_RESULT_SECOND) && ($key == PAGE_RESULT_FIRST))) return true;
}

$namePage = PAGE_FIND;
if (isset($_GET[MODULE_SHEDULE])) $namePage = protection($_GET[MODULE_SHEDULE]);

$find = false;
foreach($arMessages['steps'] as $key=>$val) {
	if (getDubPage($key, $namePage)) $find = true;	
}

if (((isset($_GET['edit'])) && ($namePage == PAGE_RESERV)) || ($namePage == PAGE_RESERVPAY)) {
	$find = false;
	$title_step = $arMessages["UserCabinet"]["edit_step"][MESSAGE_LANG];
	if ($namePage == PAGE_RESERVPAY) $title_step = $arMessages["ReservPay"]["head"][MESSAGE_LANG];
?><div class="ucs_step">
	<a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true);?>"><?=$arMessages["menu"]["cabinet"][MESSAGE_LANG];?></a>
	<font>&nbsp;&nbsp;&rarr;&nbsp;&nbsp;</font>
	<span><?=$title_step;?></span>
</div><?
}	

$i = 0;
$show = false;
if ($find) { ?>
<div class="ucs_step">
	<? foreach($arMessages['steps'] as $key=>$val) { 
		
		if (getDubPage($key, $namePage)) $show = true;
		
		if ($show) {
			if (getDubPage($key, $namePage)) echo '<span>';
			echo $arMessages['steps'][$key][MESSAGE_LANG];
			if (getDubPage($key, $namePage)) echo '</span>';
		}else{
			echo '<a href="'.change_address(Array(MODULE_SHEDULE=>$key), true).'">'.$arMessages['steps'][$key][MESSAGE_LANG].'</a>';
		}
		if ($i < (sizeof($arMessages['steps']) - 1)) echo '<font>&nbsp;&nbsp;&rarr;&nbsp;&nbsp;</font>';
		$i++;
	?>
	
	<? } ?>
</div>
<? } ?>

<? if ($find && (($namePage == PAGE_RESULT_FIRST) || ($namePage == PAGE_RESULT_SECOND))) info_about_reserv(); ?>
<? if (($namePage == PAGE_RESERV) || ($namePage == PAGE_RESULT_FIRST)) getContractsSelect(); ?>