<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<? if (sizeof($arResult['packageServiceList']) > 0) { ?>
	<div class="ucs_operation_name"><?=$arMessages["Reserv"]["services"][MESSAGE_LANG];?> <div class="ucs_check_clear">[<span id="pservicesclear_<?=$i;?>" class="ucs_pservices_check_clear"><?=$arMessages["Reserv"]["reset"][MESSAGE_LANG];?></span>]</div></div>
	<table class="ucs_pservices">					
	<? 
	for($j = 0; $j < sizeof($arResult['packageServiceList']); $j++) {			
		$check_pservices = '';
		if (isset($arResult['packageServiceCheck'][$i])){
			if (in_array($arResult['packageServiceList'][$j]['id'], $arResult['packageServiceCheck'][$i])) $check_pservices = ' checked="checked"';			
		}	
	?>
	<tr>
		<td><input type="checkbox" id="pservices_<?=$i;?>_<?=$arResult['packageServiceList'][$j]['id'];?>" class="ucs_pservices_check ucs_checkbox"<?=$check_pservices;?> /></td>
		<td><label for="pservices_<?=$i;?>_<?=$arResult['packageServiceList'][$j]['id'];?>"><?=i($arResult['packageServiceList'][$j]['name']);?></label></td>
	</tr>	
	<? } ?>		
	</table>
<? } ?>