<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<table class="ucs_reserv">
<tr <? if (!(($arResult['col'] > 1))) { ?>class="ucs_none"<? } ?>>
	<td colspan="4" class="ucs_zag">
	<img class="ucs_spanremove" name="spanremove<?=$i;?>" onclick="reserv_remove('spanremove<?=$i;?>')" src="<?=PATH_IMG;?>close.png" /> 
	<?=$arMessages["Reserv"]["info"][MESSAGE_LANG].' '.($i+1);?> 
	</td>
</tr>
<tr>
	<td><?=$arMessages["Reserv"]["hotel"][MESSAGE_LANG];?></td>
	<td><?=$_SESSION['Shelter']['Data']['HotelList'][$arResult['hotel']];?></td>
	<td <?if ((!SETTLE_NUMB_ROOMS) || (SHOW_PEOPLE_INFO)){?>class="ucs_none"<?}?>><?=$arMessages["Reserv"]["colnumbers"][MESSAGE_LANG];?></td>
	<td <?if ((!SETTLE_NUMB_ROOMS) || (SHOW_PEOPLE_INFO)){?>class="ucs_none"<?}?>><input type="text" onchange="elemcolor(this)" name="colnumbers_<?=$i;?>" onclick="select(this)" class="ucs_colnumbers ucs_width_50<?=$color_colnumbers;?>" value="<?=$arResult['res_colnumbers'][$i];?>" /></td>
</tr>
<tr>
	<td width="80px"><?=$arMessages["Reserv"]["from"][MESSAGE_LANG];?></td>
	<td><input type="text" readonly="" id="ucs_timepickerfrom<?=$i;?>" value="<?=$arResult['request_in_time'][$i][0];?>:<?=$arResult['request_in_time'][$i][1];?>" class="ucs_timepicker ucs_from_time" /><input type="text" readonly="" id="ucs_datepickerfrom<?=$i;?>" name="from<?=$i;?>" onchange="elemcolor(this)" class="ucs_from ucs_width_150" value="<?=date('d.m.Y', $arResult['request_in'][$i]);?>" /></td>
	<td width="80px"><?=$arMessages["Reserv"]["to"][MESSAGE_LANG];?></td>
	<td><input type="text" readonly="" id="ucs_timepickerto<?=$i;?>" value="<?=$arResult['request_out_time'][$i][0];?>:<?=$arResult['request_out_time'][$i][1];?>" class="ucs_timepicker ucs_to_time" /><input type="text" name="to<?=$i;?>" id="ucs_datepickerto<?=$i;?>" readonly="" onchange="elemcolor(this)" class="ucs_to ucs_width_150" value="<?=date('d.m.Y', $arResult['request_out'][$i]);?>" /></td>
</tr>
<? 
	$droom = $arResult['room_code'][$arResult['room'][$i]];
	$link = $arMessages["Reserv"]["typenumber"][MESSAGE_LANG];
	foreach($LINK_ROOM as $key=>$val) {
		if ($key == $droom[0]) $link = '<a href="'.$val[1].'" rel="'.NUMBER_URL.protection($arCodeLang[$arResult['lang']]).'/'.$val[0].'" class="jTip" id="ucs_six'.$i.'" name="'.protection($droom[1]).'">'.$arMessages["Reserv"]["typenumber"][MESSAGE_LANG].'</a>';
	}
?>	
<tr>
	<td><?=$link;?></td>
	<td><select name="typenumber<?=$i;?>" onchange="elemcolor(this)" class="ucs_typenumbers ucs_width_200<?=$color_colnumbers;?>"><? foreach($arResult['roomkinds'][$i] as $key=>$val) { 
		$s = '';
		if ($arResult['room'][$i] == $val['id']) $s = ' selected="selected"';
		?>
			<option value="<?=$val['id'];?>"<?=$s;?>><?=$val['name'];?></option>
		<? } ?>
	</select></td>
	<?
	$rates = $arMessages["Reserv"]["tarif"][MESSAGE_LANG];
	$linkrate = '';
	$show_rate = false;
	for($j = 0; $j < sizeof($arResult['tarif_array'][$i]); $j++)
		foreach($LINK_RATE as $key=>$val) {	
			$none = ' ucs_none';
			if ($val[1] != '') $val[1] = 'href="'.$val[1].'"';
			if ($arResult['tarif_array'][$i][$j]['code'] == $key) {
				if ($arResult['res_tarif'][$i] == $arResult['tarif_array'][$i][$j]['id']) {
					$none = '';
					$show_rate = true;
				}	
				
				$linkrate .= '<a '.$val[1].' rel="'.RATE_URL.protection($arCodeLang[$arResult['lang']]).'/'.$val[0].'" class="jTip jRate tarif'.$i.$none.'" id="tarif'.$i.'_'.$arResult['tarif_array'][$i][$j]['id'].'" name="'.protection($arResult['tarif_array'][$i][$j]['name']).'">'.$arMessages["Reserv"]["tarif"][MESSAGE_LANG].'</a>';
			}
		}
	if ($show_rate) $rates = $linkrate;
	?>	
	<td><?=$rates;?></td>
	<? 
		$class_tarif = '';
		if ($arResult['tarif_array'][$i][0]['id'] == 0) $class_tarif = ' ucs_elemcolor';
	?>
	<td><select name="tarif<?=$i;?>" class="ucs_tarifs ucs_width_200<?=$class_tarif;?>"><? 
		for($j = 0; $j < sizeof($arResult['tarif_array'][$i]); $j++) {  
			$s = '';
			if ($arResult['res_tarif'][$i] == $arResult['tarif_array'][$i][$j]['id']) $s = ' selected="selected"';
			
			if ($arResult['Commission'][$i][$arResult['tarif_array'][$i][$j]['id']] > 0){
				$arResult['tarif_array'][$i][$j]['name'] .= ' ('.$arMessages["commission"]["name"][MESSAGE_LANG].': '.toCost($arResult['Commission'][$i][$arResult['tarif_array'][$i][$j]['id']]).' '.$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG].')';
			}
			?>
			<option value="<?=$arResult['tarif_array'][$i][$j]['id'];?>"<?=$s;?>><?=$arResult['tarif_array'][$i][$j]['name'];?></option>
		<? } ?>
	</select></td>
</tr>
</table>