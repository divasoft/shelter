<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<table id="ucsp_<?=$i;?>_<?=$g_i;?>" class="ucs_aboutguest<?if (!$RGI_show){?> ucs_none ucs_guest_no_exist<?}?>">
<tr>										
	<td>
	<input type="hidden" id="ucsid_<?=$i;?>_<?=$g_i;?>" value="<?=$arInfoGuest[1];?>" class="ucs_nocheck ucs_people_id" />
	<input type="hidden" value="<?=$type_place;?>" class="ucs_nocheck ucsnumbtype" />
	<select class="ucspeopletype" onchange="countsum(0, 1)">
	<? foreach($arResult['type_guest'] as $PG_type_guest_id=>$PG_type_guest_name){					
		$s = '';
		if ($PG_type_guest_id == $arInfoGuest[0]) $s = ' selected="selected"';				
		?><option value="<?=$PG_type_guest_id;?>"<?=$s;?>><?=$PG_type_guest_name;?></option>
	<? } ?>	
	</select></td>
	<? if (SHOW_PEOPLE_INFO) {?>	
	<td align="left"><div><?=$arMessages["Reserv"]["guest_f"][MESSAGE_LANG];?><span>*</span></div><input type="text" onchange="delClass(this, 'ucs_elemcolor')" id="ucsf_<?=$i;?>_<?=$g_i;?>" value="<?=i($arInfoGuest[2]);?>" class="ucs_people_f" /></td>			
	<td align="left"><div><?=$arMessages["Reserv"]["guest_n"][MESSAGE_LANG];?><span>*</span></div><input type="text" onchange="delClass(this, 'ucs_elemcolor')" id="ucsn_<?=$i;?>_<?=$g_i;?>" value="<?=i($arInfoGuest[3]);?>" class="ucs_people_n" /></td>			
	<td align="left"><div><?=$arMessages["Reserv"]["guest_o"][MESSAGE_LANG];?></div><input type="text" onchange="delClass(this, 'ucs_elemcolor')" id="ucso_<?=$i;?>_<?=$g_i;?>" value="<?=i($arInfoGuest[4]);?>" class="ucs_people_o ucs_nocheck" /></td>
	<?if (SHOW_PEOPLE_BIRTHDAY){?>
	<td align="left" colspan="3"><div><?=$arMessages["Reserv"]["guest_b"][MESSAGE_LANG];?><span>*</span></div><input type="text" onchange="delClass(this, 'ucs_elemcolor')" readonly="" id="ucsb_<?=$i;?>_<?=$g_i;?>" value="<?if ($arInfoGuest[5] != '') echo date('d.m.Y', $arInfoGuest[5]);?>" class="ucs_people_b" /></td>
	<? } ?>
	<? } ?>
	<td width="100px">
	<? if (!(($g_i == 0) && ($numb_guest_in_type_place == 1))){ ?>
	<input type="button" class="ucs_submit delguestinfo" id="delguestinfo_<?=$i;?>_<?=$g_i;?>" value="x" />
	<? } ?>
	</td>
</tr>
</table>
<? if ($RGI_newguest){?>
	<input type="button" class="ucs_submit addguestinfo" id="addguestinfo_<?=$i;?>_<?=$g_i;?>" value=" " style="background:url('<?=SCRIPT_URL;?>images/guest/addguest.png') center no-repeat" />
<?}?>
<div class="addguestinfo_load_img ucs_none<?if (!$RGI_newguest){?> addguestinfo_load_img_big<?}?>" id="addguestinfo_load_img_<?=$i;?>_<?=$g_i;?>"><img src="<?=PATH_IMG;?>load.gif" /></div>

<?if ($RGI_show){?>
<script type="text/javascript">
	$("#ucsb_<?=$i;?>_<?=$g_i;?>").datepicker({ maxDate: 0 });
	var changeYear = $("#ucsb_<?=$i;?>_<?=$g_i;?>").datepicker( "option", "changeYear" );	 
	$("#ucsb_<?=$i;?>_<?=$g_i;?>").datepicker( "option", "changeYear", true );
	var changeMonth = $("#ucsb_<?=$i;?>_<?=$g_i;?>").datepicker( "option", "changeMonth" );	 
	$("#ucsb_<?=$i;?>_<?=$g_i;?>").datepicker( "option", "changeMonth", true );
</script>
<? } ?>