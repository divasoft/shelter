<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<? if (sizeof($arResult['operation']) > 0) { ?>
	<div class="ucs_operation_name"><?=$arMessages["Reserv"]["operation"][MESSAGE_LANG];?> <div class="ucs_check_clear">[<span id="operationclear_<?=$i;?>" class="ucs_operation_check_clear"><?=$arMessages["Reserv"]["reset"][MESSAGE_LANG];?></span>]</div></div>
	<table class="ucs_operation">					
	<? 
	for($j = 0; $j < sizeof($arResult['operation']); $j++) {			
		$check_operation = '';
		if ($arResult['dopuslugi'][$i][$j]['check']) $check_operation = ' checked="checked"';			
	?>
	<tr>
		<td width="15px"><input type="checkbox" id="operation_<?=$i;?>_<?=$j;?>" name="operation_<?=$i;?>_<?=$j;?>" class="ucs_operation_check ucs_operation_check_clear_<?=$i;?> ucs_checkbox"<?=$check_operation;?> />
			<input type="hidden" id="ucs_operation_id_<?=$i;?>_<?=$j;?>" value="<?=$arResult['dopuslugi'][$i][$j]['id'];?>" class="ucs_nocheck" />
			<input type="hidden" id="ucs_operation_code_<?=$i;?>_<?=$j;?>" value="<?=i($arResult['operation'][$j]['code']);?>" class="ucs_nocheck" />
			<input type="hidden" id="ucs_operation_date_<?=$i;?>_<?=$j;?>" value="<?=toDdate(date('d.m.Y', $arResult['request_in'][$i]));?>" class="ucs_nocheck" />				
			<input type="hidden" id="ucs_operation_cost_<?=$i;?>_<?=$j;?>" value="<?=$arResult['operation'][$j]['cost'];?>" class="ucs_nocheck" />	
		</td>
		<td><label for="operation_<?=$i;?>_<?=$j;?>"><?=i($arResult['operation'][$j]['name']);?></label></td>
		<td width="80px" align="right"><?=toCost($arResult['operation'][$j]['cost']);?> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></td>
		<td width="80px" align="right"><input type="text" id="ucs_operation_quant_<?=$i;?>_<?=$j;?>" value="<?=$arResult['dopuslugi'][$i][$j]['quant'];?>" class="ucs_nocheck ucs_width_22_50 ucs_operation_quant_clear_<?=$i;?>" onclick="select(this)" onchange="chQuantOperation(<?=$i;?>,<?=$j;?>)" /> <?=$arMessages["Reserv"]["colnumbers_short"][MESSAGE_LANG];?></td>
	</tr>	
	<? } ?>		
	</table>
<? } ?>