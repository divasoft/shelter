<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<?if (SHOW_NUMBERS_EDIT_PARAMS){?>
	<form action="<?=change_address(Array(MODULE_SHEDULE=>PAGE_FIND, 'ucsredirect'=>$_GET[MODULE_SHEDULE]), true);?>" method="post">
	<div class="ucs_info_about_reserv_edit_link">
		<div id="ucs_info_about_reserv_edit_link_edit">[<font><?=$arMessages["Result"]["editparams"][MESSAGE_LANG];?></font>]</div>
		<div id="ucs_info_about_reserv_edit_link_cancel" class="ucs_none">[<font><?=$arMessages["Result"]["cancelparams"][MESSAGE_LANG];?></font>]</div>
	</div>	
<?}?>
<table class="ucs_info_about_reserv" cellspacing="0">
<? if (sizeof($_SESSION['Shelter']['Data']['HotelList']) > 1) {?>
<tr>
	<td class="ucs_info_about_reserv_name"><?=$arMessages["Reserv"]["hotel"][MESSAGE_LANG];?></td>
	<td>
		<span><?=$arResult['About']['hotel'];?></span>
		<?if (SHOW_NUMBERS_EDIT_PARAMS){?>
		<div class="ucs_about_params ucs_none">
			<? if (sizeof($_SESSION['Shelter']['Data']['HotelList']) > 1) { ?>
			<select name="request_hotel">
			<? foreach($_SESSION['Shelter']['Data']['HotelList'] as $id=>$val) { ?>
				<?
				$s = '';
				if ($_SESSION['Shelter']['Find']['hotel'] == $id) $s = ' selected="selected"';
				?>
				<option value="<?=$id;?>"<?=$s;?>><?=$val;?></option>
			<? } ?>
			</select>
			<? } ?>
		</div>
		<? } ?>
	</td>
	<? if (FIND_NUMBER_USE) { ?>
	<td class="ucs_info_about_reserv_name"><?=$arMessages["Find"]["number_room"][MESSAGE_LANG];?></td>
	<td>
		<span><?=$arResult['About']['request_colnumber'];?></span>
		<?if (SHOW_NUMBERS_EDIT_PARAMS){?><div class="ucs_about_params ucs_none"><input type="text" name="request_colnumber" class="ucs_width_50" value="<?=$arResult['About']['request_colnumber'];?>"></div><?}?>
	</td>
	<? }else{ ?>		
		<td colspan="2">&nbsp;</td>
	<? } ?>	
</tr>
<? } ?>
<tr>
	<td class="ucs_info_about_reserv_name"><?=$arMessages["Find"]["from"][MESSAGE_LANG];?></td>
	<td>
		<span><?=$arResult['About']['request_time_from'][0];?>:<?=$arResult['About']['request_time_from'][1];?> <?=$arResult['About']['request_in'];?></span>
		<?if (SHOW_NUMBERS_EDIT_PARAMS){?><div class="ucs_about_params ucs_none">
			<? if (EARLY_TIME){?><input type="text" readonly="" id="ucs_timepicker_from" name="request_time_from" class="ucs_timepicker" value="<?=$arResult['About']['request_time_from'][0];?>:<?=$arResult['About']['request_time_from'][1];?>"><?}?>
			
			<input type="text" readonly="" id="ucs_request_in" onchange="check_request_out()" name="request_in" value="<?=$arResult['About']['request_in'];?>">
		</div><?}?>
	</td>
	<td class="ucs_info_about_reserv_name"><?=$arMessages["Find"]["to"][MESSAGE_LANG];?></td>
	<td>
		<span><?=$arResult['About']['request_time_to'][0];?>:<?=$arResult['About']['request_time_to'][1];?> <?=$arResult['About']['request_out'];?></span>
		<?if (SHOW_NUMBERS_EDIT_PARAMS){?><div class="ucs_about_params ucs_none">
			<? if (EARLY_TIME){?><input type="text" readonly="" id="ucs_timepicker_to" name="request_time_to" class="ucs_timepicker" value="<?=$arResult['About']['request_time_to'][0];?>:<?=$arResult['About']['request_time_to'][1];?>"><?}?>
		
			<input type="text" readonly="" id="ucs_request_out" name="request_out" value="<?=$arResult['About']['request_out'];?>">
		</div><?}?></td>	
</tr>
<tr>
	<td class="ucs_info_about_reserv_name"><?=$arMessages["Find"]["number_person"][MESSAGE_LANG];?></td>
	<td>
		<span><?=$arResult['About']['numb'];?></span>
		<?if (SHOW_NUMBERS_EDIT_PARAMS){?>
		<table class="ucs_info_about_reserv_guests ucs_about_params ucs_none" cellspacing="0">
		<? foreach($_SESSION['Shelter']['Data']['TypeGuest'][$_SESSION['Shelter']['Find']['hotel']] as $id=>$name){		
			$col = protection($_SESSION['Shelter']['Find']['guest'][$id]);
			if ($col == 0) $col = '';
			?>
			<tr>
				<td><?=$name;?>:</td>
				<td>
					<input type="text" name="request_guest_<?=$id;?>" value="<?=$col;?>" class="ucs_width_50">
				</td>
			</tr>
		<? } ?>
		</table>
		<?}?>
	</td>
	<td class="ucs_info_about_reserv_name"><span><?=$arMessages["Find"]["number_night"][MESSAGE_LANG];?></span><div class="ucs_about_params ucs_none"><input type="submit" class="ucs_submit" value="<?=$arMessages["Find"]["submit"][MESSAGE_LANG];?>" /></div></td>
	<td><span><?=$arResult['About']['night'];?></span></td>	
</tr>

<? if (!RESULT_PAGE_ONLY){?>
<tr>
	<td colspan="4" class="ucs_info_about_reserv_to_result">
		<? if ($_GET[MODULE_SHEDULE] == PAGE_RESULT_FIRST){?>
		[<a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_RESULT_SECOND), true);?>"><?=$arMessages["Result"]["toResult"][MESSAGE_LANG];?></a>]</div>
		<?}?>
		<? if ($_GET[MODULE_SHEDULE] == PAGE_RESULT_SECOND){?>
		[<a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_RESULT_FIRST), true);?>"><?=$arMessages["Result"]["toSResult"][MESSAGE_LANG];?></a>]</div>
		<?}?>
	</td>
</tr>
<? } ?>
</table>
<?if(SHOW_NUMBERS_EDIT_PARAMS){?>
</form>
<?}?>

<?if(SHOW_NUMBERS_EDIT_PARAMS){?>
<link rel="stylesheet" type="text/css" media="screen" href="<?=SCRIPT_URL;?>js/ui/jquery-ui-1.8.15.custom.css" />
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/ui/jquery.ui.core"></script>
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/ui/jquery.ui.widget"></script>
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">	
	$(function() {
		$("#ucs_request_in").datepicker({ minDate: 0 });		
		$("#ucs_request_out").datepicker({ minDate: +1 });
		
		$(".ucs_info_about_reserv_edit_link div").click(function(){
			if ($('.ucs_info_about_reserv_guests').hasClass('ucs_none')){
				$('.ucs_about_params').removeClass('ucs_none');
				$('.ucs_info_about_reserv span').addClass('ucs_none');
				
				$('#ucs_info_about_reserv_edit_link_edit').addClass('ucs_none');
				$('#ucs_info_about_reserv_edit_link_cancel').removeClass('ucs_none');
			}else{
				$('.ucs_about_params').addClass('ucs_none');
				$('.ucs_info_about_reserv span').removeClass('ucs_none');
				
				$('#ucs_info_about_reserv_edit_link_edit').removeClass('ucs_none');
				$('#ucs_info_about_reserv_edit_link_cancel').addClass('ucs_none');
			}
		});
	});
</script>
	<? if (EARLY_TIME){?>
		<link rel="stylesheet" type="text/css" media="screen" href="<?=SCRIPT_URL;?>js/uitime/ui-timepicker.css" />
		<script type="text/javascript" src="<?=SCRIPT_URL;?>js/uitime/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="<?=SCRIPT_URL;?>js/uitime/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?=SCRIPT_URL;?>js/uitime/jquery-ui-timepicker-addon.js"></script>
		<script type="text/javascript">	
			$(function() {
				$('#ucs_timepicker_from').timepicker();
				$('#ucs_timepicker_to').timepicker();
			});
		</script>
	<? } ?>
<? } ?>