<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<div id="ucs_guest_auth">
<? if (checkAuthUser()) { ?>	
		<table class="ucs_reserv_forms ucs_main_info">
		<tr>
			<td><?=$arMessages["Reserv"]["fio"][MESSAGE_LANG];?><span>*</span></td>
			<td><input type="text" id="ucs_fio" class="ucs_width_250" value="<?=$arResult['contact'];?>" /></td>
		</tr>
		<tr>
			<td><?=$arMessages["Reserv"]["phone"][MESSAGE_LANG];?><span>*</span></td>
			<td><input type="text" id="ucs_phone" class="ucs_width_250" value="<?=$arResult['phone'];?>" /><?if (FORMAT_PHONE){?><div class="ucs_fphone"><?=$arMessages["Reserv"]["formatphone"][MESSAGE_LANG];?></div><?}?></td>
		</tr>
		<tr>
			<td><?=$arMessages["Reserv"]["email"][MESSAGE_LANG];?><span>*</span></td>
			<td><input type="text" id="ucs_email" class="ucs_width_250" value="<?=$arResult['email'];?>" /></td>
		</tr>
		<? if (CREDITCARD_USE) { 
			$id_pay = 0;
			foreach($arResult['type_pay'] as $key=>$val){
				$id_pay = $key;
				break;
			}		
			$ucs_no_check = '';
			$ucs_no_show = '';
			if ((sizeof($arResult['show_creditcard_payment']) > 0) && (!(
				$arResult['show_creditcard_payment'][0] == $id_pay
			))) {
				$ucs_nocheck = ' ucs_nocheck';
				$ucs_no_show = ' ucs_none';
			}
			?>
		<tr class="ucs_creditcard_reserv_show<?=$ucs_no_show;?>"><td colspan="2" class="ucs_creditcard_reserv"></td></tr>
		<tr class="ucs_creditcard_reserv_show<?=$ucs_no_show;?>">
			<td><?=$arMessages["Reserv"]["creditcard"][MESSAGE_LANG];?><span>*</span></td>
			<td><input type="text" id="ucs_creditcard" class="ucs_width_250<?=$ucs_nocheck;?>" value="<?=$arResult['creditcard'];?>" /></td>
		</tr>
		<tr class="ucs_creditcard_reserv_show<?=$ucs_no_show;?>">
			<td><?=$arMessages["Reserv"]["creditcarddate"][MESSAGE_LANG];?><span>*</span></td>
			<td><?=$arMessages["Reserv"]["creditcarddatem"][MESSAGE_LANG];?> <input type="text" id="ucs_creditcardm" class="ucs_width_50<?=$ucs_nocheck;?>" value="<?=$arResult['creditcarddate'][0].$arResult['creditcarddate'][1];?>" maxlength="2" /> / <?=$arMessages["Reserv"]["creditcarddatey"][MESSAGE_LANG];?> <input type="text" id="ucs_creditcardy" class="ucs_width_50<?=$ucs_nocheck;?>" value="<?=$arResult['creditcarddate'][2].$arResult['creditcarddate'][3];?>" maxlength="2" /></td>
		</tr>
		<? } ?>
		</table>
		<span class="ucs_none" id="ucs_f"><?=$GUEST['name'];?></span>
		<span class="ucs_none" id="ucs_n"><?=$GUEST['name1'];?></span>
		<span class="ucs_none" id="ucs_o"><?=$GUEST['name2'];?></span>
		<span class="ucs_none" id="ucs_id"><?=$GUEST['id'];?></span>
	<? } else { ?>
		<div class="ucs_reserv_forms_login">			
			<?if ($arResult['captcha_user']){?><div class="ucs_none"><img src="<?=PATH_CAPTCHA;?>captcha.php?width=100&height=35&characters=5" id="ucs_cph" /></div><?}?>
			<table class="ucs_reserv_forms" id="ucs_loginTable">
			<tr>
				<td colspan="2" class="ucs_reservation_login"><?=$arMessages["Reserv"]["login_zag"][MESSAGE_LANG];?></td>
			</tr>
			<tr>
				<td><?=$arMessages["Reserv"]["login"][MESSAGE_LANG];?></td>
				<td><input type="text" id="ucs_login" value="" /></td>
			</tr>
			<tr>
				<td><?=$arMessages["Reserv"]["pass"][MESSAGE_LANG];?></td>
				<td><input type="password" id="ucs_pass" value="" /></td>
			</tr>
			<tr id="ucs_forget_email_tr">
				<td>&nbsp;</td>
				<td align="right">
					<span class="ucs_link_forget" id="ucs_link_forget_1"><?=$arMessages["LoginUser"]["forget"][MESSAGE_LANG];?></span>
					<span class="ucs_link_forget ucs_none" id="ucs_link_forget_0"><?=$arMessages["ForgetPassword"]["reg"][MESSAGE_LANG];?></span>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="button" class="ucs_submit" id="ucs_login_reserv" value="<?=$arMessages["Reserv"]["login_enter"][MESSAGE_LANG];?>"></td>
			</tr>			
			</table>
			
			<table class="ucs_reserv_forms" id="ucs_regTable">
			<tr>
				<td colspan="2" class="ucs_reservation_login"><?=$arMessages["Reserv"]["reg_zag"][MESSAGE_LANG];?></td>
			</tr>
			<tr>
				<td><?=$arMessages["Reserv"]["guest_f"][MESSAGE_LANG];?><span>*</span></td>
				<td><input type="text" id="ucs_reg_f" class="ucs_reg_login" value="" /></td>
			</tr>
			<tr>
				<td><?=$arMessages["Reserv"]["guest_n"][MESSAGE_LANG];?><span>*</span></td>
				<td><input type="text" id="ucs_reg_n" class="ucs_reg_login" value="" /></td>
			</tr>
			<tr>
				<td><?=$arMessages["Reserv"]["guest_o"][MESSAGE_LANG];?></td>
				<td><input type="text" id="ucs_reg_o" class="ucs_reg_login ucs_nocheck" value="" /></td>
			</tr>
			<tr>
				<td><?=$arMessages["Reserv"]["reg_email"][MESSAGE_LANG];?><span>*</span></td>
				<td><input type="text" id="ucs_reg_email" class="ucs_reg_login" value="" /></td>
			</tr>
			<tr>
				<td><?=$arMessages["Reserv"]["reg_phone"][MESSAGE_LANG];?><span>*</span></td>
				<td><input type="text" id="ucs_reg_phone" class="ucs_reg_login" value="" /><?if (FORMAT_PHONE){?><div class="ucs_fphone"><?=$arMessages["Reserv"]["formatphone"][MESSAGE_LANG];?></div><?}?></td>
			</tr>
			<?if ($arResult['captcha_user']){?>			
			<tr>
				<td><?=$arMessages["CreateUser"]["countenter"][MESSAGE_LANG];?><span>*</span><p class="ucs_cph"></p></td>
				<td><input type="text" id="ucs_reg_count" class="ucs_reg_login" value="" maxlength="7" /></td>
			</tr>
			<?}else{?><input type="hidden" id="ucs_reg_count" value="" /><?}?>
			<tr>
				<td colspan="2"><input type="button" class="ucs_submit" id="ucs_reg_reserv" value="<?=$arMessages["Reserv"]["reg_enter"][MESSAGE_LANG];?>"></td>
			</tr>
			</table>
			
			<table class="ucs_reserv_forms" id="ucs_forgetTable">
			<tr>
				<td colspan="2" class="ucs_reservation_login"><?=$arMessages["ForgetPassword"]["head"][MESSAGE_LANG];?></td>
			</tr>
			<tr>
				<td><?=$arMessages["ForgetPassword"]["login"][MESSAGE_LANG];?><span>*</span></td>
				<td><input type="text" id="ucs_forget_email" class="ucs_forget_login" value=""></td>
			</tr>
			<?if ($arResult['captcha_user']){?>
			<tr>
				<td><?=$arMessages["CreateUser"]["countenter"][MESSAGE_LANG];?><span>*</span><p class="ucs_cph"></p></td>
				<td><input type="text" id="ucs_forget_count" class="ucs_forget_login" value="" maxlength="7" /></td>
			</tr>
			<?}else{?><input type="hidden" id="ucs_forget_count" value="" /><?}?>
			<tr>
				<td colspan="2"><input type="button" class="ucs_submit" id="ucs_forget_reserv" value="<?=$arMessages["ForgetPassword"]["submit"][MESSAGE_LANG];?>" /></td>
			</tr>
			</table>			
		</div>
		<p id="ucs_error_login" class="ucs_clear"></p>	
		<p id="ucs_login_load" class="ucs_clear ucs_none"><img src="<?=PATH_IMG;?>load.gif" /></p>
		
		<script type="text/javascript">
			<?if ($arResult['captcha_user']){?>$('.ucs_cph').html($('#ucs_cph').show());<?}?>
			$('.ucs_link_forget').click(function(){arid = $(this).attr('id').split('ucs_link_forget_');id = arid[1];forget_show(id);});
			$('#ucs_login_reserv').click(function(){login_reserv();});
			$('#ucs_reg_reserv').click(function(){reg_reserv();});
			$('#ucs_forget_reserv').click(function(){forget_reserv();});
		</script>
	<? } ?>			
</div>	