<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>
<div class="ucs_dop_info">
<?
$roomInfo = $arResult['room_info'][$roomkind[$i]];

$old_type_place = 0;
$g_i = 0;
foreach($arResult['guest_numb'][$i] as $type_place=>$infoGuest)
if ($roomInfo[$type_place]['quant'] > 0){
	$g_info = 0;
	
	if (RESERV_MAIN_NUMB_CHECK){
		if ((isset($arResult['type_main_numb_error'][$i])) && ($type_place == 2)){
			$arResult['send_data'] = false;
			?><div class="ucs_error_block"><?=$arMessages["Reserv"]["mainnumb"][MESSAGE_LANG];?></div><?
		}
	}
	
	$numb_guest_in_type_place = 0;
	foreach($infoGuest as $id_guest=>$quant_guest)
		$numb_guest_in_type_place += $quant_guest;
	
	$error_numb_guest = false;
	if ($numb_guest_in_type_place > $roomInfo[$type_place]['quant']) {
		$error_numb_guest = true;
		$arResult['mess'] = $arMessages["general"]["errorguestnumb"][MESSAGE_LANG];
	}
	
	if ($old_type_place != $type_place){
		if ($old_type_place > 0) {?></div><?}
		?><div><span class="ucs_type_place_name<?if (!SHOW_PEOPLE_INFO){?> ucs_type_place_name_wg<?}?><?if ($error_numb_guest){?> ucs_error_block<?}?>"><?=$roomInfo[$type_place]['name'];?> (<?=$roomInfo[$type_place]['quant'];?> <?=$arMessages["Reserv"]["colnumbers_short"][MESSAGE_LANG];?>)</span><?
	}
	
	$RGI_show = true;
	$RGI_newguest = false;
	foreach($infoGuest as $id_guest=>$quant_guest){
		for($j = 0; $j < $quant_guest; $j++) { 
			$arInfoGuest = Array(0, 0);
			$strInfo = $arResult['request_people_info'][$i][$type_place][$g_info];			
			if ($strInfo != '')	$arInfoGuest = explode(';', $strInfo);
			
			include(PATH_TEMPLATE_INC.'ReservationGuestInfo.php');			
			
			$g_i++;
			$g_info++;
		}		
	}	
	
	$id_guest = 1;
	$RGI_show = false;
	$arInfoGuest = Array(0, 0);	
	for($j = $g_info; $j < $roomInfo[$type_place]['quant']; $j++){				
		$RGI_newguest = false;		
		if ($j == $g_info) {
			$RGI_newguest = true;		
			include(PATH_TEMPLATE_INC.'ReservationGuestInfo.php');		
		}	
		$g_i++;
	}
	
	$old_type_place = $type_place;
}
if ($old_type_place > 0) {?></div><?}
?>
</div>

<script type="text/javascript">
	$(function(){
		$('.addguestinfo').click(function(){
			id_param = $(this).attr('id').split('addguestinfo_');
			id = id_param[1];
			
			$('#addguestinfo_load_img_'+id).removeClass('ucs_none');
			$('#ucsp_'+id).removeClass('ucs_guest_no_exist');
			
			$(this).hide();			
			countsum(0, 0);
		});
		
		$('.delguestinfo').click(function(){
			id_param = $(this).attr('id').split('delguestinfo_');
			id = id_param[1];
			
			$('#ucsp_'+id).remove();
			$('#addguestinfo_load_img_'+id).removeClass('ucs_none');
			
			$(this).hide();			
			countsum(0, 0);
		});
	});
</script>