<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<table class="ucs_reserv_no_rates" cellspacing="0">
<tr>
	<td width="130px" class="ucs_name_reservation"><?=$arMessages["Reserv"]["hotel"][MESSAGE_LANG];?></td>
	<td><?=$_SESSION['Shelter']['Data']['HotelList'][$arResult['hotel']];?></td>
	<td width="130px" class="ucs_name_reservation"><?=$arMessages["Reserv"]["colnumbers"][MESSAGE_LANG];?></td>
	<td><?=$arResult['res_colnumbers'][$i];?></td>
</tr>
<tr>
	<td class="ucs_name_reservation"><?=$arMessages["Reserv"]["from"][MESSAGE_LANG];?></td>
	<td><?=$arResult['request_in_time'][$i][0];?>:<?=$arResult['request_in_time'][$i][1];?> <?=date('d.m.Y', $arResult['request_in'][$i]);?></td>
	<td class="ucs_name_reservation"><?=$arMessages["Reserv"]["to"][MESSAGE_LANG];?></td>
	<td><?=$arResult['request_out_time'][$i][0];?>:<?=$arResult['request_out_time'][$i][1];?> <?=date('d.m.Y', $arResult['request_out'][$i]);?></td>
</tr>
<tr>
	<td class="ucs_name_reservation"><?=$arMessages["Reserv"]["typenumber"][MESSAGE_LANG];?></td>
	<td><? foreach($arResult['roomkinds'][$i] as $key=>$val) { 			
		if ($arResult['room'][$i] == $val['id']) {
			$droom = $arResult['room_code'][$arResult['room'][$i]];
			$link = $val['name'];
			foreach($LINK_ROOM as $keyroom=>$valroom) {
				if ($keyroom == $droom[0]) $link = '<a href="'.$valroom[1].'" rel="'.NUMBER_URL.protection($arCodeLang[$arResult['lang']]).'/'.$valroom[0].'" class="jTip" id="ucs_six'.$i.'" name="'.protection($droom[1]).'">'.$val['name'].'</a>';
			}
			echo $link;
		}	
	}?></td>
	<td class="ucs_name_reservation"><?=$arMessages["Reserv"]["tarif"][MESSAGE_LANG];?></td>
	<td><?for($j = 0; $j < sizeof($arResult['tarif_array'][$i]); $j++) {  
		if ($arResult['res_tarif'][$i] == $arResult['tarif_array'][$i][$j]['id']) {
			
			if (isset($arResult['Commission'][$i][$arResult['tarif_array'][$i][$j]['id']])){
				if ($arResult['Commission'][$i][$arResult['tarif_array'][$i][$j]['id']] > 0){
					$arResult['tarif_array'][$i][$j]['name'] .= ' ('.$arMessages["commission"]["name"][MESSAGE_LANG].': '.toCost($arResult['Commission'][$i][$arResult['tarif_array'][$i][$j]['id']]).' '.$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG].')';
				}
			}
			
			$rates = $arResult['tarif_array'][$i][$j]['name'];
			for($k = 0; $k < sizeof($arResult['tarif_array'][$i]); $k++)
				foreach($LINK_RATE as $key=>$val) {	
					if ($val[1] != '') $val[1] = 'href="'.$val[1].'"';
					if (($arResult['tarif_array'][$i][$k]['code'] == $key) && ($arResult['res_tarif'][$i] == $arResult['tarif_array'][$i][$k]['id'])) {
						$rates = '<a '.$val[1].' rel="'.RATE_URL.protection($arCodeLang[$arResult['lang']]).'/'.$val[0].'" class="jTip jRate tarif'.$i.'" id="tarif'.$i.'_'.$arResult['tarif_array'][$i][$k]['id'].'" name="'.protection($arResult['tarif_array'][$i][$k]['name']).'">'.$arResult['tarif_array'][$i][$k]['name'].'</a>';
					}
				}
			echo $rates;	
		}	
	}?></td>
</tr>
</table>
<input type="hidden" value="<?=$arResult['request_in_time'][$i][0];?>:<?=$arResult['request_in_time'][$i][1];?>" class="ucs_from_time" />
<input type="hidden" class="ucs_from" value="<?=date('d.m.Y', $arResult['request_in'][$i]);?>" />
<input type="hidden" value="<?=$arResult['request_out_time'][$i][0];?>:<?=$arResult['request_out_time'][$i][1];?>" class="ucs_to_time" />
<input type="hidden" class="ucs_to" value="<?=date('d.m.Y', $arResult['request_out'][$i]);?>" />
<select class="ucs_typenumbers ucs_none">
<? foreach($arResult['roomkinds'][$i] as $key=>$val) 
	if ($arResult['room'][$i] == $val['id']) {?>
		<option value="<?=$val['id'];?>"><?=$val['name'];?></option>
	<? } ?>
</select>
<select class="ucs_tarifs ucs_none"><? 
	for($j = 0; $j < sizeof($arResult['tarif_array'][$i]); $j++) 
		if ($arResult['res_tarif'][$i] == $arResult['tarif_array'][$i][$j]['id']) {?>
		<option value="<?=$arResult['tarif_array'][$i][$j]['id'];?>"><?=$arResult['tarif_array'][$i][$j]['name'];?></option>
	<? } ?>
</select>
<input type="hidden" class="ucs_colnumbers" name="colnumbers_<?=$i;?>" value="<?=$arResult['res_colnumbers'][$i];?>" />