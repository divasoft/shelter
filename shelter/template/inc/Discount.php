<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>
<? if (USE_PDS){?>
	<div class="ucs_discount_block">
		<div class="ucs_discount_title"><?=$arMessages["discount"]["title"][MESSAGE_LANG];?></div>
		<? if ($arResult['discountcode'] == ''){?>
		<table>
			<tr>
				<td><?=$arMessages["discount"]["number"][MESSAGE_LANG];?></td>
				<td><input type="text" id="ucs_discount_number" value="" /></td> <?/*80025*/?>
				<td><?=$arMessages["discount"]["name"][MESSAGE_LANG];?></td>
				<td><input type="text" id="ucs_discount_name" value="" /></td>			
				<td><input type="button" class="ucs_discount_send ucs_submit" value="<?=$arMessages["discount"]["send"][MESSAGE_LANG];?>" /><img src="<?=PATH_IMG;?>load.gif" class="load_discount_img ucs_none" /></td>			
			</tr>
		</table>
		<input type="hidden" id="ucs_discount_id" value="" />
		<div id="ucs_discount_error"></div>
		<script type="text/javascript">
			var discountAction = "<?=$arMessages["discount"]["action"][MESSAGE_LANG];?>";
			$(function(){				
				$('.ucs_discount_send').click(function(){					
					setDiscountCard();
				});
			});
		</script>
		<? }else{ ?>
			<input type="hidden" id="ucs_discount_id" value="<?=$arResult['discountcode'];?>" />
			<div class="ucs_discount_ex"><?=$arMessages["discount"]["use"][MESSAGE_LANG];?><? if ($arResult['discountcost'] < 0){?> <b><?=$arResult['discountcost'];?> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></b><?}?> [<span class="ucs_discount_delete"><?=$arMessages["discount"]["cancel"][MESSAGE_LANG];?></span>]</div><img src="<?=PATH_IMG;?>load.gif" class="load_discount_img ucs_none" />
			<script type="text/javascript">
				$(function(){
					$('.ucs_discount_delete').click(function(){
						clearDiscountCard();
					});
				});
			</script>			
		<? } ?>
	</div>	
<? } ?>