<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<script  type="text/javascript" src="<?=SCRIPT_URL;?>js/jqueryui.custom.js"></script>
<script  type="text/javascript" src="<?=SCRIPT_URL;?>js/ui.custom.js"></script>
<link type="text/css" href="<?=SCRIPT_URL;?>js/jqueryui.custom.css" rel="stylesheet">

<table class="ucs_firmcabinet">
<tr>
	<td><?=$arMessages["UserCabinet"]["number"][MESSAGE_LANG];?></td>
	<td class="ui-widget"><select id="number">
		<option value=""></option>
		<? for($i = 0; $i < sizeof($arResult['firmCabinet']['number']); $i++) { ?><option value="<?=$arResult['firmCabinet']['number'][$i]['id'];?>"><?=$arResult['firmCabinet']['number'][$i]['code'];?></option><? } ?>
	</select></td>
</tr>
<tr>		
	<td><?=$arMessages["Reserv"]["fio"][MESSAGE_LANG];?></td>
	<td><input type="text" id="fio" value="" /></td>
</tr>
<tr>		
	<td><?=$arMessages["Reserv"]["phone"][MESSAGE_LANG];?></td>
	<td><input type="text" id="phone" value="" /></td>
</tr>
<tr>		
	<td colspan="2" height="30px"><div id="clear" class="ucs_none"><?=$arMessages["UserCabinet"]["clear"][MESSAGE_LANG];?></div></td>
</tr>
</table>
<table class="ucs_firmcabinet">
<tr>		
	<td><?=$arMessages["UserCabinet"]["hotel"][MESSAGE_LANG];?></td>
	<td class="ui-widget"><select id="hotel">
		<option value=""></option>
		<? for($i = 0; $i < sizeof($arResult['firmCabinet']['hotel']); $i++) { ?><option value="<?=$arResult['firmCabinet']['hotel'][$i];?>"><?=$arResult['firmCabinet']['hotel'][$i];?></option><? } ?>
	</select></td>
</tr>
<tr>		
	<td><?=$arMessages["UserCabinet"]["from"][MESSAGE_LANG];?></td>
	<td><input type="text" id="date" value="" /></td>
</tr>
<tr>		
	<td><?=$arMessages["UserCabinet"]["status"][MESSAGE_LANG];?></td>
	<td><select id="reserves" class="users">
		<option></option>
		<option value="0"><?=$arMessages["UserCabinet"]["nopaid"][MESSAGE_LANG];?></option>
		<option value="1"><?=$arMessages["UserCabinet"]["deposit"][MESSAGE_LANG];?></option>
		<option value="2"><?=$arMessages["UserCabinet"]["paid"][MESSAGE_LANG];?></option>
	</select></td>
</tr>
<tr>		
	<td><?=$arMessages["UserCabinet"]["reservation"][MESSAGE_LANG];?></td>
	<td><select id="status" class="users">
		<option></option>
		<option value="2"><?=$arMessages["status_reservation"][2][MESSAGE_LANG];?></option>
		<option value="1"><?=$arMessages["status_reservation"][1][MESSAGE_LANG];?></option>
		<option value="0"><?=$arMessages["status_reservation"][0][MESSAGE_LANG];?></option>
	</select></td>
</tr>
</table>
<table class="ucs_firmcabinet">
<tr>		
	<td><input type="checkbox" id="active"<?if (!(isset($_GET['allreservation']))){?> checked="checked"<?}?> value="" class="ucs_input_check" /></td>
	<td><label for="active"><?=$arMessages["UserCabinet"]["active"][MESSAGE_LANG];?></label></td>	
</tr>
</table>
<div class="ucs_links_color"></div>

<p class="ucs_empty ucs_none"><?=$arMessages["UserCabinet"]["find"][MESSAGE_LANG];?>

<link rel="stylesheet" type="text/css" media="screen" href="<?=SCRIPT_URL;?>js/ui/jquery-ui-1.8.15.custom.css" />
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/ui/jquery.ui.core"></script>
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/ui/jquery.ui.widget"></script>
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">	
	
	var arIdStart = ['', '', '', '', '', '', ''];	
	var arId = ['', '', '', '', '', '', ''];
	
	function setId(){
		for(i = 0; i < arId.length; i++) {
			arId[i] = arIdStart[i];
		}
	}
	
	$(function() {
		$("#date").datepicker();
		$("#number").combobox();
		$("#hotel").combobox();
		setId();
		
		$("#clear").click();
	});
	
	$("#clear").click(function() {
		$(".ucs_firmcabinet input").val('');		
		$(".ucs_firmcabinet .users").val('');		
				
		setId();		
		setFilter();
	});
	
	$("#number").change(function() {
		arId[0] = this.value;
		setFilter();
	});
	$("#fio").keyup(function() {
		arId[1] = this.value;			
		setFilter();
	});
	$("#phone").keyup(function() {
		arId[2] = this.value;			
		setFilter();
	});
	$("#hotel").change(function() {
		arId[3] = this.value;
		setFilter();
	});
	$("#date").change(function() {
		arId[4] = this.value;		
		setFilter();
	});
	$("#reserves").change(function() {
		arId[5] = this.value;			
		setFilter();
	});
	$("#status").change(function() {
		arId[6] = this.value;			
		setFilter();
	});
	$("#active").click(function() {
		str_active_reservation = '<?=change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true);?>';
		str_all_reservation = '<?=change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET, 'allreservation'=>1), true);?>';
		
		str = '';
		if ($(this).is(':checked')) str = str_active_reservation;
			else str = str_all_reservation;	
		
		window.location.href = str;
	});		
	
	function setFilter(){
		var find = false;
		$('.ucs_usercabinet .ucs_reservation').each(function(){			
			show = true;
			tr_id = $(this).attr('id').split('_');
						
			for(i = 0; i < arId.length; i++) {
				if ((i == 1) || (i == 2)) {
					if (tr_id[i] != null) {
						exp = new RegExp(arId[i] , 'i');
						if (tr_id[i].match(exp) == null) show = false;
					}
				}
				else if (!((tr_id[i] == arId[i]) || (arId[i] == ''))) show = false;
			}
			
			if (show) {
				$(this).removeClass('ucs_none');
				find = true;
			}	
			else $(this).addClass('ucs_none');			
		});
		
		$('.ucs_informs').addClass('ucs_none');
		$('#clear').addClass('ucs_none');
		for(i = 0; i < arId.length; i++) {
			if (arId[i] != arIdStart[i]) $('#clear').removeClass('ucs_none');			
		}
		
		if (find) $('.ucs_empty').addClass('ucs_none');
			else $('.ucs_empty').removeClass('ucs_none');
	}
	
</script>