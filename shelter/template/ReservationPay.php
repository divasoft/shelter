<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<h1><?=$arMessages["ReservPay"]["head"][MESSAGE_LANG];?></h1>
<div class="ucs_payment">
	<form action="" method="post">
	<table cellspacing="0" class="ucs_reservpay">
	<tr>
		<td width="30%"><?=$arMessages["ReservPay"]["order"][MESSAGE_LANG];?></td>
		<td><span><?=$arResult['code'];?></span></td>
	</tr>
	<tr>
		<td><?=$arMessages["ReservPay"]["summa"][MESSAGE_LANG];?></td>
		<td><span><?=toCost($arResult['original_summa']).' '.$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></span></td>
	</tr>
	<? if ($arResult['balance_summa'] > 0) { ?>
	<tr>
		<td><?=$arMessages["ReservPay"]["paysumma"][MESSAGE_LANG];?></td>
		<td><span><b><?=toCost($arResult['balance_summa']).' '.$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?></b></span></td>
	</tr>
	<? } ?>
	<? if (MIN_PAY_PERCENT < 100) { ?>
	<tr>
		<td><?=$arMessages["ReservPay"]["submit"][MESSAGE_LANG];?></td>
		<td align="right"><input type="text" id="summa" name="summa" value="<?=$arResult['user_summa'];?>" style="width:70px" /> <?=$arMessages["current"][CURRENCYSHOW][MESSAGE_LANG];?>
		<? if (($arResult['balance_summa'] == 0) && (MIN_PAY_PERCENT > 0)) { 
			if (MIN_PAY_PERCENT > 1){?>
			<div class="ucs_reservpay_desc"><?=str_replace("#SUMM#", MIN_PAY_PERCENT, $arMessages["ReservPay"]["about_pay"][MESSAGE_LANG]);?></div></td>
			<? }elseif (MIN_PAY_PERCENT == 1){ ?>
			<div class="ucs_reservpay_desc"><?=$arMessages["ReservPay"]["about_pay_day"][MESSAGE_LANG];?></div></td>
			<? } ?>
		<? } ?>
	</tr>
	<? }else{ ?>
		<input type="hidden" name="summa" value="<?=$arResult['user_summa'];?>" />
	<? } ?>
	<tr>
		<td><?=$arMessages["ReservPay"]["payment"][MESSAGE_LANG];?></td>
		<td><span><?=$arMessages["payment_system"][PAY_SYSTEM_CODE][MESSAGE_LANG];?></span></td>
	</tr>
	<tr>
		<td colspan="2" align="right"><input type="submit" class="ucs_submit" value="<?=$arMessages["ReservPay"]["submit"][MESSAGE_LANG];?>" /></td>	
	</tr>
	</table>
	</form>
</div>
<script type="text/javascript">
	var min_summa = <?=$arResult['min_summa'];?>;
	var max_summa = <?=$arResult['max_summa'];?>;
	$(function(){
		$('#summa').change(function(){
			if ($(this).val() < min_summa) $(this).val(min_summa);
			if ($(this).val() > max_summa) $(this).val(max_summa);
		});
	});
</script>