<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<p><a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_LOGINUSER), true);?>"><?=$arMessages["ForgetPassword"]["back"][MESSAGE_LANG];?></a></p>
<form action="" method="post">
<input type="hidden" name="ucs_checkcode" value="<?=$arResult['captcha'];?>" />
<h1><?=$arMessages["ForgetPassword"]["head"][MESSAGE_LANG];?></h1>
<div <?if ($arResult['mess'] != ''){?>class="ucs_textcolor"<?}?>><?=$arResult['mess'];?></div>
<table class="ucs_add_guest">
	<tr>
		<td><?=$arMessages["ForgetPassword"]["login"][MESSAGE_LANG];?><span>*</span></td>
		<td><input type="text" name="login" value="<?=$arResult['form']['login'];?>"></td>
	</tr>
	<?if ($arResult['captcha_user']){?>
	<tr>
		<td><?=$arMessages["CreateUser"]["countenter"][MESSAGE_LANG];?><span>*</span><p><img src="<?=PATH_CAPTCHA;?>captcha.php?width=100&height=35&characters=5" /></p></td>
		<td><input type="text" name="count" autocomplete="off" value="" maxlength="7" /></td>
	</tr>
	<?}?>
	<tr>
		<td colspan="2" align="right"><input type="submit" class="ucs_submit" value="<?=$arMessages["ForgetPassword"]["submit"][MESSAGE_LANG];?>" /></td>
	</tr>
</table>
</form>