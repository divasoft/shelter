<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<form action="" method="post">
<input type="hidden" name="ucs_checkcode" value="<?=$arResult['captcha'];?>" />
<h1><?=$arMessages["CreateUser"]["head"][MESSAGE_LANG];?></h1>
<? if (strlen($arResult['mess']) > 0) { ?>
<p class="ucs_textcolor"><?=$arResult['mess'];?></p>
<? } ?>
<table class="ucs_add_guest ucs_add_guest_pd">
<tr>
	<td><?=$arMessages["Reserv"]["guest_f"][MESSAGE_LANG];?><span>*</span></td>
	<td><input type="text" name="name" value="<?=$arResult['form']['name'];?>" /></td>
</tr>
<tr>
	<td><?=$arMessages["Reserv"]["guest_n"][MESSAGE_LANG];?><span>*</span></td>
	<td><input type="text" name="name1" value="<?=$arResult['form']['name1'];?>" /></td>
</tr>
<tr>
	<td><?=$arMessages["Reserv"]["guest_o"][MESSAGE_LANG];?></td>
	<td><input type="text" name="name2" value="<?=$arResult['form']['name2'];?>" /></td>
</tr>
<tr>
	<td><?=$arMessages["Reserv"]["reg_email"][MESSAGE_LANG];?><span>*</span></td>
	<td><input type="text" name="mail" value="<?=$arResult['form']['mail'];?>" /></td>
</tr>
<tr>
	<td><?=$arMessages["Reserv"]["reg_phone"][MESSAGE_LANG];?><span>*</span></td>
	<td><input type="text" name="phone" value="<?=$arResult['form']['phone'];?>" /><?if (FORMAT_PHONE){?><div class="ucs_fphone"><?=$arMessages["Reserv"]["formatphone"][MESSAGE_LANG];?></div><?}?></td>
</tr>
<?if ($arResult['captcha_user']){?>
<tr>
	<td><?=$arMessages["CreateUser"]["countenter"][MESSAGE_LANG];?><span>*</span><p><img src="<?=PATH_CAPTCHA;?>captcha.php?width=100&height=35&characters=5" /></p></td>
	<td><input type="text" name="count" autocomplete="off" value="" maxlength="7" /></td>
</tr>
<?}?>
<tr>
	<td colspan="2"><input type="submit" class="ucs_submit" name="send" value="<?=$arMessages["Reserv"]["reg_enter"][MESSAGE_LANG];?>"></td>
</tr>
</table>
</form>
