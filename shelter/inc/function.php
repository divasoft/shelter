<?php	
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
		
	function checkConnect(){
		global $arMessages;
		if (isset($_GET['connect_ok'])) {
			$_SESSION['connect_ok'] = 'ok';		
			header('location: '.change_address(Array(), Array('connect_ok', 'use')));
			exit;
		}	
		if (!isset($_SESSION['connect_ok'])) {			
			include_once(PATH_TEMPLATE.PAGE_CHECKCONNECT.'.php');
			exit;
		}	
	}
	
	function getHotelList(){
		$arHotel = Array();
		$arHotelShow = unserialize(HOTEL_SHOW);
		include_once(PATH_MODULES.'HotelList.php');
		$rkr = new HotelList();	
		$obj_hotel = $rkr->Request($rkr->GetXML());	
		
		foreach($obj_hotel->hotel as $hotel) {
			$id = intval($hotel->attributes()->id);		
			if ((sizeof($arHotelShow) == 0) || (in_array($id, $arHotelShow)))
				$arHotel[$id] = i(strval($hotel->attributes()->name));		
		}
		
		return $arHotel;
	}
	
	function getDefHotel($find_hotel = 0){		
		if ((isset($_SESSION['Shelter']['Find']['hotel'])) && (intval($_SESSION['Shelter']['Find']['hotel']) > 1) && $find_hotel == 0) return intval($_SESSION['Shelter']['Find']['hotel']);
		
		$arHotelShow = unserialize(HOTEL_SHOW);
		if ((MAIN_HOTEL > 0) && ($find_hotel == 0) && 
			(sizeof($arHotelShow) == 0) || (in_array(MAIN_HOTEL, $arHotelShow))) 
				return MAIN_HOTEL; // КОНКРЕТНЫЙ отель и поиска НЕТ
		
		include_once(PATH_MODULES.'HotelList.php');
		$rkr = new HotelList();	
		$obj_hotel = $rkr->Request($rkr->GetXML());
		foreach($obj_hotel->hotel as $hotel) {
			$id = intval($hotel->attributes()->id);			
			if ((sizeof($arHotelShow) == 0) || (in_array($id, $arHotelShow))){
				if ((MAIN_HOTEL == 0) && ($find_hotel == 0)) return $id; // ЛЮБОЙ отель и поиска НЕТ
				if (intval($find_hotel) == $id) return $id;	// поиск ЕСТЬ
			}
		}	
		
		die('ERROR CONNECT');
	}	
	
	function getSettingShelter($hotel){		
		include_once(PATH_MODULES.'GetSettings.php');
		$rkr = new GetSettings($hotel);
		$obj = $rkr->Request($rkr->GetXML());
		
		$s_hour = floatval($obj->attributes()->standard_settle_hour);
		$d_hour = floatval($obj->attributes()->standard_departure_hour);
		
		$_SESSION['Shelter']['Data']['TimeSettings'] = Array(
			'From' => round($s_hour*60),
			'To' => round($d_hour*60),
		);
	}	
	
	function upSess(){
		$_SESSION['Shelter']['Data']['RoomFactor'] = Array();
		$_SESSION['Shelter']['Data']['TypeGuest'] = Array();	
	}
		
	function getDefDates() {
		if (!(isset($_SESSION['Shelter']['Find']['hotel']))) $_SESSION['Shelter']['Find']['hotel'] = getDefHotel(0);
		if (!(isset($_SESSION['Shelter']['Find']['request_in']))) $_SESSION['Shelter']['Find']['request_in'] = date('d.m.Y', time()+86400*START_DATE);	
		if (!(isset($_SESSION['Shelter']['Find']['request_out']))) $_SESSION['Shelter']['Find']['request_out'] = date('d.m.Y', time()+(86400*DATE_IN_OUT + 86400*START_DATE));
		
		if (!(isset($_SESSION['Shelter']['Find']['request_colnumber']))) $_SESSION['Shelter']['Find']['request_colnumber'] = 1;
		if (!($_SESSION['Shelter']['Find']['request_colnumber'] > 0)) $_SESSION['Shelter']['Find']['request_colnumber'] = 1;
		
		if (!(isset($_SESSION['Shelter']['Find']['guest']))){
			$arTypeGuestData = $_SESSION['Shelter']['Data']['TypeGuest'][$_SESSION['Shelter']['Find']['hotel']];
			$arGuestData = Array();
			$first = true;
			foreach($arTypeGuestData as $key=>$name){
				$numb = 0;
				if ($first){
					$numb = 1;
					$first = false;
				}
				$arGuestData[$key] = $numb;
			}
			$_SESSION['Shelter']['Find']['guest'] = $arGuestData;
		}
	}
		
	function checkDaysForRate($days, $rate){
		$days1 = intval($rate->attributes()->days1);
		$days2 = intval($rate->attributes()->days2);
		if (($days1 == 0) && ($days2 == 0)) return true;
		if (($days1 <= $days) && (($days <= $days2) || ($days2 == 0))) return true;
		return false;
	}
	
	function prt($data, $exit = false){
		echo '<pre>'; print_r($data); echo '</pre>';	
		if ($exit){	exit;}
	}
	
	function checkMail($text) {
		if (preg_match("/[0-9a-z_]+@[0-9a-z_\-^\.]+\.[a-z]{2,3}/i", $text) && ($text != '')) return true;
		return false;
	}
	
	function checkPhone($text) {
		if (!FORMAT_PHONE) return true;
		
		if (preg_match("/^[\+]?[0-9]+$/", $text) && ($text != '')) return true;
		return false;
	}
	
	function latin($text) {
		if (preg_match("/^[A-z0-9]+$/", $text) && ($text != '')) return true;
		return false;
	}
	
	function toIdate($date, $format = false) {  		
  		$form = (int)(SCHEDULE_DAY_SECONDS*($date-SCHEDULE_START_DATE_I) + 1);  		
		if ($format != false) $form = gmdate($format,$form); 
			else $form = strtotime(gmdate('d.m.Y',$form)); 
  		return $form;
	} // переводит toIdate(41088, 'd.m.Y H:i') в нужный формат 28.06.2012
	
	function toDdate($param = false) {
		if (!$param) $param = time();
		$time_start = strtotime('01.01.2000'); // точка отсчета 
		$time_now = strtotime($param);
		return (ceil(($time_now - $time_start) / SCHEDULE_DAY_SECONDS) + SCHEDULE_START_DATE_D);
	} // toDdate('28.11.2011');  переводит в формат double
	
	function sumDate($date, $time){
		$date = bcadd($date, $time, 12).'';
		
		$f = false;
		$j = strlen($date) - 1;
		for($i = (strlen($date) - 1); $i >=0; $i--){
			if (($date[$i] > 0) || ($date[$i] == '.')) break;			
			$j = $i;
		}
		
		$date_new = '';
		for($i = 0; $i < $j; $i++)
			$date_new .= $date[$i];
		
		return $date_new;		
	} // складывает дату и время
	
	// время проживания ---------------------------------------- //
	function setDM($arTime) {
		return intval($arTime[0])*60 + intval($arTime[1]);		
	} // перевод часы и минуты в минуты
	
	function getDM($minutes) {
	
		$hour = floor($minutes/60);
		$min = $minutes - $hour*60;
		if ($hour < 10) $hour = '0'.$hour;
		if ($min < 10) $min = '0'.$min;
	
		return Array($hour, $min);		
	} // перевод минуты в часы и минуты
	
	function getHourSet($minutes) {
		$hour = floor($minutes/60);
		if ($hour < 10) $hour = '0'.$hour;
		return $hour;
	} // получение часов из времени проживания
	
	function getMinutesSet($minutes) {
		$hour = floor($minutes/60);
		$min = $minutes - $hour*60;
		if ($min < 10) $min = '0'.$min;
		return $min;
	} // получение минут из времени проживания
	// время проживания ---------------------------------------- //
	
	function toDtime($hour) {
		$time = $hour/(24*60);
		return $time;
	} // toDtime(30);  переводит минуты в формат double
	
	function toItime($hour) {
		$time = round($hour*24*60);		
		return $time;
	} // toDtime(0.5);  переводит формат double в минуты
	
	function minFDate($date) {		
		if (strtotime($date) < strtotime(date('d.m.Y', time()))) {
			$_SESSION['Shelter']['Info']['Message'] = $arMessages["message"]["datechange"][MESSAGE_LANG].' ';
			$date = date('d.m.Y', time());
		}	
		return $date;
	}
	
	function maxFDate($startdate, $date) {
		global $arMessages;
		
		$min = MIN_COL_DATE*86400;
		if (strtotime($date) < strtotime($startdate) + $min) {
			$_SESSION['Shelter']['Info']['Message'] = $arMessages["message"]["datechange"][MESSAGE_LANG].' ';
			$date = date('d.m.Y', (strtotime($startdate) + $min));
		}	
		
		return $date;	
	}
	
	function maxDate($setdate, $depdate) {
		$e = false;
		if ($depdate < $setdate + MIN_COL_DATE) {
			$e = true;
			$depdate = $setdate + MIN_COL_DATE;
		}
					
		return Array($depdate, $e);	
	}
		
	// обработка данных ---------------------------------------- //
	
	function protection($text) {
		return htmlspecialchars(strip_tags($text));
	}
	
	function change_address($change = Array(), $delete = Array()) {
		
		if (($delete) && (!is_array($delete))) $delete = $_GET;	
		
		$sumbol = '?';
		if (strpos(MAIN_URL, '?')) $sumbol = '&';
		
		$userpath = Array();
		$path_a = explode('&',substr(MAIN_URL,(strpos(MAIN_URL, '?')+1),strlen(MAIN_URL)));
		for($i = 0; $i < sizeof($path_a); $i++){
			$path_mas_t = explode('=',$path_a[$i]);
			$userpath[$i] = $path_mas_t[0];			
		}	
		$stroka = MAIN_URL.$sumbol;
		$no = Array();
		foreach($change as $key=>$val) {
			$stroka .= protection($key).'='.protection($val).'&';
			$no[] = protection($key);
		}	
		if ($delete != $_GET) {
			foreach($_GET as $key=>$val) {		
				if ((!in_array($key, $no)) && (!in_array($key, $delete)) && (!in_array($key, $userpath))) $stroka .= protection($key).'='.protection($val).'&';
			}
		}
		$stroka = substr($stroka, 0, (strlen($stroka) - 1));
		
		$page_change = false; // NOTC
		if ((isset($_GET[MODULE_SHEDULE])) && (isset($change[MODULE_SHEDULE]))){
			if ($_GET[MODULE_SHEDULE] == $change[MODULE_SHEDULE]) $page_change = true;
		}
		
		return $stroka;
	} // delete - массив удаляемых переменных, change - массив заменяемых переменных, формат ?page=main
	
	function showpage($name) {	
		global $arMessages, $LINK_ROOM;
		$name = $name.'.php';
		if (file_exists(PATH_MODULES.$name)) include_once(PATH_MODULES.$name);	
		if (file_exists(PATH_CONTOLLER.$name)) include_once(PATH_CONTOLLER.$name);	
		if (file_exists(PATH_TEMPLATE.$name)) include_once(PATH_TEMPLATE.$name);		
	}
		
	function toArray($arRecordsElement) {
		if (!is_array($arRecordsElement[0])) $arRecords[0] = $arRecordsElement;
			else $arRecords = $arRecordsElement;
		return $arRecords;
	}
	
	// перевод в денежное представление ----------------- //
	function toCost($cost, $f = 0) {	
		
		$sumbol = '.';
		if (strpos($cost, ',') != '') $sumbol = ',';		
		
		$ar = explode($sumbol,$cost);
		
		$j = 0;		
		$price = '';
		for($i = (strlen($ar[0])-1); $i >= 0; $i--) {
			if (($j%3==0) && ($j > 0) && (is_numeric($ar[0][$i]))) $price = ' '.$price;
			$price = $ar[0][$i].$price;
			$j++;
		}	
		
		if (isset($ar[1])) $price .= $sumbol.$ar[1];
			elseif($f) $price .= $sumbol.'00';
		
		return $price;
	}	

	function toFCost($cost) {
		return number_format($cost, 2, '.', '');
	}
	
	// перевод в денежное представление ----------------- //
	
	
	function setLatin($name){
		return preg_replace('/[-`~!#$%^&*()_=+\\\\|\\/\\[\\]{};:"\',<>?]+/', '', htmlspecialchars_decode(trim($name)));
	}
	
	function getUserInfo($login, $pass, $type) {				
		$login = strtolower(protection($login));
		$pass = getID($pass);
		$type = protection($type);
		$nobooking = 0;
		
		if ($type == 0) {		
			include_once(PATH_MODULES.PAGE_LOGINUSER.'.php');	
			$rkr = new LoginUser($login, $pass);
			$obj = $rkr->Request($rkr->GetXML());
			$debtor = 0;
			$id = intval($obj->attributes()->id);
			
		}else{
			include_once(PATH_MODULES.PAGE_LOGINFIRM.'.php');	
			$rkr = new LoginFirm($login, $pass);
			$obj = $rkr->Request($rkr->GetXML());
			
			$id = intval($obj->attributes()->guestid);			
			$debtor = intval($obj->attributes()->id);			
			$nobooking = intval($obj->attributes()->nobooking);			
		}	
		
		if ($id > 0) {
			return Array(
				'id' => $id,
				'pass' => i($pass),			
				'email' => i(strval($obj->attributes()->email)),			
				'name' => setLatin(i(strval($obj->attributes()->name))),
				'name1' => setLatin(i(strval($obj->attributes()->name1))),
				'name2' => setLatin(i(strval($obj->attributes()->name2))),				
				'phones' => i(strval($obj->attributes()->phones)),
				'debtor' => $debtor, // для агентства 
				'nobooking' => $nobooking, // для агентства 
				'type' => $type,
			);
		} 
		return Array();
	}
	
	function getBookingAllow(){
		global $GUEST;
		
		if ($GUEST['id'] > 0){
			if ($GUEST['nobooking'] == 1) return false;
		}
		
		return true;
	}
	
	function checkBookingAllow(){
		if (!getBookingAllow()){
			header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true));
			exit;
		}
	}
	
	function getUser() {
				
		$arData = Array(
			'id' => 0,
			'pass' => '',			
			'email' => '',			
			'name' => '',
			'name1' => '',
			'name2' => '',			
			'phones' => '',
			'type' => 0,
		);
		if (isset($_SESSION['Shelter']['User']['Code']))
			if (getKS($_SESSION['Shelter']['User']['Code'])) {		
				$arData = getUserInfo($_SESSION['Shelter']['User']['Login'], $_SESSION['Shelter']['User']['Code'], $_SESSION['Shelter']['User']['Type']);
		} 
		
		// --------------------
		$reserv_status = RESERV_STATUS_USER;
		$debtor_on = USE_DEBTOR_ON;
		$user_use = USER_AUTH_USE;
		
		$conrtact_on = USE_CONTRACT_ON;
		$contract_commision = 0;
		$contract_discount = 0;
		
		if ((USE_DEBTOR_ON > 0) && ($arData['type'] == 0)){
			$arContract = getContractForDebtor($debtor_on, $conrtact_on);
			$conrtact_on = $arContract['id'];
			$contract_commision = $arContract['commission'];
			$contract_discount = $arContract['discount'];
		} // организация в настройках
		
		if (($arData['type'] == 1) && USE_DEBTORS_AUTH) {
			$reserv_status = RESERV_STATUS_FIRM;
			$debtor_on = $arData['debtor'];			
			$arContract = getContractForDebtor($arData['debtor'], 0);
			$conrtact_on = $arContract['id'];
			$contract_commision = $arContract['commission'];
			$contract_discount = $arContract['discount'];
			$user_use = 1;
		} // организация по авторизации
		
		$quota = 0;
		if ($debtor_on > 0) $quota = USE_DEBTOR_QUOTA_ON; // если используется юр.лицо ставим ограничения
		
		define('RESERV_STATUS', $reserv_status);
		define('USE_DEBTOR_ID', $debtor_on);
		define('USE_DEBTOR_CONTRACT', $conrtact_on);
		define('USE_DEBTOR_CONTRACT_COMMISION', $contract_commision);
		define('USE_DEBTOR_CONTRACT_DISCOUNT', $contract_discount);		
		define('USE_DEBTOR_QUOTA', $quota);
		define('USER_USE', $user_use);
		// --------------------
		
		return $arData;
	}
	
	function getContractForDebtor($id, $conrtact_def){
		$arId = Array();
		
		include_once(PATH_MODULES.'Contracts.php');
		$rkr = new Contracts($id);		
		$obj = $rkr->Request($rkr->GetXML());		
		
		$c_def = 0;		
		$def = 0;		
		$choose = 0;		
		foreach($obj->contract as $i=>$contract) {	
			$contract_id = intval($contract->attributes()->id);
			
			if ($conrtact_def == $contract_id) $c_def = $contract_id;
			if (intval($contract->attributes()->isdefault) == 1) $def = $contract_id;
			$choose = $contract_id;		

			$arId[] = $contract_id;
		}
		if ($def > 0) $choose = $def;		
		if ($c_def > 0) $choose = $c_def;		
		
		if ((!in_array($_SESSION['Shelter']['User']['Contract'], $arId)) || ((!isset($_SESSION['Shelter']['User']['Contract'])) || ($_SESSION['Shelter']['User']['Contract'] == ''))) 
			$_SESSION['Shelter']['User']['Contract'] = $choose;
		
		$arData = Array('id' => intval($_SESSION['Shelter']['User']['Contract']));
		foreach($obj->contract as $i=>$contract) {	
			if (intval($contract->attributes()->id) == $arData['id']){
				$arData['commission'] = intval($contract->attributes()->commisionid);
				$arData['discount'] = intval($contract->attributes()->discountid);
			}
		}
		
		return $arData;
	} // задание контракта
	
	function getContractsListForDebtor(){
		global $GUEST;
		include_once(PATH_MODULES.'Contracts.php');
		$rkr = new Contracts($GUEST['debtor']);
		$obj = $rkr->Request($rkr->GetXML());
		
		$arResult = Array();
		foreach($obj->contract as $id=>$arData){			
			$arResult[intval($arData['id'])] = strval($arData['number']);			
		}
		
		return $arResult;
	}
	
	function getContractsSelect(){
		global $arMessages, $GUEST;
		if (!authFirm()) return;
		
		$arResult = getContractsListForDebtor();
		
		include_once(PATH_TEMPLATE_INC.'Contracts.php');	
	} // список договоров для организаций	
	
	function getMessageForUser($t = ''){
		$text = $t;
		if ($_SESSION['Shelter']['Info']['Message'] != '') {
			$text = protection($_SESSION['Shelter']['Info']['Message']);
			$_SESSION['Shelter']['Info']['Message'] = '';
		}
		return $text;
	}
	
	function clearUserData(){
		$_SESSION['Shelter']['User'] = Array();
		unset($_SESSION['Shelter']['User']);
	}
	
	function setUserData($login, $password, $type){
		$_SESSION['Shelter']['User']['Type'] = protection($type);
		$_SESSION['Shelter']['User']['Login'] = protection($login);
		$_SESSION['Shelter']['User']['Code'] = setKS(protection($password));	
	}
	
	// проверка на авторизацию -------------------------- //	
	function checkAuthUser() {
		global $GUEST;
		if (($GUEST['id'] > 0) || (!USER_USE)) return true;
		return false;
	} // проверяет авторизацию клиента или параметр без авторизации
	
	function getNumbRatesForClient(){
		global $GUEST;
		if (($GUEST['id'] > 0) || (!USER_USE)){
			if ($GUEST['type'] == 1) return MAX_NUMB_RATES_FIRMS;
		}		
		return MAX_NUMB_RATES_CLIENTS;
	} // определяет количество заездов для клиента
	
	function checkUserIsGuest($obj){
		global $GUEST;
		if (!USER_CABINET_SHOW_RESERV_SHELTER) return false;		
		$guestEx = false;
		foreach($obj->inquiry as $inq) {
			foreach($inq->claim->guest as $key=>$val) {
				if (intval($val->attributes()->id) == intval($GUEST['id'])) $guestEx = true;
			}
		}
		return $guestEx;
	}
	
	function checkUser($obj) {
		global $GUEST;
		if ((((intval($obj->attributes()->owner) == $GUEST['id']) && (checkAuthUser())) || (($GUEST['type'] == 1) && (intval($obj->attributes()->debtor) == $GUEST['debtor']))) && (intval($obj->attributes()->state) == 1)) return true;
		return false;
	} // проверяет принадлежность брони пользователю
	
	function authFirm(){
		global $GUEST;
		
		if (($GUEST['id'] > 0) && ($GUEST['type'] == 1)) return true;
		return false;			
	} // проверка на авторизированное агентство
	
	function auth($ok = 1) {
		global $GUEST;
		
		$send = PAGE_LOGINUSER;
		if (!USER_USE) $send = PAGE_FIND;
		
		if ($ok) {
			if (!(checkAuthUser()) || (!USER_USE)) {
				header('location: '.change_address(Array(MODULE_SHEDULE=>$send), true));		
				exit;
			}
		}else{
			if (checkAuthUser()) {
				header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true));		
				exit;
			}
		}
	} // проверяет авторизацию клиента или параметр без авторизации и переадресует при ошибке
	
	function encodePass($string) {
		$StrLen = strlen($string);
		$Seq = KEY_SECURITY_2;
		$Gamma = '';
		while (strlen($Gamma)<$StrLen) {
			$Seq = pack("H*",sha1($Gamma.$Seq.KEY_SECURITY_2)); 
			$Gamma.=substr($Seq,0,8);
		}		
		return $string^$Gamma;
	}
	
	function getID($id) {
		$id = encodePass($id);
		$col = strlen($id) - 8;		
		return substr($id, 0, $col);
	}
	
	function setKS($id) {
		$word = md5(md5(KEY_SECURITY_1).$id);
		$ks = encodePass($id.$word[3].$word[7].$word[1].$word[5].$word[4].$word[15].$word[11].$word[17]);		
		return $ks;
	}
	
	function getKS($ks = '') {
		if (!(strlen($ks) > 0)) return false;
		
		$id = getID($ks);		
		$ks = encodePass($ks);
		$word = md5(md5(KEY_SECURITY_1).$id);
		if ($ks == $id.$word[3].$word[7].$word[1].$word[5].$word[4].$word[15].$word[11].$word[17]) return true;
		return false;
	}	
	// проверка на авторизацию -------------------------- //
		
	
	// ----------------------------
	function passCC() {
		$text = 'SHELTER';
		$res = 0;	
		for($i = 0; $i < strlen($text); $i++)
			$res += ord($text[$i]);
		return $res;
	}
	
	function decodeCC($str) {	
		$str = substr($str, 1, strlen($str)); // убираем версию
		$str = base64_decode($str);
		for ($i = 0; $i < strlen($str); $i++) {
			$str[$i] = chr(ord($str[$i]) - passCC() + ($i + 1));
		}
		$str = substr($str, 1, strlen($str)); // убираем &
		return $str;
	}
	
	function encodeCC($str) {
		for ($i = 0; $i < strlen($str); $i++) {
			$str[$i] = chr(ord($str[$i]) + passCC() - ($i + 1));
		}	
		$str = '1'.base64_encode($str); // добавляем версию
		return $str;
	}	
	// ----------------------------
	
	// оплата ------------------------------------------- //	
	function savePayLog($arData) {
		$file = fopen(FILE_ORDERS.date('Ymd', time()).'.php',"a+");		
		
		$text = '';
		if (isset($arData['id'])) $text .= 'ORDER '.$arData['id'].'; ';
		if (isset($arData['s_id'])) $text .= 'ID '.$arData['s_id'].'; ';
		if (isset($arData['s_code'])) $text .= 'CODE '.$arData['s_code'].'; ';
		if (isset($arData['sum'])) $text .= 'SUM '.$arData['sum'].'; ';
		if (isset($arData['system'])) $text .= $arData['system'].'; ';
		if (isset($arData['guest'])) $text .= 'GUEST '.$arData['guest'].'; ';
		$text .= 'TIME '.date('d.m.Y H:i', time()).'; ';
		if (isset($arData['text'])) $text .= $arData['text'];
		$text .= '
';
		if ($file) fputs ($file, $text);						
		fclose ($file);
	} 
	
	function genId($id) {
		global $GUEST;
		
		$InvId = $GUEST["ID"].$id.date('si',time());
		
		if (strlen($InvId) > 9) $InvId = substr($InvId, 0, 9);
		
		return $InvId;
	}	
	// оплата ------------------------------------------- //
	
	function iUW($text) {
		return iconv("utf-8", "windows-1251", $text);		
	}
	function i($text) {
		if (SCHEDULE_ENCODING == "windows-1251") return iconv("utf-8", "windows-1251", $text);
		return $text;
	}
	function u($text) {
		if (SCHEDULE_ENCODING == "windows-1251") return iconv("windows-1251", "utf-8", $text);
		return $text;
	}
	
	function tarif_read($obj) {				
		global $arMessages;
		
		$noTarif = Array(Array('id' => 0, 'code' => 'NO_TARIF', 'name' => $arMessages["Reserv"]["notarif"][MESSAGE_LANG], 'cost' => '0'));
		if (!$obj) return $noTarif;
		
		$arNoShowRates = unserialize(NO_SHOW_RATES);
		
		$tarif_array = Array();
		$find = false;
		
		$checkCount = 0;
		$arCheckCountRate = Array();
		
		foreach ($obj as $vacrk) {			
			foreach($vacrk->counter as $record) {
				$j = 0;
				$tarif_rate = Array();
				foreach($record->rate as $val) {
					$arCosts = Array();
					if (isset($tarif_array[intval($val->attributes()->id)])){
						$arCosts = $tarif_array[intval($val->attributes()->id)]['cost'];
					}
					$arCosts[intval($record->attributes()->id)] = floatval($val->attributes()->sumcost);
					
					$arDiscounts = Array();
					if (USE_PDS || USE_DEBTOR_CONTRACT_DISCOUNT){					
						if (isset($tarif_array[intval($val->attributes()->id)])){
							$arDiscounts = $tarif_array[intval($val->attributes()->id)]['discount'];
						}
						$arDiscounts[intval($record->attributes()->id)] = floatval($val->attributes()->sumdiscount);
					}
					
					$arCommission = Array();
					if (isset($tarif_array[intval($val->attributes()->id)])){
						$arCommission = $tarif_array[intval($val->attributes()->id)]['commission'];
					}
					$arCommission[intval($record->attributes()->id)] = floatval($val->attributes()->sumcommission);
					
					if ((sizeof($arNoShowRates) == 0) || (!in_array(strval($val->attributes()->code), $arNoShowRates))){
						$tarif_array[intval($val->attributes()->id)] = Array(
							'id' => intval($val->attributes()->id),
							'code' => i(strval($val->attributes()->code)),
							'name' => i(strval($val->attributes()->name)),
							'cost' => $arCosts,
							'discount' => $arDiscounts,
							'isfixcost' => intval($val->attributes()->isfixcost),
							'commission' => $arCommission,
						);
					}	
				}
			}
		}
		$request_in = intval($obj->attributes()->startdate);
		$request_out = intval($obj->attributes()->finishdate);
		foreach($tarif_array as $key=>$arData){			
			if ($arData['isfixcost'] == 0){
				$firstElement = true;
				$lastElement = 0;
				$check = true;
				
				foreach($arData['cost'] as $date=>$cost){
					if ($firstElement){
						if ($request_in != $date) $check = false;
						$firstElement = false;
					} // check first element
					else{
						if (($lastElement+1) != $date) $check = false;
					}
					$lastElement = $date;
				}
				if ($lastElement != $request_out) $check = false;
				
				if (!$check) unset($tarif_array[$key]);				
			}
		}
		foreach($tarif_array as $key=>$arParam){
			$cost = 0;
			foreach($arParam['cost'] as $date=>$sum){
				$cost += $sum;
			}
			$tarif_array[$key]['cost'] = $cost;
			
			$discount = 0;
			foreach($arParam['discount'] as $date=>$sum){
				$discount += $sum;
			}
			$tarif_array[$key]['discount'] = $discount;
			
			$commission = 0;
			foreach($arParam['commission'] as $date=>$sum){
				$commission += $sum;
			}
			$tarif_array[$key]['commission'] = $commission;
			
			$fullcost = 0;
			if (USE_PDS || USE_DEBTOR_CONTRACT_DISCOUNT){
				if ($tarif_array[$key]['discount'] < 0){
					$fullcost = $tarif_array[$key]['cost'] - $tarif_array[$key]['discount'];					
				}
			}	
			$tarif_array[$key]['fullcost'] = $fullcost;			
		}
		
		
		// ------------------------------
		$tarif_array = array_values($tarif_array);
		// ------------------------------		
		for($i = 0; $i < sizeof($tarif_array) - 1; $i++)
			for($j = $i+1; $j < sizeof($tarif_array); $j++)
			if ($tarif_array[$i]['cost'] > $tarif_array[$j]['cost']){
				$t = $tarif_array[$i];
				$tarif_array[$i] = $tarif_array[$j];
				$tarif_array[$j] = $t;
			}
		// ------------------------------
		if (sizeof($tarif_array) == 0) $tarif_array = $noTarif;
		
		return $tarif_array;
	}
	
	function getStatusLimit($reservation_id, $arfrom, $arto, $arid, $armin, $col) {				
		// ----------------------		
		$arRoomGuestAll = Array();
		include_once(PATH_MODULES.PAGE_RESERVINFO.'.php');
		$rkr = new ReservationInfo($reservation_id, 0);		
		$obj = $rkr->Request($rkr->GetXML());
		foreach($obj->inquiry as $inquiry) {
			$arRoomGuestAll[] = Array(
				'roomkind' => intval($inquiry->attributes()->roomkind), 
				'col' => intval($inquiry->attributes()->quant),
				'from' => intval($inquiry->attributes()->arrival),
				'to' => intval($inquiry->attributes()->departure),
			);
		} // если бронь пересохраняется
		
		$arRoomGuest = Array();
		for($i = 0; $i < sizeof($arRoomGuestAll); $i++) {
			for($j = $arRoomGuestAll[$i]['from']; $j < $arRoomGuestAll[$i]['to']; $j++) {
				$arRoomGuest[$arRoomGuestAll[$i]['roomkind']][$j] += $arRoomGuestAll[$i]['col'];
			}
		} // группируем старые брони
		// ----------------------
		
		$arRoomAll = Array();
		for($i = 0; $i < $col; $i++) {			
			$arRoomAll[] = Array('id' => $arid[$i], 'col' => $armin[$i]);
		} // общий массив бронируемых номеров
		
				
		// ----------------------------- //
		$from = -1; // минимальная дата заезда
		$to = 0; // максимальная дата выезда
		$arIdAll = Array();	// общее количество бронируемых номеров
		$arFromDateInt = Array(); // дни проживания для каждого заезда		
		for($i = 0; $i < $col; $i++) {			
			for($j = floatval($arfrom[$i]); $j < floatval($arto[$i]); $j++) {
				$arIdAll[$arid[$i]][$j] = $arIdAll[$arid[$i]][$j] + $armin[$i];
				if (($from > $j) || ($from == -1)) $from = $j;				
				$arFromDateInt[$i][] = intval($j);
			}	
			
			for($j = floatval($arto[$i]); $j <= floatval($arto[$i]); $j++) 
				if ($to < $j) $to = $j;			
		}
		// распределение бронируемых номеров, для каждого дня задаем общее количество бронируемых номеров
		
		// ----------------------------- //
		$arIdBlockCol = Array();
		$arIdBlock = Array();		
		foreach($arIdAll as $roomkind=>$val) {
			include_once(PATH_MODULES.'ReservationLimit.php');		
			$rkr = new ReservationLimit($from, $to, $roomkind);
			$obj = $rkr->Request($rkr->GetXML());
			$from_block = intval($from);			
			foreach($obj->block as $block) 
				foreach($block->record as $record) {
					$date = floatval($record->attributes()->date);				
					$blockid = intval($record->attributes()->blockid);				
					if (($from_block <= $date) && ($date < $to)) {
						$free = intval($record->attributes()->quant_free);
						
						if ($arRoomGuest[$roomkind][$date] > 0) $free += $arRoomGuest[$roomkind][$date]; // добавляем старые брони
						
						$arIdBlock[$roomkind][$blockid][$date] = $free;	// количество номеров в базе по blockid
						$arIdBlockCol[$roomkind][$date] = $arIdBlockCol[$roomkind][$date] + $free;	// суммарное количество номеров по всем blockid
					}
				}							
		}
		// добавление номеров из базы 
		
		// ----------------------------- //
		$arRes = Array();
		for($i = 0; $i < $col; $i++) {	
			$id = $arRoomAll[$i]['id'];
			$col_room = $arRoomAll[$i]['col'];
			
			foreach($arIdBlock as $roomkind=>$arBlock) {				
				if ($id  == $roomkind) {
					$ex = false;
					foreach($arBlock as $blockid=>$arDate) {
						if (!$ex) {
							$er1 = false;
							foreach($arDate as $date=>$c) {
								if (in_array($date, $arFromDateInt[$i])) {							
									if (($arIdBlock[$roomkind][$blockid][$date] - $col_room) < 0) $er1 = true; // если номеров меньше, чем нужно
								}
							}
							
							if (!$er1) {
								foreach($arDate as $date=>$c) {
									if (in_array($date, $arFromDateInt[$i])) {
										$arIdBlock[$roomkind][$blockid][$date] = $arIdBlock[$roomkind][$blockid][$date] - $col_room;
										if ($arIdBlock[$roomkind][$blockid][$date] >= 0) {								
											$arRes[$i] = $blockid; // добавляем blockid для заезда
											$ex = true; // blockid найден, остальные blockid пропускаем
										}
									}
								}
							}
						}	
					}
					if (!$ex) $error = true;		
				}
			}
		}		
		// проверка на свободные номера
		
		if (!$error) return $arRes;
		return;	
	} // работа с ограничениями
	
	function setCaptcha(){
		$_SESSION['Shelter']['Info']['CodeForm'] = md5(time()*rand(1, 10));
		return $_SESSION['Shelter']['Info']['CodeForm'];
	}
	
	function checkCaptcha($code){
		$codeForm = '';
		if (isset($_SESSION['Shelter']['Info']['CodeForm'])) $codeForm = $_SESSION['Shelter']['Info']['CodeForm'];
		$_SESSION['Shelter']['Info']['CodeForm'] = '';
		unset($_SESSION['Shelter']['Info']['CodeForm']);
		
		if (($code == $codeForm) && ($code != '')) return true;
		return false;
	}
	
	function checkCaptchaUser($code){
		$codeForm = $_SESSION['Shelter']['Info']['Code'];
		$_SESSION['Shelter']['Info']['Code'] = '';
		unset($_SESSION['Shelter']['Info']['Code']);
		
		if ((($code == $codeForm) && ($code != '')) || (!CAPTCHA_USER)) return true;
		return false;
	}
	
	function getRoomkinds($hotel, $request_in, $request_out) {
		$arNoShowRooms = unserialize(NO_SHOW_ROOMS);;		
		
		//$request_out--;
		
		if (USE_DEBTOR_QUOTA) {
			include_once(PATH_MODULES.'ResultLimit.php');		
			$rkr = new ResultLimit($hotel, $request_in, $request_out);
		}else{
			include_once(PATH_MODULES.'Result.php');		
			$rkr = new Result($hotel, $request_in, $request_out);
		}		
		
		$obj = $rkr->Request($rkr->GetXML());
		
		$roomkinds = Array();
		foreach ($obj as $roomkind) {
			$tmp_roomkind = array();
			$tmp_roomkind['id'] = intval($roomkind->attributes()->id);
			$tmp_roomkind['code'] = i(strval($roomkind->attributes()->code));
			$tmp_roomkind['name'] = i(strval($roomkind->attributes()->name));
			$tmp_roomkind['dates'] = array();
			foreach ($roomkind as $record) {
				$room = intval($record->attributes()->amountvacancyrooms);
				if ($room < 0) $room = 0;
				$tmp_roomkind['dates'][intval($record->attributes()->date)] = $room;
			}
			if (!in_array($tmp_roomkind['code'], $arNoShowRooms))
				$roomkinds[] = $tmp_roomkind;
		}
		
		for($i = 0; $i < (sizeof($roomkinds) - 1); $i++)
			for($j = ($i + 1); $j < sizeof($roomkinds); $j++) 
				if ($roomkinds[$i]['name'] > $roomkinds[$j]['name']){
					$t = $roomkinds[$i];
					$roomkinds[$i] = $roomkinds[$j];
					$roomkinds[$j] = $t;
				}			
		
		return $roomkinds;
	} // свободные номера
	
	function getMinSummPay($obj_summ, $summa){
		if ($obj_summ == $summa) {
			$min_obj = round($obj_summ*MIN_PAY_PERCENT/100);
			if ($min_obj > $summa) $min_obj = $summa;
		}else{
			$min_obj = 0;
		}
		
		return $min_obj;
	}
	
	function putIdOrder($id){
		$file = fopen(FILE_ORDERS_SUCCESS,"a+");
		if ($file) fputs ($file, $id.';');	
		fclose($file);
	}
	
	function isSecondOrder($id){
		$orders = file_get_contents(FILE_ORDERS_SUCCESS);		
		$arOrders = explode(';', $orders);		
		
		if (in_array($id, $arOrders)) return true;
		return false;
	}
	
	function SetPay($order, $sum) {
		if (!(($order > 0) && ($sum > 0) && PAYMENT)) return;
		
		$id = intval($order);
			
		include_once(PATH_MODULES.PAGE_RESERVINFO.'.php');
		$rkr = new ReservationInfo($id, 0);
		$obj = $rkr->Request($rkr->GetXML());
		
		$obj['action'] = 'SAVE';
		$obj['create_folio'] = '1';
		$obj['payment_summa'] = $sum;
		$obj['operation'] = CODE_OPERATION;
		
		$maindata = Array();
		foreach($obj->attributes() as $key=>$val)
			$maindata[$key] = strval($val);
		
		$inquiry = Array();
		$i = 0;
		$p = 0;
		$r = 0;
		foreach($obj->inquiry as $inq) {
			$j = 0;	
			foreach($inq->attributes() as $key=>$val)
				$inquiry[$i][$key] = strval($val);
			foreach($inq->claim->attributes() as $key=>$val)
				$inquiry[$i]['claim'][$key] = strval($val);
			foreach($inq->claim->guest as $key=>$val) {											
				foreach($val->attributes() as $key2=>$val2) 
					$inquiry[$i]['claim']['guests'][$j][$key2] = strval($val2);										
				$j++;
			}
			foreach($inq->claim->operation as $key=>$val) {											
				foreach($val->attributes() as $key2=>$val2) 
					$inquiry[$i]['claim']['operation'][$p][$key2] = strval($val2);										
				$p++;
			}
			
			foreach($inq->claim->discounts as $key=>$val) {			
				foreach($val as $key2=>$val2) {				
					$inquiry[$i]['claim']['discount'][] = intval($val2->attributes()->id);										
				}			
			}
			
			foreach($inq->claim->rate_adds as $key=>$val) {			
				foreach($val as $key2=>$val2) {				
					$inquiry[$i]['claim']['rate_adds'][] = intval($val2->attributes()->id);										
				}			
			}
			
			foreach($inq->rs_quant as $key=>$val) {											
				foreach($val->attributes() as $key2=>$val2) 
					$inquiry[$i]['rs_quant'][$r][$key2] = strval($val2);										
				$r++;
			}
			
			$i++;
		}
		
		include_once(PATH_MODULES.PAGE_SENDRESERV.'.php');
		$rkr = new SendReservation($maindata, $inquiry);
		$xml = $rkr->Request($rkr->GetXML());
		$resid = intval($xml->attributes()->id);
	
		if ($resid > 0) return true;
		return false;
	} // оплата номера
	
	function getPayTypeOnline() {
		$arData = Array();
		include_once(PATH_MODULES.'TypePaySystem.php');	
		$rkr = new TypePaySystem();
		$xml = $rkr->Request($rkr->GetXML());
		foreach($xml->payment as $id=>$payment_val) {
			if (intval($payment_val->attributes()->useinternet) > 0) $arData[intval($payment_val->attributes()->id)] = i(strval($payment_val->attributes()->name));
		}
		return $arData;
	} // типы оплаты
	
	function setFullName($name, $name1, $name2){
		$name = strval($name);
		$name1 = strval($name1);
		$name2 = strval($name2);
		
		$fullname = protection($name);
		if ($name1 != '') $fullname .= ' '.protection($name1);
		if (($name1 != '') && ($name2 != '')) $fullname .= ' '.protection($name2);
		
		return $fullname;
	}
	
	function getFullName(){
		global $GUEST;
		
		$name = '';
		if ($GUEST['name'] != '') $name = $GUEST['name'];
		if ($GUEST['name1'] != '') $name .= ' '.$GUEST['name1'];
		if ($GUEST['name2'] != '') $name .= ' '.$GUEST['name2'];
		
		return $name;
	}
	
	function getContacts() {
		global $arMessages, $GUEST;
		$arResult['contact'] = getFullName();
		$arResult['phone'] = $GUEST['phones'];
		$arResult['email'] = $GUEST['email'];
		$arResult['creditcard'] = $GUEST['creditcard'];
		$arResult['show_creditcard_payment'] = unserialize(SHOW_CREDIT_CARD_FOR_TYPE_PAYMENT);
		include_once(PATH_TEMPLATE_INC.'ReservForm.php');	
	} // загрузка данных пользователя после авторизации на странице бронирования
	
	function sendMailForMeneger($template, $arData, $tosubject = ''){
		$arSendMessageManager = unserialize(SEND_MESSAGE_MANAGER);
		
		if (sizeof($arSendMessageManager) > 0) {		
			for($i = 0; $i < sizeof($arSendMessageManager); $i++) 
				sendMail($arSendMessageManager[$i], $template, $arData, $tosubject);			
		}
	}
	
	function sendMail($email, $mailTemplate, $arData, $tosubject = '') {
		global $arMessages;		
		$subject = $arMessages["email"]["subject"][MESSAGE_LANG];
		if ($arMessages["email"][$mailTemplate][MESSAGE_LANG] != '') $subject = $arMessages["email"][$mailTemplate][MESSAGE_LANG].$tosubject;		
		
		include_once(PATH_MODULES.'Mail.php');	
		$mail = new Mail();
		$template = $mail->Compile($mailTemplate, $arData);		
		
		return $mail->Send($email, $subject, $template);
	} // отправка письма
	
	function show_step() {
		global $arMessages;
		include_once(PATH_TEMPLATE_INC.'Step.php');	
	} // показывать шаг бронирования
	
	function getReservForm() {
		global $arMessages, $arResult;
		$arResult['show_creditcard_payment'] = unserialize(SHOW_CREDIT_CARD_FOR_TYPE_PAYMENT);
		include_once(PATH_TEMPLATE_INC.'ReservForm.php');	
	} // формы для авторизации на странице бронирования
	
	function getOperation($hotel) {
		$arShowOperations = unserialize(SHOW_OPERATIONS);		
		$list_operation = Array();
		
		if (sizeof($arShowOperations) > 0) {
			include_once(PATH_MODULES.'Operations.php');
			$rkr = new Operations('0,'.$hotel);
			$obj = $rkr->Request($rkr->GetXML());			
			$i = 0;
			foreach($obj->operation as $op_id=>$op_val) 
			if (in_array($op_val['code'], $arShowOperations)) {
				$list_operation[$i]['id'] = 0;
				$list_operation[$i]['quant'] = 1;		
				$list_operation[$i]['code'] = intval($op_val['id']);
				$list_operation[$i]['name'] = html_entity_decode(strval($op_val['name']));
				$list_operation[$i]['cost'] = floatval($op_val['cost']);
				$i++;			
			}
		}
		
		return $list_operation;
	} // дополнительные услуги
	
	function info_about_reserv() {
		global $arMessages;
		
		getDefDates();
		$arResult['About']['night'] = round(round(strtotime($_SESSION['Shelter']['Find']['request_out']) - strtotime($_SESSION['Shelter']['Find']['request_in']))/SCHEDULE_DAY_SECONDS);	
		
		$colPl = 0;
		$arColPeople = setNumbDef(true);
		foreach($arColPeople as $id=>$numb)
			$colPl += $numb;
		
		$arResult['About']['hotel'] = $_SESSION['Shelter']['Data']['HotelList'][$_SESSION['Shelter']['Find']['hotel']];		
		$arResult['About']['numb'] = $colPl;
		$arResult['About']['request_in'] = protection($_SESSION['Shelter']['Find']['request_in']);
		$arResult['About']['request_out'] = protection($_SESSION['Shelter']['Find']['request_out']);
		$arResult['About']['request_colnumber'] = protection($_SESSION['Shelter']['Find']['request_colnumber']);
		$arResult['About']['request_time_from'] = getDM(FROM_HOUR);
		$arResult['About']['request_time_to'] = getDM(TO_HOUR);
		
		include_once(PATH_TEMPLATE_INC.'About.php');	
	} // информация о бронировании
	
	function checkReservForDelete($date, $status, $state, $balance){
		global $GUEST;
		
		if ($balance > 0) return false; // бронь оплачена
		if (intval($state) == 2) return false; // бронь удалена
		if (!USER_RESERV_CANCEL) return false; // запрет удаления для всех
		if ((toDdate(date('d.m.Y', time()))+USER_RESERV_CANCEL) > intval($date)) return false; // заезд осуществлен
		
		$type_cancel = USER_RESERV_CANCEL_USER;
		if ($GUEST['type'] == 1) $type_cancel = USER_RESERV_CANCEL_AGENT;		
		if ((($type_cancel == 1) && (intval($status) == 0)) || ($type_cancel == 2)) return false; // запрет для типа пользователя
				
		return true;
	} // проверка удаления брони
	
	function checkReservForEdit($date){
		if (toDdate(date('d.m.Y', time())) > intval($date)) return false;
		
		return true;
	} // проверка редактирования брони
		
	function deleteReserv($id) {
		global $arMessages, $GUEST;
		
		if (!USER_RESERV_CANCEL) return false;
		
		$gid = $GUEST['id'];
		if ($GUEST['type'] == 1) $gid = 0;
		
		include_once(PATH_MODULES.PAGE_RESERVINFO.'.php');
		$rkr = new ReservationInfo($id, $gid);
		$obj = $rkr->Request($rkr->GetXML());	
		
		if ((!checkUser($obj)) || (!($obj->attributes()->id == $id)) || (!checkReservForDelete($obj->attributes()->arrival, $obj->attributes()->status, $obj->attributes()->state, $obj->attributes()->balance))) return false;		
		
		$gid_res = intval($obj->attributes()->owner);
		$code_res = intval($obj->attributes()->code);
		
		if (strval($obj->attributes()->extno) != '') {		
			if (file_exists(PATH_PM.'gnrt.php')) {
				include_once(PATH_PM.'gnrt.php');
				deleteFromRM($obj);	// удаление с РезервМастера			
			}	
		}	
		
		include_once(PATH_MODULES.PAGE_RESERVDELETE.'.php');
		$rkr = new ReservationDelete($id, $gid_res);
		$obj = $rkr->Request($rkr->GetXML());
		
		if (intval($obj->attributes()->resid) > 0) return $code_res;
		return false;
	} // удаление бронирования
	
	function genNumbGuest($roomkind_id, $arNumbDef){
		$arGuestNumbI = Array();
		$hotelid = intval(protection($_SESSION['Shelter']['Find']['hotel']));
		$arTypeNumbRoom = $_SESSION['Shelter']['Data']['RoomFactor'][$hotelid][$roomkind_id];
		$arTypeGuest = $_SESSION['Shelter']['Data']['TypeGuest'][$hotelid];
				
		$find = false;
		foreach($arTypeNumbRoom as $f1=>$arTypeNumb) {			
			foreach($arTypeGuest as $type_guest=>$val){	
				$quant = 0;
				if ((!$find) && ($f1 == 2) && NECESSARILY_ADULT_IN_RATE) {
					$quant = 1; //$arNumbDef[$type_guest];
					$find = true;
				}	
				$arGuestNumbI[$f1][$type_guest] = $quant;				
			}
			
		}		
		return $arGuestNumbI;
	}
	
	function setNumbDef($v){		
		$hotelid = intval(protection($_SESSION['Shelter']['Find']['hotel']));
		
		if ($v){
			$arNumbDef = Array();
			$numb = 0;
			foreach($_SESSION['Shelter']['Data']['TypeGuest'][$hotelid] as $id=>$name){
				$numb += intval($_SESSION['Shelter']['Find']['guest'][$id]);
				$arNumbDef[$id] = intval($_SESSION['Shelter']['Find']['guest'][$id]);
			}
			if ($numb == 0) $v = false;
		}
		
		if (!$v){
			$arNumbDef = Array();
			foreach($_SESSION['Shelter']['Data']['TypeGuest'][$hotelid] as $id=>$name){
				$numb = 0;
				//if ($id == 1) $numb = 1;
				$arNumbDef[$id] = $numb;
			}		
		}
		return $arNumbDef;
	}
	
	function setNumbGuestDef($roomkind, $arNumbDef){		
		$arGuestNumb = Array();
		for($i = 0; $i < sizeof($roomkind); $i++) {
			$arGuestNumb[$i] = genNumbGuest($roomkind[$i], $arNumbDef);			
		}
		$arGuestNumb = setFirstGuest($arGuestNumb);
		return $arGuestNumb;
	}
	
	function checkMainTypeNumb($arGuestNumb, $roomkind){
		if (!RESERV_MAIN_NUMB_CHECK) return Array();
		
		$hotelid = intval(protection($_SESSION['Shelter']['Find']['hotel']));		
		$arError = Array();		
		for($i = 0; $i < sizeof($roomkind); $i++) {			
			$mainPl = false; // свободны основные места
			foreach($arGuestNumb[$i] as $type_place=>$arNumbPl){
				$findPl = 0;
				foreach($arNumbPl as $type_guest=>$colPl){
					$findPl += $colPl;
				}
				
				$arTypeNumbRoom = $_SESSION['Shelter']['Data']['RoomFactor'][$hotelid][$roomkind[$i]][$type_place];
				if ($type_place == 2){
					if ($arTypeNumbRoom['quant'] <= $findPl) $mainPl = true; // заняты основные места
				}else{					
					if ((!$mainPl) && ($findPl > 0)) $arError[$i] = true;
				}						
			}
		}		
		return $arError;
	}
	
	function setFirstGuest($arGuestNumb){
		for($i = 0; $i < sizeof($arGuestNumb); $i++){
			$first_type_place = 0;
			$first_type_guest = 0;
			$numb = 0;
			foreach($arGuestNumb[$i] as $type_place=>$arNumbGuest){
				if ($first_type_place == 0) $first_type_place = $type_place;
				foreach($arNumbGuest as $type_guest=>$numb_guest){
					if ($first_type_guest == 0) $first_type_guest = $type_guest;
					$numb += $numb_guest;
				}	
			}	
			if ($numb == 0) $arGuestNumb[$i][$first_type_place][$first_type_guest] = 1;
		}
		
		return $arGuestNumb;
	}
	
	function getNumbGuest($res_guest, $roomkind){
		$arGuestNumb = Array();
		$request_guest = explode(';',$res_guest);
		$arNumbDef = setNumbDef(false);
		
		for($i = 0; $i < sizeof($roomkind); $i++){			
			$arGuestNumb[$i] = genNumbGuest($roomkind[$i], $arNumbDef);
			
			if (isset($request_guest[$i])){
				if ($request_guest[$i] != ''){
					$str_guest = explode('#',$request_guest[$i]);
					for($j = 0; $j < sizeof($str_guest); $j++){
						$info = explode('_',$str_guest[$j]);
						$arGuestNumb[$i][$info[0]][$info[1]] = $info[2];
					}
					
					$gf = false;
					foreach($arGuestNumb[$i][2] as $key=>$val){
						if ($val > 0) $gf = true;
					}
					if (!$gf) $arGuestNumb[$i][2][1] = 1;				
				}
			}
		}
		
		$arGuestNumb = setFirstGuest($arGuestNumb);
		
		return $arGuestNumb;
	}
	
	function checkTypeNumb($arGuestNumb, $roomkind){
		$hotelid = intval(protection($_SESSION['Shelter']['Find']['hotel']));
		$arTypeNumbError = Array();
		for($i = 0; $i < sizeof($roomkind); $i++) {			
			foreach($arGuestNumb[$i] as $type_place=>$arNumbPl){
				$findPl = 0;
				foreach($arNumbPl as $type_guest=>$colPl){
					$findPl += $colPl;
				}
				
				$arTypeNumbRoom = $_SESSION['Shelter']['Data']['RoomFactor'][$hotelid][$roomkind[$i]][$type_place];
				if ($arTypeNumbRoom['quant'] < $findPl)
					$arTypeNumbError[$i][$type_place] = 1;				
			}
		}
		return $arTypeNumbError;
	}
	
	function setEmptySess(){
		$_SESSION['Shelter']['Data'] = Array();
		unset($_SESSION['Shelter']['Data']);
	}
	
	function getFactor1(){
		global $arMessages;
		
		$arHotelShow = unserialize(HOTEL_SHOW);
		
		$mess = false;
		if (sizeof($arMessages['GetRoomKindFactor1Name']) > 0) $mess = true;
		
		include_once(PATH_MODULES.'GetFactor1.php');
		$rkr = new GetFactor1();
		$obj = $rkr->Request($rkr->GetXML());
		$arRoomFactor = Array();
		foreach($obj->roomkind as $r_id=>$r_val){	
			$hotel = 0;
			$id_hotel = intval($r_val->attributes()->hotel);
			if ((sizeof($arHotelShow) == 0) || (in_array($id_hotel, $arHotelShow)))
				$hotel = $id_hotel;
			
			if ($hotel > 0)
				foreach($r_val->roomkindfactor1 as $rf_id=>$rf_val)	{		
					$name = strval($rf_val->attributes()->name);
					if ($mess){
						$f1 = intval($rf_val->attributes()->f1);
						if (is_array($arMessages['GetRoomKindFactor1Name'][$f1]) && ($arMessages['GetRoomKindFactor1Name'][$f1][MESSAGE_LANG] != ''))
							$name = $arMessages['GetRoomKindFactor1Name'][$f1][MESSAGE_LANG];
					}					
					$arRoomFactor[intval($r_val->attributes()->hotel)][intval($r_val->attributes()->id)][intval($rf_val->attributes()->f1)] = Array(
						'name' => $name,
						'quant' => intval($rf_val->attributes()->quant),
					);	
					
					$_SESSION['Shelter']['Data']['RoomFactorList'][intval($r_val->attributes()->hotel)][intval($rf_val->attributes()->f1)] = $name;
				}	
		}
		
		return $arRoomFactor;
	} // получить список типов мест в номерах
			
	function getFactor2(){
		global $arMessages;
		
		$arHotelShow = unserialize(HOTEL_SHOW);		
		$arShowTypeGuest = unserialize(SHOW_TYPE_GUEST);
		
		$mess = false;
		if (sizeof($arMessages['GetFactor2Name']) > 0) $mess = true;
		
		include_once(PATH_MODULES.'GetFactor2.php');
		$rkr = new GetFactor2();
		$obj = $rkr->Request($rkr->GetXML());
		$arTypeGuest = Array();
		foreach($obj->factors as $f_hotel=>$f_hotel_factors){	
			$hotel = 0;
			$id_hotel = intval($f_hotel_factors->attributes()->hotel);
			if ((sizeof($arHotelShow) == 0) || (in_array($id_hotel, $arHotelShow)))
				$hotel = $id_hotel;
				
			if ($hotel > 0)
				foreach($f_hotel_factors->factor2 as $f_id=>$f_val)
					if (strval($f_val->attributes()->paramstr) == ''){			
						$id = intval($f_val->attributes()->id);
						$name = strval($f_val->attributes()->name);	
						if ($mess){
							if (is_array($arMessages['GetFactor2Name'][$id]) && ($arMessages['GetFactor2Name'][$id][MESSAGE_LANG] != ''))
								$name = $arMessages['GetFactor2Name'][$id][MESSAGE_LANG];
						}
						if ((sizeof($arShowTypeGuest) == 0) || (in_array($id, $arShowTypeGuest)))
							$arTypeGuest[$hotel][$id] = $name;
					}
		}		
		return $arTypeGuest;
	} // получить список категорий гостей
	
	function getVacancyRooms($hotel, $start, $findish, $roomkind){
		include_once(PATH_MODULES.'VacancyRooms.php');
		$rkr = new VacancyRooms(intval($hotel), intval($start), intval($findish), intval($roomkind));
		$obj = $rkr->Request($rkr->GetXML());
		
		$arRooms = Array();
		foreach($obj->building as $bd=>$building){
			foreach($building->level as $fl=>$level){
				foreach($level->room as $rm=>$room){					
					$ex = true;
					foreach($room->record as $record){
						if (!(intval($record->attributes()->amountvacancyrooms) > 0)) $ex = false;
					}					
					if ($ex) $arRooms[] = intval($room->attributes()->id);
				}
			}			
		}		
		return $arRooms;
	}
	
	function refreshRooms($arData){
		$arDataNew = Array();
		for($i = 1; $i < sizeof($arData); $i++)
			$arDataNew[] = $arData[$i];
		return $arDataNew;	
	}
	
	function getInfoAboutReserv($id){
		include_once(PATH_MODULES.PAGE_RESERVINFO.'.php');
		$rkr = new ReservationInfo($id, 0);		
		$obj = $rkr->Request($rkr->GetXML());
		
		$arRooms = Array();
		foreach($obj->inquiry as $inq){
			$roomkind = intval($inq->attributes()->roomkind);
			foreach($inq->claim as $claim){
				$arRooms[$roomkind][] = intval($claim->attributes()->idroom);
			}
		}			
		return $arRooms;		
	}
	
	function setParamToArray($text){
		$text = str_replace('/E', '-', $text);
		$arData = explode('&', $text);
		$res = '';
		for($i = 1; $i < sizeof($arData); $i++){			
			$arDataF1 = explode('=', $arData[$i]);
			$f1 = $arDataF1[0].'-';
			
			$inner = explode('/A', $arDataF1[1]);
			for($j = 1; $j < sizeof($inner); $j++){
				$res .= $f1.$inner[$j].';';
			}			
		}
		$res = substr($res, 0, (strlen($res) - 1));
		
		return $res;
	}
	
	function checkMainNumbForReserv($id, $rate){
		$arNoPlaceForGuest = unserialize(TYPE_GUEST_NO_CHECK);
		
		$arData = explode(';', $rate);		
		$arTypePlaceNumbGuest = Array();
		for($i = 0; $i < sizeof($arData); $i++){
			$info = explode('-', $arData[$i]);
			if (!in_array($info[1], $arNoPlaceForGuest)){
				if (!(isset($arTypePlaceNumbGuest[$info[0]]))) $arTypePlaceNumbGuest[$info[0]] = 0;
				$arTypePlaceNumbGuest[$info[0]] += $info[2];
			}	
		}
		
		$arTypePlaceNumbGuest[2] = intval($arTypePlaceNumbGuest[2]);
		
		if ($arTypePlaceNumbGuest[2] == 0) return false;
		
		if ($_SESSION['Shelter']['Data']['RoomFactor'][$_SESSION['Shelter']['Find']['hotel']][$id][2]['quant'] < $arTypePlaceNumbGuest[2]) return false;
		
		if (!RESERV_MAIN_NUMB_CHECK) return true;
		
		if (($_SESSION['Shelter']['Data']['RoomFactor'][$_SESSION['Shelter']['Find']['hotel']][$id][2]['quant'] > $arTypePlaceNumbGuest[2]) && (sizeof($arTypePlaceNumbGuest) > 1)) return false;
		
		return true;
	}
	
	function getNumbGuestForRates($arGuestNumb){
		$guestNoCheck = unserialize(TYPE_GUEST_NO_CHECK);
		
		$numbGuest = 0;
		foreach($arGuestNumb as $f1=>$arInfo){
			$arDataInfo = explode(';', $arInfo);			
			for($i = 0; $i < sizeof($arDataInfo); $i++){
				$arInfoNumb = explode('-', $arDataInfo[$i]);
				if (!(in_array($arInfoNumb[1], $guestNoCheck))) $numbGuest += $arInfoNumb[2];
			}	
			break;				
		}
		
		return $numbGuest;
	}
	
	function getRoomkindsNoRatesSResultRate($hotel, $request_in, $request_out, $arGuestNumb){
		$request_out++;
		
		if (USE_DEBTOR_QUOTA) {
			include_once(PATH_MODULES.'SResultLimit.php');		
			$rkr = new SResultLimit($hotel, $request_in, $request_out, $arGuestNumb);			
		}else{
			include_once(PATH_MODULES.'SResult.php');		
			$rkr = new SResult($hotel, $request_in, $request_out, $arGuestNumb);			
		}
		
		$numbGuest = getNumbGuestForRates($arGuestNumb);
		
		$arRooms = Array();
		$arRoomsInfo = $rkr->Request($rkr->GetXML());
		
		foreach ($arRoomsInfo as $roomkind) {
			$arFreeNumb = 0;
			$arFreeNumbRoom = $_SESSION['Shelter']['Data']['RoomFactor'][$_SESSION['Shelter']['Find']['hotel']][intval($roomkind['id'])];
			foreach($arFreeNumbRoom as $f1=>$arInfo){
				$arFreeNumb += $arInfo['quant'];
			}
			if ($arFreeNumb >= $numbGuest) $arRooms[] = $roomkind;			
		}
		
		foreach ($arRooms as $roomkind) {
			if (isset($roomkind->record[(sizeof($roomkind->record)- 1)])) unset($roomkind->record[(sizeof($roomkind->record)- 1)]);
		}
		
		return $arRooms;	
	}
	
	function setNumbGuestSResultRate(){		
		$hotelid = intval(protection($_SESSION['Shelter']['Find']['hotel']));
		$arRoomkindId = Array();
		foreach($_SESSION['Shelter']['Data']['RoomFactor'][$hotelid] as $roomkind_id=>$info){
			$arRoomkindId[] = $roomkind_id;
		}
		$arNumbDef = setNumbDef(true);
		$arRes = genNumbGuestByTypes($arRoomkindId, $arNumbDef);	
		
		return $arRes;
	}
	
	function genNumbGuestByTypes($arRoomkindId, $arNumbDef){		
		$min_place = 1000;
		$hotelid = intval(protection($_SESSION['Shelter']['Find']['hotel']));
		foreach($_SESSION['Shelter']['Data']['RoomFactor'][$hotelid] as $roomkind_id=>$arInfo){
			if ($arInfo[2]['quant'] < $min_place) $min_place = $arInfo[2]['quant'];
		}
				
		$arGuestNumb = Array();		
		$need_place = 0;
		foreach($arNumbDef as $id=>$numb){
			$need_place += $numb;
			if ($numb > 0) $arGuestNumb[$id] = $numb;
		}
		
		$arRoomkindIdExNumb = Array();
		foreach($_SESSION['Shelter']['Data']['RoomFactor'][$hotelid] as $roomkind_id=>$arInfo){
			$pl = 0;
			foreach($arInfo as $type_place=>$arQuant){
				$pl += $arQuant['quant'];
			}
			if ($pl >= $need_place) $arRoomkindIdExNumb[$roomkind_id] = $arInfo;
		}
		
		$fl = 1;
		$arRsQuant = Array();
		foreach($arRoomkindIdExNumb as $room_id=>$arTypePlace){
			$arGuestInfo = $arGuestNumb;
			
			foreach($arGuestInfo as $f2=>$guest_numb){

				while(($guest_numb > 0) && ($fl < 100)){
					
					foreach($arTypePlace as $f1=>$arNumb){
						if (($arNumb['quant'] > 0) && ($guest_numb > 0)){
							$write_numb = $guest_numb;
							if ($guest_numb > $arNumb['quant']) $write_numb = $arNumb['quant'];
							
							if (isset($arRsQuant[$room_id])) {
								if ($arRsQuant[$room_id] != '') $arRsQuant[$room_id] .= ';';
							}else{
								$arRsQuant[$room_id] = '';
							}
							
							$arRsQuant[$room_id] .= $f1.'-'.$f2.'-'.$write_numb;
							$guest_numb = $guest_numb - $write_numb;
							$arTypePlace[$f1]['quant'] -= $write_numb;
						}
					}
					$fl++;
				}
			}
		}
		
		$arRsQuant = array_unique($arRsQuant);		
		
		return $arRsQuant;
	}
	
	function getDescriptionFactors($room, $text){
		global $arMessages;
		
		$rateInfo = explode('#', $text);
		$arData = explode(';', $rateInfo[1]);
		
		for($i = 0; $i < sizeof($arData) - 1; $i++)
			for($j = $i + 1; $j < sizeof($arData); $j++)
				if ($arData[$i] > $arData[$j]){
					$t = $arData[$i];
					$arData[$i] = $arData[$j];
					$arData[$j] = $t;
				}
		
		$numbGuest = 0;
		
		$arResult = Array();
		for($i = 0; $i < sizeof($arData); $i++){
			$param = explode('-', $arData[$i]);
			if (!TYPE_GUEST_IMG){
				$arResult['desc'][$_SESSION['Shelter']['Data']['RoomFactorList'][$_SESSION['Shelter']['Find']['hotel']][$param[0]]][$_SESSION['Shelter']['Data']['TypeGuest'][$_SESSION['Shelter']['Find']['hotel']][$param[1]]] = $param[2];
			}else{
				$arResult['desc'][$param[0]][$param[1]] = $param[2];
			}		
			$numbGuest += $param[2];
		}
		$arResult['roomid'] = $room;
		$arResult['text'] = $text;
		
		if ($numbGuest > 1){
			$f = 'Factors';
			if (TYPE_GUEST_IMG) $f = 'FactorsImg';
			include(PATH_TEMPLATE_INC.$f.'.php');	
		}
	}
	
	function getNumbForListGuest($res_guest){
		$noEditNumbForGuest = false;
			
		$arResGuestTextBl = explode(';', $res_guest);
		for($j = 0; $j < sizeof($arResGuestTextBl); $j++){
			
			$arResGuestText = explode('#', $arResGuestTextBl[$j]);
			
			if (!$noEditNumbForGuest){
				$arCheckForEditNumb = explode('_', $arResGuestText[0]);					
				if (sizeof($arCheckForEditNumb) == 3){
					$noEditNumbForGuest = true;
					break;
				}
			} // если гости объеденены
			
			$arResGuestData = Array();
			foreach($arResGuestText as $key=>$val)
				$arResGuestData[$j][$val] = 0;
			foreach($arResGuestText as $key=>$val)
				$arResGuestData[$j][$val]++;
			
			$s = '';
			foreach($arResGuestData[$j] as $key=>$val){
				if ($s != '') $s .= '#';
				$s .= $key.'_'.$val;
			}	
			$arResGuestTextBl[$j] = $s;
		}
		if (!$noEditNumbForGuest)
			$res_guest = implode(';', $arResGuestTextBl);
			
		return $res_guest;	
	}
	
	function generateDiscountCode($arDiscounts){
		$discounts = '';
		
		if (sizeof($arDiscounts) > 0){
			$dis = implode('-',$arDiscounts);
			$key = '';
			for($i = 0; $i < 5; $i++){
				$key .= rand(1, 10);
			}	
			
			$discounts = $dis.'-'.$key.'-'.md5($key.$dis);
		}
		
		return $discounts;
	}
	
	function checkDiscountCode($text){
		$dis = explode('-',$text);
		
		$len = sizeof($dis);
		
		$check = false;
		$arDiscount = Array();
		
		if ($len > 1){
			for($i = 0; $i < ($len - 2); $i++){
				if (intval($dis[$i]) > 0){
					$arDiscount[] = intval($dis[$i]);
				}	
			}
		}
		
		if (sizeof($arDiscount) > 0){
			$ardis = implode('-',$arDiscount);
			$key = $dis[($len - 2)];
			$word = $dis[($len - 1)];			
			
			if (md5($key.$ardis) == $word){
				$check = true;
			}
		}
		if (!$check) $arDiscount = Array();
				
		return $arDiscount;
	}
	
	function getDiscount($number, $name){
		global $arMessages;
		if (!USE_PDS) echo $arMessages["general"]["error"][MESSAGE_LANG];
		
		include_once(PATH_MODULES.'GetPDSCardInfo.php');
		$rkr = new GetPDSCardInfo($number, $name);		
		$obj = $rkr->Request($rkr->GetXML());
		
		if (isset($obj->attributes()->error)){
			echo $arMessages["discount"]["error"][MESSAGE_LANG];
			exit;
		}
		
		$arDiscounts = Array();
		if (isset($obj->discounts)) {
			foreach($obj->discounts->discount as $discount){
				$arDiscounts[] = intval($discount->attributes()->id);
			}
			
			echo '1;'.generateDiscountCode($arDiscounts);
			exit;
		}

		echo $arMessages["discount"]["error_connect"][MESSAGE_LANG];
		exit;
	}
	
	function getAboutReservInfo($obj){
		$arData = Array();
		$arData['hotel'] = intval($obj->attributes()->hotel);
		$arData['contract'] = intval($obj->attributes()->contract);
		$arData['payment'] = strval($obj->attributes()->paymentname);
		
		$arData['request_in'] = Array();
		$arData['request_in_time'] = Array();
		$arData['request_out'] = Array();
		$arData['request_out_time'] = Array();
		
		$arData['typenumber'] = Array();
		$arData['tarif'] = Array();
		$arData['roomkind'] = Array();
		$arData['colnumbers'] = Array();
		
		$arData['cost'] = Array();
		$arData['discount'] = Array();
		$arData['commission'] = Array();
		
		$arData['guest'] = Array();
		
		$col = 0;
		$arTypeGuest = $_SESSION['Shelter']['Data']['TypeGuest'][$arData['hotel']];			
		
		foreach($obj->inquiry as $inquiry) {
			
			$res_request_in = floatval($inquiry->attributes()->arrival);
			$res_request_out = floatval($inquiry->attributes()->departure);			
			$arTime = explode('.', $res_request_in);
			$request_in_time = '0.'.$arTime[1];
			$arTime = explode('.', $res_request_out);				
			$request_out_time = '0.'.$arTime[1];
			$arData['request_in'][$col] = toIdate($res_request_in);
			$arData['request_out'][$col] = toIdate($res_request_out);		
			$arData['request_in_time'][$col] = getDM(toItime($request_in_time));		
			$arData['request_out_time'][$col] = getDM(toItime($request_out_time));
			
			$arData['roomkind'][$col] = intval($inquiry->attributes()->roomkind);			
			$arData['colnumbers'][$col] = intval($inquiry->attributes()->quant);			
			$arData['typenumber'][$col] = strval($inquiry->claim->attributes()->roomkindname);
			$arData['tarif'][$col] = strval($inquiry->claim->attributes()->ratename);
			$arData['cost'][$col] = floatval($inquiry->attributes()->summa);
			
			$arData['discount'][$col] = floatval($inquiry->claim->attributes()->sumdiscount);
			$arData['commission'][$col] = floatval($inquiry->claim->attributes()->sumcommission);
			
			$arGuest = Array();
			$arGuestInq = $inquiry->claim->guest;
			for($j = 0; $j < sizeof($arGuestInq); $j++){
				
				$fio = strval($arGuestInq[$j]->attributes()->name);
				if (strval($arGuestInq[$j]->attributes()->name1) != '') {
					$fio .= ' '.strval($arGuestInq[$j]->attributes()->name1);
					if (strval($arGuestInq[$j]->attributes()->name2) != '') {
						$fio .= ' '.strval($arGuestInq[$j]->attributes()->name2);
					}
				}	
				
				$arGuest[intval($arGuestInq[$j]->attributes()->f1)][intval($arGuestInq[$j]->attributes()->f2)][] = Array(
					'fio' => $fio,
					'birth' => toIdate(intval($arGuestInq[$j]->attributes()->birthdate))
				);	
			}
			$arData['guest'][$col] = $arGuest;
			
			$arOperation = Array();
			if (isset($inquiry->claim->operation)){
				for($j = 0; $j < sizeof($inquiry->claim->operation); $j++){
					
					$operation = $inquiry->claim->operation[$j];
					
					$arOperation[] = Array(
						'name' => strval($operation['operationname']),
						'cost' => floatval($operation['cost']),
						'quant' => intval($operation['quant']),
					);				
				}
				
			}	
			$arData['fullcost'][$col] = $arData['cost'][$col];
			$arData['cost'][$col] = $arData['cost'][$col] + $arData['discount'][$col];
			$arData['operation'][$col] = $arOperation;
			
			$arPackageService = Array();
			if (isset($inquiry->claim->rate_adds)){
				$arPackageServiceList = $inquiry->claim->rate_adds;			
				
				for($j = 0; $j < sizeof($arPackageServiceList->rate_add); $j++){
					$service = $arPackageServiceList->rate_add[$j];
					$arPackageService[] = Array(
						'name' => strval($service->attributes()->name),						
					);
				}
			}
			$arData['packageService'][$col] = $arPackageService;			
			
			$col++;
		}
		
		$arData['col'] = $col;
		
		return $arData;
	}
	
	function sortAr($mas, $field, $righttoleft = false) {
		for($i = 0; $i < (sizeof($mas) - 1); $i++) 
			for($j = $i + 1; $j < sizeof($mas); $j++)	{
				if ($righttoleft) {
					if ($mas[$i][$field] < $mas[$j][$field]) {
						$temp = $mas[$i];
						$mas[$i] = $mas[$j];
						$mas[$j] = $temp;
					}	
				}else{
					if ($mas[$i][$field] > $mas[$j][$field]) {
						$temp = $mas[$i];
						$mas[$i] = $mas[$j];
						$mas[$j] = $temp;
					}	
				}
		}		
		return $mas;
	}
	
	function getPackageServices(){
		$arPackageService = Array();
		
		if (!USE_PACKAGE_SERVICE) return $arPackageService;
		
		$arFilterCode = unserialize(USE_PACKAGE_SERVICE_CODE);
		
		include_once(PATH_MODULES.'PackageService.php');
		$rkr = new PackageService();		
		$obj = $rkr->Request($rkr->GetXML());
		
		foreach($obj->extra as $pk){
			$code = strval($pk->attributes()->code);
			
			$error = false;
			if (USE_PACKAGE_SERVICE_FILTER){
				$error = true;
				if ((USE_PACKAGE_SERVICE_FILTER == 1) && (in_array($code, $arFilterCode))) $error = false;
				if ((USE_PACKAGE_SERVICE_FILTER == 2) && (!in_array($code, $arFilterCode))) $error = false;
			}
			
			if (!$error){
				$arPackageService[] = Array(
					'id' => intval($pk->attributes()->id),
					'code' => $code,
					'name' => html_entity_decode(strval($pk->attributes()->name)),
				);
			}
		}	
		
		return sortAr($arPackageService, 'name');
	}
	
?>