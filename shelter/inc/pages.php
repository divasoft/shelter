<?php
	define('PAGE_ABOUTRESERV', 'AboutReserv');
	define('PAGE_ABOUTUSER', 'AboutUser');
	define('PAGE_CREATEUSER', 'CreateUser');
	define('PAGE_FIND', 'Find');
	define('PAGE_FORGETPASSWORD', 'ForgetPassword');
	define('PAGE_LOGINUSER', 'LoginUser');
	define('PAGE_LOGINFIRM', 'LoginFirm');
	define('PAGE_RESERV', 'Reservation');
	define('PAGE_RESERV_RULES', 'Rules');
	define('PAGE_RESERVPAY', 'ReservationPay');	
	define('PAGE_RESERVDELETE', 'ReservationDelete');	
	define('PAGE_RESERVNDELETE', 'ReservationNR');	
	//define('PAGE_RESULT', $ResultPage);
	
	define('PAGE_RESULT_FIRST', 'SResult');
	define('PAGE_RESULT_SECOND', 'Result');
	
	define('PAGE_RESULT_MODULE', 'Result');	
	define('PAGE_USERCABINET', 'UserCabinet');	
	define('PAGE_SENDRESERV', 'SendReservation');	
	define('PAGE_EXIT', 'Exit');		
	define('PAGE_RESERVINFO', 'ReservationInfo');
	define('PAGE_CHECKCONNECT', 'CheckConnect');
	
	define('PAGE_PAYMENTSUCCESS', 'PaymentSuccess');
	define('PAGE_PAYMENTFAIL', 'PaymentFail');
	define('PAGE_PAYMENTEXECUTION', 'PaymentExecution');
?>
