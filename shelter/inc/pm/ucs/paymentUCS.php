<?php
class paymentUCS{
    
	public function sendData($arData){	
		$soapClient = new orderv2\orderv2(null, array('location' => UCSPAY_URL_PAY,
											  'uri'      => UCSPAY_URL_MODULE,
											  'login'    => UCSPAY_LOGIN,
											  'password' => UCSPAY_PASS,
											  'trace'    => 1,
											  'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
											  'connection_timeout' => 12));   
		$itemPNR = $arData['id'];
		$order = new orderv2\OrderID(); 
		$order->shop_id = UCSPAY_SHOPID;
		$order->number = $arData['id'];       
		
		$cost = new orderv2\Amount();
		$cost->amount = $arData['sum'];
		$cost->currency = 'RUB';

		$description = new orderv2\OrderInfo();
		$description->paytype = 'card';
		$description->shopref = $arData['comment'];

		$itemCost = new orderv2\Amount();
		$itemCost->amount = $arData['sum'];
		$itemCost->currency = 'RUB';

		$item = new orderv2\OrderItem();
		$item->typename = 'hotel';
		$item->number = $itemPNR;
		$item->amount = $itemCost;
		$item->host = '';

		$items = new SoapVar(array(
									new SoapVar($item, SOAP_ENC_OBJECT, null, null, 'OrderItem'),
							), SOAP_ENC_OBJECT);
		$description->items = $items;

		$language = new orderv2\PostEntry();
		$language->name = 'Language';
		$language->value = 'ru';

		$returnUrlOk = new orderv2\PostEntry();
		$returnUrlOk->name = 'ReturnURLOk';
		$returnUrlOk->value = $arData['status_ok'];

		$returnUrlFault = new orderv2\PostEntry();
		$returnUrlFault->name = 'ReturnURLFault';
		$returnUrlFault->value = $arData['status_fail'];

		$request = new orderv2\register_online();     
		$request->order = $order;
		$request->cost = $cost;
		$request->description = $description;

		$postdata = new SoapVar(array(
										new SoapVar($language, SOAP_ENC_OBJECT, null, null, 'PostEntry'),
										new SoapVar($returnUrlOk, SOAP_ENC_OBJECT, null, null, 'PostEntry'),
										new SoapVar($returnUrlFault, SOAP_ENC_OBJECT, null, null, 'PostEntry'),
								), SOAP_ENC_OBJECT);     

		$request->postdata = $postdata;

		try {
			$info = $soapClient->register_online($request);
			header('Location: ' . $info->redirect_url . '?session=' . $info->session);
			die();
		} catch (SoapFault $fault) {
			echo 'SOAP error: ', $fault->faultcode, '-', $fault->faultstring;
		} catch (Exception $fault) {
			echo 'PHP Exception: ', $fault->__toString();
		}
	}
	
	public function getStatus($number){
		$arData = Array();
		$errorText = '';
		
		$soapClient = new orderv2\orderv2(null, array('location' => UCSPAY_URL_PAY,
                                          'uri'      => UCSPAY_URL_MODULE,
                                          'login'    => UCSPAY_LOGIN,
                                          'password' => UCSPAY_PASS,
                                          'trace'    => 1,
                                          'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
                                          'connection_timeout' => 12));   

		$order = new orderv2\OrderID(); 
		$order->shop_id = UCSPAY_SHOPID;
		$order->number = $number;		
		$status = new orderv2\get_status();
		$status->order = $order;
		
		try {
			$info = $soapClient->get_status($status);			
			//$file_order = fopen('pay.txt',"w+"); if ($file_order) fputs ($file_order, var_export($info,true));
			
			$text = '
';
			$now_date = date('Ymd', time());
			$file = fopen(FILE_ORDERS.'ucs/'.$now_date.'.php',"a+");
			if ($file) {
				fputs ($file, date('d.m.Y H:i:s', time()).$text);
				fputs ($file, var_export($info,true));
				fputs ($file, $text.$text);
			}	
			fclose($file);
			
			$arComment = explode(';', $info->shopref);
			$arInfo = Array(
				'id' => $arComment[0],
				'guest_id' => $arComment[1],
				'code' => $arComment[2],
				'email' => $arComment[3]
			);
			
			$arData = Array(
				'status' => $info->status,
				'shop_id' => $info->order->shop_id,
				'number' => $info->order->number,
				'amount' => $info->payments->Payment->amount->amount,
				'info' => $arInfo,
			);
		} catch (SoapFault $fault) {
			$errorText = 'SOAP error: '.$fault->faultcode.'-'.$fault->faultstring;
		} catch (Exception $fault) {
			$errorText = 'PHP Exception: '.$fault->__toString();
		}
		
		return Array('data' => $arData, 'error' => $errorText);
	}
}	
?>