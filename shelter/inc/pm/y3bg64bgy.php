<?	
	define('PAY_FILE_RUN', true);
	include_once('../../config.php');	
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	if (sizeof($_POST) == 0) die();
	
	function checkData($arData){
		$hash = md5($arData['action'].';'.$arData['orderSumAmount'].';'.$arData['orderSumCurrencyPaycash'].';'.$arData['orderSumBankPaycash'].';'.YANDEX_SHOPID.';'.$arData['invoiceId'].';'.$arData['customerNumber'].';'.YANDEX_SHOPPASSWORD);		
		if (strtolower($hash) == strtolower($arData['md5'])){
			return true;
		}		
		return false;
	}
	
	$arCode = explode(';', $_POST['comment']);
	$DATA_RESERV_ID = intval($arCode[0]);	
	$DATA_RESERV_CODE = intval($arCode[1]);
	$DATA_RESERV_EMAIL = protection($arCode[2]);
	$DATA_RESERV_GUEST = intval($_POST['customerNumber']);
	$DATA_RESERV_SUM = floatval($_POST['orderSumAmount']);
	$DATA_RESERV_NUMBER = strval($_POST['invoiceId']);
	
	$arDataLog = Array(
		'id' => $DATA_RESERV_NUMBER,
		'sum' => $DATA_RESERV_SUM,
		'system' => 'YANDEX',
		'guest' => $DATA_RESERV_GUEST,
		'text' => '',
	);	
	
	$XML_TYPE = 'errorResponse';
	$XML_CODE = 1;
	$SENDMAIL = false;
	if (checkData($_POST) && PAYMENT){
		if (isset($_POST['action'])){
			if ($_POST['action'] == 'checkOrder'){				
				$XML_TYPE = 'checkOrderResponse';
				$XML_CODE = 0;
				$arDataLog['text'] = 'CHECK OK';
			}
			elseif($_POST['action'] == 'paymentAviso'){
				$XML_TYPE = 'paymentAvisoResponse';
				if (isSecondOrder($DATA_RESERV_NUMBER)){
					$XML_CODE = 0;
					$arDataLog['text'] = 'ERROR ALREADY PAY';
				}else{
					if (SetPay($DATA_RESERV_ID, $DATA_RESERV_SUM)) {
						putIdOrder($DATA_RESERV_NUMBER); 
						
						$XML_CODE = 0;
						$SENDMAIL = true;
						$arDataLog['text'] = 'PAY';
					}else{
						$arDataLog['text'] = 'ERROR SAVE';	
						$XML_CODE = 100;
					}	
				}
			}else
				$arDataLog['text'] = 'ERROR ACTION ('.$_POST['invoiceId'].' / '.$_POST['action'].')';
		}else
			$arDataLog['text'] = 'ERROR ACTION EMPTY ('.$_POST['invoiceId'].' / '.$_POST['action'].')';		
	}else
		$arDataLog['text'] = 'ERROR RUN ('.$_POST['invoiceId'].' / '.$_POST['action'].' / '.PAYMENT.')';
	
	savePayLog($arDataLog);
	
	if ($SENDMAIL){
		$arData = Array(
			'EMAIL' => $DATA_RESERV_EMAIL,				
			'ORDER' => $DATA_RESERV_CODE,				
			'SUMM' => $DATA_RESERV_SUM,
			'CURRENT' => $arMessages["current"][CURRENCYSHOW][MESSAGE_LANG],
		);								
		sendMail($DATA_RESERV_EMAIL, 'Payment', $arData);
		sendMailForMeneger('ForMenegerPayment', $arData, $DATA_RESERV_CODE);			
	}
	
	print '<?xml version="1.0" encoding="UTF-8"?>';
	print '<'.$XML_TYPE.' performedDatetime="'.$_POST['requestDatetime'].'" code="'.$XML_CODE.'" invoiceId="'.$_POST['invoiceId'].'" shopId="'.YANDEX_SHOPID.'" />';
	
?>