<?php
	ob_start();	
	
	define('PAY_FILE_RUN', true);
	include_once('../../config.php');	
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	if (sizeof($_POST) == 0) die();
	
	include_once(PATH_PM.'rsbank/rsbank.php');

	$bankRS = new RSBANK();
	$arStatus = Array();
	
	$pay = false;
	$sendmail = false;
	$mess = $arMessages["PaymentFail"]["title"][MESSAGE_LANG];
	
	$arDataLog = Array(
		'id' => 0,
		'sum' => 0,
		'system' => PAY_SYSTEM_CODE,
		'guest' => 0,
		'text' => '',
	);	
	
	if (isset($_POST['trans_id'])){
		
		$transaction = $_POST['trans_id'];		
		$arDataLog['id'] = $transaction;
		
		if (isSecondOrder($transaction)) $pay = true;
		
		if (($transaction != '') && (!$pay) && PAYMENT) {
		
			$arData = Array(
				'trans_id' => $transaction, 
				'client_ip_addr' => $_SERVER['REMOTE_ADDR']		
			);
			$arStatus = $bankRS->getStatus($arData);
			
			$arDataComment = explode(';', $arStatus['MRCH_TRANSACTION_ID']);				
			$arDataReserv = Array(
				'id' => $arDataComment[0],
				'guest' => $arDataComment[1],
				'code' => $arDataComment[2],
				'summ' => floatval($arDataComment[3]),
				'email' => $arDataComment[4],
			);
			
			if (($arStatus['RESULT'] == 'OK') && ($arStatus['RESULT_CODE'] == '000')){
				
				$arDataLog['sum'] = $arDataReserv['summ'];
				$arDataLog['guest'] = $arDataReserv['guest'];
				
				if (SetPay($arDataReserv['id'], $arDataReserv['summ'])) {
					putIdOrder($transaction); 
					
					$pay = true;
					$sendmail = true;
					
					$arDataLog['text'] = 'PAY';
					
					$mess = $arMessages["PaymentSuccess"]["title"][MESSAGE_LANG];
				}else
					$arDataLog['text'] = 'ERROR SAVE';
			}else
				$arDataLog['text'] = 'ERROR CHECK (RESULT '.$arStatus['RESULT'].'), (RESULT_PS '.$arStatus['RESULT_PS'].'), (RESULT_CODE '.$arStatus['RESULT_CODE'].')';
		}else
			$arDataLog['text'] = 'ERROR RUN';
		
		//echo '<pre>'; print_r($_POST); echo '</pre>';
		//echo '<pre>'; print_r($arStatus); echo '</pre>';
	}else
		$arDataLog['text'] = 'ERROR POST';
	
	savePayLog($arDataLog);
	
	if (!$pay) {
		if (isset($arMessages["RusBankErrorCode"][$arStatus['RESULT_CODE']][MESSAGE_LANG])){
			$mess .= ' '.$arMessages["RusBankErrorCode"][$arStatus['RESULT_CODE']][MESSAGE_LANG];
		}else{
			$mess .= ' '.$arMessages["RusBankErrorCode"]["other"][MESSAGE_LANG];
		}
	}	
	$_SESSION['Shelter']['Info']['Message'] = $mess;
	
	if ($sendmail){
		$arData = Array(
			'EMAIL' => $arDataReserv['email'],				
			'ORDER' => $arDataReserv['code'],				
			'SUMM' => $arDataReserv['summ'],
			'CURRENT' => $arMessages["current"][CURRENCYSHOW][MESSAGE_LANG],
		);								
		sendMail($arDataReserv['email'], 'Payment', $arData);
		sendMailForMeneger('ForMenegerPayment', $arData, $arDataReserv['code']);	
	}
	
	if (USER_AUTH_USE){
		$arPath = Array(MODULE_SHEDULE=>"UserCabinet");
	}else{
		$arPath = Array(
			MODULE_SHEDULE=>PAGE_RESERVNDELETE, 
			'id'=>$arDataReserv['id'], 
			'email'=>$arDataReserv['email'], 
			'code'=>md5($arDataReserv['id'].KEY_SECURITY)
		);
	}
	
	header('location: '.change_address($arPath, true));
	exit;
	
	ob_end_flush();
?>