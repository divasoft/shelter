<?php	
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
			
	function sendToPaymentSystem($arParam) {
		global $GUEST, $arMessages;
		
		$id = $arParam['id'];
		$order_id = $id.'-'.date('His', time());
		$code = $arParam['code'];
		$summa = $arParam['summa'];
		$obj = $arParam['obj'];
		
		$arDataLog = Array(
			'id' => $order_id,
			's_id' => $id,
			's_code' => $code,
			'sum' => $summa,
			'system' => PAY_SYSTEM_CODE,
			'guest' => $GUEST['id'],
			'text' => 'SEND',
		);
		if (PAY_SYSTEM_CODE != 'RUSSTANDART'){
			savePayLog($arDataLog);
		}	
		
		if (PAY_SYSTEM_CODE == 'ASSIST') {				
			header('location: '.ASSIST_PURCHASE.'?Merchant_ID='.ASSIST_MERCHANT_ID.'&OrderNumber='.$order_id.'&OrderAmount='.$summa.'&Language='.CURRENCY.'&ordercomment='.$id.';'.$GUEST['id'].';'.$code.';'.$GUEST['email']); 
			// .'&URL_RETURN='.urlencode(URL_RETURN)
		}
		elseif (PAY_SYSTEM_CODE == 'ROBOKASSA') {
			
			$mrh_login = ROBOKASSA_MrchLogin;
			$mrh_pass1 = ROBOKASSA_mrh_pass1;

			$SHPId = $GUEST['id'];
			$SHPCode = $code;
			$SHPLogin = $GUEST['email'];			
			$SHPOrder = $order_id;			
			
			$culture = CURRENCY;
			
			$SignatureValue = md5("$mrh_login:$summa:$id:$mrh_pass1:SHPCode=$SHPCode:SHPId=$SHPId:SHPLogin=$SHPLogin:SHPOrder=$SHPOrder");
			
                        // DIVASOFT - Нужно вернуться на основной сайт. Делаем хак.
			$url = urlencode(''.ROBOKASSA_PURCHASE.'MrchLogin='.$mrh_login.'&OutSum='.$summa.'&InvId='.$id.'&SignatureValue='.$SignatureValue.'&Culture='.$culture.'&SHPCode='.$SHPCode.'&SHPLogin='.$SHPLogin.'&SHPId='.$SHPId.'&SHPOrder='.$SHPOrder);	
			header('location: divasoft/redirect.php?url='.$url);	
            
			//header('location: '.ROBOKASSA_PURCHASE.'MrchLogin='.$mrh_login.'&OutSum='.$summa.'&InvId='.$id.'&SignatureValue='.$SignatureValue.'&Culture='.$culture.'&SHPCode='.$SHPCode.'&SHPLogin='.$SHPLogin.'&SHPId='.$SHPId.'&SHPOrder='.$SHPOrder);	
		}
		elseif (PAY_SYSTEM_CODE == 'YANDEX') {
			
			$arData = Array(
				'shopId' => YANDEX_SHOPID,
				'scId' => YANDEX_SCID,
				'ShopPassword' => YANDEX_SHOPPASSWORD,
				'customerNumber' => $GUEST['id'],
				'shopSuccessURL' => urlencode(change_address(Array(MODULE_SHEDULE=>PAGE_PAYMENTSUCCESS), true)),
				'shopFailURL' => urlencode(change_address(Array(MODULE_SHEDULE=>PAGE_PAYMENTFAIL), true)),
				'paymentType' => YANDEX_PAYMENTTYPE,
				'Sum' => $summa,
				'comment' => urlencode($id.';'.$code.';'.$GUEST['email']),
			);
			
			$link = '';
			foreach($arData as $key=>$val){
				if (strlen($link) > 0) $link .= '&';
				$link .= $key.'='.$val;
			}
			
			header('location: '.YANDEX_REDIRECT.'?'.$link);				
		}
		elseif (PAY_SYSTEM_CODE == 'UNITELLER') {
			
			$comment = $GUEST['id'].'*'.$id.'*'.$code.'*'.$GUEST['email'];
			$signature = strtoupper(
				md5(
					md5(UNITELLER_SHOP_ID).'&'.
					md5($order_id).'&'.
					md5($summa).'&'.
					md5('').'&'.
					md5('').'&'.
					md5('').'&'.
					md5('').'&'.
					md5('').'&'.
					md5('').'&'.
					md5('').'&'.
					md5(UNITELLER_PASS)
				));			
			$url = change_address(Array(MODULE_SHEDULE=>PAGE_PAYMENTEXECUTION), true);
			
			header('location: '.UNITELLER_URL_SEND.'?Shop_IDP='.UNITELLER_SHOP_ID.'&Order_IDP='.$order_id.'&Subtotal_P='.$summa.'&Currency='.CURRENCY.'&Signature='.$signature.'&Comment='.$comment.'&URL_RETURN='.$url);			
		}
		elseif (PAY_SYSTEM_CODE == 'RUSSTANDART') {
			include_once(PATH_PM.'rsbank/rsbank.php');
			
			$bankRS = new RSBANK();
			$arData = Array(
				'client_ip_addr' => $_SERVER['REMOTE_ADDR'], 
				'amount' => ($summa*100), 
				'mrch_transaction_id' => $id.';'.$GUEST['id'].';'.$code.';'.$summa.';'.$GUEST['email'],
			);
			$transaction = $bankRS->getTransaction($arData);
			
			$arDataLog['id'] = urldecode($transaction);
			savePayLog($arDataLog);
			
			header('location:'.BANK_RS_URL_REDIRECT.$transaction);	
			
		}
		elseif (PAY_SYSTEM_CODE == 'UCSPAY') {
			include_once(PATH_PM.'ucs/orderv2.php');
			include_once(PATH_PM.'ucs/paymentUCS.php');
			
			$paymentUCS = new paymentUCS();
			
			$arData = Array(
				'id' => $order_id,
				'sum' => $summa,
				'comment' => $id.';'.$GUEST['id'].';'.$code.';'.$GUEST['email'],
				'status_ok' => change_address(Array(MODULE_SHEDULE=>PAGE_PAYMENTSUCCESS), true),
				'status_fail' => change_address(Array(MODULE_SHEDULE=>PAGE_PAYMENTFAIL), true),
			);
			$paymentUCS->sendData($arData);
			
		}
		elseif (PAY_SYSTEM_CODE == 'RESERVMASTER') {
			
			$id_paysystem = 1; // paysystem RM
			$action = 'ADD';
			if (strval($obj->attributes()->extno) != strval($obj->attributes()->code)) $action = 'MODIFY';			
			$resid = $id;
			$operation = 0;
			$name = strval($obj->attributes()->contact);
			$phone = strval($obj->attributes()->phone);
			$info = strval($obj->attributes()->info);
			$begindate = floatval($obj->attributes()->arrival);
			$enddate = floatval($obj->attributes()->departure);
			$currency = "RUB";			
			
			// ---------------------------- //
			include_once(PATH_MODULES.'TypePaySystem.php');
			$rkr = new TypePaySystem();
			$obj_type = $rkr->Request($rkr->GetXML());			
			foreach($obj_type->payment as $key=>$type) {
				if (intval($type->attributes()->paysystem) == 1) $operation = intval($type->attributes()->id);
			}
			// ---------------------------- //
			
			if ($operation > 0)	{
				include_once(PATH_MODULES.'PaySystem.php');
				$rkr = new PaySystem($id_paysystem, $action, $resid, $operation, $name, $phone, $summa, $info, $begindate, $enddate, $currency);	
				$obj = $rkr->Request($rkr->GetXML());
				
				$_SESSION['Shelter']['Info']['Message'] = $arMessages["ReservPay"]["reservmaster_send"][MESSAGE_LANG];
			}else{
				$_SESSION['Shelter']['Info']['Message'] = $arMessages["ReservPay"]["reservmaster_nosend"][MESSAGE_LANG];
			}
			header('location: '.change_address());
		}
		exit;		
	}
	
	function deleteFromRM($obj) {
		$id_paysystem = 1; // paysystem RM	
		$action = 'DELETE';	
		$resid = intval($obj->attributes()->id);
		$operation = 0;
		$name = strval($obj->attributes()->contact);
		$phone = strval($obj->attributes()->phone);
		$summa = floatval($obj->attributes()->summa);
		$info = strval($obj->attributes()->info);
		$begindate = floatval($obj->attributes()->arrival);
		$enddate = floatval($obj->attributes()->departure);
		$currency = "RUB";			
		
		// ---------------------------- //
		include_once(PATH_MODULES.'TypePaySystem.php');
		$rkr = new TypePaySystem();
		$obj_type = $rkr->Request($rkr->GetXML());			
		foreach($obj_type->payment as $key=>$type) {
			if (intval($type->attributes()->paysystem) == 1) $operation = intval($type->attributes()->id);
		}
		// ---------------------------- //
		
		if ($operation > 0)	{
			include_once(PATH_MODULES.'PaySystem.php');
			$rkr = new PaySystem($id_paysystem, $action, $resid, $operation, $name, $phone, $summa, $info, $begindate, $enddate, $currency);	
			$rkr->Request($rkr->GetXML());
		}
	}
	
?>
