<?	
	define('PAY_FILE_RUN', true);
	include_once('../../config.php');	
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	if (sizeof($_POST) == 0) die();
	
	function checkSignature($Order_ID, $Status, $Signature) {
		return ($Signature == strtoupper(md5($Order_ID.$Status.UNITELLER_PASS)));
	}

	function getData($Order_ID) {
		global $arDataLog;
		
		$sPostFields = "Shop_ID=".UNITELLER_SHOP_ID."&Login=".UNITELLER_LOGIN."&Password=".UNITELLER_PASS."&Format=1&ShopOrderNumber=".
		$Order_ID."&Success=1&S_FIELDS=Status;OrderNumber;Total;PaymentType;Comment;Currency";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, UNITELLER_URL_CHECK);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $sPostFields);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		$curl_response = curl_exec($ch);
		$curl_error = curl_error($ch);
		//echo 'Res '.$curl_response;
		
		$arResult = Array();
		
		if ($curl_error) {
			$arDataLog['text'] = 'ERROR CURL ERROR';
			savePayLog($arDataLog);			
		} else {
			$arr = explode(";", $curl_response);
			
			$arComment = explode('*', $arr[4]);
			
			$g_id = $arComment[0];
			$c_order_id = $arComment[1];
			$c_order_code = $arComment[2];
			$c_mail = $arComment[3];
			
			$arRes = Array(
				'result' => $arr[0], // Authorized or Paid
				'u_order_id' => $arr[1], // OrderNumber
				'summ' => $arr[2], // Summ
				'paytype' => $arr[3], // PaymentType
				
				'guest_id' => $g_id,
				'order_id' => $c_order_id,
				'order_code' => $c_order_code,
				'mail' => $c_mail,
			);
			
			$arDataLog['sum'] = $arRes['summ'];
			$arDataLog['guest'] = $arRes['guest_id'];
			
			if ((
					(($arRes['result'] == 'Authorized') && ($arRes['paytype'] == 1)) || 
					(($arRes['result'] == 'Paid') && ($arRes['paytype'] == 3))
				) && ($arRes['u_order_id'] == $Order_ID)){
				
				$arResult = Array(
					'guest_id' => $arRes['guest_id'],				
					'summ' => $arRes['summ'],
					'order_id' => $arRes['order_id'],
					'order_id_full' => $arRes['u_order_id'],
					'order_code' => $arRes['order_code'],
					'mail' => $arRes['mail'],
				);
				
			}		
		}
		return $arResult;
	}

	$arDataLog = Array(
		'id' => strval($_POST["Order_ID"]),
		'sum' => '-',
		'system' => 'UNITELLER',
		'guest' => '-',
		'text' => '',
	);
	
	$pay = false;
	if (count($_POST) && isset($_POST["Order_ID"]) && isset($_POST["Status"]) && isset($_POST["Signature"]) && PAYMENT) {
		$_POST["Order_ID"] = strval($_POST["Order_ID"]);
		
		if (checkSignature($_POST["Order_ID"], $_POST["Status"], $_POST["Signature"])){
			
			$arData = getData($_POST["Order_ID"]);
			if (sizeof($arData) > 0){			
				//echo '<pre>'; print_r($arData); echo '</pre>';
				
				if (!(isSecondOrder($arData['order_id_full']))){
				
					if (SetPay($arData['order_id'], floatval($arData['summ']))){						
						putIdOrder($arData['order_id_full']); 
						
						$arDataLog['text'] = 'PAY';
						
						$pay = true;
					}else
						$arDataLog['text'] = 'ERROR PayOrder';
				}else
					$arDataLog['text'] = 'ERROR Already';
			}else
				$arDataLog['text'] = 'ERROR getData';
		}else
			$arDataLog['text'] = 'ERROR checkSignature';
	}
	
	savePayLog($arDataLog);
	
	if ($pay){
		//echo 'ok';
		
		$arMail = Array(
			'EMAIL' => $arData['mail'],				
			'ORDER' => $arData['order_code'],				
			'SUMM' => floatval($arData['summ']),
			'CURRENT' => $arMessages["current"][CURRENCYSHOW][MESSAGE_LANG],
		);								
		sendMail($arData['mail'], 'Payment', $arMail);
		sendMailForMeneger('ForMenegerPayment', $arMail, $arData['order_code']);
	}else{
		header('location: '.change_address(Array(), true));
		exit;
	}
?>