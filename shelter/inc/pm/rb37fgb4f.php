<?	
	define('PAY_FILE_RUN', true);
	include_once('../../config.php');	
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	if (sizeof($_REQUEST) == 0) die();
	
	$mrh_pass2 = ROBOKASSA_mrh_pass2;

	$tm=getdate(time()+9*3600);
	$date="$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]";

	$out_summ = $_REQUEST["OutSum"];
	$inv_id = $_REQUEST["InvId"];
	$shp_item = $_REQUEST["Shp_item"];
	$crc = $_REQUEST["SignatureValue"];
	$SHPId = $_REQUEST["SHPId"];
	$SHPCode = $_REQUEST["SHPCode"];
	$SHPLogin = $_REQUEST["SHPLogin"];
	$SHPOrder = $_REQUEST["SHPOrder"];
	$crc = strtoupper($crc);

	$my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2:SHPCode=$SHPCode:SHPId=$SHPId:SHPLogin=$SHPLogin:SHPOrder=$SHPOrder"));
		
	$mess = "bad sign\n";
	
	$arDataLog = Array(
		'id' => $SHPOrder,
		'sum' => floatval($out_summ),
		'system' => 'ROBOKASSA',
		'guest' => $SHPId,
		'text' => '',
	);
	
	$pay = false;
	if (($my_crc==$crc) && (PAYMENT)) {
		
		if (!(isSecondOrder($SHPOrder))){
			
			if (SetPay($inv_id, floatval($out_summ))) {
				putIdOrder($SHPOrder); 
				
				$pay = true;
				
				$arDataLog['text'] = 'PAY';
				
				$mess = "OK$inv_id\n";
			}else
				$arDataLog['text'] = 'ERROR SAVE';
		}else
			$arDataLog['text'] = 'ERROR PAY ALREADY';
	}else
		$arDataLog['text'] = 'ERROR CHECK SUM';
	
	echo $mess;
	
	savePayLog($arDataLog);
	
	if ($pay){
		$arData = Array(
			'EMAIL' => $SHPLogin,				
			'ORDER' => $SHPCode,				
			'SUMM' => floatval($out_summ),
			'CURRENT' => $arMessages["current"][CURRENCYSHOW][MESSAGE_LANG],
		);								
		sendMail($SHPLogin, 'Payment', $arData);
		sendMailForMeneger('ForMenegerPayment', $arData, $SHPCode);	
	}

?>