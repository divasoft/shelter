<?
	define('PAY_FILE_RUN', true);
	include_once('../../config.php');
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	if (sizeof($_POST) == 0) die();
	
	$id_assist = protection(strval($_POST['ordernumber']));
	$orderamount = 0;
	
	$guestinfo = explode(';',$_POST['ordercomment']);
	$id = intval($guestinfo[0]);
	$GUEST = Array();
	$GUEST['id'] = protection($guestinfo[1]);
	$code = protection($guestinfo[2]);
	$GUEST['email'] = protection($guestinfo[3]);	
		
	//$file_order = fopen('pay.txt',"w+");
	//if ($file_order) fputs ($file_order, var_export($_POST,true));
	
	$assistpostdata = "Ordernumber=".$id_assist."&Merchant_ID=".ASSIST_MERCHANT_ID."&Login=".ASSIST_LOGIN."&Password=".ASSIST_PASSWORD."&Format=3";
	
	$pay = false;
	$sendmail = false;
	
	$arDataLog = Array(
		'id' => $id_assist,
		'sum' => floatval($_POST['amount']),
		'system' => 'ASSIST',
		'guest' => $GUEST['id'],
		'text' => '',
	);
	
	if (isSecondOrder($id_assist)) $pay = true;
	
	if (($id_assist > 0) && (!$pay) && PAYMENT) {			
		$ch = curl_init(ASSIST_ORDERSTATE);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $assistpostdata);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$payments_xml = curl_exec($ch);	
		$ok = true;
		if (curl_errno($ch)) $ok = false;
		curl_close($ch);		
		if ($ok) {	
			$obj = simplexml_load_string(trim($payments_xml));
			foreach($obj->order as $res) 
				if (strval($res->orderstate) == 'Approved') $orderamount = $res->orderamount;	
			if ($orderamount > 0) {		
				if (SetPay($id, floatval($orderamount))) {
					putIdOrder($id_assist); 
					
					$pay = true;
					$sendmail = true;
					
					$arDataLog['text'] = 'PAY';
				}else
					$arDataLog['text'] = 'ERROR SAVE';				
			}else
				$arDataLog['text'] = 'ERROR AMOUNT';
		}else
			$arDataLog['text'] = 'ERROR CONNECTING';
	}else
		$arDataLog['text'] = 'ERROR RUN';
		
	savePayLog($arDataLog);
	
	if ($pay) {
		//echo 'ok';
		if ($sendmail){
			$arData = Array(
				'EMAIL' => $GUEST['email'],				
				'ORDER' => $code,				
				'SUMM' => floatval($orderamount),
				'CURRENT' => $arMessages["current"][CURRENCYSHOW][MESSAGE_LANG],
			);								
			sendMail($GUEST['email'], 'Payment', $arData);
			sendMailForMeneger('ForMenegerPayment', $arData, $code);			
		}
	}else{
		header('location: '.change_address(Array(MODULE_SHEDULE=>"UserCabinet"), true));
		exit;
	}
?>