<?php
	define('PAY_FILE_RUN', true);
	include_once('../../../config.php');	
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
		
	include_once(PATH_PM.'rsbank/rsbank.php');

	$bankRS = new RSBANK();
	$arStatus = Array();
	
	$date = date('d.m.Y', time());
	
	$arData = Array(
		'text' => 'CLOSE '.$date.';'
	);
	
	$arStatus = $bankRS->setClose();
	if ((is_array($arStatus)) && (sizeof($arStatus) > 0)){
		$arData['text'] .= ' RESULT '.$arStatus['RESULT'].'; RESULT_CODE '.$arStatus['RESULT_CODE'].';';
		
		$arMail = Array(
			'DATE' => $date,
			'RESULT' => $arStatus['RESULT'],
			'RESULT_CODE' => $arStatus['RESULT_CODE'],
		);
		
		sendMailForMeneger('ForMenegerPaymentRSClose', $arMail, $date);
	}
	
	savePayLog($arData);
	
	//echo '<pre>'; print_r($arStatus); echo '</pre>';
?>