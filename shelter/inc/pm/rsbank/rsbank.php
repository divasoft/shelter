<?php

class RSBANK{
	
	private function getCURL($data){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, BANK_RS_URL_TRANSACTION);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 35);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 Firefox/1.0.7");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_SSLKEY, BANK_RS_UNIX_PATH_KEYS.BANK_RS_TSP_ID.'.key');
		curl_setopt($ch, CURLOPT_SSLCERT, BANK_RS_UNIX_PATH_KEYS.BANK_RS_TSP_ID.'.pem');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
		curl_setopt($ch, CURLOPT_CAINFO, BANK_RS_UNIX_PATH_KEYS.'chain-ecomm-ca-root-ca.crt');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSLVERSION, '1');
		$result_curl = curl_exec($ch);
		
		$text = 
'time: '.date('d.m.Y H:i:s', time()).'
data: '.$data.'
curl_errno: '.curl_errno($ch).'
curl_exec: '.$result_curl.'

';
		curl_close($ch);
		
		$now_date = date('Ymd', time());
		$file = fopen(FILE_ORDERS.'rsbank/'.$now_date.'.php',"a+");
		if ($file) fputs ($file, $text);
		fclose($file);
		
		return $result_curl;
	}
	
	private function getResult($result_curl){		
		$arStatus = Array();
		$arStatusElements = explode('%0A', urlencode($result_curl));
		
		for($i = 0; $i < sizeof($arStatusElements); $i++){
			$arStr = explode(': ',urldecode($arStatusElements[$i]));
			$arStatus[$arStr[0]] = $arStr[1];
		}
		
		return $arStatus;
	}
	
	public function getTransaction($arData){		
		$data = "command=v";
		$data .= "&msg_type=SMS";
		$data .= "&currency=643";
		$data .= "&server_version=2.0";	
		foreach($arData as $key=>$val){
			$data .= "&".$key."=".$val;
		}
		
		$resID = '';
		$result_curl = $this->getCURL($data);
		if(preg_match('@(TRANSACTION_ID): ([a-z0-9\/\+\=]{28})@i',$result_curl,$result_ok)){
			$resID = urlencode($result_ok[2]);						
		}
		
		return $resID;
	}
	
	public function getStatus($arData){		
		$data = "command=c";
		$data .= "&server_version=2.0";	
		foreach($arData as $key=>$val){
			if ($key == 'trans_id') $val = urlencode(strval($val));
			$data .= "&".$key."=".$val;
		}
		
		$result_curl = $this->getCURL($data);		
		$arStatus = $this->getResult($result_curl);
		
		return $arStatus;
	}
	
	public function setClose(){		
		$data = "command=b";
		
		$result_curl = $this->getCURL($data);		
		$arStatus = $this->getResult($result_curl);
		
		return $arStatus;
	}
	
}

?>