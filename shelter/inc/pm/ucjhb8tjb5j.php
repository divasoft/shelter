<?	
	define('PAY_FILE_RUN', true);
	include_once('../../config.php');	
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	include_once(PATH_PM.'ucs/orderv2.php');
	include_once(PATH_PM.'ucs/paymentUCS.php');
	$paymentUCS = new paymentUCS();
	
	$number = '';
	$soapXMLResult = file_get_contents('php://input');	
	
	if ($soapXMLResult){
		$soap = simplexml_load_string($soapXMLResult);
		//$file_order = fopen('soap.txt',"w+");if ($file_order) fputs ($file_order, $soapXMLResult);
		$number = strval($soap->children('http://schemas.xmlsoap.org/soap/envelope/')->Body->children()->notify->order->number);
	}else{
		die();
	}
	
	$arDataLog = Array(
		'id' => strval($number),
		'sum' => '-',
		'system' => 'UCSPAY',
		'guest' => '-',
		'text' => '',
	);
	
	$pay = false;
	$arData = Array();
	if ($number != ''){
		$arStatus = $paymentUCS->getStatus($number);
		
		$arData = $arStatus['data'];
		$errorText = $arStatus['error'];
		
		$arDataLog['sum'] = $arData['amount'];
		$arDataLog['guest'] = $arData['info']['guest_id'];		
		
		if (sizeof($arData) > 0){			
			if (UCSPAY_SHOPID == $arData['shop_id']){
				if ($arData['status'] == 'acknowledged'){
					$arData['amount'] = (float)$arData['amount'];
					if (($arData['number'] != '') && ($arData['amount'] > 0)){
						
						if (!(isSecondOrder($arData['number']))){
				
							if (SetPay($arData['info']['id'], floatval($arData['amount']))){						
								putIdOrder($arData['number']); 
								
								$arDataLog['text'] = 'PAY';
								
								$pay = true;
							}else
								$arDataLog['text'] = 'ERROR PayOrder';
						}else
							$arDataLog['text'] = 'ERROR Already';		
					}else
						$arDataLog['text'] = 'ERROR NUMBER('.$arData['number'].') AMOUNT('.$arData['amount'].')';	
				}else
					$arDataLog['text'] = 'ERROR STATUS('.$arData['status'].')';
			}else
				$arDataLog['text'] = 'ERROR SHOP('.$arData['shop_id'].')';
		}else
			$arDataLog['text'] = 'ERROR check STATUS('.$errorText.')';		
	}else
		$arDataLog['text'] = 'ERROR NUMBER';

	savePayLog($arDataLog);
		
	if ($pay){
		$arMail = Array(
			'EMAIL' => $arData['info']['email'],				
			'ORDER' => $arData['info']['code'],				
			'SUMM' => floatval($arData['amount']),
			'CURRENT' => $arMessages["current"][CURRENCYSHOW][MESSAGE_LANG],
		);								
		sendMail($arData['info']['email'], 'Payment', $arMail);
		sendMailForMeneger('ForMenegerPayment', $arMail, $arData['info']['code']);
	}
	
?>