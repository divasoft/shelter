<script  type="text/javascript" src="<?=SCRIPT_URL;?>js/jquery.js"></script>
<link href="<?=SCRIPT_URL;?>style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">	
	t_dayNamesMin = ['<?=$arMessages["daysInf"][6][MESSAGE_LANG];?>',<?for($i = 0; $i < 6; $i++){?>'<?=$arMessages["daysInf"][$i][MESSAGE_LANG];?>',<?}?>];	
	t_monthNames = [<?for($i = 1; $i <= 12; $i++) {?>'<?=$arMessages["monthInf"][$i][MESSAGE_LANG];?>',<?}?>];
	
	<? if (EARLY_TIME) { ?>
	t_time_title = '<?=$arMessages["timepicker"][1][MESSAGE_LANG];?>';
	t_time_hour = '<?=$arMessages["timepicker"][2][MESSAGE_LANG];?>';
	t_time_minute = '<?=$arMessages["timepicker"][3][MESSAGE_LANG];?>';
	
	<?
	$from_def = getDM($_SESSION['Shelter']['Data']['TimeSettings']['From']);
	$to_def = getDM($_SESSION['Shelter']['Data']['TimeSettings']['To']);
	?>
	t_standartSet = '<?=$arMessages["Find"]["standart_time_set"][MESSAGE_LANG];?> <span><?=$from_def[0];?>:<?=$from_def[1];?></span>';
	t_standartDep = '<?=$arMessages["Find"]["standart_time_dep"][MESSAGE_LANG];?> <span><?=$to_def[0];?>:<?=$to_def[1];?></span>';	
	<? } ?>
	
	path_file = '<?=SCRIPT_URL?>';
	error_field = '<?=$arMessages["general"]["errorfield"][MESSAGE_LANG];?>';
	reserv_infolen = <?=RESERV_INFO_LEN;?>;
	
	phone_format_write = <?=FORMAT_PHONE;?>;
</script>
<script type="text/javascript" src="<?=SCRIPT_URL;?>js/script.js"></script>