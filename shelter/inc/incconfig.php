<?php	
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	if (!defined('PAY_FILE_RUN')) define('PAY_FILE_RUN', false);
		
	include_once(PATH_MODULES.'Main.php');
	include_once(PATH_INC.'pages.php');			
	include_once(PATH_INC.'lang.php');	
	include_once(PATH_INC.'function.php');								
	
	if (isset($_GET['ucsclear'])){
		if ($_GET['ucsclear'] == 1) $_SESSION = Array();
			else clearUserData();
		header('location: '.change_address(Array(), Array('ucsclear')));	
		exit;	
	}
	
	// LANG ------------------------------- //
	$arMessageLang = unserialize(USER_LANG_SHOW);
	
	$user_lang_start = USER_LANG_START;	
	if ((!isset($_SESSION['Shelter']['System']['Lang'])) || ($_SESSION['Shelter']['System']['Lang'] == '')) $_SESSION['Shelter']['System']['Lang'] = $arCodeLang[$user_lang_start];
	if (sizeof($arMessageLang) < 2) $_SESSION['Shelter']['System']['Lang'] = $arCodeLang[$user_lang_start];
	
	$code = 0;
	if (in_array($_SESSION['Shelter']['System']['Lang'], $arCodeLang)) {
		for($i = 0; $i < sizeof($arCodeLang); $i++)
			if ($arCodeLang[$i] == $_SESSION['Shelter']['System']['Lang']) 
				$code = $i;
	}	
	define('CODE_LANG', protection($_SESSION['Shelter']['System']['Lang']));
	define('MESSAGE_LANG', $code);
	
	if (isset($_GET['slang'])) {
		for($i = 0; $i < sizeof($arMessageLang); $i++)
			if (strval($_GET['slang']) == $arMessageLang[$i]) $_SESSION['Shelter']['System']['Lang'] = $arMessageLang[$i];
			
		upSess();
		
		header('location: '.change_address(Array(), Array('slang')));
		exit;
	}		
	// LANG ------------------------------- //
	
	
	if (!PAY_FILE_RUN){
		if (MODULE_CHECK_CONNECT > 0) checkConnect();
		
		if ((!isset($_SESSION['Shelter']['Find']['hotel'])) || (intval($_SESSION['Shelter']['Find']['hotel']) < 1)) $_SESSION['Shelter']['Find']['hotel'] = getDefHotel(0);
		if (!isset($_SESSION['Shelter']['Data']['RoomFactor']) || (sizeof($_SESSION['Shelter']['Data']['RoomFactor']) == 0)) 
			$_SESSION['Shelter']['Data']['RoomFactor'] = getFactor1();
		if (!isset($_SESSION['Shelter']['Data']['TypeGuest']) || (sizeof($_SESSION['Shelter']['Data']['TypeGuest']) == 0)) 
			$_SESSION['Shelter']['Data']['TypeGuest'] = getFactor2();	
		if (!isset($_SESSION['Shelter']['Data']['HotelList']) || (sizeof($_SESSION['Shelter']['Data']['HotelList']) == 0)) 
			$_SESSION['Shelter']['Data']['HotelList'] = getHotelList();
			
		$GUEST = getUser();	

		// TIME ------------------------------- //
		if (!isset($_SESSION['Shelter']['Data']['TimeSettings']) || (sizeof($_SESSION['Shelter']['Data']['TimeSettings']) == 0)) 
			getSettingShelter($_SESSION['Shelter']['Find']['hotel']);
			
		$time_from = $_SESSION['Shelter']['Data']['TimeSettings']['From'];
		$time_to = $_SESSION['Shelter']['Data']['TimeSettings']['To'];
		if (EARLY_TIME){
			if ($_SESSION['Shelter']['Find']['request_time_from'] != '') {
				$time_from = intval($_SESSION['Shelter']['Find']['request_time_from']);
				$time_to = intval($_SESSION['Shelter']['Find']['request_time_to']);
			}	
		}
		
		define('FROM_HOUR', $time_from);
		define('FROM_HOUR_DOUBLE', toDtime(FROM_HOUR));
		define('TO_HOUR', $time_to);
		define('TO_HOUR_DOUBLE', toDtime(TO_HOUR));
		// TIME ------------------------------- //	
	}
	
	//echo '<pre>'; print_r($obj); echo '</pre>';
?>