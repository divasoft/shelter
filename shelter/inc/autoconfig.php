<?php		
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();

	if (file_exists(PATH_SECTION.'inc/xml/sh356fdb4g.xml')) {
		$file = simplexml_load_file(PATH_SECTION.'inc/xml/sh356fdb4g.xml');	
		foreach($file->category as $c=>$category){
			foreach($category->param as $p=>$param){
				$name = strval($param->attributes()->name);
				$type = intval($param->attributes()->type);			
				$value = strval($param->value);
				
				if ($type == 2) {
					$arValue = explode(';', $value);
					foreach($arValue as $k=>$v){
						if ($v == '') unset($arValue[$k]);
					}
					$value = serialize($arValue);
				}	
				define($name, $value);
				//echo $name.' '.$value.'<br>';
			}
		}	
	}else{
		die('FILE SETTINGS NOT FOUND');
	}	
	
	define('TYPE_GUEST_NO_CHECK', ''); // comment in xml
	
	if (DUPLICATE_URL){		
		$arURL = Array(
			'MAIN_URL_D' => MAIN_URL_D,
			'SCRIPT_URL_D' => SCRIPT_URL_D,
		);		
		$SERVER_URL = $_SERVER['HTTP_HOST'];
		$SERVER_FIND = 'www.';		
		foreach($arURL as $key=>$val){
			$start = 7;
			if (stristr($val, 'https://')) $start = 8;
			
			if (stristr($SERVER_URL, 'www.')){
				if (!stristr($val, 'www.')) $val = substr($val, 0, $start).$SERVER_FIND.substr($val, $start, (strlen($val) - $start));
			}else{
				if (stristr($val, 'www.')) $val = substr($val, 0, $start).substr($val, ($start + strlen($SERVER_FIND)), (strlen($val) - $start - strlen($SERVER_FIND)));
			}
			$arURL[$key] = $val;
		}
		//echo '<pre>'; print_r($arURL); echo '</pre>'; exit;
		define('MAIN_URL', $arURL['MAIN_URL_D']);
		define('SCRIPT_URL', $arURL['SCRIPT_URL_D']);
	}else{
		define('MAIN_URL', MAIN_URL_D);
		define('SCRIPT_URL', SCRIPT_URL_D);
	}
	
	define('NUMBER_URL', SCRIPT_URL.'about/rooms/'); // путь для описания номеров
	define('RATE_URL', SCRIPT_URL.'about/rates/'); // путь для описания тарифов
	define('PAY_URL', SCRIPT_URL.'inc/pm/'); // путь к модулям платежных систем, без параметров
	define('PATH_IMG', SCRIPT_URL.'images/');  
	define('PATH_CAPTCHA', SCRIPT_URL.'inc/');  
	define('PATH_MODULES', PATH_SECTION.'modules/'); 
	define('PATH_CONTOLLER', PATH_SECTION.'controller/'); 
	define('PATH_TEMPLATE', PATH_SECTION.'template/'); 
	define('PATH_TEMPLATE_INC', PATH_SECTION.'template/inc/'); 
	define('PATH_MAIL', PATH_SECTION.'mail/'); 
	define('PATH_INC', PATH_SECTION.'inc/');
	define('PATH_PM', PATH_SECTION.'inc/pm/');
	define('FILE_LOG', PATH_SECTION.'logs/xml/'); // запросы
	define('FILE_ORDERS', PATH_SECTION.'logs/payment/'); // оплата
	define('FILE_ORDERS_SUCCESS', PATH_SECTION.'logs/payment/payment_success.php'); // успешные оплаты
	
	define('KEY_SECURITY_SHELTER_VERSION', 12); // определение версии сервера для модуля
	
	define('KEY_SECURITY', 'nf783bf7dbd'); // произвольные латинские символы и цифры
	define('KEY_SECURITY_1', 'jdy5vdhf'); // произвольные латинские символы и цифры
	define('KEY_SECURITY_2', 'dhdrt7sdhd'); // произвольные латинские символы и цифры
	
	define('SCHEDULE_ENCODING', 'utf-8'); // кодировка модуля
	define('SHELTER_ENCODING', 'utf-8'); // кодировка запросов
	define('SCHEDULE_MAIL_ENCODING', 'utf-8'); // кодировка письма
	define('MODULE_SHEDULE', 'shelter'); // параметр передачи названия модулей
	define('LOG_RESPONSE', 0); // записывать ответ в лог (0 - длину ответа, 1 - ответ)
	define('LOG_RESPONSE_ALL', LOG_RESPONSE); // записывать в ответ пароли и тесты сообщений (0 - длину ответа, 1 - ответ)
		
	define('SCHEDULE_START_DATE_I', 25569);
	define('SCHEDULE_START_DATE_D', 36526);
	define('SCHEDULE_DAY_SECONDS', 86400);		
	
	$arCodeLang = Array('RU', 'EN', 'LT', 'BG'); // коды языков для запросов
	
	include_once(PATH_SECTION.'inc/incconfig.php');
?>