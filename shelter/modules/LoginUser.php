<?php

class LoginUser extends Main {

	private $email;
	private $password;
	
	public function __construct($email, $password) {
		$this->email = $email;
		$this->password = $password;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><guestlogin/>');
		$xmlobj->addAttribute('login', $this->protection($this->email));
		$xmlobj->addAttribute('password', $this->protection($this->password));
		return $xmlobj->asXML();
	}
}
?>