<?php		

class UserCabinet extends Main {
	
	public $guest = null;
	public $type = null;	
	public $active = null;	
	
	public function __construct($guest, $type, $active) {
		$this->guest = $guest;
		$this->type = $type;
		$this->active = $active;
	}
	
	public function GetXML($shelter = false) {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><reservlist/>');
		$xmlobj->addAttribute('hotel', '-1');
		$xmlobj->addAttribute('operation', $this->protection(CODE_OPERATION));
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		$xmlobj->addAttribute('startdate', 0);
		$xmlobj->addAttribute('finishdate', 999999);
		if ($this->active == 1)
		$xmlobj->addAttribute('is_actual', $this->active);
		
		$fld = 'owner';
		$resid = '-1';
		if (($this->type) == 1){
			$fld = 'debtor';
			$resid = '-1234098';
		}	
		
		if ($shelter){
			$fld = 'guest';
			$resid = '-1';
		}
		
		$xmlobj->addAttribute($fld, $this->protection($this->guest));
		$xmlobj->addAttribute('resid', $resid);	
		
		return $xmlobj->asXML();
	}
}

?>