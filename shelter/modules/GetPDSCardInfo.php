<?php
class GetPDSCardInfo extends Main {
	
	public $cardno = null;
	public $fullname = null;
	
	public function __construct($cardno, $fullname) {
		$this->cardno = $cardno;
		$this->fullname = $fullname;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><GetPDSCardInfo/>');
		$xmlobj->addAttribute('cardno', $this->cardno);
		$xmlobj->addAttribute('fullname', $this->fullname);
		
		return $xmlobj->asXML();
	}
	
}
?>