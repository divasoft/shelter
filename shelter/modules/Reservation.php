<?php

class Reservation extends Main {
	
	public $startdate = null;
	public $finishdate = null;
	public $settlehour = null;
	public $departurehour = null;
	public $roomkind = null;
	public $hotel = null;
	public $request_quest = null;
	public $arDiscount = null;
	public $arCommission = null;
	public $arPackageService = null;
	
	public function __construct($hotel, $startdate, $finishdate, $settlehour, $departurehour, $roomkind, $request_quest, $arDiscount, $arCommission, $arPackageService) {
		$this->startdate = $startdate;
		$this->finishdate = $finishdate;
		$this->settlehour = $settlehour;
		$this->departurehour = $departurehour;
		$this->roomkind = $roomkind;
		$this->hotel = $hotel;
		$this->request_quest = $request_quest;
		$this->arDiscount = array_unique($arDiscount);
		$this->arCommission = array_unique($arCommission);
		if (sizeof($arPackageService) > 0)
		$this->arPackageService = array_unique($arPackageService);
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><getratesfactor/>');
		$xmlobj->addAttribute('hotel', $this->protection($this->hotel));
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		$xmlobj->addAttribute('startdate', $this->protection($this->startdate));
		$xmlobj->addAttribute('finishdate', $this->protection($this->finishdate) - 1);
		$xmlobj->addAttribute('debtor', USE_DEBTOR_ID);
		$xmlobj->addAttribute('contract', USE_DEBTOR_CONTRACT);
		if (EARLY_TIME){
		$xmlobj->addAttribute('settlehour', $this->protection($this->settlehour));
		$xmlobj->addAttribute('departurehour', $this->protection($this->departurehour));		
		}
		$xmlobj->addAttribute('id', $this->protection($this->roomkind));
		$xmlobj->addAttribute('selectmode', 1);
		if (SETTLE_TYPE_PLACE == 1)
			$xmlobj->addAttribute('use_typesale', 1);
			
		$factor = $this->request_quest;
		$rs_quant = $xmlobj->addChild('rs_quant','');
		foreach($factor as $type_numb=>$guest){			
			$rs_f1 = $rs_quant->addChild('f1','');			
			$rs_f1->addAttribute('id', $this->protection($type_numb));
			foreach($guest as $type_guest=>$quant){
				$rs_f2 = $rs_f1->addChild('f2','');
				$rs_f2->addAttribute('id', $this->protection($type_guest));
				$rs_f2->addAttribute('quant', $this->protection($quant));
			}
		}
		
		if (sizeof($this->arDiscount) > 0){
			$arDiscounts = $this->arDiscount;
			$discounts = $xmlobj->addChild('discounts','');
			for($i = 0; $i < sizeof($arDiscounts); $i++){							
				$discount = $discounts->addChild('discount','');
				$discount->addAttribute('id', $arDiscounts[$i]);				
			}
		}
		
		if (sizeof($this->arCommission) > 0){
			$arCommission = $this->arCommission;
			$commissions = $xmlobj->addChild('commissions','');
			for($i = 0; $i < sizeof($arCommission); $i++){							
				$commission = $commissions->addChild('commission','');
				$commission->addAttribute('id', $arCommission[$i]);				
			}
		}
		
		if (sizeof($this->arPackageService) > 0){
			$arPackageService = $this->arPackageService;
			$rate_adds = $xmlobj->addChild('rate_adds','');
			for($i = 0; $i < sizeof($arPackageService); $i++){							
				$rate_add = $rate_adds->addChild('rate_add','');
				$rate_add->addAttribute('id', $arPackageService[$i]);				
			}
		}
		
		/*if ((USE_DEBTOR_ID > 0) && (USE_DEBTOR_CONTRACT > 0)){
			if (USE_DEBTOR_CONTRACT_DISCOUNT > 0){
				if (!$ex_discounts){
				$discounts = $xmlobj->addChild('discounts','');	
				}
				$discount = $discounts->addChild('discount','');
				$discount->addAttribute('id', USE_DEBTOR_CONTRACT_DISCOUNT);			
			}
			if (USE_DEBTOR_CONTRACT_COMMISION > 0){
				$commissions = $xmlobj->addChild('commissions','');	
				$commission = $commissions->addChild('commission','');
				$commission->addAttribute('id', USE_DEBTOR_CONTRACT_COMMISION);			
			}
		}*/	
		
		return $xmlobj->asXML();
	}
	
}

?>