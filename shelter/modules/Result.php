<?php

class Result extends Main {
	
	public $hotel = null;
	public $startdate = null;
	public $finishdate = null;
	
	public function __construct($hotel, $startdate, $finishdate) {
		$this->hotel = $hotel;
		$this->startdate = $startdate;
		$this->finishdate = $finishdate;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><vacancybyroomkind/>');
		$xmlobj->addAttribute('hotel', $this->protection($this->hotel));
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		$xmlobj->addAttribute('startdate', $this->protection($this->startdate));
		$xmlobj->addAttribute('finishdate', $this->protection($this->finishdate));
		if (SETTLE_TYPE_PLACE == 1)
			$xmlobj->addAttribute('use_typesale', 1);
		if (SHOW_NUMBERS_VIEW_QUOTE)
			$xmlobj->addAttribute('allow_quote', 1);
		
		return $xmlobj->asXML();
	}
	
}
?>