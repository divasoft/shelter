<?php	
class Main
{		
	public function protection($text) {
		return htmlspecialchars(strip_tags($text));
	}
	
	public function toFile($text, $str) {	
		$now_date = date('Ymd', time());
		$arWords = Array('password', 'newpassword', 'body');		
		if (LOG_RESPONSE_ALL) $arWords = Array();
		
		for($i = 0; $i < sizeof($arWords); $i++) {
			$pattern = '#'.$arWords[$i].'="([^"]+)"#i';
			$replacement = $arWords[$i].'="deleted"';			
			$text = preg_replace($pattern, $replacement, $text);
		}						
		if ($str) $text .= '

';		
		$file = fopen(FILE_LOG.$now_date.'.php',"a+");
		if ($file) fputs ($file, $text);				
		fclose ($file);
	}
	
	public function getSign($text){		
		$res = '';
		for($i = 0 ;$i < strlen($text); $i++)
			if ((ord($text[$i]) >= 32) && (ord($text[$i]) <= 127))
				$res .= $text[$i];		
		$res = hash('sha512', $res);		
		return $res;
	}
	
	public function Request($out) {		
		if (KEY_SECURITY_SHELTER != '') {
			$xmlobj = new SimpleXMLElement($out);
			$xmlobj->addAttribute('version', KEY_SECURITY_SHELTER_VERSION);
			$xmlobj->addAttribute('sign', $this->getSign($xmlobj->asXML().KEY_SECURITY_SHELTER));
			$out = $xmlobj->asXML();	
		}
		
		$str1 = date('H:i:s', time()).' toXML: '.$out;
		$this->toFile($str1, false);	
  		$instr = '';
  		$err = 1;
  		$errname = 'Error';
  		$socket = pfsockopen(XML_PATH,XML_PORT,$err,$errname);
  		if ($socket) {
    		$len = strlen($out);
    		$out = str_pad($len, 9, "0", STR_PAD_LEFT) . $out; 
    		$ncount = 8192;		
			$i = 0;
			while(true)	{
      			$ostr = substr($out, $i, $ncount);
      			fputs($socket, $ostr, $ncount);
      			$i = $i + $ncount;  
      			if (!($i < strlen($out))) break;
    		} 
    		$r = 32768;
    		$r1 = 0;
    		$strr1 = fgets($socket, $r);
    		$r1 = (int)$strr1;
    		while (strlen($instr) < $r1) {
      			$instr .= fgets($socket, $r);
    		};
  		} else {
    		fclose($socket);
    		return false;
  		};
  		fclose($socket);
		
		$response = strlen($instr);
		if ((LOG_RESPONSE) || (strpos($out, 'action="SAVE"'))) $response = $instr;
		
		$str2 = date('H:i:s', time()).' fromXML: '.$response;
		$this->toFile($str2, true);	
		if (SHELTER_ENCODING == 'utf-8')
			$instr = trim(str_replace('windows-1251', 'UTF-8', $instr));			
		else	
			$instr = trim(str_replace('UTF-8', 'windows-1251',$instr));			
		if (!strpos($instr, 'xml')) $instr = '<?xml version="1.0"?>'.$instr;
		if (!strpos($instr, 'encoding')) $instr = trim(str_replace('version="1.0"', 'version="1.0" encoding="'.SHELTER_ENCODING.'"',$instr));
		if (strpos($instr, 'encoding'))	$instr = simplexml_load_string($instr);
		
  		return $instr;
	}	
}    

?>