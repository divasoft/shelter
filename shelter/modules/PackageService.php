<?php		

class PackageService extends Main {
	
	public function __construct() {		
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><getextra/>');
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		return $xmlobj->asXML();
	}
	
}

?>