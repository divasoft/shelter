<?php		

class GetSettings extends Main {
	private $hotel = null;
	
	public function __construct($hotel) {	
		$this->hotel = $hotel;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><default_values/>');
		$xmlobj->addAttribute('hotel', $this->protection($this->hotel));
		return $xmlobj->asXML();
	}
	
}

?>