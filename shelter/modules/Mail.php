<?php

class Mail extends Main {
	
	public function Compile($template_name, $parameters) {
		$template_path = PATH_MAIL.$template_name.'.tpl';
		$template = file_get_contents($template_path);
		if (!$template) {
			return false;
		}
		
		$template = $this->cicle($template, $parameters); // отправка на замену блоков (^)
		
		foreach ($parameters as $k=>$v) {
			$template = str_replace('#'.$k.'#', $this->protection($v), $template);
		}
		
		$template_path = PATH_MAIL.'Header.tpl';
		$template_header = file_get_contents($template_path);
		if ($template_header) $template = $template_header.$template;
		
		$template_path = PATH_MAIL.'Footer.tpl';
		$template_footer = file_get_contents($template_path);
		if ($template_footer) $template .= $template_footer;
		
		return $template;
	}
	
	public function cicle($file, $arData) {
		$count = round(substr_count($file, '^') / 2); // количество блоков
		for($i = 0; $i < $count; $i++)
			$file = $this->readSub($file, $arData);
		
		return $file;	
	}
	
	public function readSub($file, $arData) {
		
		// нахождение первого блока
		$first = strpos($file, '^') + 1;
		$file_temp = substr($file, $first, strlen($file));
		$second = strpos($file_temp, '^');
		$string = substr($file, $first, $second); 			
		
		// нахождение первого заменяемого элемента
		$first1 = strpos($string, '#') + 1;
		$file_temp = substr($string, $first1, strlen($file));
		$second1 = strpos($file_temp, '#');
		
		$elemets = explode(';',$arData[substr($string, $first1, $second1)]); // нахождение массива заменяемых элементов
		
		$str = ''; // строки с заменяемыми элементами
		for($i = 0; $i < sizeof($elemets); $i++) 
			$str .= '^'.($i+1).'^'; 
		
		$file = substr_replace($file, $str, ($first - 1), ($second + 2)); // замена блока на множество блоков
		
		for($i = 0; $i < sizeof($elemets); $i++) {				
			$string_temp = $string;
			foreach($arData as $key=>$val) {
				$val = explode(';',$val);
				$val[$i] = str_replace('&quot;', '"', $val[$i]);
				$string_temp = str_replace('#'.$key.'#', $val[$i], $string_temp); // замена внутренних элементов
			}				
			$file = str_replace('^'.($i+1).'^', $string_temp, $file); // запись блоков
		}			
		return $file;
	}
	
	private function SendShelter($to, $subject, $body) {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SCHEDULE_MAIL_ENCODING.'"?><sendmail/>');
		$xmlobj->addAttribute('mail_to', u($this->protection($to)));
		$xmlobj->addAttribute('subject', u($this->protection($subject)));
		$xmlobj->addAttribute('charset', SCHEDULE_MAIL_ENCODING);
		$xmlobj->addAttribute('contenttype', 'text/html');
		$xmlobj->addAttribute('body', u($body));
		//echo '1'.$xmlobj->asXML();
		$xml = $xmlobj->asXML();
		
		$obj = $this->Request($xml);
		
		if (!empty($obj)) {
			if (intval($obj->LastCmdResult->attributes()->numericcode) == 250) return true;
				else return false;			
		} else 
			return false;	
	}
	
	private function SendServer($to, $subject, $body) {		
		if (MODULE_MAIL_ADDRESS == '') return false;
		
        return DivasoftShelter::mail($to, $subject, $body); // DIVASOFT
        
		$address = htmlspecialchars_decode(MODULE_MAIL_ADDRESS);
		$header = "From: ".$address."\r\nReply-To: ".$address."\r\nReturn-Path: ".$address."\r\nContent-Type: text/html; charset=utf-8";
		
		$text = '';
		if (LOG_RESPONSE_ALL) $text = ' body: '.$body;
		
		$this->toFile(date('H:i:s', time()).' mailto: '.$to.' subject: '.$subject.$text, true);
		
		if (mail($to, $subject, $body, $header)) return true;
		return false;
	}
	
	public function Send($to, $subject, $body) {
		if (SEND_MESSAGE_MODE == 0){
			return $this->SendServer($to, $subject, $body);
		}else{
			return $this->SendShelter($to, $subject, $body);
		}
	}
	
}

?>