<?php

class LoginFirm extends Main {

	private $login;
	private $password;
	
	public function __construct($login, $password) {
		$this->login = $login;
		$this->password = $password;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><debtorlogin/>');
		$xmlobj->addAttribute('login', $this->protection($this->login));
		$xmlobj->addAttribute('password', $this->protection($this->password));
		return $xmlobj->asXML();
	}
}
?>