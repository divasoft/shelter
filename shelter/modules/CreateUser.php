<?php		

class CreateUser extends Main {
	
	public $name = null;
	public $name1 = null;
	public $name2 = null;
	public $fullname = null;
	public $email = null;
	public $phones = null;	
	
	public function __construct($name, $name1, $name2, $fullname, $email, $phones) {
		$this->name = $name;
		$this->name1 = $name1;
		$this->name2 = $name2;		
		$this->fullname = $fullname;
		$this->email = $email;
		$this->phones = $phones;		
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><guestcreate/>');
		$xmlobj->addAttribute('name', $this->protection($this->name));
		$xmlobj->addAttribute('name1', $this->protection($this->name1));
		$xmlobj->addAttribute('name2', $this->protection($this->name2));
		$xmlobj->addAttribute('fullname', $this->protection($this->fullname));
		$xmlobj->addAttribute('email', $this->protection($this->email));
		$xmlobj->addAttribute('phones', $this->protection($this->phones));
		return $xmlobj->asXML();
	}
	
}

?>