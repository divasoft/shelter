<?php

class AboutUser extends Main {

	private $login;
	private $password;
	private $newpassword;	
	private $email;
	private $name;
	private $name1;
	private $name2;
	private $fullname;
	private $phones;
	
	public function __construct($login, $password, $newpassword, $email, $name, $name1, $name2, $fullname, $phones) {
		$this->login = $login;
		$this->password = $password;
		$this->newpassword = $newpassword;		
		$this->email = $email;
		$this->name = $name;
		$this->name1 = $name1;
		$this->name2 = $name2;
		$this->fullname = $fullname;
		$this->phones = $phones;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><guestlogin_change/>');
		$xmlobj->addAttribute('login', $this->protection($this->login));
		$xmlobj->addAttribute('password', $this->protection($this->password));
		if ($this->newpassword != '')
			$xmlobj->addAttribute('newpassword', $this->protection($this->newpassword));
		if ($this->email != '')
			$xmlobj->addAttribute('email', $this->protection($this->email));
		$xmlobj->addAttribute('name', $this->protection($this->name));
		$xmlobj->addAttribute('name1', $this->protection($this->name1));
		$xmlobj->addAttribute('name2', $this->protection($this->name2));
		$xmlobj->addAttribute('fullname', $this->protection($this->fullname));
		$xmlobj->addAttribute('phones', $this->protection($this->phones));
		return $xmlobj->asXML();
	}
}
?>