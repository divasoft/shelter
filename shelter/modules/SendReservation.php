<?php

class SendReservation extends Main {
	
	public $hotel = null;
	public $id = null;
	public $arrival = null;
	public $departure = null;
	public $kind = null;
	public $status = null;
	public $state = null;
	public $debtor = null;
	public $agent = null;	
	public $contact = null;
	public $contract = null;
	public $phone = null;
	public $email = null;
	public $creditcard = null;	
	public $info = null;
	public $payment = null;
	public $owner = null;
	public $action = null;
	public $create_folio = null;
	public $payment_summa = null;
	public $operation = null;
	public $inquiry = null;		
	
	public function __construct($maindata, $inqury) {
		$this->hotel = $maindata['hotel'];
		$this->id = $maindata['id'];
		$this->arrival = $maindata['arrival'];
		$this->departure = $maindata['departure'];
		$this->kind = 1;
		$this->status = $maindata['status'];
		$this->state = 1;
		$this->debtor = $maindata['debtor'];
		$this->agent = $maindata['agent'];
		$this->contact = $maindata['contact'];
		$this->contract = $maindata['contract'];
		$this->phone = $maindata['phone'];
		$this->email = $maindata['email'];
		$this->info = $maindata['info'];
		$this->creditcard = $maindata['creditcard'];
		$this->payment = $maindata['payment'];
		$this->owner = $maindata['owner'];
		$this->action = $maindata['action'];
		
		if ((isset($maindata['create_folio'])) && ($maindata['create_folio'] != '')) {
			$this->create_folio = $maindata['create_folio'];
			$this->operation = $maindata['operation'];
			if ($maindata['payment_summa'] != '') 
				$this->payment_summa = $maindata['payment_summa'];
		}
		
		$this->inquiry = $inqury;
		
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><reservation/>');
		
		$xmlobj->addAttribute('hotel', $this->protection($this->hotel));
		$xmlobj->addAttribute('id', $this->protection($this->id));
		$xmlobj->addAttribute('arrival', $this->protection($this->arrival));
		$xmlobj->addAttribute('departure', $this->protection($this->departure));
		$xmlobj->addAttribute('kind', $this->protection($this->kind));
		$xmlobj->addAttribute('status', $this->protection($this->status));
		$xmlobj->addAttribute('state', $this->protection($this->state));
		$xmlobj->addAttribute('debtor', $this->protection($this->debtor));
		$xmlobj->addAttribute('agent', $this->protection($this->agent));
		$xmlobj->addAttribute('contract', $this->protection($this->contract));
		$xmlobj->addAttribute('contact', $this->protection($this->contact));
		$xmlobj->addAttribute('phone', $this->protection($this->phone));
		$xmlobj->addAttribute('email', $this->protection($this->email));
		$xmlobj->addAttribute('info', $this->protection($this->info));
		$xmlobj->addAttribute('cc_info', $this->protection($this->creditcard));
		$xmlobj->addAttribute('payment', $this->protection($this->payment));
		$xmlobj->addAttribute('owner', $this->protection($this->owner));
		$xmlobj->addAttribute('action', $this->protection($this->action));		
		
		if ($this->create_folio != '') {
			$xmlobj->addAttribute('create_folio', $this->protection($this->create_folio));
			$xmlobj->addAttribute('operation', $this->protection($this->operation));
			
			if ($this->payment_summa != '') 
				$xmlobj->addAttribute('payment_summa', $this->protection($this->payment_summa));
			
		}
		foreach ($this->inquiry as $inq) {
			$child = $xmlobj->addChild('inquiry','');
			$child->addAttribute('id', $this->protection($inq['id']));
			$child->addAttribute('resid', $this->protection($this->id));
			$child->addAttribute('roomkind', $this->protection($inq['roomkind']));
			if (SETTLE_TYPE_PLACE == 1)
				$child->addAttribute('use_typesale', 1);
			if (intval($inq['idroom']) > 0)
				$child->addAttribute('idroom', $this->protection($inq['idroom']));						
			$child->addAttribute('arrival', $this->protection($inq['arrival']));
			$child->addAttribute('departure', $this->protection($inq['departure']));
			$child->addAttribute('quant', $this->protection($inq['quant']));
			$child->addAttribute('rate', $this->protection($inq['rate']));
			$child->addAttribute('blockid', $this->protection($inq['blockid']));
			
			if (is_array($inq['claim'])) {
				$claim = $child->addChild('claim','');
				$claim->addAttribute('id', $this->protection($inq['claim']['id']));
				$claim->addAttribute('inquiry', $this->protection($inq['id']));
				
				if (is_array($inq['claim']['guests'])) {
					foreach ($inq['claim']['guests'] as $g) {
						$guest = $claim->addChild('guest','');
						$guest->addAttribute('id', $this->protection($g['id']));
						$guest->addAttribute('claim', $this->protection($inq['claim']['id']));
						$guest->addAttribute('name', $this->protection($g['name']));
						$guest->addAttribute('name1', $this->protection($g['name1']));
						$guest->addAttribute('name2', $this->protection($g['name2']));
						$guest->addAttribute('fullname', $this->protection($g['fullname']));
						$guest->addAttribute('email', $this->protection($g['email']));
						$guest->addAttribute('phones', $this->protection($g['phones']));
						//$guest->addAttribute('doc_series', $this->protection($g['doc_series']));
						//$guest->addAttribute('noguest', $this->protection($g['noguest']));											
						$guest->addAttribute('birthdate', $this->protection($g['birthdate']));											
						$guest->addAttribute('f1', $this->protection($g['f1']));											
						$guest->addAttribute('f2', $this->protection($g['f2']));											
					}
				}
				
				if (is_array($inq['claim']['operation'])) {
					foreach ($inq['claim']['operation'] as $g) {
						$operation = $claim->addChild('operation','');
						$operation->addAttribute('id', $this->protection($g['id']));
						$operation->addAttribute('operation', $this->protection($g['operation']));
						$operation->addAttribute('date', $this->protection($g['date']));
						$operation->addAttribute('quant', $this->protection($g['quant']));
						$operation->addAttribute('cost', $this->protection($g['cost']));									
					}
				}
				
				if (sizeof($inq['claim']['discount']) > 0){
					$arDiscounts = array_unique($inq['claim']['discount']);					
					$discounts = $claim->addChild('discounts','');
					for($i = 0; $i < sizeof($arDiscounts); $i++){
						$discount = $discounts->addChild('discount','');
						$discount->addAttribute('id', $this->protection($arDiscounts[$i]));
					}
				}
				
				if ($inq['claim']['commission'] > 0){
					$commissions = $claim->addChild('commissions','');
					$commission = $commissions->addChild('commission','');
					$commission->addAttribute('id', $this->protection($inq['claim']['commission']));
				}
				
				if ($inq['claim']['rate_adds'] > 0){
					$arPackageService = array_unique($inq['claim']['rate_adds']);	
					$rate_adds = $claim->addChild('rate_adds','');
					for($i = 0; $i < sizeof($arPackageService); $i++){
						$rate_add = $rate_adds->addChild('rate_add','');
						$rate_add->addAttribute('id', $this->protection($arPackageService[$i]));
					}
				}
			}
			
			if (is_array($inq['rs_quant'])) {
				foreach ($inq['rs_quant'] as $g) {
					$rs_quant = $child->addChild('rs_quant','');
					$rs_quant->addAttribute('f1', $this->protection($g['f1']));
					$rs_quant->addAttribute('f2', $this->protection($g['f2']));
					$rs_quant->addAttribute('quant', $this->protection($g['quant']));										
					
					$field = '';
					if (($g['f1'] == 2) && ($g['f2'] == 1)) $field = 'adult';
					if (($g['f1'] == 2) && ($g['f2'] == 2)) $field = 'child';
					if (($g['f1'] == 2) && ($g['f2'] == 3)) $field = 'infant';					
					if (($g['f1'] == 3) && ($g['f2'] == 1)) $field = 'placesex';
					if (($g['f1'] == 3) && ($g['f2'] == 2)) $field = 'placesex_child';
					if (($g['f1'] == 3) && ($g['f2'] == 3)) $field = 'placesex_infant';					
					if ($field != '') $child->addAttribute($field, $this->protection($g['quant']));
				}
			}
		}
		
		return $xmlobj->asXML();
	}
	
}
?>