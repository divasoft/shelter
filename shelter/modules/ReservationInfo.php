<?php

class ReservationInfo extends Main {
	
	public $id = null;
	public $owner = null;
	
	public function __construct($id, $owner) {
		$this->id = $id;
		$this->owner = $owner;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><reservation/>');
		$xmlobj->addAttribute('action', 'GETINFO');
		$xmlobj->addAttribute('id', $this->protection($this->id));
		if (($this->owner) > 0)
			$xmlobj->addAttribute('owner', $this->protection($this->owner));
		return $xmlobj->asXML();
	}
	
}

?>