<?php

class VacancyRooms extends Main {
	
	public $hotel = null;
	public $startdate = null;
	public $finishdate = null;	
	public $id = null;
	
	public function __construct($hotel, $startdate, $finishdate, $id) {
		$this->hotel = $hotel;
		$this->startdate = $startdate;
		$this->finishdate = $finishdate;		
		$this->id = $id;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><vacancybyrooms/>');
		$xmlobj->addAttribute('hotel', $this->protection($this->hotel));
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		$xmlobj->addAttribute('startdate', $this->startdate);
		$xmlobj->addAttribute('finishdate', $this->finishdate);
		$xmlobj->addAttribute('id', $this->id);
		
		return $xmlobj->asXML();
	}
	
}
?>