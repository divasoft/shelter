<?php		

class ReservationDelete extends Main {
	
	public $id = null;
	public $owner = null;
	
	public function __construct($id, $owner) {
		$this->id = $id;
		$this->owner = $owner;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><reservation_cancel/>');
		$xmlobj->addAttribute('resid', $this->protection($this->id));
		$xmlobj->addAttribute('res_owner', $this->protection($this->owner));		
		return $xmlobj->asXML();
	}
	
}

?>