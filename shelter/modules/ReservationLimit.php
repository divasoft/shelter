<?php

class ReservationLimit extends Main {
	
	public $arrival = null;
	public $departure = null;
	public $roomkind = null;
	
	public function __construct($arrival, $departure, $roomkind) {
		$this->arrival = $arrival;
		$this->departure = $departure;
		$this->roomkind = $roomkind;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><getroomkindblockid/>');
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		$xmlobj->addAttribute('arrival', $this->protection($this->arrival));
		$xmlobj->addAttribute('departure', $this->protection($this->departure));
		$xmlobj->addAttribute('debtor', USE_DEBTOR_ID);
		$xmlobj->addAttribute('roomkind', $this->protection($this->roomkind));		
		return $xmlobj->asXML();
	}
	
}

?>