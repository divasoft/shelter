<?php		

class Operations extends Main {
	
	public $hotel = null;		
	
	public function __construct($hotel) {
		$this->hotel = $hotel;		
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><getoperations/>');
		$xmlobj->addAttribute('hotel', $this->protection($this->hotel));
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		return $xmlobj->asXML();
	}
}


?>