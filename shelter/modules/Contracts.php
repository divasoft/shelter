<?php

class Contracts extends Main {

	private $debtor;
		
	public function __construct($debtor) {
		$this->debtor = $debtor;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><getdebtorcontracts/>');
		$xmlobj->addAttribute('debtor', $this->protection($this->debtor));
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		return $xmlobj->asXML();
	}
}
?>