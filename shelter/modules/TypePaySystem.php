<?php

class TypePaySystem extends Main {
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><getpayments/>');
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		return $xmlobj->asXML();
	}
	
}

?>