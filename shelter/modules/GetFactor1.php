<?php

class GetFactor1 extends Main {
	
	//public $hotel = null;
	private $langcode = null;
	//public $roomkinds = null;
	
	public function __construct() {
		//$this->hotel = $hotel;
		//$this->roomkinds = $roomkinds;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><GetRoomKindFactor1/>');
		//$xmlobj->addAttribute('hotel', $this->protection($this->hotel));
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		//$xmlobj->addAttribute('roomkinds', $this->protection($this->roomkinds));
		return $xmlobj->asXML();
	}
}

?>