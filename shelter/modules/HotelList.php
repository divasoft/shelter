<?php		

class HotelList extends Main {
	
	public function __construct() {		
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><hotelslist/>');
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		return $xmlobj->asXML();
	}
	
}

?>