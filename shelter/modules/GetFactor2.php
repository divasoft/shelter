<?php

class GetFactor2 extends Main {
	
	//public $hotel = null;
	private $langcode = null;
	//public $roomkinds = null;
	
	public function __construct() {
		//$this->hotel = $hotel;
		//$this->roomkinds = $roomkinds;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><GetFactor2/>');
		$xmlobj->addAttribute('hotel', '');
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		//$xmlobj->addAttribute('roomkinds', $this->protection($this->roomkinds));
		return $xmlobj->asXML();
	}
}

?>