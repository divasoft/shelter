<?php

class SResultLimit extends Main {
	
	public $hotel = null;
	public $startdate = null;
	public $finishdate = null;
	public $debtor = null;
	public $request_quest = null;
	
	public function __construct($hotel, $startdate, $finishdate, $request_quest) {
		$this->hotel = $hotel;
		$this->startdate = $startdate;
		$this->finishdate = $finishdate;
		$this->request_quest = $request_quest;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><vacancyquote_rates/>');
		$xmlobj->addAttribute('hotel', $this->protection($this->hotel));
		$xmlobj->addAttribute('langcode', $this->protection(CODE_LANG));
		$xmlobj->addAttribute('startdate', $this->protection($this->startdate));
		$xmlobj->addAttribute('finishdate', $this->protection($this->finishdate));
		$xmlobj->addAttribute('ignore_days', IGNORE_DAYS);
		$xmlobj->addAttribute('use_entire_period', SETTLE_ENTIRED_PERIOD);
		if (SETTLE_TYPE_PLACE == 1)
			$xmlobj->addAttribute('use_typesale', 1);
			
		$xmlobj->addAttribute('debtor', USE_DEBTOR_ID);
		$xmlobj->addAttribute('contract', USE_DEBTOR_CONTRACT);
		if ((USE_DEBTOR_ID > 0) && (USE_DEBTOR_CONTRACT > 0)){
			if (USE_DEBTOR_CONTRACT_DISCOUNT > 0){
				$commissions = $xmlobj->addChild('discounts','');	
				$commission = $commissions->addChild('discount','');
				$commission->addAttribute('id', USE_DEBTOR_CONTRACT_DISCOUNT);			
			}
			if (USE_DEBTOR_CONTRACT_COMMISION > 0){
				$commissions = $xmlobj->addChild('commissions','');	
				$commission = $commissions->addChild('commission','');
				$commission->addAttribute('id', USE_DEBTOR_CONTRACT_COMMISION);			
			}
		}
		
		if (EARLY_TIME){
		$xmlobj->addAttribute('settlehour', FROM_HOUR_DOUBLE);
		$xmlobj->addAttribute('departurehour', TO_HOUR_DOUBLE);		
		}
		
		$factors = $this->request_quest;
		$rs_quant = $xmlobj->addChild('rs_quant','');
		
		/*foreach($factor as $type_numb=>$guest){			
			$rs_f1 = $rs_quant->addChild('f1','');			
			$rs_f1->addAttribute('id', $this->protection($type_numb));
			foreach($guest as $type_guest=>$quant){
				$rs_f2 = $rs_f1->addChild('f2','');
				$rs_f2->addAttribute('id', $this->protection($type_guest));
				$rs_f2->addAttribute('quant', $this->protection($quant));
			}
		}*/
		
		foreach($factors as $i=>$val){			
			$factor_block = explode(';', $val);			
			$rs_quant = $xmlobj->addChild('rs_quant','');			
			
			for($j = 0; $j < sizeof($factor_block); $j++){			
				$factor = explode('-', $factor_block[$j]);				
				$rs_f1 = $rs_quant->addChild('f1','');
				$rs_f1->addAttribute('id', $this->protection($factor[0]));			
				$rs_f2 = $rs_f1->addChild('f2','');
				$rs_f2->addAttribute('id', $this->protection($factor[1]));
				$rs_f2->addAttribute('quant', $this->protection($factor[2]));
			}
		}
		
		return $xmlobj->asXML();
	}
	
}
?>