<?php

class PaySystem extends Main {
	
	public $id = null;
	public $action = null; // ADD, MODIFY, DELETE
	public $resid = null;
	public $operation = null;
	public $name = null;
	public $phone = null;
	public $summa = null;
	public $info = null;
	public $begindate = null;
	public $enddate = null;
	public $currency = null;
	
	public function __construct($id, $action, $resid, $operation, $name, $phone, $summa, $info, $begindate, $enddate, $currency) {
		$this->id = $id;
		$this->action = $action;
		$this->resid = $resid;
		$this->operation = $operation;
		$this->name = $name;
		$this->phone = $phone;
		$this->summa = $summa;
		$this->info = $info;
		$this->begindate = $begindate;
		$this->enddate = $enddate;
		$this->currency = $currency;
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><paysystem/>');
		$xmlobj->addAttribute('id', $this->protection($this->id));
		$xmlobj->addAttribute('action', $this->protection($this->action));
		$xmlobj->addAttribute('resid', $this->protection($this->resid));
		$xmlobj->addAttribute('operation', $this->protection($this->operation));
		$xmlobj->addAttribute('name', $this->protection($this->name));
		$xmlobj->addAttribute('phone', $this->protection($this->phone));
		$xmlobj->addAttribute('summa', $this->protection($this->summa));
		$xmlobj->addAttribute('info', $this->protection($this->info));
		$xmlobj->addAttribute('begindate', $this->protection($this->begindate));
		$xmlobj->addAttribute('enddate', $this->protection($this->enddate));
		$xmlobj->addAttribute('currency', $this->protection($this->currency));		
		return $xmlobj->asXML();
	}
	
	// <paysystem id="1" action="ADD" resid="1" operation="97" name="Guest Name" phone="89000000000" summa="1.0" info="Information" begindate="41250.5" enddate="41251.5" currency="RUB"/>
}
?>