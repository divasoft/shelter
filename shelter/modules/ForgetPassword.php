<?php

class ForgetPassword extends Main {

	private $login;	
	
	public function __construct($login) {
		$this->login = $login;		
	}
	
	public function GetXML() {
		$xmlobj = new SimpleXMLElement('<?xml version="1.0" encoding="'.SHELTER_ENCODING.'"?><guestexists/>');
		$xmlobj->addAttribute('login', $this->protection($this->login));		
		return $xmlobj->asXML();
	}
}
?>