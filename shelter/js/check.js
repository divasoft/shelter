
var check_connect_ok = false;
function get_connect(){	
	$.ajax({		
		type: "POST",
		url: path_file+"ajax.php",	
		data: "check_connect=1",		
		success: function(res)
		{	
			if (res != '') check_connect_ok = true;
		}
	});
}

function set_time_connect(){
	if (check_connect_ok) {
		window.location.href = path_url;	
	}else{
		if (time > 0) {
			time--;
			window.setTimeout("set_time_connect();", 1000);	
		}else{
			$('.ucs_check_connect').addClass('ucs_none');
			$('.ucs_no_connect').removeClass('ucs_none');
		}
	}
}

$(function(){
	get_connect();
	set_time_connect(time);	
});