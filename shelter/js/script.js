
function check_request_out() {
	date_ar = $("#ucs_request_in").val();
	
	date_ar = date_ar.split('.');
	
	day = date_ar[0];
	if (day[0] == 0) day = day[1];
	
	day = parseInt(day) + 1;
	
	date = new Date(date_ar[2], (parseInt(date_ar[1]) - 1), day);
	
	day = parseInt(date.getDate());
	if (day < 10) day = '0' + day;
	
	month = parseInt(date.getMonth()) + 1;
	if (month < 10) month = '0' + month;
	
	$("#ucs_request_out").val(day + '.' + month + '.' + date.getFullYear());
} /* обработка при изменении даты выезда */

function str_replace(search, replace, subject) {
    return subject.split(search).join(replace);
} /* замена подстроки */

function readId(arText, j) {	
	text = arText.split('_');
	return text[j];
} /* обработка идентификаторов сообщения */

function readtext(arText, j) {	
	text = arText.split(';');
	return text[j];
} /* обработка принятого сообщения */

function toInput() {
	
	id = '';
	min = '';
	max = '';
	for(i = 0; i < ar_id.length; i++) {
		id = id + ar_id[i];
		min = min + ar_c_min[ar_id[i]];
		max = max + (ar_c_max[ar_id[i]] - 1);
		if (i < (ar_id.length-1)) {
			id = id + ';';
			min = min + ';';
			max = max + ';';
		}
	}
	
	$('#ucs_res_request_in').val(min);
	$('#ucs_res_request_out').val(max);
	$('#ucs_res_request_id').val(id);
	
	if ($('#ucs_res_request_id').val() != '') $('#ucs_res_sub').show();
		else $('#ucs_res_sub').hide();
	
} /* запись в input параметров заездов Result */ 

function delClass(obj, name) {
	$(obj).removeClass(name);	
} /* удаление класса */

function setClass(obj, name) {
	$(obj).addClass(name);	
} /* добавление класса */

function textfind(elem, text) {
	if (elem.indexOf(text) + 1) return true;
	return false;
}

function resval(id) {	
	
	if (id < 0) {		
		id = -id;
		
		$('.ucs_room'+id).each(function(){		
			delClass('#'+this.id, 'ucs_chooseroom');		
		});
		
		tar_id = new Array();
		for(i=0;i<ar_id.length;i++)
			if (ar_id[i] != id) tar_id.push(ar_id[i]);
		ar_id = tar_id;
		ar_c_min[id] = 0;
		ar_c_max[id] = 0;
	}else{	
		ar_c_min[id] = c_min;
		ar_c_max[id] = c_max;
	}
	toInput();
} /* пересчет параметров заездов Result */ 

ar_id = new Array(); ar_c_min = new Array(); ar_c_max = new Array();
function choose_room(from, to, id) {	
	// проверка на наличие id в массиве
	find = false;
	for(j=0;j<ar_id.length;j++)
		if (ar_id[j] == id) find = true;
	
	type_user = ($('#ucs_res_reserv').val() - 1);
	if ((ar_id.length > type_user) && (!find)) return;	
	
	
	$('.ucs_room'+id).each(function(){		
		delClass('#'+this.id, 'ucs_chooseroom');		
	});
	
	if (ar_c_min[id] == from) {		
		resval(-id);
		return;
	}	
	
	if (!find) {
		ar_id.push(id);
		ar_c_min[id] = from;
		ar_c_max[id] = to;		
	}
	
	c_min = ar_c_min[id];
	c_max = ar_c_max[id];	
	// проверка на наличие id в массиве
	
	
	if (c_min < from) c_max = to;
	if (c_min > from) c_min = from;
	
	for(j = c_min; j < c_max; j++) {		
		el = $('#ucs_room_'+j+'_'+id).attr('class');		
		if (($('#ucs_room_'+j+'_'+id).hasClass('ucs_norooms')) && ((j < (c_max - 1)) || (c_min == (c_max - 1)))) {
			resval(-id);
			return;	
		}		
		setClass('#ucs_room_'+j+'_'+id, 'ucs_chooseroom');		
	}	
	resval(id);
} /* изменение параметров заездов Result */ 

function elemcolor(elem) {	
	countsum(0, 1);
	
	/*setClass(elem, 'ucs_elemcolor');
	
	name = $(elem).attr('name');
	if (textfind(name, 'typenumber')) {
		id = name.split('typenumber');
		tarif = id[1];
		
		$('.ucs_reserv .ucs_tarifs').each(function(){
			if ($(this).attr('name') == ('tarif'+tarif)) $(this).attr('disabled', 'true');
		});
	} // если изменили категорию номера, убираем тарифы 	
	
	$('#ucs_reserv_but').remove();*/
} /* выделение цветом неправильно заполненного поля */

function toCost(cost, f) {
	cost = cost.toString();	
	
	var sumbol = '.';		
	if (textfind(cost, ',')) sumbol = ',';
	
	ar = cost.split(sumbol);
	
	j = 0;		
	price = '';
	for(i = ((ar[0].length)-1); i >= 0; i--) {
		if ((j%3==0) && (j > 0)) price = ' ' + price;
		price = ar[0][i] + price;
		j++;
	}

	if (ar[1]) price = price + sumbol + ar[1];
		else {
			if(f) price = price + sumbol + '00';
		}	
	
	return price;

} /* преобразование цены */

function setLenInfo(){
	$('#ucs_reserv_infolen_of').html($('#ucs_comment').val().length);
}
function checkLenInfo(){
	if ($('#ucs_comment').val().length > reserv_infolen) 
		setClass($('.ucs_reserv_infolen'), 'ucs_elemcolor');
	else 
		delClass($('.ucs_reserv_infolen'), 'ucs_elemcolor');
	setLenInfo();	
}

function checkData() {
	var check = true;
	
	$('.ucs_guest_no_exist').remove();
	
	$('.ucs_main_info input').each(function(){		
		if ((this.value == '') && (!($(this).hasClass('ucs_nocheck')))) {
			setClass(this, 'ucs_elemcolor');
			check = false;
		}	
		else delClass(this, 'ucs_elemcolor');
		
		if (this.id == 'ucs_phone') {
			if (!checkPhone(this.value)) {
				setClass(this, 'ucs_elemcolor');
				check = false;
			}
		}
		if (this.id == 'ucs_email') {
			if (!checkMail(this.value)) {
				setClass(this, 'ucs_elemcolor');
				check = false;
			}
		}
	});
	
	$('.ucs_race input').each(function(){	
		if ((this.value == '') && (!($(this).hasClass('ucs_nocheck')))) {
			setClass(this, 'ucs_elemcolor');			
			
			if (($(this).hasClass('ucs_people_f')) || ($(this).hasClass('ucs_people_n')) || ($(this).hasClass('ucs_people_b'))){
				id_field = $(this).attr('id').split('_');
				$('#ucsp_'+id_field[1]+'_'+id_field[2]).removeClass('ucs_none');
				$('.ucs_guestnumbhead_block'+id_field[1]).removeClass('ucs_none');
			}
			
			check = false;		
		}	
		else delClass(this, 'ucs_elemcolor');	
	});
	
	$('.ucs_race select').each(function(){	
		if ($(this).hasClass('ucs_elemcolor')) check = false;		
	});	
	
	if (!$('.ucs_capcha_reserv').hasClass('ucs_none')) {
		elem_class = '#ucs_send_count';
		if ($(elem_class).val() == '') {
			setClass(elem_class, 'ucs_elemcolor');	
			check = false;	
		}
		else delClass(elem_class, 'ucs_elemcolor');		
	}
	
	delClass($('.ucs_reserv_infolen'), 'ucs_elemcolor');
	if ($('#ucs_comment').val().length > reserv_infolen){
		setClass($('.ucs_reserv_infolen'), 'ucs_elemcolor');	
		check = false;	
	}
	
	return check;
} /* проверка на заполненность */ 

function getNumbGuest(link){
	guest = '';
	elem = $(link).find('.ucs_aboutguest');	
	$(elem).each(function(){
		if (!($(this).hasClass('ucs_guest_no_exist'))){
			if (guest != '') guest = guest + '#';
			
			type_numb = $(this).find('.ucsnumbtype').val();
			type_guest = $(this).find('.ucspeopletype').find("option:selected").val();
			
			guest = guest + type_numb + '_' + type_guest;
		}
	});
	return guest;	
}

function toData() {
	i = 0; 
	
	res_request_id = '';
	res_request_name = '';
	res_request_in = '';
	res_request_out = '';
	res_request_in_time = '';
	res_request_out_time = '';
	res_colnumbers = '';
	res_tarif = '';
	res_tarif_name = '';
	res_guest = '';
	res_numb_type = '';
	res_people_type = '';
	res_people_f = '';
	res_people_n = '';
	res_people_o = '';
	res_people_b = '';
	//res_people_ps = '';
	//res_people_pn = '';
	res_people_id = '';
	res_inquiry_id = '';
	res_claim_id = '';
	res_operation = '';
	res_package_service = '';
	$('.ucs_race').each(function(){		
		
		if (i > 0) {
			res_request_id = res_request_id + ';';
			res_request_name = res_request_name + ';';
			res_request_in = res_request_in + ';';
			res_request_out = res_request_out + ';';
			res_request_in_time = res_request_in_time + ';';
			res_request_out_time = res_request_out_time + ';';
			res_colnumbers = res_colnumbers + ';';
			res_tarif = res_tarif + ';';
			res_tarif_name = res_tarif_name + ';';
			res_guest = res_guest + ';';
			res_numb_type = res_numb_type + ';';
			res_people_type = res_people_type + ';';
			res_people_f = res_people_f + ';';
			res_people_n = res_people_n + ';';
			res_people_o = res_people_o + ';';
			res_people_b = res_people_b + ';';
			//res_people_ps = res_people_ps + ';';
			//res_people_pn = res_people_pn + ';';
			res_people_id = res_people_id + ';';
			res_inquiry_id = res_inquiry_id + ';';
			res_claim_id = res_claim_id + ';';
			res_operation = res_operation + ';';
			res_package_service = res_package_service + ';';
		}
		
		elem = $(this).find('.ucs_typenumbers');	
		$(elem).each(function(){
			res_request_id = res_request_id + $(this).find("option:selected").val();						
			res_request_name = res_request_name + encodeURIComponent($(this).find("option:selected").text());
		});
		
		res_guest = res_guest + getNumbGuest($(this));
		
		elem = $(this).find('.ucs_tarifs');	
		$(elem).each(function(){
			res_tarif = res_tarif + $(this).find("option:selected").val();						
			res_tarif_name = res_tarif_name + encodeURIComponent($(this).find("option:selected").text());
		});
		
		// people
		elem = $(this).find('.ucs_people_f');	
		$(elem).each(function(){
			res_people_f = res_people_f + $(this).val() + '-';
		});
				
		elem = $(this).find('.ucs_people_n');	
		$(elem).each(function(){
			res_people_n = res_people_n + $(this).val() + '-';						
		});
		
		elem = $(this).find('.ucs_people_o');	
		$(elem).each(function(){
			res_people_o = res_people_o + $(this).val() + '-';						
		});
		
		elem = $(this).find('.ucs_people_b');	
		$(elem).each(function(){
			res_people_b = res_people_b + $(this).val() + '-';						
		});
		
		// people
		elem = $(this).find('.ucs_people_id');	
		$(elem).each(function(){
			res_people_id = res_people_id + $(this).val() + '-';						
		});
		
		elem = $(this).find('.ucsnumbtype');	
		$(elem).each(function(){
			res_numb_type = res_numb_type + $(this).val() + '-';						
		});
		
		elem = $(this).find('.ucspeopletype');	
		$(elem).each(function(){
			res_people_type = res_people_type + $(this).find("option:selected").val() + '-';						
		});
		
		/*elem = $(this).find('.ucs_people_id');	
		$(elem).each(function(){			
			if (!($(this).hasClass('ucs_guest_no_exist'))){
				pclass = $(this).attr('class').split(' ');
				for(j = 0; j < pclass.length; j++){
					pvclass = pclass[j].split('_');
					if (pvclass[0] == 'ucsnumbtype') res_numb_type = res_numb_type + pvclass[1] + '-';
					if (pvclass[0] == 'ucspeopletype') res_people_type = res_people_type + pvclass[1] + '-';
				}
				res_people_id = res_people_id + $(this).val() + '-';
			}				
		});	*/	
		// people
		
		elem = $(this).find('.ucs_operation_check');	
		op_id = 1;
		$(elem).each(function(){
			if ($(this).is(':checked') == 1) {
				if (op_id > 1) res_operation = res_operation + '*';				
				op_i = readId($(this).attr('name'), 1);
				op_j = readId($(this).attr('name'), 2);				
				res_operation = res_operation + $('#ucs_operation_id_'+op_i+'_'+op_j).val() + '#' + $('#ucs_operation_code_'+op_i+'_'+op_j).val() + '#' + $('#ucs_operation_date_'+op_i+'_'+op_j).val() + '#' + $('#ucs_operation_quant_'+op_i+'_'+op_j).val();	
				op_id++;
			}
			
		});
		
		elem = $(this).find('.ucs_pservices_check');	
		op_id = 1;
		$(elem).each(function(){
			if ($(this).is(':checked') == 1) {
				if (op_id > 1) res_package_service = res_package_service + '*';				
				
				res_package_service = res_package_service + readId($(this).attr('id'), 2);	
				op_id++;
			}
			
		});
		
		res_numb_type = res_numb_type.substr(0,(res_numb_type.length-1));
		res_people_type = res_people_type.substr(0,(res_people_type.length-1));
		res_people_f = res_people_f.substr(0,(res_people_f.length-1));
		res_people_n = res_people_n.substr(0,(res_people_n.length-1));
		res_people_o = res_people_o.substr(0,(res_people_o.length-1));
		res_people_b = res_people_b.substr(0,(res_people_b.length-1));
		//res_people_ps = res_people_ps.substr(0,(res_people_ps.length-1));
		//res_people_pn = res_people_pn.substr(0,(res_people_pn.length-1));
		res_people_id = res_people_id.substr(0,(res_people_id.length-1));		
		
		res_request_in = res_request_in + $(this).find('.ucs_from').val();
		res_request_out = res_request_out + $(this).find('.ucs_to').val();
		res_request_in_time = res_request_in_time + $(this).find('.ucs_from_time').val();
		res_request_out_time = res_request_out_time + $(this).find('.ucs_to_time').val();
		res_colnumbers = res_colnumbers + $(this).find('.ucs_colnumbers').val();
		res_inquiry_id = res_inquiry_id + $(this).find('.ucs_inquiry_id').val();
		res_claim_id = res_claim_id + $(this).find('.ucs_claim_id').val();
		
		i++;
	});
	
	// защита
	var ucs_send_count = '';
	elem = $('.ucs_block').find('#ucs_send_count');	
	$(elem).each(function(){
		ucs_send_count = $(this).val();						
	});
	// защита
	
	// кредитка
	var creditcard = ''; 
	elem1 = $('.ucs_main_info').find('#ucs_creditcard');	
	elem2 = $('.ucs_main_info').find('#ucs_creditcardm');	
	elem3 = $('.ucs_main_info').find('#ucs_creditcardy');	
	$(elem1).each(function(){
		creditcard = '&creditcard=' + $(this).val();						
	});
	$(elem2).each(function(){
		creditcard = creditcard + '&creditcarddate=' + $(this).val();						
	});
	$(elem3).each(function(){
		creditcard = creditcard + $(this).val();						
	});	
	// кредитка
	
	ucs_discount_id = '';
	if ($('#ucs_discount_id').length > 0){
		ucs_discount_id = '&ucs_discount_id=' + $('#ucs_discount_id').val();
	}
	
	return '&res_request_id=' + res_request_id + '&res_request_name=' + res_request_name + '&res_request_in=' + res_request_in + '&res_request_out=' + res_request_out + '&res_request_in_time=' + res_request_in_time + '&res_request_out_time=' + res_request_out_time + '&res_colnumbers=' + res_colnumbers + '&res_tarif=' + res_tarif + '&res_tarif_name=' + res_tarif_name + '&res_guest=' + res_guest + '&res_people_f=' + res_people_f + '&res_people_n=' + res_people_n + '&res_people_o=' + res_people_o + '&res_people_b=' + res_people_b + '&res_people_id=' + res_people_id + '&res_inquiry_id=' + res_inquiry_id + '&res_claim_id=' + res_claim_id + '&res_operation=' + res_operation + '&res_package_service=' + res_package_service + '&res_send_count=' + ucs_send_count + creditcard + ucs_discount_id;
} /* формирование данных на отправку */ 

function checkMail(elem) {
	var rep = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i; 
    if (rep.exec(elem) != null) { 
        return true;
    }
	return false;	
} /* проверка E-Mail */
			
function checkPhone(elem) {
	if (phone_format_write == 0) return true;
	
	var rep = /^[\+]?[0-9]+$/; 
    if (rep.exec(elem) != null) { 
        return true;
    }
	return false;	
} /* проверка телефона */

function countsum(addrate, loader) {
	$('.ucs_guest_no_exist').remove();
	
	$('.ucs_block input').each(function(){
		$(this).attr('disabled', true);
	});
	
	data = toData();
	
	if (addrate == 1) data = data + '&addrate=1';
	
	arTarif = '';
	if (loader == 1) $('#ucs_load').show();
	$.ajax({
		type: "POST",
		url: path_file+"ajax.php",	
		data: "countsum=1&ajax=1"+data,		
		success: function(html)
		{	
			if (loader == 1) $('#ucs_load').hide();
			//$('#ucs_cph').remove();
			//$('.ucs_block').empty();
			$('.ucs_block').html(html);			
		}
	});	
} /* формирование заездов */ 

function replacePlus(text) {
	return text.replace(/\+/, '-');
} /* замена "+" на "-" для правильной передачи данных через Ajax */

function reserv() {
	$('#ucs_error').empty();
	$('#ucs_error').removeClass('ucs_textcolor');
	if (!checkData()) {
		$('#ucs_error').html(error_field);
		$('#ucs_error').addClass('ucs_textcolor');
		return;
	}	
	
	data = toData();	
	
	data = data + '&fio='+$('#ucs_fio').val() + '&phone='+ replacePlus($('#ucs_phone').val()) + '&email='+$('#ucs_email').val() + '&pay='+$('#ucs_pay').val() + '&comment='+$('#ucs_comment').val() + '&hotel='+$('#ucs_hotelid').val() + '&ucs_checkcode=' + $('#ucs_checkcode').val() + '&reservation_id='+$('#ucs_reservation_id').val() + '&allcost=' + $('#ucs_cost').html();	
	
	arTarif = '';
	$('#ucs_reserv_button').hide();
	$('#ucs_load').show();
	$('#ucs_error').html('');
	$('#ucs_error').removeClass('ucs_textcolor');
	
	$.ajax({		
		type: "POST",
		url: path_file+"ajax.php",	
		data: "addreserv=1"+data,		
		success: function(res)
		{	
			//$('#ucs_error').html(res); return;
			if ((readtext(res, 0) != '0') && (res !== '')) {			
				window.location.href = readtext(res, 1);
			}else{
				countsum(0, 1);				
			}
		}
	});	
} /* добавление брони */ 

function chQuantOperation(i, j){
	elem = '#operation_'+i+'_'+j;
	
	numb = parseInt($('#ucs_operation_quant_'+i+'_'+j).val());
	if (/[^[0-9]/.test(numb)) numb = 0;
	
	if ((!$(elem).is(':checked')) && (numb > 0)) $(elem).click();
	
	setCost();
}

function setCost() {
	coststr = changetarif();
	arCost = coststr.split(';');
	
	cost1 = parseFloat(arCost[0]);
	fullcost1 = parseFloat(arCost[1]);
	cost2 = parseFloat(dopuslugi());
	
	cost = toCost(cost1 + cost2);
	fullcost = toCost(fullcost1 + cost2);
	
	/* rate */
	$('.ucs_cost_rate span').each(function(){
		$(this).html(toCost($(this).html()));
	});
	/* rate */
	
	$('#ucs_cost').html(cost);
	if (fullcost1 > 0){
		$('#ucs_fullcost').html(fullcost);
	}	
}

function dopuslugi() {
	var sum = 0;
	//$('.ucs_operation_numb').html('0');
	
	$('.ucs_race').each(function(){
		checkbox = $(this).find('.ucs_operation_check');				
		
		$(checkbox).each(function(){
			if ($(this).is(':checked')) {
				id = $(this).attr('id');
				id = id.split('_');
				
				numb = parseInt($('#ucs_operation_quant_'+id[1]+'_'+id[2]).val());
				if (/[^[0-9]/.test(numb)) numb = 0;
				cost = numb * parseFloat($('#ucs_operation_cost_'+id[1]+'_'+id[2]).val());
				
				/* rate */
				$('#ucs_cost_rate'+id[1]).html(parseInt($('#ucs_cost_rate'+id[1]).html()) + cost);
				/* rate */
				
				if (parseInt($('#ucs_fullcost_rate'+id[1]).html()) > 0){
					$('#ucs_fullcost_rate'+id[1]).html(parseInt($('#ucs_fullcost_rate'+id[1]).html()) + cost);
				}
				
				//$('#ucs_operation_numb'+id[1]).html(parseInt($('#ucs_operation_numb'+id[1]).html()) + numb);
				
				sum = sum + cost;
			}	
		});		
	});
	
	return sum;
}

function changetarif() {
	
	var cost = 0;
	var fullcost = 0;
	typenumber = new Array();
	
	$('.ucs_race').each(function(){
		
		id_typenumber = 0;
		typenumber = $(this).find('.ucs_typenumbers');
		$(typenumber).each(function(){
			id_typenumber = parseInt($(this).find("option:selected").val());						
		});	
		
		col = $(this).find('.ucs_colnumbers').val();
		name = $(this).find('.ucs_colnumbers').attr('name');
		i = name.split('_');		
		zaezd = i[1];
		
		id_tarif = 0;
		tarifs = $(this).find('.ucs_tarifs');
		$(tarifs).each(function(){
			id_tarif = parseInt($(this).find("option:selected").val());						
		});				
				
		for(i = 0; i < arTarif.length; i++) {
			if ((arTarif[i][0] == zaezd) && (arTarif[i][1] == id_typenumber) && (arTarif[i][2] == id_tarif)) {
				cost = cost + arTarif[i][3]*col;
				
				/* rate */
				$('#ucs_cost_rate'+arTarif[i][0]).html(arTarif[i][3]*col);
				/* rate */
				
				if (arTarif[i][4] > 0){
					fullcost = fullcost + arTarif[i][4]*col;
					$('#ucs_fullcost_rate'+arTarif[i][0]).html(arTarif[i][4]*col);
				}
			}	
		}

		
	});
	
	return (cost+';'+fullcost);
	
} /* пересчет суммы заездов */ 

function reserv_remove(ename) {
	i = 0;
	col = 0;
	elem = '';
	
	$('.ucs_race').each(function(){
		span_reserv_remove = $(this).find('.ucs_spanremove');
		name = $(span_reserv_remove).attr('name');
		if (name == ename) {
			elem = this;
			i = col;
		}	
		col++;
	});	
	
	if ((col > 1) && (elem != '')) {
		$('#ucs_login_load').hide();
		$('.ucs_spanremove').remove();
		setCheckPackageService(2, i);
		$(elem).remove();
		countsum(0, 1);
	}	
} /* удаление заезда */ 

function about_reserv(id) {
	var show_info = false;
	if (!($('#ucs_load_block_'+id).hasClass('ucs_none'))) show_info = true;
	
	$('.ucs_informs').each(function(){
		$(this).addClass('ucs_none');
	});
	
	if (!($('#ucs_load_block_'+id).hasClass('ucs_show_about_reserv'))) {		
		$('#ucs_load_block_'+id).addClass('ucs_show_about_reserv');
		
		$.ajax({
			type: "POST",
			url: path_file+"ajax.php",	
			data: "about_reserv=1&id="+id,		
			success: function(html)
			{	
				$('#ucs_load_block_'+id).html(html);			
			}
		});
	}
	
	if (!(show_info)) $('#ucs_load_block_'+id).removeClass('ucs_none');
} /* информация по брони */ 

function login_reserv() {
	
	$('#ucs_error_login').empty();
	$('#ucs_error_login').removeClass('ucs_textcolor');	
	
	var login = $('#ucs_login').val();
	var	pass = $('#ucs_pass').val();
	
	check = true;
	
	if (!checkMail($('#ucs_login').val())) {
		setClass($('#ucs_login'), 'ucs_elemcolor');
		check = false;
	}
	else delClass($('#ucs_login'), 'ucs_elemcolor');
	
	if (pass == '') {
		check = false;
		setClass($('#ucs_pass'), 'ucs_elemcolor');
	}		
	else delClass($('#ucs_pass'), 'ucs_elemcolor');
		
	if (check) {	
		$('#ucs_login_load').show();
		
		$.ajax({
			type: "POST",
			url: path_file+"ajax.php",	
			data: "login_reserv=1&login="+login+"&pass="+pass+'&ucs_checkcode='+$('#ucs_checkcode').val(),		
			success: function(html)
			{	
				$('#ucs_login_load').hide();
				if (readtext(html,0) == 0) {
					$('#ucs_error_login').html(readtext(html,1));				
					$('#ucs_error_login').addClass('ucs_textcolor');				
					$('#ucs_pass').val('');
					delClass($('#ucs_login'), 'ucs_elemcolor');
					delClass($('#ucs_pass'), 'ucs_elemcolor');
					reload_capcha();					
				} else {
					$('.ucs_cph').remove();
					$('#ucs_error_login').hide();
					$('#ucs_error_login').addClass('ucs_textcolor');					
					$('#ucs_guest_auth').html(html);
					$('#ucs_reserv_but').show();
					$('.ucs_menu_noregister').hide();
					$('.ucs_menu_register').show();
					$('#ucs_reservinfo').show();
					
					$('#ucsf_0_0').val($('#ucs_f').html());
					$('#ucsn_0_0').val($('#ucs_n').html());
					$('#ucso_0_0').val($('#ucs_o').html());
					$('#ucsid_0_0').val($('#ucs_id').html());

					countsum(0, 1);
				}	
			}
		});	
	}else{
		$('#ucs_error_login').html(error_field);
		$('#ucs_error_login').addClass('ucs_textcolor');
	}	
} /* авторизация при бронировании */ 

function reload_client_capcha() {
	$.ajax({
		type: "POST",
		url: path_file+"ajax.php",	
		data: "reload_clientcapcha=1",		
		success: function(html)
		{	
			$('#ucs_reg_count').val('');
			$('#ucs_forget_count').val('');
			$('.ucs_cph').html('');
			$('.ucs_cph').html(html);
		}
	});
} /* обновление изображения */

function reload_capcha() {
	$.ajax({
		type: "POST",
		url: path_file+"ajax.php",	
		data: "reload_capcha=1",		
		success: function(html)
		{	
			$('#ucs_checkcode').val(html);			
		}
	});
	reload_client_capcha();
} 

function showLoginForm(email) {
	$('#ucs_login').val(email);
	$('#ucs_regTable').hide();
	$('#ucs_forgetTable').hide();
	$('#ucs_forget_email_tr').remove();
	$('#ucs_loginTable').css('float', 'none');
} /* отображение формы авторизации */

function reg_reserv() {
	$('#ucs_login_load').show();
	$('#ucs_error_login').empty();
	$('#ucs_error_login').removeClass('ucs_textcolor');
	
	var data = '';	
	data = data + '&name=' + $('#ucs_reg_f').val();
	data = data + '&name1=' + $('#ucs_reg_n').val();
	data = data + '&name2=' + $('#ucs_reg_o').val();
	data = data + '&mail=' + $('#ucs_reg_email').val();
	data = data + '&phone=' + replacePlus($('#ucs_reg_phone').val());	
	data = data + '&ucs_checkcode=' + $('#ucs_checkcode').val();	
	data = data + '&count=' + $('#ucs_reg_count').val();	
	
	check = true;
	$('.ucs_reg_login').each(function(){
		if ((this.value == '') && (!($(this).hasClass('ucs_nocheck')))) {
			check = false;
			setClass($(this), 'ucs_elemcolor');
		}	
		else delClass($(this), 'ucs_elemcolor');
	});
	
	if (!checkPhone($('#ucs_reg_phone').val())) {
		setClass($('#ucs_reg_phone'), 'ucs_elemcolor');
		check = false;
	}
	
	if (!checkMail($('#ucs_reg_email').val())) {
		setClass($('#ucs_reg_email'), 'ucs_elemcolor');
		check = false;
	}
	
	if (!check) {
		$('#ucs_error_login').html(error_field);
		$('#ucs_error_login').addClass('ucs_textcolor');
		$('#ucs_login_load').hide();
		return;
	}
	
	$.ajax({
		type: "POST",
		url: path_file+"ajax.php",	
		data: "reg_reserv=1"+data,		
		success: function(html)
		{	
			$('#ucs_login_load').hide();			
			if (readtext(html,0) == 1) {
				showLoginForm($('#ucs_reg_email').val());				
			}else{
				if (readtext(html,0) == 2) {
					forget_show(1);
					$('#ucs_forget_email').val($('#ucs_reg_email').val());
				}else{				
					reload_capcha();					
				}		
			}
			$('#ucs_error_login').html(readtext(html,1));
			$('#ucs_error_login').addClass('ucs_textcolor');			
		}
	});	
} /* регистрация при бронировании */ 

function forget_show(i) {
	if (i == 1) {
		$('#ucs_regTable').hide();
		$('#ucs_forgetTable').show();
	}else{
		$('#ucs_regTable').show();
		$('#ucs_forgetTable').hide();
	}
	
	$('.ucs_link_forget').removeClass('ucs_none');
	$('#ucs_link_forget_'+i).addClass('ucs_none');
} /* смена форм регистрации и напоминания пароля */ 

function forget_reserv() {
	
	$('#ucs_error_login').empty();
	$('#ucs_error_login').removeClass('ucs_textcolor');
	
	check = true;
	$('.ucs_forget_login').each(function(){
		if ($(this).val() == '') {
			check = false;
			setClass($(this), 'ucs_elemcolor');
		}	
		else delClass($(this), 'ucs_elemcolor');
	});
	
	if (!checkMail($('#ucs_forget_email').val())) {
		setClass($('#ucs_forget_email'), 'ucs_elemcolor');
		check = false;
	}
	
	if (check) {
		$('#ucs_login_load').show();
		data = '&login='+$('#ucs_forget_email').val()+'&ucs_checkcode='+$('#ucs_checkcode').val()+'&count='+$('#ucs_forget_count').val();
		$.ajax({
			type: "POST",
			url: path_file+"ajax.php",	
			data: "forget_reserv=1"+data,		
			success: function(html)
			{	
				$('#ucs_login_load').hide();
				$('#ucs_error_login').html(readtext(html,1));
				$('#ucs_error_login').addClass('ucs_textcolor');
				if (readtext(html,0) == 1) {				
					showLoginForm($('#ucs_forget_email').val());					
				}else{				
					if (readtext(html,0) == 0) $('#ucs_forget_email').val('');
					reload_capcha();
				}	
			}
		});
	}else{
		$('#ucs_error_login').html(error_field);
		$('#ucs_error_login').addClass('ucs_textcolor');
	}	
} /* напоминание пароля */ 

function changePayDesc(i) {
	$('.ucs_pay_desc div').hide();
	$('.ucs_desc_'+i).show();
} /* смена описания платежных систем */ 

function delReserv(path, message) {
	if (confirm(message)) window.location.href = path;
} /* удаление брони */

function change_current() {	
	$.ajax({
		type: "POST",
		url: path_file+"ajax.php",	
		data: "change_current=1",		
		success: function(html)
		{				
			if (html != '') {
				var current_list = html.split('*');
				var current_name = current_list[0].split(';');
				var current_price = current_list[1].split(';');
				var current_code = current_list[2].split(';');
								
				for(i = 0; i < current_name.length; i++)
					$('#ucs_current_list').append('<option value="'+current_price[i]+'*'+current_code[i]+'">'+current_name[i]+'</option>');
				$('#ucs_change_current_after').show();	
				$('#ucs_change_current_before').hide();	
			}else
			$('#ucs_change_current_before').hide();	
		}
	});
} /* запрос на смену курсов валют */

function current_show(price) {	
	price = price.split('*');	
	$('.prices').each(function(){
		new_price = $(this).find('.new_price');	
		rub = parseFloat($(this).find('.old_price').val());		
		new_p = toCost((Math.round(rub/price[0]*100)/100), 1);
		new_price.html(new_p);
		
		new_price_current = $(this).find('.new_price_code');
		new_price_current.html(price[1]);
	});
} /* перерасчет цены за номер при смене курса валюты */

function rule_confirm(elem) {
	if ($(elem).is(':checked')) $('#ucs_reserv_but').show();
		else $('#ucs_reserv_but').hide();
} /* подтверждение правил отеля */

function setDiscountCard(){
	number = $('#ucs_discount_number').val();
	name = $('#ucs_discount_name').val();
	if (number != '') {					
		$('.load_discount_img').removeClass('ucs_none');
		$('.ucs_discount_send').hide();
		$('#ucs_discount_error').html('');
		$('#ucs_discount_error').removeClass('ucs_textcolor');
		$.ajax({
			type: "POST",
			url: path_file+"ajax.php",	
			data: "discount=1&number="+number+"&name="+name,		
			success: function(html)
			{				
				$('.load_discount_img').addClass('ucs_none');
				
				if (readtext(html, 0) != '0'){
					$('.ucs_discount_block table').hide();
					$('#ucs_discount_error').html(discountAction);					
					$('#ucs_discount_id').val(readtext(html,1));					
					countsum(0, 1);
				}else{
					$('.ucs_discount_send').show();			
					$('#ucs_discount_error').html(html);
				}	
				$('#ucs_discount_error').addClass('ucs_textcolor');
			}
		});
	}
}

function clearDiscountCard(){
	$('.load_discount_img').removeClass('ucs_none');
	$('.ucs_discount_ex').hide();
	$.ajax({
		type: "POST",
		url: path_file+"ajax.php",	
		data: "discountclear=1",		
		success: function(html)
		{				
			$('#ucs_discount_id').val('');
			countsum(0, 1);
		}
	});
}

function setCheckPackageService(v, i){	
	data = '';
	$('.ucs_pservices_check').each(function(){
		if ($(this).is(':checked')) {
			idinfo = $(this).attr('id').split('_');
			
			if (data.length > 0) data = data + ';';
			data = data + idinfo[1] + '_' + idinfo[2];
		}	
	});
	
	$.ajax({
		type: "POST",
		url: path_file+"ajax.php",	
		data: "set_pack_service=1&data="+data+"&v="+v+"&i="+i,		
		success: function(html)
		{		
			if (html == 1) countsum(0, 1);
		}
	});
}
