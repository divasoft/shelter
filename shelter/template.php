<? if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die(); ?>

<? if (MODULE_USE) { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=SCHEDULE_ENCODING;?>">
<title><?=$arMessages["general"]["title"][MESSAGE_LANG];?></title>
<? } ?>
<? include_once(PATH_INC.'head.php'); ?>
<? if (MODULE_USE) { ?>
</head>
<body>
<? } ?>
<div class="ucs_mainschedule">
	<div class="ucs_schedule">
		<div class="ucs_menuschedule">			
			<div class="ucs"><a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_FIND), true);?>"><img src="<?=PATH_IMG;?>ucs.png" /></a></div>
			<div class="ucs_relative">			
				<? if (sizeof($arMessageLang) > 1){?>
				<div class="ucs_lang"><ul><? 
				for($i = 0; $i < sizeof($arMessageLang); $i++){
					?><li><a href="<?=change_address(Array('slang'=>$arMessageLang[$i]));?>"><img src="<?=PATH_IMG.$arMessageLang[$i];?>.png" alt="<?=$arMessageLang[$i];?>" /></a></li><?
				}
				?></ul></div>			
				<? } ?>
				<? if ((!($GUEST['id'] > 0)) && USE_DEBTORS_AUTH) { ?>
				<ul class="ucs_top_menu">
					<li class="ucs_menu_noregister ucs_menu_firm"><a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_LOGINFIRM), true);?>"><?=$arMessages["menu"]["loginfirm"][MESSAGE_LANG];?></a></li>
				</ul>
				<? } ?>
			<?
			if (USER_USE) {				
				$reg = '';
				$noreg = '';
				
				if (checkAuthUser()) $noreg = ' ucs_none';
					else $reg = ' ucs_none';
				?>			
				<ul class="ucs_top_menu">				
					<? if (getBookingAllow()){?>
					<li><a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_FIND), true);?>"><?=$arMessages["menu"]["find"][MESSAGE_LANG];?></a></li>
					<? } ?>
					
					<li class="ucs_menu_register<?=$reg;?>"><a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true);?>"><?=$arMessages["menu"]["cabinet"][MESSAGE_LANG];?></a></li>		
					
					<? if ($GUEST['type'] == 0) { ?>								
					<li class="ucs_menu_register<?=$reg;?>"><a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_ABOUTUSER), true);?>"><?=$arMessages["menu"]["personal"][MESSAGE_LANG];?></a></li> 
					<? } ?>
					
					<li class="ucs_menu_register<?=$reg;?>"><a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_EXIT), true);?>"><?=$arMessages["menu"]["exit"][MESSAGE_LANG];?></a></li>
					<li class="ucs_menu_noregister<?=$noreg;?>"><a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_LOGINUSER), true);?>"><?=$arMessages["menu"]["login"][MESSAGE_LANG];?></a></li>
					<li class="ucs_menu_noregister<?=$noreg;?>"><a href="<?=change_address(Array(MODULE_SHEDULE=>PAGE_CREATEUSER), true);?>"><?=$arMessages["menu"]["create"][MESSAGE_LANG];?></a></li>
					
				</ul>					
			<? } ?>		
				
			</div>			
		</div>
		
		<? show_step(); ?>
		
		<div class="ucs_dataschedule">
		<?	
			if (file_exists($arResult['include_modules'])) include_once($arResult['include_modules']);	
			if (file_exists($arResult['include_controller'])) include_once($arResult['include_controller']);	
			include_once($arResult['include_template']);
		?>
		<div class="ucs_load_img"><img src="<?=PATH_IMG;?>load.gif" id="ucs_load" class="ucs_none" /></div>
		</div>		
	</div>
</div>

<? if (MODULE_USE) { ?>
</body>
</html>
<? } ?>