<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	$arResult = Array();
	
	$arResult['include_modules'] = PATH_MODULES.PAGE_FIND.'.php';
	$arResult['include_controller'] = PATH_CONTOLLER.PAGE_FIND.'.php';
	$arResult['include_template'] = PATH_TEMPLATE.PAGE_FIND.'.php';
	
	if (isset($_GET[MODULE_SHEDULE])) {
		if (((file_exists(PATH_TEMPLATE.$_GET[MODULE_SHEDULE].'.php')) || (file_exists(PATH_MODULES.$_GET[MODULE_SHEDULE].'.php')) || (file_exists(PATH_CONTOLLER.$_GET[MODULE_SHEDULE].'.php'))) && (preg_match("/^[0-9a-z]+$/i", $_GET[MODULE_SHEDULE]))) {			
			$arResult['include_modules'] = PATH_MODULES.protection($_GET[MODULE_SHEDULE]).'.php';
			$arResult['include_controller'] = PATH_CONTOLLER.protection($_GET[MODULE_SHEDULE]).'.php';
			$arResult['include_template'] = PATH_TEMPLATE.protection($_GET[MODULE_SHEDULE]).'.php';
		}
	}
?>