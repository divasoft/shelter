<div>Уважаемый гость, #FIO#!</div>
<div>Благодарим Вас за выбор нашего отеля!</div>
<div>Ниже Вы найдете номер подтверждения и дополнительные детали Вашего бронирования.</div>

<p>Номер подтверждения <span style="font-size:21px;">#RESID#</span></p>

^<p>
<div style="padding-bottom:3px;">Заезд: <span style="font-size:17px;">#DATE_FROM#</span></div>
<div style="padding-bottom:3px;">Выезд: <span style="font-size:17px;">#DATE_TO#</span></div>
<div style="padding-bottom:3px;">Номер: <span style="font-size:17px;">#TYPE#</span></div>
<div style="padding-bottom:3px;">Тариф: <span style="font-size:17px;">#TARIF#</span></div>
<div style="padding-bottom:3px;">Пакеты услуг: <span style="font-size:17px;">#PACKAGESERVICE#</span></div>
<div style="padding-bottom:3px;">Доп. услуги: <span style="font-size:17px;">#SERVICE#</span></div>
<div style="padding-bottom:3px;">Кол-во ночей: <span style="font-size:17px;">#NIGHTS#</span></div>
<div style="padding-bottom:3px;">Кол-во гостей: <span style="font-size:17px;">#PEOPLES#</span></div>
<div>Гости: <span style="font-size:17px;">#PEOPLESNAME#</div>
</p>^

<p>К оплате <span style="font-size:21px;">#SUM#</span> #CURRENT#</p>

<p>Для просмотра информации о брони необходимо перейти по ссылке <br />#LINK#</p>

<p>Если ссылка не работает, скопируйте и вставьте в адресную строку браузера</p>