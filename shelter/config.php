<?php	
	if (!isset($_SESSION)) {session_start();}
	error_reporting(0);
    
        // DIVASOFT - Библиотека с доп.функциями
        require_once $_SERVER["DOCUMENT_ROOT"]."/divasoft/DivasoftShelter.class.php";

	/*
		В данном файле представлены настройки
			- TCPHost и TCPPort для подключения к shelter-online-server
			- пароль для выполнения запросов
			- описания для номеров и тарифов
			- настройки платежных систем
			
		Для дополнительных настроек	модуля необходимо перейти в БРАУЗЕРЕ по адресу <Страница отображения модуля>/shelter/settings/
		Логин admin
		Пароль shelter
	*/
	
	define('XML_PATH', '127.0.0.1'); // TCPHost shelter-online-server (тестирование на 127.0.0.1 92.39.135.234)
	define('XML_PORT', '7779'); // TCPPort shelter-online-server
	define('KEY_SECURITY_SHELTER', 'shelter'); // пароль указан в настройках Shelter, если не используется задать пустым ''	
	
	// -------------------------- ОПИСАНИЕ
	// описание для номера, выводится при поиске номеров и просмотре заездов, при наведении на Тип номера
	$LINK_ROOM = Array(
		//'codeRoom1'=>Array('codeRoom1.html', 'http://localhost/hotel/?page=rooms&room=codeRoom1'),
		//'codeRoom2'=>Array('codeRoom2.html', 'http://localhost/hotel/?page=rooms&room=codeRoom2'),
		//'codeRoom3'=>Array('codeRoom3.html', 'http://localhost/hotel/?page=rooms&room=codeRoom3')
	);
	// 'код номера' => Array('название файла с описанием в разделе /about/rooms/', 'ссылка при нажатии, ввести путь на основном сайте для детального просмотра информации о номере'),
	
	// описание для тарифов, выводится при поиске номеров и просмотре заездов, при наведении на Тип тарифа
	$LINK_RATE = Array(
		//'codeRate1'=>Array('codeRate1.html'),
		//'codeRate2'=>Array('codeRate2.html')
	);
	// 'код тарифа' => Array('название файла с описанием в разделе /about/rates/'),	
	// -------------------------- ОПИСАНИЕ
	
	
	// -------------------------- ОПЛАТА
	// настройка платежной системы ROBOKASSA	
	// соответствует файлу /inc/pm/rb37fgb4f.php
    
    // Боевой конфиг
	define('ROBOKASSA_MrchLogin', '***'); // логин платежной системы ROBOKASSA
	define('ROBOKASSA_mrh_pass1', '***'); // пароль 1 платежной системы ROBOKASSA
	define('ROBOKASSA_mrh_pass2', '***'); // пароль 2 платежной системы ROBOKASSA
	define('ROBOKASSA_PURCHASE', 'https://auth.robokassa.ru/Merchant/Index.aspx?'); // отправка данных в ROBOKASSA (isTest=1& - режим тестирования)
	
    /*
    // Тестовый конфиг
	define('ROBOKASSA_MrchLogin', '***'); // логин платежной системы ROBOKASSA
	define('ROBOKASSA_mrh_pass1', '***'); // пароль 1 платежной системы ROBOKASSA
	define('ROBOKASSA_mrh_pass2', '***'); // пароль 2 платежной системы ROBOKASSA
	define('ROBOKASSA_PURCHASE', 'https://auth.robokassa.ru/Merchant/Index.aspx?isTest=1&'); // отправка данных в ROBOKASSA (isTest=1& - режим тестирования)
	*/
    
	// настройка платежной системы ASSIST	
	// соответствует файлу /inc/pm/ab37ggh4.php
	define('ASSIST_MERCHANT_ID', '914136'); // ID - магазина платежной системы Assist
	define('ASSIST_LOGIN', 'testucsrudiler'); // логин платежной системы Assist
	define('ASSIST_PASSWORD', 'testucsrudiler1'); // пароль платежной системы Assist
	define('ASSIST_PURCHASE', 'https://test.paysecure.ru/pay/order.cfm'); // отправка данных в платежную систему Assist
	define('ASSIST_ORDERSTATE', 'https://test.paysecure.ru/orderstate/orderstate.cfm'); // проверка заказа платежной системы Assist	
	
	// настройка платежной системы YANDEX	
	// соответствует файлу /inc/pm/y3bg64bgy.php
	define('YANDEX_SHOPID', ''); // shopId магазина платежной системы Yandex
	define('YANDEX_SCID', ''); // scId магазина платежной системы Yandex
	define('YANDEX_SHOPPASSWORD', ''); // пароль платежной системы Yandex
	define('YANDEX_PAYMENTTYPE', 'AC'); // тип оплаты (PC – оплата из кошелька Яндекс.Деньги, AC – оплата с произвольной банковской карты)
	define('YANDEX_REDIRECT', 'https://demomoney.yandex.ru/eshop.xml'); // отправка данных в платежную систему Yandex
	
	// настройка Банка Русский Стандарт
	// соответствует файлу /inc/pm/rs5ogvu5hf.php	
	define('BANK_RS_URL_TRANSACTION', 'https://testsecurepay.rsb.ru:9443/ecomm2/MerchantHandler');
	define('BANK_RS_URL_REDIRECT', 'https://testsecurepay.rsb.ru/ecomm2/ClientHandler?trans_id=');
	define('BANK_RS_UNIX_PATH_KEYS', '');
	define('BANK_RS_TSP_ID', '');
	
	// настройка платежной системы Uniteller
	// соответствует файлу /inc/pm/ug8ht74hg.php	
	define('UNITELLER_SHOP_ID', '');
	define('UNITELLER_LOGIN', '');
	define('UNITELLER_PASS', '');
	define('UNITELLER_URL_SEND', 'https://test.wpay.uniteller.ru/pay/');	
	define('UNITELLER_URL_CHECK', 'https://test.wpay.uniteller.ru/results/');	
	
	// настройка платежной системы UCS
	// соответствует файлу /inc/pm/ucjhb8tjb5j.php	
	define('UCSPAY_URL_MODULE', 'http://shelter-online.ucs.ru/');
	define('UCSPAY_LOGIN', '');
	define('UCSPAY_PASS', '');
	define('UCSPAY_SHOPID', '');
	define('UCSPAY_URL_PAY', 'https://tws.egopay.ru/order/v2/');
	// -------------------------- ОПЛАТА
	
	define('PATH_CORRECT', true);
	define('PATH_SECTION', realpath(dirname(__FILE__)).'/');
	include_once(PATH_SECTION.'inc/autoconfig.php');	
	
?>