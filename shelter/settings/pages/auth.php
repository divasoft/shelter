<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true || USER_CHECK)die();
	
	if (($_POST['login'] != '') && ($_POST['pass'] != '')){
		if (authUser(protection($_POST['login']), setAlgPass(getAlgPass($_POST['pass'])))){
			$_SESSION['ShelterUser']['Settings']['Login'] = protection($_POST['login']);
			$_SESSION['ShelterUser']['Settings']['Code'] = setAlgPass(getAlgPass($_POST['pass']));
		}
		header('location: '.$_SERVER['PHP_SELF']);
		exit;
	}
?>
<div class="forms">
	<h1><?=$arMessages["auth"]["title"][CODE_LANG];?></h1>
	<form action="" method="POST">
		<table cellpadding="0" cellspacing="0">
		<tr>
			<td><?=$arMessages["auth"]["login"][CODE_LANG];?></td>
			<td><input type="text" name="login" /></td>
		</tr>
		<tr>
			<td><?=$arMessages["auth"]["pass"][CODE_LANG];?></td>
			<td><input type="password" name="pass" /></td>
		</tr>
		<tr>
			<td colspan="2" align="right"><a href="<?=$_SERVER['PHP_SELF'];?>?page=forgotpassword"><?=$arMessages["auth"]["forgotpassword"][CODE_LANG];?></a></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="<?=$arMessages["auth"]["send"][CODE_LANG];?>" /></td>
		</tr>
		</table>
	</form>
</div>