<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true || (!USER_CHECK))die();
	
	$mess = '';
	if (($_POST['pass1'] != '') && ($_POST['pass2'] != '')){
		if ($_POST['pass1'] == $_POST['pass2']){
			setPass($_POST['pass1']);
			$_SESSION['ShelterUser']['Settings']['Code'] = setAlgPass(getAlgPass($_POST['pass1']));
			header('location: '.$_SERVER['PHP_SELF']);
			exit;
		}else
			$mess = $arMessages["changepass"]["pass_different"][CODE_LANG];
	}
?>
<div class="forms">	
	<h1><?=$arMessages["changepass"]["title"][CODE_LANG];?></h1>
	<div class="error"><?=$mess;?></div>
	<form action="" method="POST">
		<table cellpadding="0" cellspacing="0">
		<tr>
			<td><?=$arMessages["changepass"]["pass1"][CODE_LANG];?></td>
			<td><input type="password" name="pass1" /></td>
		</tr>
		<tr>
			<td><?=$arMessages["changepass"]["pass2"][CODE_LANG];?></td>
			<td><input type="password" name="pass2" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="<?=$arMessages["changepass"]["send"][CODE_LANG];?>" /></td>
		</tr>
		</table>
	</form>
</div>