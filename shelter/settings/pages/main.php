<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true || (!USER_CHECK))die();
	
	$file = simplexml_load_file(XML_SETTINGS);		
	
	if ($_POST['save'] != ''){		
		$arNewSettings = Array();
		foreach($_POST as $key=>$val){
			$val = trim($val); //$val = str_replace(' ', '', protection($val));			
			$arNewSettings[$key] = $val;
		}		
		foreach($file->category as $c=>$category)
			foreach($category->param as $p=>$param){
				if ($arNewSettings[strval($param->attributes()->name)] == ''){
					if ($param->attributes()->empty != 1) $arNewSettings[strval($param->attributes()->name)] = 0;
				}	
				$param->value = $arNewSettings[strval($param->attributes()->name)];
			}		
		saveParams($file->asXml());
		header('location: '.$_SERVER['PHP_SELF']);
		exit;
	}
	
	$codeLang = LANG_SHOW;
	$arSettings = Array();	
	foreach($file->category as $c=>$category){
		$arData = Array();
		foreach($category->param as $p=>$param){
			$name = strval($param->attributes()->name);
			$text = strval($param->text->$codeLang);
			$desc = strval($param->desc->$codeLang);
			$type = intval($param->attributes()->type);
			$empty = '';
			if (isset($param->attributes()->empty)){
				if (intval($param->attributes()->empty) > 0) $empty = 'empty_allow';
			}	
			$value = strval($param->value);
			$select = Array();
			if ($type == 3){
				if (isset($param->variants)){
					foreach($param->variants->$codeLang->variant as $list_elem=>$list_val){
						$select[strval($list_val->attributes()->v)] = strval($list_val);
					}
				}
			}						
			$arData[$name]['text'] = $text;
			$arData[$name]['desc'] = str_replace('-BR-', '<br />', $desc);
			$arData[$name]['type'] = $type;
			$arData[$name]['value'] = $value;
			$arData[$name]['select'] = $select;
			$arData[$name]['empty'] = $empty;			
		}
		$arSettings[strval($category->attributes()->$codeLang)] = $arData;
	}	
	//echo '<pre>'; print_r($arSettings); echo '</pre>'; 
	
	/*
		type = 0 - checkbox
		type = 1 - text
		type = 2 - array
		type = 3 - select
	*/
	if ($_SESSION['ShelterUser']['Settings']['Code'] == PATH_DF){?><div class="error_ps"><?=$arMessages["main"]["passdf"][CODE_LANG];?></div><?}
	?><form action="" method="POST" onsubmit="if (!checkForm()) return false"><table cellpadding="0" cellspacing="0" class="settings"><?
	$numb_categ = 1;
	foreach($arSettings as $category=>$arDataCategory){
		$numb_param = 1;
		?><tr class="grey"><td class="numb_categ"><?=$numb_categ;?></td><td colspan="2" class="category"><?=$category;?></td></tr><?
		foreach($arDataCategory as $key=>$arData){
			$desc = '';
			if ($arData['desc'] != '') $desc = '<div class="desc">'.$arData['desc'].'</div>';
			
			?><tr><td class="numb_param"><?=$numb_categ.'.'.$numb_param;?></td><?
			if ($arData['type'] == 0){
				?><td colspan="2"><input type="checkbox" id="check<?=$key;?>" class="checkelem <?=$arData['empty'];?>" <?if ($arData['value'] == 1) {?>checked<?}?>/> <label for="check<?=$key;?>"><?=$arData['text'];?></label><?=$desc;?><input type="hidden" name="<?=$key;?>" id="<?=$key;?>" value="<?=$arData['value'];?>" /></td><?
			}
			elseif ($arData['type'] == 1){
				$link = '';
				if ($key == 'MAIN_URL_D') $link = ' [<a href="'.$arData['value'].'" target="_blank">'.$arMessages["main"]["go"][CODE_LANG].'</a>]';
				?><td><?=$arData['text'].$link;?><?=$desc;?></td>
				<td class="param"><input type="text" name="<?=$key;?>" class="<?=$arData['empty'];?>" value="<?=$arData['value'];?>" /></td><?
			}
			elseif ($arData['type'] == 2){
				?><td><?=$arData['text'];?><?=$desc;?></td>
				<td class="param"><input type="text" name="<?=$key;?>" class="<?=$arData['empty'];?>" value="<?=$arData['value'];?>" /><div class="desc"><?=$arMessages["main"]["multidata"][CODE_LANG];?> "<?=DATA_SHARE;?>" (<?=$arMessages["main"]["multidata_desc"][CODE_LANG];?>)</div></td><?
			}
			elseif ($arData['type'] == 3){				
				?><td><?=$arData['text'];?><?=$desc;?></td>
				<td class="param"><select name="<?=$key;?>" class="<?=$arData['empty'];?>">
				<?
					foreach($arData['select'] as $select_id=>$select_val){
						$s = '';
						if ($arData['value'] == $select_id) $s = ' selected';
						?><option value="<?=$select_id;?>"<?=$s;?>><?=$select_val;?></option><?
					}
				?>
				</select></td><?
			}
			?></tr><?
			$numb_param++;
		}
		$numb_categ++;
	}
	?><tr><td colspan="3" align="center"><div class="error_mess"><?=$arMessages["main"]["error"][CODE_LANG];?></div><input type="submit" name="save" value="<?=$arMessages["main"]["save"][CODE_LANG];?>" /></td></tr></table>
	</form><?
?>
<script type="text/javascript">
	$('.checkelem').click(function(){
		id = $(this).attr('id').split('check');
		
		numb = 0;
		if ($(this).is(':checked') == 1) numb = 1;
		
		$('#'+id[1]).val(numb);
	});
	
	function checkForm(){
		$('.error_mess').hide();
		$('input').removeClass('empty_allow_check');
		
		var error = false;
		$('input').each(function(){
			if (($(this).val() == '') && (!$(this).hasClass('empty_allow'))) {
				$(this).addClass('empty_allow_check');
				error = true;
			}	
		});
		
		if (error) {
			$('.error_mess').show();
			return false;			
		}	
		return true;
	}
</script>