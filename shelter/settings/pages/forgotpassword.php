<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true || USER_CHECK)die();
	
	if (($_POST['login'] != '') && ($_POST['pass'] != '')){
		if (authUser(protection($_POST['login']), getAlgPass($_POST['pass']))){
			$_SESSION['ShelterUser']['Settings']['Login'] = protection($_POST['login']);
			$_SESSION['ShelterUser']['Settings']['Code'] = getAlgPass($_POST['pass']);
		}
		header('location: '.$_SERVER['PHP_SELF']);
		exit;
	}
?>
<div class="btext">
	<div class="text"><?=$arMessages["forgotpassword"]["text"][CODE_LANG];?></div>
	<div><a href="<?=$_SERVER['PHP_SELF'];?>"><?=$arMessages["forgotpassword"]["back"][CODE_LANG];?></a></div>
</div>