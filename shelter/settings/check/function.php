<?
	//include_once('config.php');
	
	$PHPVERSION = 5.3;
	
	$arCheck = Array(		
		1 => 'Проверка сессий',
		2 => 'Версия PHP '.$PHPVERSION.' и выше',
		3 => 'Проверка SimpleXMLElement',
		4 => 'Проверка simplexml_load_string()',
		5 => 'Проверка создания изображения',		
		6 => 'Проверка функций bcmath',
		7 => 'Проверка функций cUrl (для Ассист)',
		8 => 'Проверка функции pfsockopen()', // hotel		
		9 => 'Проверка функции file_get_contents()',
		10 => 'Получение информации с <span class="INFO_XML_PATH"></span>',		
		11 => 'Проверка JSON', // abonement
		12 => 'Проверка остальных функций',
	);
		
	$OK = 'OK';
	$ERROR = 'ERROR';
	$ADDRESS = 'www.microsoft.com';
	$ENCODE = 'windows-1251';
	$IMAGES = './images/';
?>