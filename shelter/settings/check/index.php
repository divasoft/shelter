<? include_once('function.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$ENCODE;?>">
<script type="text/javascript" src="jquery.js"></script>
<title>Модуль Тест-Онлайн</title>
<script type="text/javascript">	
	var numbCheck = <?=sizeof($arCheck) - 1;?>;
	var checkProgram = 0;
	var isCheck = 0;
	var arCheckId = [<?foreach($arCheck as $key=>$val){
		echo "'".$key."',";
	}?>];
	var arCheckInfo = [<?foreach($arCheck as $key=>$val){
		echo "'".$val."',";
	}?>];
	
	var XML_PATH = '', XML_PORT = '', XML_SIGN = '', XML_VERSION = '', idField = '';
	
	function getCheck(){
		idField = arCheckId[isCheck];		
		$('#check_'+idField+' .res').html('<img src="./images/load.gif" />');
		
		$.ajax({
			type: "POST",
			url: "ajax.php",	
			data: "check="+arCheckId[isCheck]+"&checkProgram="+checkProgram+"&XML_PATH="+XML_PATH+"&XML_PORT="+XML_PORT+"&XML_SIGN="+XML_SIGN+"&XML_VERSION="+XML_VERSION,		
			success: function(html)
			{
				$("#isCheck").html(isCheck + 1);				
				$('#check_'+idField+' .res').html(html);
				
				ok = false;
				if (html == 'OK') $('#check_'+idField+' .res').addClass('ok');
				
				isCheck++;		
				if (numbCheck >= isCheck) getCheck();
			}
		});	
	}
	
	function showFuncProgram(i){
		if (i == 0) {
			$('#check_8').show();
			$('#check_11').hide();
		}else{
			$('#check_8').hide();
			$('#check_11').show();
		}
	}
	
	function changeProgram(i){
		checkProgram = i;
		
		if (checkProgram == 0){
			$('.changeProgram0').show();
			$('.changeProgram1').hide();
			$('#XML_VERSION').val('9');
			$('#XML_PORT').val('7779');
			$('#XML_SIGN').val('shelter');
			
			showFuncProgram(i);
		}
		if (checkProgram == 1){
			$('.changeProgram1').show();
			$('.changeProgram0').hide();
			$('#XML_VERSION').val('0');
			$('#XML_PORT').val('7790');
			$('#XML_SIGN').val('');
			
			showFuncProgram(i);
		}
		
		$('.INFO_XML_PATH').html($('#XML_PATH').val()+':'+$('#XML_PORT').val());
		$('.res').removeClass('ok');	
		$('.res').html('-');	
	}
	
	function getChecked(){
		isCheck = 0;
		
		$('.res').removeClass('ok');	
		$('.res').html('-');	
		
		XML_PATH = $('#XML_PATH').val();
		XML_PORT = $('#XML_PORT').val();
		XML_SIGN = $('#XML_SIGN').val();
		XML_VERSION = $('#XML_VERSION').val();
		$('.INFO_XML_PATH').html(XML_PATH+':'+XML_PORT);
		
		getCheck();
	}
	
	$(document).ready(function(){
		changeProgram(0);
		XML_PATH = $('#XML_PATH').val();
		XML_PORT = $('#XML_PORT').val();
		XML_SIGN = $('#XML_SIGN').val();
		XML_VERSION = $('#XML_VERSION').val();
		$('.INFO_XML_PATH').html(XML_PATH+':'+XML_PORT);		
	});
	
</script>
</head>
<body>
	<div><img src="./images/ucs.png" /></div>
	<p class="info">Модуль Тест-Онлайн устанавливается на тот сервер (хостинг), где будет установлен модуль БРОНИРОВАНИЯ. Если все проверки прошли со статусом <span class="ok">ОК</span>, значит модуль БРОНИРОВАНИЯ можно настраивать и устанавливать на сервер (хостинг) для дальнейшего тестирования. Если результаты не приходят продолжительное время, попробуйте закрыть и отрыть браузер и заново запустить проверку.</p>
	<p><select onchange="changeProgram(this.value)">
		<option value="0">Shelter (shelter-online-server)</option>
		<option value="1">Абонемент (connecter)</option>
	</select></p>
	<table cellpadding="5" cellspacing="0" border="0" class="restable" width="250px">
	<tr>
		<td width="100px">Хост</td>
		<td><input type="text" id="XML_PATH" value="92.39.135.234" /></td>
	</tr>
	<tr>
		<td>Порт</td>
		<td><input type="text" id="XML_PORT" value="7779" /></td>
	</tr>
	<tr>
		<td>Пароль</td>
		<td><input type="text" id="XML_SIGN" value="shelter" /></td>
	</tr>
	<tr class="changeProgram0">
		<td><span class="changeProgram0">Версия</span></td>
		<td><input type="text" id="XML_VERSION" value="9" /></td>
	</tr>	
	<tr>
		<td></td>
		<td align="right"><input type="button" value="Проверить" onclick="getChecked();" /></td>
	</tr>
	</table>
	
	<table id="results" cellpadding="5" cellspacing="0" border="0" class="restable">
	<tr>
		<td width="350px" class="results_name">Название функции</td>
		<td class="results_name">Стастус</td>
	</tr>
	<?foreach($arCheck as $key=>$val){?>
	<tr id="check_<?=$key;?>">		
		<td><?=$val;?></td>
		<td class="res">-</td>
	</tr>
	<?}?>
	</table>		
</body>
</html>
<style type="text/css">
	.checkNumbInfo {font-size:16px; font-weight:bold;}
	.restable {float:left; margin-right:50px;}
	.results_name {font-weight:bold;}
	#results {border-left:1px #ccc solid; border-top:1px #ccc solid; margin-top:5px; font-size:16px;}
	#results td {border-right:1px #ccc solid; border-bottom:1px #ccc solid; padding:5px 10px;}
	#results .res {color:#ff0000;}
	.info {background:#eee; padding:10px; font-size:14px; line-height:1.5em;}
	.ok {color:#07a707;}
	#results .ok {color:#07a707;}
</style>