<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	$code = 0;
	$arCodeLangForXml = Array('ru', 'en', 'lt', 'bg');
	for($i = 0; $i < sizeof($arCodeLangForXml); $i++){
		if ($arCodeLangForXml[$i] == LANG_SHOW) $code = $i;
	}	
	define('CODE_LANG', $code);
	
	if (isset($_GET['exit'])){
		$_SESSION['ShelterUser']['Settings'] = Array();
		header('location: '.$_SERVER['PHP_SELF']);
		exit;
	}
	
	$user = false;
	if ((isset($_SESSION['ShelterUser']['Settings']['Login'])) && ($_SESSION['ShelterUser']['Settings']['Login'] != '') && (isset($_SESSION['ShelterUser']['Settings']['Code'])) && ($_SESSION['ShelterUser']['Settings']['Code'] != '')) 
		$user = authUser($_SESSION['ShelterUser']['Settings']['Login'], $_SESSION['ShelterUser']['Settings']['Code']);
	define('USER_CHECK', $user);
	
	if (USER_CHECK){
		define('PATH_DF', setAlgPass('ddbbfb5cf855d894d1dfb946e25b7866'));	
	}
?>