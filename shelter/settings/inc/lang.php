<?php
	/* 
	массив выводимых записей на странице
	первый уровень массива - название страницы, второй уровень - массив записей с разделением на языки 
	для добавления языка, необходимо добавить во все записи второго уровня соответствующие слова 
	*/	
	$arMessages = Array(
		"general" => Array(	
			"title" => Array("Shelter-Online Settings","Shelter-Online Settings","","Shelter-Online Настройки"),
		),
		"auth" => Array(	
			"title" => Array("Вход","Authorization","","Вход"),
			"login" => Array("Логин","Login","","Потребител"),
			"pass" => Array("Пароль","Password","","Парола"),
			"send" => Array("Войти","Login","","Вход"),			
			"forgotpassword" => Array("Забыли пароль?","Forgot your password?","","Забравена парола?"),			
		),
		"forgotpassword" => Array(	
			"title" => Array("Напоминание пароля","Password reminder","","Напомняне за парола"),
			"text" => Array("Для восстановления пароля отправьте письмо на <b>d.lahno@ucs.ru</b>","To recover your password send mail to <b>d.lahno@ucs.ru</b>","","За възстановяване на парола, изпратете писмо на <b>d.lahno@ucs.ru</b>"),
			"back" => Array("Вернуться на главную","Return Home","","Връщане в началото"),	
		),
		"changepass" => Array(	
			"title" => Array("Изменение пароля","Change password","","Промяна на парола"),
			"login" => Array("Логин","Login","","Вход"),
			"pass1" => Array("Новый пароль","New password","","Нова парола"),
			"pass2" => Array("Подтверждение пароля","Confirm password","","Потвърдете паролата"),
			"pass_different" => Array("Пароли не совпадают!","Passwords do not match!","","Паролите не съвпадат"),
			"send" => Array("Изменить","Change","","Промени"),
		),
		"main" => Array(	
			"first" => Array("Настройки модуля","Module settings","","Настройки на модула"),
			"go" => Array("ОТКРЫТЬ","OPEN","","ОТВОРИ"),
			"changepass" => Array("Изменить пароль","Change password","","Промяна на парола"),
			"exit" => Array("Выйти","Logout","","Изход"),
			"multidata" => Array("перечислить через ","enumerate through","","изброй чрез"),
			"multidata_desc" => Array("точка с запятой","semicolon","","точка и запетая"),
			"not_use" => Array("не использовать","do not use","","не използвайте"),
			"save" => Array("Сохранить","Save","","Съхрани"),
			"error" => Array("Проверьте правильность заполнения всех полей. Ошибки выделены цветом","Check that all the fields. Errors highlighted","","Проверете правилно ли са попълнени всички полета! Грешните са маркирани с цветен контур."),
			"passdf" => Array("Внимание! Используется стандартный пароль!","Caution: Use the default password!","","Внимание! Използва се стандартна парола!"),
		),
	);	
?>