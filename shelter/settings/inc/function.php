<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	function protection($text){
		return htmlspecialchars(stripslashes(trim($text)));
	}
	
	function setAlgPass($text){
		$text = protection($text);
		return md5(date('dmY', time()).$text);
	}
	
	function getAlgPass($text){
		$text = protection($text);
		return md5($text.'ucs');
	}
	
	function authUser($login, $pass){
		$file = simplexml_load_file(XML_PASS);
		foreach($file->param as $k=>$param){
			if ((strval($param->attributes()->login) == protection($login)) && (setAlgPass(strval($param)) == protection($pass))) return true;
		}			
		return false;
	}	
	
	function setPass($new_pass){
		$file = simplexml_load_file(XML_PASS);
		$arData = Array();
		foreach($file->param as $k=>$param){
			$arData[strval($param->attributes()->login)] = strval($param);
		}	
		
		$xml = new DomDocument('1.0','utf-8');
		$content = $xml->appendChild($xml->createElement('xml'));
		foreach($arData as $login=>$pass){
			if ($login == 'admin') $pass = getAlgPass($new_pass);
			
			$param = $content->appendChild($xml->createElement('param'));			
			
			$attribute = $xml->createAttribute('login');
			$attribute->value = $login;
			$param->appendChild($attribute);
			
			$param->appendChild($xml->createTextNode($pass));			
		}
		$xml->formatOutput = true;
		$xml->save(XML_PASS);
	}
	
	function saveParams($text){		
		$xml = new DomDocument('1.0','utf-8');		
		$xml->loadXML($text);
		$xml->formatOutput = true;
		$xml->save(XML_SETTINGS);
	}
	
	function settingsSetTime($str){
		$arTime = explode('-', $str);
		$val = 720;
		
		if (sizeof($arTime) == 2){
			if (($arTime[0] >= 0) && ($arTime[0] <= 23) && ($arTime[1] >= 0) && ($arTime[1] <= 59)){
				$val = $arTime[0]*60+$arTime[1];
			}
		}
		return $val;
	}
	
	function settingsGetTime($minutes){
		$hour = floor($minutes/60);
		$min = $minutes - $hour*60;
		if ($hour < 10) $hour = '0'.$hour;
		if ($min < 10) $min = '0'.$min;
		
		return $hour.'-'.$min;
	}
	
	function getVersion(){
		if (file_exists('../info.php')){
			$f = file_get_contents('../info.php');
			$text = explode('&', $f);
			$s = trim($text[0]);
			if (strlen($s) > 20) $s = '';
			return $s;
		}
	}
?>