<?
	ob_start();
	include_once('config.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?=$arMessages["general"]["title"][CODE_LANG];?></title>
<script type="text/javascript" src="js/jquery.js"></script>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>	
	<div class="page">
		<?	
		if (!USER_CHECK){
			if ($_GET['page'] == 'forgotpassword')
				include_once(PATH_PAGE.'forgotpassword.php');
			else	
				include_once(PATH_PAGE.'auth.php');
		}else{
			?>
			<div class="menu">
				<a href="<?=$_SERVER['PHP_SELF'];?>"><img src="<?=PATH_IMG;?>logo.jpg" /></a>
				<div>
					<a href="<?=$_SERVER['PHP_SELF'];?>"><?=$arMessages["main"]["first"][CODE_LANG];?></a> | 
					<a href="<?=$_SERVER['PHP_SELF'];?>?page=changepass"><?=$arMessages["main"]["changepass"][CODE_LANG];?></a> | 				
					<a href="<?=$_SERVER['PHP_SELF'];?>?exit=1"><?=$arMessages["main"]["exit"][CODE_LANG];?></a>
				</div>
			</div>
			<?
			$page = PATH_PAGE.'main.php';
			if ((file_exists(PATH_PAGE.$_GET['page'].'.php')) && (preg_match("/^[0-9a-z]+$/i", $_GET['page']))) $page = PATH_PAGE.$_GET['page'].'.php';	
			include_once($page);				
		}		
		?>
		<div class="cp">
			Shelter-Online <a href="http://www.ucs.ru" target="_blank">UCS</a><br />
			<?=getVersion();?> Dmitry Lakhno<br />
			<a href="check/" target="_blank">check server</a><br />
			<? if (SHOW_PHPINFO || ((!SHOW_PHPINFO) && USER_CHECK)){?><a href="php_info.php" target="_blank">php_info</a><?}?>
		</div>	
	</div>
</body>	
</html>	
<?ob_end_flush();?>