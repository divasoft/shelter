Version 2.45 & creator DMITRYLAKHNO

2.45
Изменен регистр для логина для авторизации

2.44
Добавлена возможность не изменять статус брони при повторном сохранении

2.43
Исправлена ошибка отправки специальных символов в письмах
Добавлена возможность повторной отправки статуса оплаты для Uniteller

2.42
Добавлена возможность оплаты за первые сутки проживания
Изменен алгорим запроса для свободных номеров с тарифами
Добавлен фильтр тарифов на страницу бронирования номера

2.41
Подключена процессинговая компания Яндекс.Касса
Изменены почтовые шаблоны

2.40
Добавлена возможность добавления пакетов услуг
Изменены шаблоны писем
Изменен шаблон результатов бронирования без регистрации
Добавлен Болгарский язык
Добавлена настройка даты заезда по умолчанию

2.39
Добавлена возможность отправки почты с помощью веб-сервера
Исправлен алгоритм формирования заездов

2.38
Подключена процессинговая компания United Card Services 

2.37
Добавлена автоматическая установка времени заезда и выезда из Shelter

2.36
Обновлены платежные файлы
Добавлен механизм проверки дублирования оплаты
Изменено ведение логов
Добавлен вывод броней созданных в Shelter

2.35
Подключена процессинговая компания Uniteller
Изменена логика работы запроса Подробной информации о брони в личном кабинете
Реализован учет количества дней до заезда

2.34
Модуль не зависит от таймзоны

2.33
Добавлена возможность просмотра цен в другой валюты для Европейских стран

2.32
Добавлены проверочные коды при отправке данных
Изменена форма авторизации и регистрации на странице Бронирование номера
Изменен алгоритм формирования заездов
Более гибкие настройки формирования заездов и редактирования брони
Возможность редактирования параметров поиска на странице Выбор номера

2.31
Добавлена настройка использования строго формата телефона
Отображение сообщения при отправке данных в РезервМастер
Добавлена возможность использования тарифов с фиксированной стоимостью
Установка вывода возможных тарифов
Возможные тарифы конвертируются в иностранную валюту

2.30
Добавлено автоматическое распределение гостей по основным и дополнительным местам при поиске номера
Добавлена возможность привязки изображения к категории гостя для страницы Выбор номера
Добавлена возможность введения запрета на создание новых броней для определенных агентств

2.29
Доступен ввод даты рождения гостей при бронировании
Добавлен вывод названия гостиницы
Изменен скрипт проверки доступности сервера

2.28
Добавлен литовский язык
Доступно выводить только необходимые языки перевода
Исправлена функция заполнения гостей на основные места 
Изменен алгоритм вывода кода валюты

2.27
Введено ограничение на одновременное использования ввода количества номеров и заполнение данных о гостях
Отмена бронирования доступна после перехода по ссылке, указанной в письме, в режиме модуля без личного кабинета

2.26
Добавлено автоматическое распределение номеров внутри типа номера при бронировании
Исправлена функция оплаты и сохранения брони

2.25
Добавлена возможность установки отмены бронирования для разных типов пользователей в зависимости от статуса брони
Добавлена дополнительная проверка прав на сохранение брони
Изменена проверка на повторное сохранение брони

2.24
Возможность запрещать самостоятельную отмену бронирования номера за определенное количество дней
Доступность редактирования брони до дня заселения

2.23
Изменена логика записи логов

2.22
Добавлены визуальные настройки

1.21
Изменена обработка параметров
Добавлены шаблоны для отображения результатов выполнения платежа

1.20
Изменена логика работы с организациями
Добавлена возможность использования тарифов организации
Использование тарифов организации не зависит от ограничений на бронирование
Добавлена возможность отображение всего номерного фонда или за исключением квот

1.19
Добавлен фильтр по тарифам
Добавлена сортировка по выводимым ценам
Изменен алгоритм отображения выбора свободных номеров с тарифами

1.18
Доработана проверка соединения для исключительных ситуаций
Внесены изменения в структуру внутренних функций

1.17
Добавлено исключение фиксированных тарифов
Формирование заездов - дополнительная отключаемая функция, отображаемая на отдельной странице выбора номеров
Скрыты поля распределения гостей по категориям мест на странице бронирования
Добавлено количество выбранных гостей и дополнительных услуг для каждого заезда

1.16
В разделе Бронирование номера показывается сумма за каждый заезд
Изменен внешний вид подробной информации о брони в разделе Список броней
Добавлено удаление примечания для брони
Добавлен контроль и отображение максимального количества символов для примечания
Изменена прокрутка таблицы на странице формирования заездов

1.15
Добавлена возможность переопределения названий типов размещения и возрастных категорий, возвращаемых из Shelter
Изменены настройки модуля

1.14
Отображается код брони
Изменен способ записи логов
Изменены настройки платежных систем

1.13
Модуль обменивается сообщениями с Shelter в кодировке utf-8
Новый способ записи логов
Изменен алгоритм использования сессий
Настройки модуля разделены на разные файлы

1.12
Добавлена дополнительная защита для проведения платежей

1.11
Категории гостей загружаются с Shelter
Категории мест размещения загружаются с Shelter
Добавлена возможность заселения на дополнительные места
Добавлена возможность изменения количества дополнительных услуг при бронировании номера
Изменен алгоритм построения заездов на этапе бронирования

1.10
Добавлено автоматическое определение разделов на сервере

1.9
Изменен способ записи логов
Добавлен ранний заезд и поздний выезд

1.8
Список типов номеров упорядочен по наименованию

1.7
Добавлен подсчет валюты для Украины
Описания для тарифов

1.6
Возможность запрещать самостоятельную отмену бронирования номера
Добавлена проверка доступности подключения к Shelter
Возможность задавать ввод данных кредитной карты для определенных типов оплат

1.5
Добавлена возможность работы для юридических лиц
Возможность задавать статус брони для юридических лиц при сохранении брони
Возможность работы с ограничениями для каждого агентства
Обновлен внешний вид и расширен функционал личного кабинета
Добавлена дополнительная защита передаваемых параметров

1.4
Добавлена сортировка по цене в выводе результатов поиска номеров
Добавлена отправка сообщений пользователю и менеджерам о создании, оплате и отмене бронирования
Добавлена возможность частичной оплаты брони

1.3
Добавлено шифрование данных кредитной карты
Изменена структура модуля при использовании отдельно от основного сайта
Установка статуса брони при сохранении
Введена дополнительная защита скриптов

1.2
Добавлена возможность работы с квотами
Реализована поддержка полной мультиязычности

1.1 
Доступны режимы работы с авторизацией и без авторизации, 
Если без авторизации, отмена брони через специальную форму ссылка высылается на почту при бронировании,
Возможность выбора формирований заездов в выборе и бронировании номера, 
Ввод защитного кода при сохранении брони, 
Ввод количества номеров в форме поиска, 
Реализованы дополнительные услуги,
Просмотр цен в другой валюте,
Ввод номера кредитной карты,
Подтверждение правил бронирования
