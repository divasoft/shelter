<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	if ((!(checkAuthUser())) || (!(checkCaptcha($_POST['ucs_checkcode'])))) { 
		$_SESSION['Shelter']['Info']['Message'] = $arMessages["general"]["error"][MESSAGE_LANG];
		exit;
	}		
	
	if (CAPTCHA_RESERV) {
		if (!(checkCaptchaUser($_POST['res_send_count']))) {
			$_SESSION['Shelter']['Info']['Message'] = $arMessages["Reserv"]["secret_code"][MESSAGE_LANG];
			exit;
		}
	}

	$error = false;	
	
	$arMailData = Array(
		'Service' => Array(),
		'PackageService' => Array(),
	);
	
	$hotel = protection(intval($_POST['hotel']));
	
	// контактная информация
	$fio = protection(trim(strval($_POST['fio'])));
			
	$phone = protection(strval($_POST['phone']));
	$phone = str_replace('-', '+', $phone);
	$pay = protection(intval($_POST['pay']));
	$comment = protection(strval($_POST['comment']));
	if ($comment == '') $comment = ' ';
	
	$creditcard = '';
	if (isset($_POST['creditcard'])) {
		if ($_POST['creditcard'] != '') {
			$creditcard = protection(strval($_POST['creditcard']));
			$creditcarddate = protection(strval($_POST['creditcarddate']));	
			if (strlen($creditcarddate) < 4) $creditcarddate = '0'.$creditcarddate;	
			$creditcard = encodeCC('&cc_number='.$creditcard.'&cc_expdate='.$creditcarddate);
		}
	}
	
	$email = protection(strval(strtolower($_POST['email'])));
	// контактная информация
	
	if (!USER_USE) {
		$GUEST['phones'] = $phone;
		$GUEST['email'] = $email;
	}
	
	$claim_id = explode(';', $_POST['res_claim_id']); // заезды при редактировании
	$inquiry_id = explode(';', $_POST['res_inquiry_id']); // заезды при редактировании
			
	$roomkind = explode(';',$_POST['res_request_id']); // номера
	$roomkindname = explode(';',urldecode($_POST['res_request_name'])); // номера
	$res_tarif = explode(';',$_POST['res_tarif']);	// тарифы	
	$res_tarif_name = explode(';',urldecode($_POST['res_tarif_name']));	// тарифы	
	
	$col = sizeof($roomkind); // количество заездов	
	$col_max = getNumbRatesForClient();
	if ($col > $col_max) $col = $col_max; // если заездов больше, чем разрешено
	
	// дата
	$res_request_in =  explode(';',$_POST['res_request_in']);
	$res_request_out =  explode(';',$_POST['res_request_out']);	
	$res_request_in_time =  explode(';',$_POST['res_request_in_time']);	
	$res_request_out_time =  explode(';',$_POST['res_request_out_time']);	
	$main_request_in = -1;
	$main_request_out = -1;	
	for($i = 0; $i < $col; $i++) {		
		if ($res_request_out[$i] < $res_request_in[$i]) $res_request_out[$i] = $res_request_in[$i] + 1;		
				
		$time_in = toDtime(FROM_HOUR);		
		$time_out = toDtime(TO_HOUR);
		if (EARLY_TIME){
			$arTime_in = explode(':', $res_request_in_time[$i]);
			$arTime_out = explode(':', $res_request_out_time[$i]);	
			$time_in = toDtime(setDM($arTime_in));
			$time_out = toDtime(setDM($arTime_out));
		}
		
		$request_in[$i] = sumDate(intval($res_request_in[$i]), $time_in);		
		$request_out[$i] = sumDate(intval($res_request_out[$i]), $time_out);	
		
		if (($main_request_in == -1) || ($main_request_in > $request_in[$i])) $main_request_in = $request_in[$i];
		if (($main_request_out == -1) || ($main_request_out < $request_out[$i])) $main_request_out = $request_out[$i];
		
	}
	// дата
			
	$res_colnumbers =  explode(';',$_POST['res_colnumbers']); // количество номеров
	if (((!SETTLE_NUMB_ROOMS) || (SHOW_PEOPLE_INFO))){
		for($i = 0; $i < $col; $i++)
			$res_colnumbers[$i] = 1;
	}
	
	// распределение гостей по местам
	$rs_quant = Array();
	$rs_quant_check = Array();
	
	$_POST['res_guest'] = getNumbForListGuest($_POST['res_guest']);
	
	$res_guest = explode(';', $_POST['res_guest']);
	for($i = 0; $i < $col; $i++) {		
		$facts = explode('#', $res_guest[$i]);
		for($j = 0; $j < sizeof($facts); $j++) {
			$f = explode('_', $facts[$j]);
			if ($f[2] > 0){						
				$arRs = Array(
					'f1'=>$f[0],
					'f2'=>$f[1],
					'quant'=>$f[2],
				);
				$rs_quant[$i][] = $arRs;
			}
			$rs_quant_check[$i][$f[0]][$f[1]] = $f[2];
		}		
	}

	$arPeopleType = Array();
	$arNumbType = Array();
	for($i = 0; $i < $col; $i++){
		for($g = 0; $g < sizeof($rs_quant[$i]); $g++){
			for($n = 0; $n < $rs_quant[$i][$g]['quant']; $n++){
				$arPeopleType[$i][] = $rs_quant[$i][$g]['f2'];
				$arNumbType[$i][] = $rs_quant[$i][$g]['f1'];						
			}	
		}				
	}	
	
	$arTypeNumbError = checkTypeNumb($rs_quant_check, $roomkind);
	if (sizeof($arTypeNumbError) > 0) $error = true;
	
	$arMainTypeNumbError = checkMainTypeNumb($rs_quant_check, $roomkind);
	if (sizeof($arMainTypeNumbError) > 0) $error = true;
	// распределение гостей по местам
	
	$reservation_id = 0;
	if ($_POST['reservation_id']) $reservation_id = protection(intval($_POST['reservation_id']));
	
	// guest	
	$owner = $GUEST['id'];
	
	// форматируем массив гостей	
	$guests_for_xml = array();
	$arGuest_f = explode(';', $_POST['res_people_f']);
	$arGuest_n = explode(';', $_POST['res_people_n']);
	$arGuest_o = explode(';', $_POST['res_people_o']);
	$arGuest_b = explode(';', $_POST['res_people_b']);
	$arGuest_id = explode(';', $_POST['res_people_id']);
	$iguest = true;
	$arNumbPeopleRate = Array();
	$arNumbPeopleName = Array();
	$arShowPeopleInfoData = unserialize(SHOW_PEOPLE_INFO_DATA);
	for($i = 0; $i < $col; $i++) {
		$arIGuest_f = explode('-', $arGuest_f[$i]); 
		$arIGuest_n = explode('-', $arGuest_n[$i]); 
		$arIGuest_o = explode('-', $arGuest_o[$i]); 
		$arIGuest_b = explode('-', $arGuest_b[$i]); 
		$arIGuest_id = explode('-', $arGuest_id[$i]); 
		
		for($j = 0; $j < sizeof($arIGuest_id); $j++) {			
			$id = $arIGuest_id[$j];
			if ($arIGuest_id[$j] == $GUEST['id']) $id = 0;
			$gemail = '';
			$gphone = '';
			
			$arIGuest_f[$j] = trim($arIGuest_f[$j]);
			$arIGuest_n[$j] = trim($arIGuest_n[$j]);
			$arIGuest_o[$j] = trim($arIGuest_o[$j]);
			$bt = 0;
			if (SHOW_PEOPLE_BIRTHDAY) $bt = toDdate(trim($arIGuest_b[$j]));
			$arIGuest_b[$j] = $bt;
			
			if (($arIGuest_f[$j] == '') && ($arIGuest_n[$j] == '') && ($arIGuest_o[$j] == '')) {
				$arIGuest_f[$j] = $arShowPeopleInfoData[0];
				$arIGuest_n[$j] = $arShowPeopleInfoData[1];
				$arIGuest_o[$j] = $arShowPeopleInfoData[2];
			}			
					
			if (((!USER_USE) || (($arIGuest_f[$j] == u($GUEST['name'])) && ($arIGuest_n[$j] == u($GUEST['name1'])) && ($arIGuest_o[$j] == u($GUEST['name2'])))) && $iguest) {
				$id = $owner;
				$gemail = $GUEST['email'];
				$gphone = $GUEST['phones'];
				$iguest = false;
			} 
			
			$name = $arIGuest_f[$j].' '.$arIGuest_n[$j];
			if (strlen($arIGuest_o[$j]) > 0) $name .= ' '.$arIGuest_o[$j];
			
			$guests_for_xml[$i][$j] = array (
				'id' => $id,
				'claim' => $claim_id[$i],
				'name' => $arIGuest_f[$j],
				'name1' => $arIGuest_n[$j],
				'name2' => $arIGuest_o[$j],
				'fullname' => $name,
				'email' => $gemail,
				'phones' => $gphone,
				'birthdate' => $arIGuest_b[$j],
				'f1' => $arNumbType[$i][$j],
				'f2' => $arPeopleType[$i][$j],	
			);		
			$arNumbPeopleName[$i][] = $name;
		}		
		if (!isset($arNumbPeopleRate[$i])) $arNumbPeopleRate[$i] = 0;
		$arNumbPeopleRate[$i] += sizeof($arIGuest_id);
	}
	// guest
	
	// operation
	$operation_for_xml = Array();
	$operation_list = explode(';', $_POST['res_operation']);
	
	$list_op = getOperation($hotel);
	
	for($i = 0; $i < $col; $i++) {
		$operation_list_aritem = explode('*', $operation_list[$i]);		
		for($j = 0; $j < sizeof($operation_list_aritem); $j++) 		
			if ($operation_list_aritem[$j] != '') {
			
				$operation_list_item = explode('#', $operation_list_aritem[$j]);
				
				$cost = -1;
				$name_service = '';
				for($s = 0; $s < sizeof($list_op); $s++) {
					if ($operation_list_item[1] == $list_op[$s]['code']) {
						$cost = floatval($list_op[$s]['cost']);
						$name_service = $list_op[$s]['name'];
					}	
				}
				
				$quant = intval($operation_list_item[3]);
				if ($quant > 0)	{			
					$operation_for_xml[$i][] = Array(
						'id' => intval($operation_list_item[0]),
						'operation' => intval($operation_list_item[1]),
						'date' => intval($operation_list_item[2]),
						'quant' => $quant,
						'cost' => $cost,
					);
					
					$arMailData['Service'][$i][] = Array(
						'name' => $name_service,
						'quant' => $quant,
					);
				}	
			}
		if (!isset($operation_for_xml[$i])) $operation_for_xml[$i] = Array();
	}
	// operation
	
	// package_service
	$package_service_for_xml = Array();
	$package_service_list = explode(';', $_POST['res_package_service']);
	
	$arPackageService = getPackageServices();
	
	for($i = 0; $i < $col; $i++) {
		$arPackageServiceId = explode('*', $package_service_list[$i]);
		$arPackageServiceAdd = Array();
		for($j = 0; $j < sizeof($arPackageService); $j++) {
			if (in_array($arPackageService[$j]['id'], $arPackageServiceId)) {
				$arPackageServiceAdd[] = $arPackageService[$j]['id'];
				$arMailData['PackageService'][$i][] = $arPackageService[$j]['name'];
			}	
		}
		$package_service_for_xml[$i] = $arPackageServiceAdd;
	}
	// package_service
	
	// limits
	$blockid = 0;
	if (USE_DEBTOR_QUOTA) {
		$blockid = getStatusLimit($reservation_id, $request_in, $request_out, $roomkind, $res_colnumbers, $col);
		if (!$blockid) $error = true;
	}
	// limits
	
	// idroom
	$idroom = Array();
	for($i = 0; $i < $col; $i++) 
		$idroom[$i] = 0;
		
	if (SETTLE_VACANCY_IN_ROOM && SETTLE_ENTIRED_PERIOD){
		$arFreeRoomsRoomkind = Array();
		$arFreeRooms = Array();
			
		$arFreeRoomsEx = Array();	
		if ($reservation_id > 0) $arFreeRoomsEx = getInfoAboutReserv($reservation_id);		
		
		for($i = 0; $i < $col; $i++){
			if (!(in_array($roomkind[$i], $arFreeRoomsRoomkind))){
				// заполняем номера из брони
				$arFreeRooms[$roomkind[$i]] = $arFreeRoomsEx[$roomkind[$i]]; 
				
				// заполняем свободные номера
				$arFreeRoomsTemp = getVacancyRooms($hotel, $request_in[$i], ($request_out[$i] - 1), $roomkind[$i]);				
				for($j = 0; $j < sizeof($arFreeRoomsTemp); $j++)
					$arFreeRooms[$roomkind[$i]][] = $arFreeRoomsTemp[$j];	
					
				$arFreeRoomsRoomkind[] = $roomkind[$i];
			}
			
			$idroom[$i] = intval($arFreeRooms[$roomkind[$i]][0]);
			$arFreeRooms[$roomkind[$i]] = refreshRooms($arFreeRooms[$roomkind[$i]]);
		}
		
		for($i = 0; $i < $col; $i++)
			if (!($idroom[$i] > 0)) $error = true;
	}
	// idroom
	
	// inquiry
	$inquiry = Array();
	$arInquiryInfo = Array();	
		
	$arDiscounts = Array();
	if (isset($_POST['ucs_discount_id'])){
		if ($_POST['ucs_discount_id'] != ''){
			$arDiscounts = checkDiscountCode($_POST['ucs_discount_id']);
		}
	} // скидки по карте
	if (USE_DEBTOR_CONTRACT_DISCOUNT > 0){
		$arDiscounts[] = USE_DEBTOR_CONTRACT_DISCOUNT;
	} // скидки для организации
	
	for($i = 0; $i < $col; $i++) {		
		$inquiry[$i] = Array(
			'id' => $inquiry_id[$i],
			'roomkind' => $roomkind[$i],
			'idroom' => $idroom[$i],
			'arrival' => $request_in[$i],
			'departure' => $request_out[$i],
			'quant' => $res_colnumbers[$i],
			'rate' => $res_tarif[$i],
			'blockid' => $blockid[$i],
			'claim' => array(
				'id' => $claim_id[$i],				
				'inquiry' => $inquiry_id[$i],
				'guests' => $guests_for_xml[$i],
				'operation' => $operation_for_xml[$i],
				'discount' => $arDiscounts,
				'commission' => USE_DEBTOR_CONTRACT_COMMISION,
				'rate_adds' => $package_service_for_xml[$i],
			),
			'rs_quant' => $rs_quant[$i],
		);
	}	
	// inquiry
	
	$action = "SAVE";
	$status = RESERV_STATUS;
	if ($reservation_id > 0) {
		$action = "GETINFO";
		
		if (RESERV_MAIN_NO_RESET_STATUS){
			include_once(PATH_MODULES.PAGE_RESERVINFO.'.php');
			$rkr = new ReservationInfo($reservation_id, $GUEST['id']);		
			$obj = $rkr->Request($rkr->GetXML());
			if (isset($obj->attributes()->status)){
				$status = intval($obj->attributes()->status);
			}
		}
	}
	
	// maindata
	$maindata = array();
	$maindata['id'] = $reservation_id;
	$maindata['hotel'] = $hotel;
	$maindata['arrival'] = $main_request_in;
	$maindata['departure'] = $main_request_out;
	$maindata['contact'] = $fio;	
	$maindata['phone'] = $phone;
	$maindata['debtor'] = USE_DEBTOR_ID;
	$maindata['agent'] = USE_DEBTOR_ID;
	$maindata['contract'] = USE_DEBTOR_CONTRACT;
	$maindata['email'] = $email;
	$maindata['info'] = $comment;
	$maindata['creditcard'] = $creditcard;	
	$maindata['payment'] = $pay;
	$maindata['action'] = $action;
	$maindata['owner'] = $owner;
	$maindata['status'] = $status;
	// maindata
	
	
	if ($error) {
		$_SESSION['Shelter']['Info']['Message'] = $arMessages["UserCabinet"]["error"][MESSAGE_LANG];
		exit;
	}
		
	$rkr = new SendReservation($maindata, $inquiry);
	//echo '1'.$rkr->GetXML(); exit;	
	$obj = $rkr->Request($rkr->GetXML());			
	
	if ($reservation_id > 0) {		
		if (!($obj) || (intval($obj->attributes()->errorcode) > 0)) {
			$_SESSION['Shelter']['Info']['Message'] = $arMessages["UserCabinet"]["error"][MESSAGE_LANG];
			exit;
		}		
		$rkr->action = "SAVE";
		//echo '1'.$rkr->GetXML(); exit;
		$obj = $rkr->Request($rkr->GetXML());			
	}
	
	$resid = intval($obj->attributes()->id);
	$rescode = intval($obj->attributes()->code);
		
	if ($resid > 0) {
		$_SESSION['Shelter']['Data']['Reservation'] = Array();
		$_SESSION['Shelter']['Data']['UsePackageService'] = Array();
		
		// собираем уведомления пользователю ----------------------------- //
		
		$arPath = Array(
			MODULE_SHEDULE=>PAGE_RESERVNDELETE, 
			'id'=>$resid, 
			'email'=>i($email), 
			'code'=>md5($resid.KEY_SECURITY)
		);		
		$mailLink = change_address($arPath, true);
		
		$arPath['nocancel'] = 1;
		$mailLinkNoCancel = change_address($arPath, true);

		$mailType = '';
		$mailTarif = '';
		$mailFrom = '';
		$mailTo = '';
		$mailNight = '';
		$mailPeople = '';
		$mailPeopleName = '';
		$mailService = '';
		$mailPackageService = '';
		for($i = 0; $i < sizeof($inquiry); $i++) {
			if ($i > 0) {
				$mailType .= ';';
				$mailTarif .= ';';
				$mailFrom .= ';';
				$mailTo .= ';';
				$mailNight .= ';';
				$mailPeople .= ';';
				$mailPeopleName .= ';';
				$mailService .= ';';
				$mailPackageService .= ';';
			}	
			$mailType .= $roomkindname[$i];
			$mailTarif .= $res_tarif_name[$i];			
			$mailFrom .= toIdate($inquiry[$i]['arrival'], 'd.m.Y H:i');
			$mailTo .= toIdate($inquiry[$i]['departure'], 'd.m.Y H:i');
			$mailNight .= round(round(toIdate($inquiry[$i]['departure']) - toIdate($inquiry[$i]['arrival']))/SCHEDULE_DAY_SECONDS);
			$mailPeople .= $arNumbPeopleRate[$i];
			
			for($j = 0; $j < sizeof($arNumbPeopleName[$i]); $j++) {
				if ($j > 0) $mailPeopleName .= ', ';
				$mailPeopleName .= $arNumbPeopleName[$i][$j];
			}			
			
			if (isset($arMailData['Service'][$i])){
				$mailServiceRate = '';
				for($j = 0; $j < sizeof($arMailData['Service'][$i]); $j++) {
					if ($j > 0) $mailServiceRate .= ', ';
					$mailServiceRate .= $arMailData['Service'][$i][$j]['name'].' ('.$arMailData['Service'][$i][$j]['quant'].' '.$arMessages["Reserv"]["colnumbers_short"][MESSAGE_LANG].')';
				}
				if (sizeof($arMailData['Service'][$i]) == 0) $mailServiceRate = $arMessages["Reserv"]["no_operation"][MESSAGE_LANG];
				$mailService .= $mailServiceRate;
			}
			
			if (isset($arMailData['PackageService'][$i])){
				$mailServiceRate = '';
				for($j = 0; $j < sizeof($arMailData['PackageService'][$i]); $j++) {
					if ($j > 0) $mailServiceRate .= ', ';
					$mailServiceRate .= $arMailData['PackageService'][$i][$j];
				}
				if (sizeof($arMailData['PackageService'][$i]) == 0) $mailServiceRate = $arMessages["Reserv"]["no_operation"][MESSAGE_LANG];
				$mailPackageService .= $mailServiceRate;
			}
		}
		
		$arData = Array(				
			'RESID' => $rescode,				
			'TYPE' => i($mailType),
			'TARIF' => i($mailTarif),
			'NIGHTS' => $mailNight,
			'PEOPLES' => $mailPeople,
			'PEOPLESNAME' => $mailPeopleName,
			'DATE_FROM' => $mailFrom,
			'DATE_TO' => $mailTo,
			'FIO' => i($fio),
			'PHONE' => i($phone),
			'EMAIL' => i($email),
			'LINK' => $mailLink,
			'SUM' => protection($_POST['allcost']),
			'CURRENT' => $arMessages["current"][CURRENCYSHOW][MESSAGE_LANG],
			'SERVICE' => $mailService,
			'PACKAGESERVICE' => $mailPackageService,
		);
		// собираем уведомления пользователю ----------------------------- //
		
		sendMailForMeneger('ForMenegerReservation', $arData, $rescode); // отправка уведомления менеджерам
		
		
		// отправка уведомления пользователю ----------------------------- //		
		$send = '';
		$tmail = 'Reservation';
		if (!USER_USE) $tmail = 'ReservationNR';		
		if (sendMail($GUEST['email'], $tmail, $arData)) $send = $arMessages["general"]["mail_send"][MESSAGE_LANG];
		// отправка уведомления пользователю ----------------------------- //			
					
		if (USER_USE) {
			$_SESSION['Shelter']['Info']['Message'] = $arMessages["UserCabinet"]["mail"][MESSAGE_LANG].' '.$send;			
			echo $resid.';'.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true);
		}else{			
			$_SESSION['Shelter']['Info']['Message'] = $arMessages["UserCabinet"]["mailNR"][MESSAGE_LANG].' '.$send;
			echo $resid.';'.$mailLinkNoCancel;
		}
	}else{
		$_SESSION['Shelter']['Info']['Message'] = $arMessages["UserCabinet"]["error"][MESSAGE_LANG]; //strval($obj->attributes()->errormessage);			
		echo '0';
	}	
?>
