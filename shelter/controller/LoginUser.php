<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	auth(0);
	
	$error = '';
	$loginOK = false;
	if (isset($_POST['login']) && isset($_POST['pass']) && (checkMail($_POST['login'])) && (latin($_POST['pass']))) {	
		if (checkCaptcha($_POST['ucs_checkcode'])){
			$login = protection($_POST['login']);
			$rkr = new LoginUser($login, protection($_POST['pass']));	
			$obj = $rkr->Request($rkr->GetXML());	
			$id = intval($obj->attributes()->id);
			$password = strval($obj->attributes()->password);
			if ($id > 0) {
				$loginOK = true;
				setUserData($login, $password, 0);
				if (!isset($_POST['login_reserv'])) {
					header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true));
					exit;
				}	
			}else{
				$error = $arMessages["LoginUser"]["error"][MESSAGE_LANG];
			}			
		}		
	}
	
	$arResult['mess'] = getMessageForUser($error);
	
	if (isset($_POST['login_reserv'])) {		
		if ($loginOK) {
			$GUEST = getUser();		
			getContacts();
		}	
		else echo '0;'.$error;
	}
	
	if (!isset($_POST['login'])) $_POST['login'] = '';
	
	$arResult['captcha'] = setCaptcha();
?>
