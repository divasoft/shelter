<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	auth();
	
	$id = protection($_GET['id']);
	$send = '';
	
	if ($code = deleteReserv($id)) {
		$arData = Array(				
			'RESID' => $code,	
			'FIO' => $GUEST['name'].' '.$GUEST['name1'],
			'PHONE' => $GUEST['phones'],
			'EMAIL' => $GUEST['email'],
		);							
		if (sendMail($GUEST['email'], 'ReservationDelete', $arData)) $send = $arMessages["general"]["mail_send"][MESSAGE_LANG];
		
		sendMailForMeneger('ForMenegerReservationDelete', $arData, $code);				
		
		$_SESSION['Shelter']['Info']['Message'] = $arMessages["UserCabinet"]["mailDelete"][MESSAGE_LANG].' '.$send;			
	}	
	
	header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true));
	exit;
	
?>