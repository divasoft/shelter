<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	if (!defined('NOCHECKBOOKINGALLOW')) checkBookingAllow();
	
	if (!(isset($_POST['ajax']))) $_POST['ajax'] = '';
	if (!(isset($_POST['res_request_id']))) $_POST['res_request_id'] = '';
	if (!(isset($_SESSION['Shelter']['Data']['Reservation']['id']))) $_SESSION['Shelter']['Data']['Reservation']['id'] = '';
	
	if (($_POST['ajax'] == '') && ($_POST['res_request_id'] != '')) {
		unset($_SESSION['Shelter']['Data']['Reservation']);
		$_SESSION['Shelter']['Data']['Reservation']['id'] = protection($_POST['res_request_id']);
		$_SESSION['Shelter']['Data']['Reservation']['in'] = protection($_POST['res_request_in']);
		$_SESSION['Shelter']['Data']['Reservation']['out'] = protection($_POST['res_request_out']);
		
		if (isset($_POST['tarif'.$_POST['res_request_id']])){
			$arRate = explode('#', $_POST['tarif'.$_POST['res_request_id']]);
			$_SESSION['Shelter']['Data']['Reservation']['tarif'] = protection($arRate[0]);		
			$s = protection($arRate[1]);
			$s = str_replace('-', '_', $s);
			$s = str_replace(';', '#', $s);				
			$_SESSION['Shelter']['Data']['Reservation']['res_guest'] =  $s;
		}else{
			$_SESSION['Shelter']['Data']['Reservation']['res_guest'] =  '2_1_1';
		}
		
		$_SESSION['Shelter']['Data']['UsePackageService'] = Array();
		
		header('location: '.change_address());
		exit;	
	}
	
	if (($_POST['ajax'] == '') && ($_SESSION['Shelter']['Data']['Reservation']['id'] != '')) {
		$_POST['res_request_id'] = protection($_SESSION['Shelter']['Data']['Reservation']['id']);
		$_POST['res_request_in'] = protection($_SESSION['Shelter']['Data']['Reservation']['in']);
		$_POST['res_request_out'] = protection($_SESSION['Shelter']['Data']['Reservation']['out']);
		$_POST['res_guest'] = protection($_SESSION['Shelter']['Data']['Reservation']['res_guest']);
				
		$res_time_in = '';
		$res_time_out = '';
		$arD = explode(';', $_POST['res_request_in']);
		for($i = 0; $i < sizeof($arD); $i++) {
			if ($i > 0) {
				$res_time_in .= ';';
				$res_time_out .= ';';
			}			
			$res_time_in .= FROM_HOUR_DOUBLE;
			$res_time_out .= TO_HOUR_DOUBLE;
		}		
		$_POST['res_request_in_time'] = $res_time_in;
		$_POST['res_request_out_time'] = $res_time_out;
		
		if (FIND_NUMBER_USE) $_POST['res_colnumbers'] = protection($_SESSION['Shelter']['Find']['request_colnumber']);
		if (isset($_SESSION['Shelter']['Data']['Reservation']['tarif'])){
			if ($_SESSION['Shelter']['Data']['Reservation']['tarif'] != '')
				$_POST['res_tarif'] = protection($_SESSION['Shelter']['Data']['Reservation']['tarif']); 	
		}	
	}
	
	$arPayData = Array();
	if ($_POST['ajax'] == '') $arPayData = getPayTypeOnline(); // загружаем типы оплаты
	
	$type_user = getNumbRatesForClient();
		
	$request_operation = array(); // дополнительные услуги
	
	$arPackageServiceCheck = $_SESSION['Shelter']['Data']['UsePackageService']; // пакеты услуг
	
	$arNoShowPrice = Array();
	$contract_id = 0;
	$mess = '';
	$reservation_id = 0;
	$arResult['discountcode'] = '';
	if (isset($_GET['edit'])) {
		$reservation_id = protection(intval($_GET['edit']));
		
		$gid = $GUEST['id'];
		if ($GUEST['type'] == 1) $gid = 0;
		
		include_once(PATH_MODULES.PAGE_RESERVINFO.'.php');
		$rkr = new ReservationInfo($reservation_id, $gid);		
		$obj = $rkr->Request($rkr->GetXML());	
		
		// проверка
		if ((!(checkUser($obj))) || ((!defined('NOCHECKBOOKINGALLOW')) && (intval($obj->attributes()->owner) != $GUEST['id']))){
			if ($_POST['about_reserv'] == 1) 
				echo $arMessages["general"]["error"][MESSAGE_LANG];
			else
				header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true));
			exit;
		}		
		if ((!checkReservForEdit($obj->attributes()->arrival)) && ($_POST['about_reserv'] != 1)){
			header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true));
			exit;
		}
		// проверка
		
		$hotel = strval($obj->attributes()->hotel);
		$_SESSION['Shelter']['Find']['hotel'] = $hotel; 		
		
		$payment = intval($obj->attributes()->payment);
		$info = i(strval($obj->attributes()->info));
		if ($info == ' ') $info = '';
		$request_in_main = strval($obj->attributes()->arrival);	
		$request_out_main = strval($obj->attributes()->departure);	
		
		$contact = i(strval($obj->attributes()->contact));	
		$phone = strval($obj->attributes()->phone);	
		$email = strval($obj->attributes()->email);	
		
		if (intval($obj->attributes()->debtor) > 0)
			$contract_id = intval($obj->attributes()->contract);	
		
		$creditcardinfo = decodeCC(strval($obj->attributes()->cc_info));
		if ($creditcardinfo != ''){
			$creditcardinfo = explode('&', $creditcardinfo);		
			$arCard = explode('=', $creditcardinfo[0]);
			$creditcard = $arCard[1];
			$arCard = explode('=', $creditcardinfo[1]);
			$creditcarddate = $arCard[1];		
		}
		
		$arDiscounts = Array();
		$arCommission = Array();
		$arPackageServiceCheck = Array();
		$numb = 0;
		foreach($obj->inquiry as $inquiry) {
			$inquiry_id[] = $inquiry->attributes()->id;
			$claim_id[] = $inquiry->claim->attributes()->id;
			$tarif[] = intval($inquiry->attributes()->rate);			
			$res_colnumbers[] = intval($inquiry->attributes()->quant);
			$roomkind[] = intval($inquiry->attributes()->roomkind);
			$request_in[] = floatval($inquiry->attributes()->arrival);
			$request_out[] = floatval($inquiry->attributes()->departure);				
			$arCost[] = floatval($inquiry->attributes()->summa)*intval($inquiry->attributes()->quant);	
			if (isset($inquiry->claim->discounts)){
				foreach($inquiry->claim->discounts->discount as $discounts){
					$arDiscounts[] = intval($discounts->attributes()->id);										
				}
			}
			if (isset($inquiry->claim->commissions)){
				foreach($inquiry->claim->commissions->commission as $commissions){
					$arCommission[] = intval($commissions->attributes()->id);										
				}
			}
			if (isset($inquiry->claim->rate_adds)){
				foreach($inquiry->claim->rate_adds->rate_add as $rate_add){
					$arPackageServiceCheck[$numb][] = intval($rate_add->attributes()->id);										
				}
			}
			if (!isset($arPackageServiceCheck[$numb])) $arPackageServiceCheck[$numb] = Array();
			$numb++;
		}
		$arResult['discountcode'] = generateDiscountCode($arDiscounts);
		
		$_SESSION['Shelter']['Data']['UsePackageService'] = $arPackageServiceCheck;
		
		$col = sizeof($roomkind);
		$cost = floatval($obj->attributes()->summa);
		$send_data = true;	
		
		// ------------------------------
		$arNumbTypeShow = Array();
		$arGuestNumb = getNumbGuest('', $roomkind);
		
		$use_request_in = Array();
		$use_request_departure = Array();
		$room_array = Array();		
		$inquiry = $obj->inquiry;
		for($i=0;$i<sizeof($inquiry);$i++) {
			$guests_f = array();
			$guests_n = array();
			$guests_o = array();
			$guests_b = array();
			$guests_id = array();
			$guests_numb = array();
			$guests_type = array();
			foreach ($inquiry[$i]->claim->guest as $guest) {
				$guests_f[] = (strval($guest->attributes()->name));
				$guests_n[] = (strval($guest->attributes()->name1));
				$guests_o[] = (strval($guest->attributes()->name2));
				$guests_b[] = toIdate(strval($guest->attributes()->birthdate));
				$guests_id[] = (intval($guest->attributes()->id));				
				$guests_numb[] = intval($guest->attributes()->f1);				
				$guests_type[] = intval($guest->attributes()->f2);				
			}
			$request_people_f[$i] = $guests_f;
			$request_people_n[$i] = $guests_n;
			$request_people_o[$i] = $guests_o;
			$request_people_b[$i] = $guests_b;
			$request_people_id[$i] = $guests_id;
			$request_numb_type[$i] = $guests_numb;
			$request_guest_type[$i] = $guests_type;
			
			for($j = 0; $j < sizeof($request_numb_type[$i]); $j++){
				$arNumbTypeShow[$i][$request_numb_type[$i][$j]][] = $request_guest_type[$i][$j].';'.$request_people_id[$i][$j].';'.$request_people_f[$i][$j].';'.$request_people_n[$i][$j].';'.$request_people_o[$i][$j].';'.$request_people_b[$i][$j];
			}
			
			// дополнительные услуги ------------------------- //			
			$j = 0;
			foreach ($inquiry[$i]->claim->operation as $operation_v) {
				$request_operation[$i][$j]['id'] = (intval($operation_v->attributes()->id));
				$request_operation[$i][$j]['code'] = (strval($operation_v->attributes()->operation));
				$request_operation[$i][$j]['quant'] = (intval($operation_v->attributes()->quant));
				$request_operation[$i][$j]['date'] = (strval($operation_v->attributes()->date));
				$j++;
			}
			// дополнительные услуги ------------------------- //
			
			// гости 
			foreach ($inquiry[$i]->rs_quant as $rs_quant) {
				$arGuestNumb[$i][intval($rs_quant->attributes()->f1)][intval($rs_quant->attributes()->f2)] = intval($rs_quant->attributes()->quant);
			}
			// гости 
			
			// основная информация
			$res_request_in[$i] = $request_in[$i]; // для шаблона
			$res_request_out[$i] = $request_out[$i]; // для шаблона
			
			for($j = 0; $j < sizeof($res_request_in); $j++){
				$arTime = explode('.', $request_in[$i]);
				$request_in_time[$i] = '0.'.$arTime[1];
				
				$arTime = explode('.', $request_out[$i]);				
				$request_out_time[$i] = '0.'.$arTime[1];
			}
			
			$request_length[$i] = ceil(intval($request_out[$i]) - intval($request_in[$i]));	// для тарифа		
			
			$rkr = new Reservation($hotel, intval($request_in[$i]), intval($request_out[$i]), $request_in_time[$i], $request_out_time[$i], $roomkind[$i], $arGuestNumb[$i], $arDiscounts, $arCommission, $arPackageServiceCheck[$i]);	
			$obj_tarif = $rkr->Request($rkr->GetXML());		
			$tarif_array[$i] = tarif_read($obj_tarif);
			
			for($j = intval($request_in[$i]); $j < intval($request_out[$i]); $j++){
				if (!isset($room_array[$roomkind[$i]][$j])) $room_array[$roomkind[$i]][$j] = 0;
				$room_array[$roomkind[$i]][$j] += $res_colnumbers[$i];
			}	
			
			$_SESSION['Shelter']['Data']['RoomsNumb'] = $room_array;
			// основная информация			
		}
		
		$arRoomkinds = getRoomkinds($hotel, $request_in_main, $request_out_main);
	}else{
		// для новой брони
		
		if ($_POST['ajax'] == '') $_SESSION['Shelter']['Data']['RoomsNumb'] = '';
		if (!isset($_POST['res_inquiry_id'])) $_POST['res_inquiry_id'] = '';
		if (!isset($_POST['res_claim_id'])) $_POST['res_claim_id'] = '';
		if (!isset($_POST['res_colnumbers'])) $_POST['res_colnumbers'] = '';
		if (!isset($_POST['addrate'])) $_POST['addrate'] = 0;
		if (!isset($_POST['res_people_f'])) $_POST['res_people_f'] = '';
		if (!isset($_POST['res_people_n'])) $_POST['res_people_n'] = '';
		if (!isset($_POST['res_people_o'])) $_POST['res_people_o'] = '';
		if (!isset($_POST['res_people_b'])) $_POST['res_people_b'] = '';
		if (!isset($_POST['res_people_id'])) $_POST['res_people_id'] = '';
		
		$contact = getFullName();
		$phone = (isset($GUEST['phones'])) ? $GUEST['phones'] : $phone = '';	
		$email = (isset($GUEST['email'])) ? $GUEST['email'] : $email = '';	
				
		$hotel = intval(protection($_SESSION['Shelter']['Find']['hotel']));
		$payment = 0;
		
		if ($_POST['res_request_id'] == '') {
			header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_RESULT_FIRST), true));
			exit;
		}
		// чтение данных
		
		
		// номера
		$roomkind = explode(';',$_POST['res_request_id']);		
		$col = sizeof($roomkind);	
		
		$res_request_in =  explode(';',$_POST['res_request_in']);
		$res_request_out =  explode(';',$_POST['res_request_out']);
		$request_in_time =  explode(';',$_POST['res_request_in_time']);
		$request_out_time =  explode(';',$_POST['res_request_out_time']);
		if ($_POST['ajax'] != '') {
			for($i = 0; $i < sizeof($request_in_time); $i++){
				$arTime = explode(':', $request_in_time[$i]);
				$request_in_time[$i] = toDtime(setDM($arTime));
				
				$arTime = explode(':', $request_out_time[$i]);				
				$request_out_time[$i] = toDtime(setDM($arTime));
			}
		}		
		$inquiry_id =  explode(';',$_POST['res_inquiry_id']);
		$claim_id =  explode(';',$_POST['res_claim_id']);		
		
		
		// количество номеров
		$res_colnumbers = Array();
		if ($_POST['res_colnumbers'] != '') {			
			$res_colnumbers =  explode(';',$_POST['res_colnumbers']);													
		}
		for($i = 0; $i < $col; $i++) {
			if (!(isset($res_colnumbers[$i]))) $res_colnumbers[$i] = 1;
			if ($res_colnumbers[$i] < 1) $res_colnumbers[$i] = 1;
		}	
		
		$room_array = Array();
		if ($_SESSION['Shelter']['Data']['RoomsNumb'] != '')
			$room_array = $_SESSION['Shelter']['Data']['RoomsNumb'];		
		// количество номеров
		
		// список заездов
		if ($_POST['res_inquiry_id'] == '') {
			$inquiry_id = Array();
			$claim_id = Array();
			for($i = 0; $i < $col; $i++) {
				$inquiry_id[$i] = 0;
				$claim_id[$i] = 0;
			}
		}		
		// список заездов
		
		// тарифы	
		$tarif = Array(); 
		if (isset($_POST['res_tarif'])){
			if ($_POST['res_tarif'] != '') $tarif =  explode(';',$_POST['res_tarif']);
		}
		if (sizeof($tarif) == 0){
			for($i = 0; $i < $col; $i++)
				$tarif[$i] = 0;				
		}			
		// тарифы
		
		// для добавления нового заезда
		if (($_POST['addrate'] == 1) && ($col < $type_user)) {
			$roomkind[] = $roomkind[0];
			$res_request_in[] = $res_request_in[0];
			$res_request_out[] =  $res_request_out[0];
			$request_in_time[] =  $request_in_time[0];
			$request_out_time[] =  $request_out_time[0];
			$res_colnumbers[] = 1;
			$tarif[] = 0;
			$inquiry_id[] = 0;
			$claim_id[] = 0;
			$col++;
		}
		// для добавления нового заезда
		
		if ($col > $type_user) $col = $type_user; // если заездов больше, чем разрешено
		
		// формируем даты с временем выезда
		for($i = 0; $i < $col; $i++) {		
			$arMaxDate = maxDate($res_request_in[$i], $res_request_out[$i]);
			$res_request_out[$i] = $arMaxDate[0];
			
			if ($arMaxDate[1]) $mess = $arMessages["message"]["maxdate"][MESSAGE_LANG];
			$request_in[$i] = intval($res_request_in[$i]) + $request_in_time[$i];
			$request_out[$i] = intval($res_request_out[$i]) + $request_out_time[$i];
			$request_length[$i] = ceil(intval($res_request_out[$i]) - intval($res_request_in[$i]));	// для тарифа			
		}
		// формируем даты с временем выезда
		
		// гости		
		$v = false;
		if ($col == 1) $v = true;		
		$arNumbDef = setNumbDef(true);
		
		$arGuestNumb = setNumbGuestDef($roomkind, $arNumbDef);
		
		if ($_POST['res_guest'] != '') {
			
			$_POST['res_guest'] = getNumbForListGuest($_POST['res_guest']);
			$arGuestNumb = getNumbGuest($_POST['res_guest'], $roomkind);
			
			$arPeopleType = Array();
			$arTypePeopleNumbAllInfo = explode(';', $_POST['res_guest']);
			for($i = 0; $i < $col; $i++){
				if (isset($arTypePeopleNumbAllInfo[$i])){
					$arInfo = explode('#', $arTypePeopleNumbAllInfo[$i]);
					for($j = 0; $j < sizeof($arInfo); $j++){
						$arInfoType = explode('_', $arInfo[$j]);
						for($g = 0; $g < $arInfoType[2]; $g++){
							$arPeopleType[$i][] = $arInfoType[1];
						}
					}
				}else{
					$arPeopleType[$i] = Array(1);
				}
			} 
			$arNumbType = Array();
			for($i = 0; $i < $col; $i++){
				foreach($arGuestNumb[$i] as $type_place=>$type_guest){
					foreach($type_guest as $id_guest=>$numb){
						if ($numb > 0){
							$ex_place = false;
							if (isset($_SESSION['Shelter']['Data']['RoomFactor'][$hotel][$roomkind[$i]][$type_place])){
								if ($_SESSION['Shelter']['Data']['RoomFactor'][$hotel][$roomkind[$i]][$type_place]['quant'] > 0){
									for($j = 0; $j < $numb; $j++){
										$arNumbType[$i][] = $type_place;
									}								
									$ex_place = true;
								}
							}
							if (!$ex_place) $arGuestNumb[$i][$type_place][$id_guest] = 0;
						}								
					}			
				}				
			}	
			
			$res_people_f = explode(';', $_POST['res_people_f']);
			$res_people_n = explode(';', $_POST['res_people_n']);
			$res_people_o = explode(';', $_POST['res_people_o']);
			$res_people_b = explode(';', $_POST['res_people_b']);
			$res_people_id = explode(';', $_POST['res_people_id']);
			for($i = 0; $i < $col; $i++) {
				if (isset($res_people_id[$i])){
					$request_people_id[$i] = explode('-', $res_people_id[$i]); 
					$request_people_f[$i] = explode('-', $res_people_f[$i]); 
					$request_people_n[$i] = explode('-', $res_people_n[$i]); 
					$request_people_o[$i] = explode('-', $res_people_o[$i]); 
					$request_people_b[$i] = explode('-', $res_people_b[$i]); 				
				}else{
					$request_people_id[$i] = Array(0);
					$request_people_f[$i] = Array();
					$request_people_n[$i] = Array();
					$request_people_o[$i] = Array();
					$request_people_b[$i] = Array();
				}
				for($j = 0; $j < sizeof($arNumbType[$i]); $j++){
					if (!isset($request_people_id[$i][$j])) $request_people_id[$i][$j] = 0;
				}
				$request_people_type[$i] = $arPeopleType[$i];
				$request_numb_type[$i] = $arNumbType[$i];				
			} 			
		}
		if (($request_people_f[0][0] == '') && ($request_people_n[0][0] == '') && ($request_people_id[0][0] == '')) {
			$request_people_id[0][0] = $GUEST['id'];
			$request_people_f[0][0] = u($GUEST['name']); 
			$request_people_n[0][0] = u($GUEST['name1']); 
			$request_people_o[0][0] = u($GUEST['name2']); 
			$request_people_b[0][0] = ''; 			
		}	
		
		$arNumbTypeShow = Array();
		for($i = 0; $i < $col; $i++) {
			for($j = 0; $j < sizeof($request_numb_type[$i]); $j++){
				if (!(isset($request_people_f[$i][$j]))) $request_people_f[$i][$j] = '';
				if (!(isset($request_people_n[$i][$j]))) $request_people_n[$i][$j] = '';
				if (!(isset($request_people_o[$i][$j]))) $request_people_o[$i][$j] = '';
				if (!(isset($request_people_b[$i][$j]))) $request_people_b[$i][$j] = '';
				if ($request_people_b[$i][$j] != '') $request_people_b[$i][$j] = strtotime($request_people_b[$i][$j]);
				$arNumbTypeShow[$i][$request_numb_type[$i][$j]][] = $request_people_type[$i][$j].';'.$request_people_id[$i][$j].';'.$request_people_f[$i][$j].';'.$request_people_n[$i][$j].';'.$request_people_o[$i][$j].';'.$request_people_b[$i][$j];
			}
		}						
		// гости
		
		// дополнительные услуги
		if (isset($_POST['res_operation'])) {
			$operation_list = explode(';', $_POST['res_operation']);
			for($i = 0; $i < $col; $i++) {
				if (isset($operation_list[$i])){
					$operation_list_aritem = explode('*', $operation_list[$i]);		
					for($j = 0; $j < sizeof($operation_list_aritem); $j++) 		
						if ($operation_list_aritem[$j] != '') {					
							$operation_list_item = explode('#', $operation_list_aritem[$j]);						
							$request_operation[$i][$j]['id'] = $operation_list_item[0];
							$request_operation[$i][$j]['code'] = $operation_list_item[1];												
							$request_operation[$i][$j]['date'] = $operation_list_item[2];
							$request_operation[$i][$j]['quant'] = $operation_list_item[3];
						}	
				}
			}
		}
		// дополнительные услуги
		
		$min_date = 0;
		$max_date = 0;
		for($i = 0; $i < $col; $i++) {
			if (($min_date > $res_request_in[$i]) || ($i == 0)) $min_date = $res_request_in[$i]; 
			if (($max_date < $res_request_out[$i]) || ($i == 0)) $max_date = $res_request_out[$i] - 1;		
		}
		$arRoomkinds = getRoomkinds($hotel, $min_date, $max_date); // запоминаем свободные номера
	}
		
	// номера		
	$arRooms = Array();
	$arResult['exist_room'] = Array();		
	$arResult['roomkinds'] = Array();		
	$arResult['free_roomkinds'] = Array();		
	
	// для проверки свободных номеров
	for($i = 0; $i < $col; $i++)
	foreach($arRoomkinds as $key=>$val) {	
		$id = $val['id'];
		$number = -1;
		foreach($val['dates'] as $date=>$coldate) {	
			if (($res_request_in[$i] <= $date) && ($date < $res_request_out[$i])) {				
					if (isset($room_array[$id][$date]))	$coldate += $room_array[$id][$date];
					$arResult['free_roomkinds'][$id][$date] = $coldate; // заполняем массив дата - количество свободных номеров, для проверки					
				}	
		}	
	}	
	// для проверки свободных номеров
	
	for($i = 0; $i < $col; $i++) {
		$numbGuestInPlace = 0;
		foreach($arGuestNumb[$i] as $type_place=>$infoGuest){				
			foreach($infoGuest as $id_guest=>$quant_guest)
				$numbGuestInPlace += $quant_guest;
		} // количество человек в заезде
		
		foreach($arRoomkinds as $key=>$val) {	
			$id = $val['id'];
			$number = -1;
			$arDates = $val['dates'];
			foreach($val['dates'] as $date=>$coldate) {	
				if ((intval($res_request_in[$i]) <= $date) && ($date < intval($res_request_out[$i]))) { 
					if ($id == $roomkind[$i]) {						
						if (isset($room_array[$id][$date])) $coldate += $room_array[$id][$date];
						$room_array[$id][$date] = 0;
						$arRoomkinds[$key]['dates'][$date] = $coldate;
					}	
					if (($number == -1) || ($number > $coldate)) $number = $coldate;
				}
			}					
			
			if (($number > 0) || ($id == $roomkind[$i])) {
				$arCheck = $_SESSION['Shelter']['Data']['RoomFactor'][$hotel][$val['id']];
				$numbPlaceForGuests = 0;
				foreach($arCheck as $idRoomfactor=>$arCheckRoomfactor){
					$numbPlaceForGuests += $arCheckRoomfactor['quant'];
				} // количество свободных мест в номере
				
				if (($numbPlaceForGuests >= $numbGuestInPlace) || ($id == $roomkind[$i]))
					$arResult['roomkinds'][$i][] = $val;				
			}	
			
			if ($id == $roomkind[$i]) {				
				$arResult['exist_room'][] = $id;
				if ($id == $roomkind[$i]) {
					$arRooms[$id] = $number;
					if (($res_colnumbers[$i] > $number) ) {
						$res_colnumbers[$i] = $number;
						if ($number > 0)
							$mess = $arMessages["message"]["numbroom"][MESSAGE_LANG];
					}elseif ($res_colnumbers[$i] < 1) $res_colnumbers[$i] = 1;						
				}	
				
				// перераспределяем количество номеров
				foreach($arDates as $date=>$coldate) 
					if ((intval($res_request_in[$i]) <= $date) && ($date < intval($res_request_out[$i]))) {
						$coldate = $arRoomkinds[$key]['dates'][$date];
						if ($coldate - $res_colnumbers[$i] >= 0) {	
							$arRoomkinds[$key]['dates'][$date] = $coldate - $res_colnumbers[$i]; 
							$arResult['free_roomkinds'][$id][$date] = $arRoomkinds[$key]['dates'][$date];
						} else {
							$res_colnumbers[$i] = 0;													
						}	
					}	
				// перераспределяем количество номеров
			}
		}
	}
	
	// если добавляем номера
	if (!(isset($_GET['edit']))) {
		$send_data = true;
		$tarif_array = Array();
		
		$arDiscounts = Array();
		if (isset($_POST['ucs_discount_id'])){
			$arResult['discountcode'] = $_POST['ucs_discount_id'];
			$arDiscounts = checkDiscountCode($_POST['ucs_discount_id']);
		} // скидки по карте		
		
		$arCommission = Array();
		if ((USE_DEBTOR_ID > 0) && (USE_DEBTOR_CONTRACT > 0)){
			if (USE_DEBTOR_CONTRACT_DISCOUNT > 0){
				$arDiscounts[] = USE_DEBTOR_CONTRACT_DISCOUNT;
			}
			if (USE_DEBTOR_CONTRACT_COMMISION > 0){
				$arCommission[] = USE_DEBTOR_CONTRACT_COMMISION;			
			}
		} // для организации
		
		for($i = 0; $i < sizeof($tarif_array); $i++){
			$find_id = false;
			for($j = 0; $j < sizeof($tarif_array[$i]); $j++){			
				if ($tarif_array[$i][$j]['id'] == $tarif[$i]) $find_id = true;
			}
			if (!$find_id) $tarif[$i] = $tarif_array[$i][0]['id'];
		}
		
		for($i = 0; $i < $col; $i++) 
			if (in_array($roomkind[$i], $arResult['exist_room'])) {
			
			if (!isset($arPackageServiceCheck[$i])) $arPackageServiceCheck[$i] = Array();
			
			$rkr = new Reservation($hotel, intval($request_in[$i]), intval($request_out[$i]), $request_in_time[$i], $request_out_time[$i],  $roomkind[$i], $arGuestNumb[$i], $arDiscounts, $arCommission, $arPackageServiceCheck[$i]);	
			$obj = $rkr->Request($rkr->GetXML());				
			$tarif_array[$i] = tarif_read($obj);
			
			if (!($tarif_array[$i][0]['id'] > 0)) {
				$send_data = false;
				$arNoShowPrice[] = $i;
			}	
		} else
			$tarif_array[$i] = tarif_read(0);
	}
	// если добавляем номера
	
	// -------------------------------------
	$arCost = Array();
	$arFullCost = Array();
	$arDiscountCost = Array();	
	
	for($i = 0; $i < $col; $i++){
		$arCost[$i] = 0;
		$arFullCost[$i] = 0;
		$arDiscountCost[$i] = 0;
	}	
		
	for($i = 0; $i < $col; $i++) {
		$sum = 0;
		$fullsum = 0;
		$discountsum = 0;		
		if ($tarif[$i] > 0) {				
			for($j = 0; $j < sizeof($tarif_array[$i]); $j++) 
				if ($tarif[$i] == $tarif_array[$i][$j]['id']){ 
					$sum = $tarif_array[$i][$j]['cost']*$res_colnumbers[$i];					
					$fullsum = $tarif_array[$i][$j]['fullcost']*$res_colnumbers[$i];					
					$discountsum = $tarif_array[$i][$j]['discount']*$res_colnumbers[$i];					
				}	
		}
		if ($sum == 0){
			$tarif[$i] = $tarif_array[$i][0]['id'];
			$sum = $tarif_array[$i][0]['cost']*$res_colnumbers[$i];
			$fullsum = $tarif_array[$i][0]['fullcost']*$res_colnumbers[$i];					
			$discountsum = $tarif_array[$i][0]['discount']*$res_colnumbers[$i];
		}

		$arCost[$i] += $sum;
		$arFullCost[$i] += $fullsum;
		$arDiscountCost[$i] += $discountsum;
	}
	// -------------------------------------
	
	if (!(checkAuthUser())) $send_data = false;
	
	// номера с описанием
	$arResult['room_code'] = Array();
	foreach($arRoomkinds as $key=>$val) {
		$id = $val['id'];
		$arResult['room_code'][$id] = Array($val['code'], $val['name']);
	}
	// номера с описанием
	
	for($i = 0; $i < $col; $i++) {		
		$arResult['request_in'][$i] = toIdate($res_request_in[$i]);
		$arResult['request_out'][$i] = toIdate($res_request_out[$i]);		
		$arResult['request_in_time'][$i] = getDM(toItime($request_in_time[$i]));		
		$arResult['request_out_time'][$i] = getDM(toItime($request_out_time[$i]));		
	}
	
	$arResult['operation'] = getOperation($hotel); // дополнительные услуги	
	
	// выбираем новые тарифы для клиента при смене номера
	
	// проверка количества гостей в номерах
	$arTypeNumbError = checkTypeNumb($arGuestNumb, $roomkind);
	if (sizeof($arTypeNumbError) > 0) {
		$send_data = false;
		foreach($arTypeNumbError as $i=>$e){
			$arNoShowPrice[] = $i;
		}		
	}	
	
	$arMainTypeNumbError = checkMainTypeNumb($arGuestNumb, $roomkind);
	if (sizeof($arMainTypeNumbError) > 0) {
		$send_data = false;
		foreach($arMainTypeNumbError as $i=>$e){
			if ($arMainTypeNumbError[$i]) $arNoShowPrice[] = $i;
		}
	}	
	
	$arShowNumb = Array();
	$arShowC = Array();
	$arGuestNumbInRate = Array();
	for($i = 0; $i < $col; $i++){
		$arShowC[$i] = 0;
		$roomInfo = $_SESSION['Shelter']['Data']['RoomFactor'][$hotel][$roomkind[$i]];
		foreach($arGuestNumb[$i] as $type_place=>$infoGuest){
			if ($roomInfo[$type_place]['quant'] > 0) 
				$arShowC[$i]++;
			
			foreach($infoGuest as $id_guest_ip=>$numb_guest_ip){
				if (!(isset($arGuestNumbInRate[$i]))) $arGuestNumbInRate[$i] = 0;
				$arGuestNumbInRate[$i] += $numb_guest_ip;
			}
		}
		if ($arShowC[$i] > 1) $arShowNumb[$i] = true;
			else $arShowNumb[$i] = false;
	}		
	
	// если ошибка, подсвечиваем тип номера и количество
	$arResult['color'] = Array();
	for($i = 0; $i < $col; $i++) {
		$arResult[$i]['color'] = false;
		
		if ($res_colnumbers[$i] < 1) {
			$mess = $arMessages["Reserv"]["notyperooms"][MESSAGE_LANG];
			$arNoShowPrice[] = $i;
			$arResult[$i]['color'] = true;
			$send_data = false;
		}else{
			foreach($arResult['free_roomkinds'] as $id=>$arData) {
				if ($id == $roomkind[$i]) {
					foreach($arData as $date_room => $col_room)
						if ($col_room < 0) {
							$arNoShowPrice[] = $i;
							$arResult[$i]['color'] = true;
							$send_data = false;
						}
				}
			}
		}
	}
	// если ошибка, подсвечиваем тип номера и количество
	
	$arResult['Commission'] = Array();
	for($i = 0; $i < $col; $i++){
		for($j = 0; $j < sizeof($tarif_array[$i]); $j++){			
			if (($tarif_array[$i][$j]['commission'] > 0) && USE_DEBTOR_COMMISSION_SHOW){
				$arResult['Commission'][$i][$tarif_array[$i][$j]['id']] = $tarif_array[$i][$j]['commission']*$res_colnumbers[$i];
			}	
		}
	}	
	
	$arNoShowPrice = array_values(array_unique($arNoShowPrice));
	
	$show_price = true;
	if (sizeof($arNoShowPrice) > 0) $show_price = false;	
	for($i = 0; $i < $col; $i++){
		$show = 1;
		if (in_array($i, $arNoShowPrice)) $show = 0;
		$arShowPrice[$i] = $show;
	}
	
	if (!(isset($creditcard))) $creditcard = '';
	if (!(isset($creditcarddate))) $creditcarddate = '';
	
	$arResult['type_pay'] = $arPayData;
	$arResult['show_creditcard_payment'] = unserialize(SHOW_CREDIT_CARD_FOR_TYPE_PAYMENT);
	$arResult['contract'] = $contract_id;
	$arResult['order_operation'] = $request_operation;
	$arResult['hotel'] = $hotel;	
	$arResult['contact'] = $contact;
	$arResult['phone'] = $phone;
	$arResult['email'] = $email;	
	$arResult['creditcard'] = $creditcard;	
	$arResult['creditcarddate'] = $creditcarddate;	
	$arResult['reservation_id'] = $reservation_id;
	$arResult['payment'] = $payment;
	$arResult['inquiry_id'] = $inquiry_id;
	$arResult['claim_id'] = $claim_id;
	$arResult['res_rooms'] = $arRooms; 
	$arResult['request_people_info'] = $arNumbTypeShow;
	$arResult['res_tarif'] = $tarif;
	$arResult['type_user'] = $type_user;
	$arResult['send_data'] = $send_data;	
	$arResult['no_show_price'] = $arShowPrice;	
	$arResult['no_show_price_total'] = $show_price;	
	$arResult['res_colnumbers'] = $res_colnumbers; 
	$arResult['tarif_array'] = $tarif_array;
	$arResult['room'] = $roomkind;	
	$arResult['col'] = $col;			
	$arResult['type_numb_error'] = $arTypeNumbError;	
	$arResult['type_main_numb_error'] = $arMainTypeNumbError;	
	$arResult['show_numb'] = $arShowNumb;	
	$arResult['room_info'] = $_SESSION['Shelter']['Data']['RoomFactor'][$hotel];	
	$arResult['guest_numb'] = $arGuestNumb;	
	$arResult['guest_numb_in_rate'] = $arGuestNumbInRate;	
	$arResult['lang'] = $_SESSION['Shelter']['System']['Lang'];	
	$arResult['type_guest'] = $_SESSION['Shelter']['Data']['TypeGuest'][$hotel];	
	$arResult['send_code'] = setCaptcha();	
	
	// дополнительные услуги
	$dopuslugi = 0;
	$arCostDopuslugi = Array();
	$arResult['dopuslugi'] = Array();
	for($i = 0; $i < $col; $i++) 
		$arCostDopuslugi[$i] = 0;
		
	for($i = 0; $i < $col; $i++){
		for($j = 0; $j < sizeof($arResult['operation']); $j++) {
			
			$arResult['dopuslugi'][$i][$j]['id'] = $arResult['operation'][$j]['id'];
			$arResult['dopuslugi'][$i][$j]['quant'] = $arResult['operation'][$j]['quant'];
			$arResult['dopuslugi'][$i][$j]['cost'] = $arResult['operation'][$j]['cost'];
			$arResult['dopuslugi'][$i][$j]['check'] = 0;
			
			if (isset($arResult['order_operation'][$i])){
				for($k = 0; $k < sizeof($arResult['order_operation'][$i]); $k++) 
				if ($arResult['order_operation'][$i][$k]['code'] == $arResult['operation'][$j]['code']) {
					$dopuslugi += $arResult['operation'][$j]['cost']*$arResult['order_operation'][$i][$k]['quant'];
					$arCostDopuslugi[$i] += $arResult['operation'][$j]['cost']*$arResult['order_operation'][$i][$k]['quant'];
					
					$arResult['dopuslugi'][$i][$j]['id'] = $arResult['order_operation'][$i][$k]['id'];
					$arResult['dopuslugi'][$i][$j]['quant'] = $arResult['order_operation'][$i][$k]['quant'];
					$arResult['dopuslugi'][$i][$j]['check'] = 1;
				}
			}	
		}	
	}	
	// дополнительные услуги
	
	for($i = 0; $i < $col; $i++) 
		$arCost[$i] += $arCostDopuslugi[$i];
	
	$cost = 0;
	$fullcost = 0;
	$discountcost = 0;
	for($i = 0; $i < $col; $i++){
		$cost += $arCost[$i];
		$fullcost += $arFullCost[$i];
		$discountcost += $arDiscountCost[$i];
	}
	
	$arResult['cost'] = $cost;	
	$arResult['fullcost'] = $fullcost;	
	$arResult['discountcost'] = $discountcost;	
	
	$arResult['arCost'] = $arCost;	
	$arResult['arFullCost'] = $arFullCost;	
	$arResult['arDiscountCost'] = $arDiscountCost;		
		
	if ($_POST['ajax'] != '') {
		$arResult['ajax'] = 1;		
	}else{ 
		$arResult['ajax'] = 0;
	}
	
	$arResult['mess'] = getMessageForUser($mess);
	if (!(isset($info))) $info = '';
	$arResult['info'] = str_replace('\n', '
', $info);
	$arResult['infoLen'] = strlen(iUW($arResult['info']));
	$arResult['commenttextlen'] = str_replace('#LEN#', RESERV_INFO_LEN, $arMessages["Reserv"]["commenttextlen"][MESSAGE_LANG]);
	
	$arResult['captcha'] = setCaptcha();
	$arResult['captcha_user'] = CAPTCHA_USER;
	$arResult['captcha_reserv'] = CAPTCHA_RESERV;
	
	$arResult['packageServiceList'] = getPackageServices();
	$arResult['packageServiceCheck'] = $arPackageServiceCheck;
	
?>