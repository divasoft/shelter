﻿<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	auth();
	if (authFirm()) {
		header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true));
		exit;
	}
	
	$mess = '';
	if (isset($_POST['email'])) {
		$_POST['email'] = strtolower($_POST['email']);
		
		if (!checkMail($_POST['email'])) $_POST['email'] = '';
		if (!checkPhone($_POST['phone'])) $_POST['phone'] = '';
		
		if (($_POST['phone'] != '') && ($_POST['email'] != '') && ($_POST['name'] != '') && ($_POST['name1'] != '')) {
		
		$check = true;
		$name = u(protection(strval($_POST['name'])));
		$name1 = u(protection(strval($_POST['name1'])));
		$name2 = u(protection(strval($_POST['name2'])));
		$phones = u(protection(strval($_POST['phone'])));
		$newpass = '';
		$email = '';		
		if (u(strval($_POST['email'])) != $GUEST['email']) $email = u(protection(strval($_POST['email'])));
		
		// проверка почты
		if ($email != '') {
			include_once(PATH_MODULES.PAGE_FORGETPASSWORD.'.php');	
			$rkr = new ForgetPassword($email);	
			$obj = $rkr->Request($rkr->GetXML());
			if (($obj->attributes()->exists > 0) || (!$obj)) {
				$check = false;
				$mess = $arMessages["message"]["mailexist"][MESSAGE_LANG];
			}
		}
		// проверка почты
		
		// проверка пароля
		if (($_POST['pass'] != '') && $check) {	
			if (latin($_POST['pass'])) {
				if (strlen($_POST['pass']) >= MIN_PASS) {
					$newpass = protection(strval($_POST['pass']));
				}else{
					$check = false;
					$mess = $arMessages["AboutUser"]["length"][MESSAGE_LANG];					
				}	
			}else{
				$check = false;
				$mess = $arMessages["AboutUser"]["allowed"][MESSAGE_LANG];			
			}
		}
		// проверка пароля
		
		if ($check) {
			$rkr = new AboutUser($GUEST['email'], $GUEST['pass'], $newpass, $email, $name, $name1, $name2, setFullName($name, $name1, $name2), $phones);	
			$obj = $rkr->Request($rkr->GetXML());
			
			if (intval($obj->attributes()->id) > 0) {	
				$mess = $arMessages["message"]["change"][MESSAGE_LANG];
				
				if ($email != '') $_SESSION['Shelter']['User']['Login'] = $email;
				if ($newpass != '') $_SESSION['Shelter']['User']['Code'] = setKS($newpass);
				
				$GUEST = getUser();
				
				if ($newpass != '') {
					$arData = Array(
						'FIO' => getFullName(), //$GUEST['fullname'],
						'PHONE' => $GUEST['phones'],
						'LOGIN' => $GUEST['email'],
						'PASSWORD' => $GUEST['pass'],
					);			
					if (sendMail($GUEST['email'], 'AboutUser', $arData)) $mess .= '. '.$arMessages["general"]["mail_send"][MESSAGE_LANG];
				}
				
				$_SESSION['Shelter']['Info']['Message'] = $mess;
				header('location: '.change_address());	
				exit;
			}else 
				$mess = $arMessages["AboutUser"]["error"][MESSAGE_LANG];
		}
	}else 
		$mess = $arMessages["AboutUser"]["all"][MESSAGE_LANG];
	}
	
	$arResult['mess'] = getMessageForUser($mess);
?>
