<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	$error = false;	
	$arResult['correct'] = false;
	$arResult['info'] = Array();
	
	if ($_GET['code'] != md5($_GET['id'].KEY_SECURITY)) {
		$error = true;		
		$_SESSION['Shelter']['Info']['Message'] = $arMessages["ReservationNR"]["error"][MESSAGE_LANG];
	}
	
	if (($_GET['id'] != '') && ($_GET['email'] != '') && ($_GET['code'] != '') && (!$error)) {	
		$id = protection(intval($_GET['id']));		
		$email = protection(strval($_GET['email']));		
				
		include_once(PATH_MODULES.PAGE_RESERVINFO.'.php');
		$rkr = new ReservationInfo($id, 0);
		$obj = $rkr->Request($rkr->GetXML());	
		
		if (intval($obj->attributes()->state) == 2) {			
			$error = true;			
			$_SESSION['Shelter']['Info']['Message'] = $arMessages["ReservationNR"]["success"][MESSAGE_LANG];
		}
		if (!(strval($obj->attributes()->email) == $email)) {
			$error = true;	
			$_SESSION['Shelter']['Info']['Message'] = $arMessages["ReservationNR"]["error"][MESSAGE_LANG];					
		}		
		
		if (!$error) {
			$arResult['correct'] = true;
			
			$arResult['AboutReserv'] = getAboutReservInfo($obj);
			
			$arResult['info']['id'] = intval($obj->attributes()->id);
			$arResult['info']['code'] = intval($obj->attributes()->code);
			$arResult['info']['arrival'] = toIdate(strval($obj->attributes()->arrival), 'd.m.Y H:i');
			$arResult['info']['departure'] = toIdate(strval($obj->attributes()->departure), 'd.m.Y H:i');
			$arResult['info']['contact'] = strval($obj->attributes()->contact);
			$arResult['info']['summa'] = floatval($obj->attributes()->summa);
			$arResult['info']['status'] = intval($obj->attributes()->status);
			$arResult['info']['state'] = intval($obj->attributes()->state);
			$arResult['info']['balance'] = floatval($obj->attributes()->balance);		
			$arResult['info']['arrivalInt'] = floatval($obj->attributes()->arrival);
			
			$arResult['info']['deleteAllow'] = false;
			if (!(isset($_GET['nocancel'])))
				$arResult['info']['deleteAllow'] = checkReservForDelete($arResult['info']['arrivalInt'], $arResult['info']['status'], $arResult['info']['state'], $arResult['info']['balance']);
			
			$GUEST['id'] = intval($obj->attributes()->owner);		
			$GUEST['fio'] = strval($obj->attributes()->contact);		
			$GUEST['phones'] = strval($obj->attributes()->phone);		
			$GUEST['email'] = strval($obj->attributes()->email);		
			$arResult['info']['payment'] = 0;
			$arResult['info']['pay'] = $arMessages["UserCabinet"]["nopaid"][MESSAGE_LANG];
			if (($arResult['info']['balance'] >= $arResult['info']['summa']) && ($arResult['info']['summa'] > 0)) { 
				$arResult['info']['pay'] = $arMessages["UserCabinet"]["paid"][MESSAGE_LANG];
				$arResult['info']['payment'] = 1;
			}	
		}
	}
	
	if (($_POST['count'] != '') && ($arResult['correct'])) {
		if (($_SESSION['Shelter']['Info']['Code'] == $_POST['count']) && ($_SESSION['Shelter']['Info']['Code'] != '')) {
			if ($code = deleteReserv($id)){
				$arData = Array(				
					'RESID' => $code,	
					'FIO' => $GUEST['fio'],
					'PHONE' => $GUEST['phones'],
					'EMAIL' => $GUEST['email'],
				);							
				if (sendMail($GUEST['email'], 'ReservationDelete', $arData)) $send = $arMessages["general"]["mail_send"][MESSAGE_LANG];
				
				sendMailForMeneger('ForMenegerReservationDelete', $arData, $code);				
				
				$_SESSION['Shelter']['Info']['Message'] = $arMessages["ReservationNR"]["success"][MESSAGE_LANG];
			}
			header('location: '.change_address());
			exit;
		}else
			$_SESSION['Shelter']['Info']['Message'] = $arMessages["ReservationNR"]["count"][MESSAGE_LANG];		
	}		
	
	$arResult['mess'] = getMessageForUser();
	
	if (PAYMENT && (!$error) && (!$arResult['info']['payment'])) {	
		include_once(PATH_CONTOLLER.PAGE_RESERVPAY.'.php');
	}	
?>