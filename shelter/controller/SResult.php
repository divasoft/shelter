<?	
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	if (SHOW_RESULT_PAGE && RESULT_PAGE_ONLY) {
		header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_RESULT_SECOND), true));
		exit;
	}
	
	checkBookingAllow();
	
	getDefDates();
	
	if ($_SESSION['Shelter']['Find'] != '') {
		$hotel = intval(protection($_SESSION['Shelter']['Find']['hotel']));
		
		$request_in = toDdate(protection($_SESSION['Shelter']['Find']['request_in']));
		$request_out = toDdate(protection($_SESSION['Shelter']['Find']['request_out'])) - 1;	
		if ($request_out < $request_in) $request_out = $request_in;
		
		$arGuestNumb = setNumbGuestSResultRate();
		$arRooms = getRoomkindsNoRatesSResultRate($hotel, $request_in, $request_out, $arGuestNumb);
		
		$arNoShowRates = unserialize(NO_SHOW_RATES);
		$arRoomkinds = Array();
		$arResult['exist'] = false;
		$daysReservation = $request_out - $request_in + 1;
		foreach ($arRooms as $roomkind) {
			$tmp_roomkind = array();
			$tmp_roomkind['id'] = intval($roomkind->attributes()->id);
			$tmp_roomkind['code'] = i(strval($roomkind->attributes()->code));
			$tmp_roomkind['name'] = i(strval($roomkind->attributes()->name));
			$tmp_roomkind['minNEW'] = 1000;
			$tmp_roomkind['ratesInfo'] = array();
			$rateNoShow = array();			
			
			foreach ($roomkind as $record) {
				$room = intval($record->attributes()->amountvacancyrooms);				
				if ($room < $tmp_roomkind['minNEW']) $tmp_roomkind['minNEW'] = $room;
				
				foreach($record->rate as $i=>$rate){
					$id_rate = intval($rate->attributes()->id).'#'.setParamToArray($rate['param']);
					$write = false;
					if ((sizeof($arNoShowRates) == 0) || (!in_array(strval($rate->attributes()->code), $arNoShowRates))){
						if (checkMainNumbForReserv($tmp_roomkind['id'], setParamToArray($rate['param']))){
							if (checkDaysForRate($daysReservation, $rate)){
								$tmp_roomkind['ratesNEW'][$id_rate][intval($record->attributes()->date)] = floatval($rate->attributes()->sumcost);
								$tmp_roomkind['ratesInfo'][$id_rate] = Array(
									'code' => i(strval($rate->attributes()->code)),
									'name' => i(strval($rate->attributes()->name)),
									'isfixcost' => intval($rate->attributes()->isfixcost),
								);	
								$write = true;
							}else{
								$sum = $tmp_roomkind['ratespossibleNEW'][$id_rate]['sum'] + floatval($rate->attributes()->sumcost);
								$tmp_roomkind['ratespossibleNEW'][$id_rate] = Array(
									'sum' => $sum,
									'name' => i(strval($rate->attributes()->name)),
									'rules' => Array('days1'=>intval($rate->attributes()->days1), 'days2'=>intval($rate->attributes()->days2)),
								);
							}
						}
					}
					if (!$write) {
						if (!(in_array($id_rate, $rateNoShow))) $rateNoShow[] = $id_rate;
					}	
				}				
			}	
			
			$min_date_ok = true;	
			
			// ------------------------------
			foreach($tmp_roomkind['ratesNEW'] as $key=>$arDateSum){
				if ($tmp_roomkind['ratesInfo'][$key]['isfixcost'] == 0){
					$firstElement = true;
					$lastElement = 0;
					$check = true;
					
					foreach($arDateSum as $date=>$sum){
						if ($firstElement){
							if ($request_in != $date) $check = false;
							$firstElement = false;
						} // check first element
						else{
							if (($lastElement+1) != $date) $check = false;
						}
						$lastElement = $date;
					}
					if ($lastElement != $request_out) $check = false;
					
					if (!$check) unset($tmp_roomkind['ratesNEW'][$key]);				
				}
			} // check rates for all day
			
			foreach($tmp_roomkind['ratesNEW'] as $key=>$arParam){
				if ((sizeof($arParam) == 0) || (in_array($key, $rateNoShow))) {
					unset($tmp_roomkind['ratesNEW'][$key]);
					unset($tmp_roomkind['ratesInfo'][$key]);
				}	
			}
			foreach($tmp_roomkind['ratesNEW'] as $key=>$arParam){
				$cost = 0;
				foreach($arParam as $date=>$sum){
					$cost += $sum;
				}
				$tmp_roomkind['ratesNEW'][$key] = $cost;
			}
			// ------------------------------
			
			if ((($tmp_roomkind['minNEW'] < $_SESSION['Shelter']['Find']['request_colnumber']) && FIND_NUMBER_USE) || ($tmp_roomkind['minNEW'] < 1)) $min_date_ok = false;
			if ($min_date_ok) {
				if ((sizeof($tmp_roomkind['ratesNEW']) > 0) || ($tmp_roomkind['ratespossibleNEW'] > 0)){
					$arResult['exist'] = true;
					$arRoomkinds[] = $tmp_roomkind;
				}
			}	
		}
		
		// -------------------------------------- 		
		for($i = 0; $i < sizeof($arRoomkinds); $i++){
			$min_price = 0;
			foreach($arRoomkinds[$i]['ratesNEW'] as $id=>$price){
				if (($min_price > $price) || ($min_price == 0)) $min_price = $price;
			}
			$arRoomkinds[$i]['minPrice'] = $min_price;
		}
		
		for($i = 0; $i < (sizeof($arRoomkinds) - 1); $i++)
			for($j = ($i+1); $j < sizeof($arRoomkinds); $j++) 
				if ($arRoomkinds[$i]['minPrice'] > $arRoomkinds[$j]['minPrice']) {
					$arElement = $arRoomkinds[$i];
					$arRoomkinds[$i] = $arRoomkinds[$j];
					$arRoomkinds[$j] = $arElement;					
				}
				
		for($i = 0; $i < sizeof($arRoomkinds); $i++)
			unset($arRoomkinds[$i]['minPrice']);
		// ����������
		
		$arResult['result_request_in'] = $request_in;
		$arResult['result_request_out'] = $request_out;
		
		$arResult['max'] = getNumbRatesForClient();
	}
	
	// ---------------------------
	$quantG = 0;
	foreach($arGuestNumb as $k=>$arQuant){
		if (is_array($arQuant))
		foreach($arQuant as $s=>$quant){
			$quantG += $quant;
		}
	}
	
	$arRoomsEx = Array();
	for($i = 0; $i < sizeof($arRoomkinds); $i++){
		$id = $arRoomkinds[$i]['id'];
		$place = 0;
		foreach($_SESSION['Shelter']['Data']['RoomFactor'][$hotel][$id] as $k=>$arPlace)
			if ($k == 2)
				$place += $arPlace['quant'];
			
		if ($quantG <= $place) $arRoomsEx[] = $arRoomkinds[$i];
	} 
	// �������� �� �������� � �������������� �����
	
	// ---------------------------	
	foreach($arRoomsEx as $id_room=>$room) { 				
		$arRate = Array();
		foreach($room['ratesNEW'] as $id=>$price) {
			$arRate[] = Array('id'=>$id, 'val'=>$price);
		}
		
		for($i = 0; $i < sizeof($arRate) - 1; $i++)
			for($j = $i+1; $j < sizeof($arRate); $j++)
			if ($arRate[$i]['val'] > $arRate[$j]['val']){
				$t = $arRate[$i];
				$arRate[$i] = $arRate[$j];
				$arRate[$j] = $t;
			}
		
		$arSum = Array();
		for($i = 0; $i < sizeof($arRate); $i++){
			$arSum[$arRate[$i]['id']] = $arRate[$i]['val'];
		}		
		
		$arRoomsEx[$id_room]['ratesNEW'] = $arSum;
	} 
	// ���������� �������
	
	// ---------------------------	
	$arResult['roomkinds'] = $arRoomsEx;
	if (sizeof($arResult['roomkinds']) == 0) $arResult['exist'] = false;
	// ---------------------------
	
	$arResult['roomInfo'] = Array();
	if (RESULT_SHOW_NUMB)
		foreach($arResult['roomkinds'] as $i=>$room)
			if (is_array($_SESSION['Shelter']['Data']['RoomFactor'][$hotel][$room['id']]))
				$arResult['roomInfo'][$room['id']] = $_SESSION['Shelter']['Data']['RoomFactor'][$hotel][$room['id']];
	
	$arResult['mess'] = getMessageForUser();
	$arResult['lang'] = $_SESSION['Shelter']['System']['Lang'];	
	$arResult['numb_rooms'] = $_SESSION['Shelter']['Find']['request_colnumber'];	
	$arResult['request_in'] = toDdate(protection($_SESSION['Shelter']['Find']['request_in']));	
	$arResult['request_out'] = toDdate(protection($_SESSION['Shelter']['Find']['request_out']));	
	$arResult['currency_show'] = unserialize(CURRENCY_SHOW_FROM_BANK);	
?>