<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	if (USER_USE) auth();
	
	$id = protection(intval($_GET['id']));
	
	$gid = $GUEST['id'];
	if (($GUEST['type'] == 1) || USER_CABINET_SHOW_RESERV_SHELTER) $gid = 0;
	
	if (!isset($_POST['summa'])) $_POST['summa'] = 0;
	
	include_once(PATH_MODULES.PAGE_RESERVINFO.'.php');
	$rkr = new ReservationInfo($id, $gid);
	$obj = $rkr->Request($rkr->GetXML());	
	
	$code = intval($obj->attributes()->code);
	$obj_summ = floatval($obj->attributes()->summa);
	$obj_balance = 0; 
	if (isset($obj->attributes()->balance)) $obj_balance = floatval($obj->attributes()->balance);
	
	if ((!(checkUser($obj) || checkUserIsGuest($obj))) || ($obj_balance >= $obj_summ) || (!PAYMENT)) {
		header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true));
		exit;
	}
	
	$summa = $obj_summ - $obj_balance;
	$min_summa = $summa;
	
	if (MIN_PAY_PERCENT == 1){
		$autoposts = $obj->inquiry->claim->autoposts->autopost;		
		$shift = 0;
		for($i = 0; $i < sizeof($autoposts); $i++){
			if (($shift > intval($autoposts[$i]->attributes()->shift)) || ($shift == 0)) $shift = intval($autoposts[$i]->attributes()->shift);
		}				
		$min_s = 0;
		for($i = 0; $i < sizeof($autoposts); $i++){
			if (isset($autoposts[$i]->attributes()->price)) {
				if ((floatval($autoposts[$i]->attributes()->price) > 0) && ($shift == intval($autoposts[$i]->attributes()->shift))) $min_s += floatval($autoposts[$i]->attributes()->price);
			}	
		}
		if ($min_s > 0) $min_summa = $min_s;
	}else{
		$min_summa = getMinSummPay($obj_summ, $summa);
	}
	
	// ---------------------- отправка данных на оплату 
	if ((file_exists(PATH_PM.'gnrt.php')) && (floatval($_POST['summa']) > 0)) {
		$user_summa = $summa;
		if (floatval($_POST['summa']) > 0)
			$user_summa = protection(floatval($_POST['summa']));
		if ($user_summa < $min_summa) $user_summa = $min_summa;
		if ($user_summa > $summa) $user_summa = $summa;
		
		include_once(PATH_PM.'gnrt.php');		
		$arParam = Array(
			'id' => $id,
			'code' => $code,
			'summa' => $user_summa,
			'obj' => $obj
		);
		sendToPaymentSystem($arParam);		
	}	
	// ---------------------- отправка данных на оплату 
	
	$user_summa = $min_summa;
	if ($obj_balance > 0) $user_summa = $summa;
	
	$arResult['original_summa'] = $obj_summ;
	$arResult['balance_summa'] = $obj_balance;	
	$arResult['user_summa'] = $user_summa;
	$arResult['min_summa'] = $min_summa;
	$arResult['max_summa'] = $summa;
	$arResult['id'] = $id;
	$arResult['code'] = intval($obj->attributes()->code);
	
?>