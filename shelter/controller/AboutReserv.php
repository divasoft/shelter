<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	if ($_POST['id'] != '') {
		$reservation_id = protection(intval($_POST['id']));
		
		$gid = $GUEST['id'];
		if ($GUEST['type'] == 1) $gid = 0;
		
		include_once(PATH_MODULES.PAGE_RESERVINFO.'.php');
		$rkr = new ReservationInfo($reservation_id, $gid);		
		$obj = $rkr->Request($rkr->GetXML());	
		
		if (!(checkUser($obj))){
			if ($_POST['about_reserv'] == 1) 
				echo $arMessages["general"]["error"][MESSAGE_LANG];
			else
				header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true));
			exit;
		}else{
			$arResult['AboutReserv'] = getAboutReservInfo($obj);
		}	
	}
	
?>
