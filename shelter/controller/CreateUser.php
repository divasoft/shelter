<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	auth(0);
	
	$mess = 0;
	$text = '';
	
	if (isset($_POST['phone'])){	
		$phone = strval($_POST['phone']);
		$_POST['phone'] = str_replace('-', '+', $phone);
		if (!checkPhone($_POST['phone'])) $_POST['phone'] = '';
	}
	if (isset($_POST['mail'])){
		$_POST['mail'] = strtolower($_POST['mail']);
		if (!checkMail($_POST['mail'])) $_POST['mail'] = '';
	}
	if (!isset($_POST['reg_reserv'])){
		$_POST['reg_reserv'] = 0;
	}
	
	if ((isset($_POST['name'])) && (isset($_POST['name1'])) && (isset($_POST['mail'])) && (isset($_POST['phone']))) {		
		if (($_POST['name'] != '') && ($_POST['name1'] != '') && ($_POST['mail'] != '') && ($_POST['phone'] != '')) {		
			if (checkCaptcha($_POST['ucs_checkcode'])){
				if (checkCaptchaUser($_POST['count'])){
					
					$_SESSION['Shelter']['Info']['Code'] = '';				
					$name = protection(strval($_POST['name']));
					$name1 = protection(strval($_POST['name1']));
					$name2 = protection(strval($_POST['name2']));
					$email = protection(strval($_POST['mail']));
					$phone = protection(strval($_POST['phone']));
					
					if ($_POST['reg_reserv'] != 1) {
						$name = u($name);
						$name1 = u($name1);
						$name2 = u($name2);
						$email = u($email);
						$phone = u($phone);
					}
					
					$fullname = setFullName($name, $name1, $name2);
					
					$rkr = new CreateUser($name, $name1, $name2, $fullname, $email, $phone);	
					$obj = $rkr->Request($rkr->GetXML());	
					$exists = intval($obj->attributes()->exists);
					$id = intval($obj->attributes()->id);
					
					if (($id > 0) && ($exists == 0)) { 			
						
						// отправка email
						$arData = Array(
							'FIO' => i($fullname),
							'LOGIN' => i($email),
							'PASSWORD' => $id,
						);
						if (sendMail($email, 'CreateUser', $arData)) $mess = 1;
							else $mess = 0;
						// отправка email
						
						if ($mess == 1) $text = $arMessages["message"]["pass_mail"][MESSAGE_LANG];	
							else $text = $arMessages["general"]["mail_error"][MESSAGE_LANG];	
										
						if ($_POST['reg_reserv'] != 1) { // просто создание
							if ($mess) {
								$_SESSION['Shelter']['Info']['Message'] = $text;
								header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_LOGINUSER), true));
								exit;
							}	
							else $arResult['mess'] = $text;				
						}				
					}else{
						$_POST['mail'] = '';
						$mess = 2;
						$text = $arMessages["CreateUser"]["exist"][MESSAGE_LANG];
					}	
				}else{
					$text = $arMessages["CreateUser"]["count"][MESSAGE_LANG];
				}
			}
		}elseif (($_POST['reg_reserv'] == 1) || ($_POST['send'] != '')) $text = $arMessages["CreateUser"]["empty"][MESSAGE_LANG];
	}
	
	if ($_POST['reg_reserv'] == 1) {		
		echo $mess.';'.$text;
		exit;		
	}
	
	
	$arResult['captcha'] = setCaptcha();
	$arResult['captcha_user'] = CAPTCHA_USER;
	
	$arResult['mess'] = '';
	if ($text != '') $arResult['mess'] = $text;
	
	$arResult['form']['name'] = '';
	$arResult['form']['name1'] = '';
	$arResult['form']['name2'] = '';
	$arResult['form']['mail'] = '';
	$arResult['form']['phone'] = '';
	if (isset($_POST['name'])) $arResult['form']['name'] = protection($_POST['name']);
	if (isset($_POST['name1'])) $arResult['form']['name1'] = protection($_POST['name1']);
	if (isset($_POST['name2'])) $arResult['form']['name2'] = protection($_POST['name2']);
	if (isset($_POST['mail'])) $arResult['form']['mail'] = protection($_POST['mail']);
	if (isset($_POST['phone'])) $arResult['form']['phone'] = protection($_POST['phone']);
	
?>
