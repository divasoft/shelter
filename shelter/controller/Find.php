<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	checkBookingAllow();
	
	getDefDates(); // ��������� ������ ����	
		
	if (isset($_GET['hotel'])) {
		setEmptySess();
		$_SESSION['Shelter']['Find']['hotel'] = getDefHotel(intval($_GET['hotel']));
		header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_FIND), true));
		exit;
	}
	
	if (isset($_POST['request_hotel'])) {
		$_SESSION['Shelter']['Find']['hotel'] = getDefHotel(intval($_POST['request_hotel']));
	}
	
	$arResult['hotel'] = $_SESSION['Shelter']['Data']['HotelList'];		
		
	$date_from = protection($_SESSION['Shelter']['Find']['request_in']);
	$date_to = protection($_SESSION['Shelter']['Find']['request_out']);	
	
	if (isset($_POST['request_in'])) {
		$hotel = protection($_SESSION['Shelter']['Find']['hotel']);
		$_SESSION['Shelter']['Info']['Message'] = '';		
		$_SESSION['Shelter']['Find']['hotel'] = $hotel;
		$_SESSION['Shelter']['Find']['request_in'] = minFDate($_POST['request_in']);
		$_SESSION['Shelter']['Find']['request_out'] = maxFDate($_SESSION['Shelter']['Find']['request_in'], $_POST['request_out']);
		
		$_SESSION['Shelter']['Find']['request_time_from'] = $_SESSION['Shelter']['Data']['TimeSettings']['From'];
		$_SESSION['Shelter']['Find']['request_time_to'] = $_SESSION['Shelter']['Data']['TimeSettings']['To'];
		if (EARLY_TIME && (isset($_POST['request_time_from'])) && (isset($_POST['request_time_to']))) {			
			$timeFR = explode(':', $_POST['request_time_from']);
			$timeTR = explode(':', $_POST['request_time_to']);
			$_SESSION['Shelter']['Find']['request_time_from'] = setDM(Array($timeFR[0], $timeFR[1]));
			$_SESSION['Shelter']['Find']['request_time_to'] = setDM(Array($timeTR[0], $timeTR[1]));
		}
		
		$numb = 1;
		if ($_POST['request_colnumber'] > 0) $numb = protection($_POST['request_colnumber']);
		$_SESSION['Shelter']['Find']['request_colnumber'] = $numb;	
		
		$guest_first = 0;
		$guest_numb = 0;
		foreach($_SESSION['Shelter']['Data']['TypeGuest'][$hotel] as $id=>$name){
			$numb = 0;
			$r_numb = intval(protection($_POST['request_guest_'.$id]));
			if ($r_numb > 0) $numb = $r_numb;
			
			$_SESSION['Shelter']['Find']['guest'][$id] = $numb;
			
			if ($guest_first == 0) $guest_first = $id;
			$guest_numb += $numb;
		}
		if ($guest_numb == 0) $_SESSION['Shelter']['Find']['guest'][$guest_first] = 1;
		
		$goPage = PAGE_RESULT_FIRST;
		if (SHOW_RESULT_PAGE) $goPage = PAGE_RESULT_SECOND;
		
		if (isset($_GET['ucsredirect'])){
			$newPage = protection($_GET['ucsredirect']);
			if (($newPage == PAGE_RESULT_FIRST) || (($newPage == PAGE_RESULT_SECOND))){
				$goPage = $newPage;
			}
		}	
		
		header('location: '.change_address(Array(MODULE_SHEDULE=>$goPage), true));
		exit;
	}
	
	$arResult['Shotel'] = protection($_SESSION['Shelter']['Find']['hotel']);
	$arResult['TypeGuest'] = $_SESSION['Shelter']['Data']['TypeGuest'][$arResult['Shotel']];
	$arResult['NumbRoom'] = protection($_SESSION['Shelter']['Find']['request_colnumber']);
	$arResult['NumbGuest'] = $_SESSION['Shelter']['Find']['guest'];
?>