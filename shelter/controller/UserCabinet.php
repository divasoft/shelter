<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	auth();
	
	$gid = $GUEST['id'];
	$is_active = 1;
	if ($GUEST['type'] == 1) {
		$gid = $GUEST['debtor'];
		if (isset($_GET['allreservation'])){
			if ($_GET['allreservation'] == 1) $is_active = 0;
		}		
	}
	
	$rkr = new UserCabinet($gid, $GUEST['type'], $is_active);
	$obj = $rkr->Request($rkr->GetXML());
	$arInfo = Array();
	$arInfo[] = Array('onlinereserv'=>1, 'data'=>$obj);
	if (USER_CABINET_SHOW_RESERV_SHELTER){
		$obj = $rkr->Request($rkr->GetXML(true));
		$arInfo[] = Array('onlinereserv'=>0, 'data'=>$obj);
	}	
	
	$reserves = Array();
	for($i = 0; $i < sizeof($arInfo); $i++)
	foreach ($arInfo[$i]['data'] as $res) {
		$reserv = array();
		if (($res->attributes()->state == 1) || (($GUEST['type'] == 1) && ($is_active == 0))) {
			if (($res['summa'] > 0) && ($res['sumdiscount'] < 0)){
				$res['summa'] = $res['summa'] + $res['sumdiscount'];				
			}
			foreach ($res->attributes() as $k => $val) {
				$reserv[$k] = i(strval($val));
			}
			$reserv['onlinereserv'] = $arInfo[$i]['onlinereserv'];
			$reserves[] = $reserv;
		}
	}
	
	$arResult['mess'] = getMessageForUser();
		
	$arResult['reserves'] = $reserves;
	
	foreach ($arResult['reserves'] as $id=>$res) {		
		$paid = 0;
		if (($res['balance'] < $res['summa']) && ($res['summa'] > 0) && ($res['balance'] > 0)) $paid = 1;
		if (($res['balance'] >= $res['summa']) && ($res['summa'] > 0)) $paid = 2;
		
		$active = 1;
		if (strtotime(toIdate($res['departure'], 'd.m.Y')) < strtotime(date('d.m.Y', time()))) $active = 0;
		
		$remove = 0;
		if ($res['state'] == 2) $remove = 1;
		
		$arResult['reserves'][$id]['COST'] = $res['summa'] - $res['balance'];		
		$arResult['reserves'][$id]['PAID'] = $paid;		
		$arResult['reserves'][$id]['REMOVE'] = $remove;		
		$arResult['reserves'][$id]['ACTIVE'] = $active;		
		$arResult['reserves'][$id]['EDIT'] = checkReservForEdit($res['arrival']);		
		$arResult['reserves'][$id]['DELETE'] = checkReservForDelete($res['arrival'], $res['status'], $res['state'], $res['balance']);		
		
		$arResult['reserves'][$id]['PAY'] = 0;		
		if (PAYMENT && ($arResult['reserves'][$id]['PAID'] < 2)) $arResult['reserves'][$id]['PAY'] = 1;		
		
		if ($res['owner'] != $GUEST['id']){
			$arResult['reserves'][$id]['EDIT'] = 0;
		}
		if ((!(getBookingAllow())) || ($arResult['reserves'][$id]['REMOVE'])){
			$arResult['reserves'][$id]['PAY'] = 0;
			$arResult['reserves'][$id]['EDIT'] = 0;
			$arResult['reserves'][$id]['DELETE'] = 0;
		}
		
		if (!$res['onlinereserv']){
			$arResult['reserves'][$id]['EDIT'] = 0;
			$arResult['reserves'][$id]['DELETE'] = 0;
		}
		
		$arResult['reserves'][$id]['COMMISSION'] = 0;
		if (($GUEST['type'] == 1) && USE_DEBTOR_COMMISSION_SHOW) {
			$arResult['reserves'][$id]['COMMISSION'] = $res['sumcommission'];
		}
	}
	
	$arResult['firmCabinet'] = Array();
	foreach ($obj as $res) 
		if (($res->attributes()->state == 1) || (($GUEST['type'] == 1) && ($is_active == 0))) {
			$arResult['firmCabinet']['number'][] = Array(
				'id' => intval($res->attributes()->id),
				'code' => intval($res->attributes()->code)
			);	
			
			$hotel = str_replace('"', '', strval($res->attributes()->hotelname));
			if (!(isset($arResult['firmCabinet']['hotel']))) $arResult['firmCabinet']['hotel'] = Array();
			if (!in_array($hotel, $arResult['firmCabinet']['hotel'])) $arResult['firmCabinet']['hotel'][] = $hotel;
		}
	//echo '<pre>'; print_r($arResult['reserves']); echo '</pre>'; 
?>