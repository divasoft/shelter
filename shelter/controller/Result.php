<?	
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	if ((!SHOW_RESULT_PAGE) && RESULT_PAGE_ONLY) {
		header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_RESULT_FIRST), true));
		exit;
	}
	
	checkBookingAllow();
	
	getDefDates();
	
	if ($_SESSION['Shelter']['Find'] != '') {
		$hotel = intval(protection($_SESSION['Shelter']['Find']['hotel']));
		
		$request_in = toDdate(protection($_SESSION['Shelter']['Find']['request_in']));
		$request_out = toDdate(protection($_SESSION['Shelter']['Find']['request_out']));	
		if ($request_out < $request_in) $request_out = $request_in;
		
		$roomkinds = getRoomkinds($hotel, $request_in, $request_out);
		
		$arResult['roomkinds'] = $roomkinds;
		$arResult['result_request_in'] = $request_in;
		$arResult['result_request_out'] = $request_out;
		
		$type_user = getNumbRatesForClient();
		$arResult['max'] = $type_user;		
	}
	
	$arResult['exist'] = false;
	if (sizeof($arResult['roomkinds']) > 0) $arResult['exist'] = true;
	
	$arResult['roomInfo'] = Array();
	if (RESULT_SHOW_NUMB)
		foreach ($arResult['roomkinds'] as $roomkind)
			if (is_array($_SESSION['Shelter']['Data']['RoomFactor'][$hotel][$roomkind['id']]))
				$arResult['roomInfo'][$roomkind['id']] = $_SESSION['Shelter']['Data']['RoomFactor'][$hotel][$roomkind['id']];
	
	
	$arResult['mess'] = getMessageForUser();
	$arResult['lang'] = $_SESSION['Shelter']['System']['Lang'];
	
?>