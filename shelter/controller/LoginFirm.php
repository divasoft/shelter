<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	
	if (!USE_DEBTORS_AUTH) {
		header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_LOGINUSER), true));		
		exit;
	}	
	
	if (authFirm()) {
		header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true));
		exit;
	}
	
	$mess = '';
	
	if (isset($_POST['login']) && isset($_POST['pass'])) {			
		if (checkCaptcha($_POST['ucs_checkcode'])){
			$login = protection($_POST['login']);
			$rkr = new LoginFirm($login, protection($_POST['pass']));	
			$obj = $rkr->Request($rkr->GetXML());	
			$id = intval($obj->attributes()->id);
			$password = strval($obj->attributes()->password);
			
			if ($id > 0) {
				setUserData($login, $password, 1);			
				header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true));
				exit;	
			}else{
				$mess = $arMessages["LoginFirm"]["error"][MESSAGE_LANG];
			}		
		}
	}
	
	$arResult['mess'] = getMessageForUser($mess);
		
	if (!isset($_POST['login'])) $_POST['login'] = '';
	
	$arResult['captcha'] = setCaptcha();
?>
