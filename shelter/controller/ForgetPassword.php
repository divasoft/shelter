<?
	if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();
	auth(0);
	
	$status = 0;
	$mess = '';
	$arResult['form']['login'] = '';
	if (isset($_POST['login']))  {
		if (checkCaptcha($_POST['ucs_checkcode'])){
			if (checkCaptchaUser($_POST['count'])){
				$login = strtolower(protection(strval($_POST['login'])));
				$arResult['form']['login'] = $login;
				$rkr = new ForgetPassword($login);	
				$obj = $rkr->Request($rkr->GetXML());
				
				$exists = intval($obj->attributes()->exists);		
				if ($exists > 0) {
					$fio = setFullName($obj->attributes()->name, $obj->attributes()->name1, $obj->attributes()->name2); //strval($obj->attributes()->fullname);
					$password = strval($obj->attributes()->passwrd);				
					$arData = Array(
						'FIO' => i($fio),
						'LOGIN' => i($login),
						'PASSWORD' => ($password),
					);		
					if (sendMail($login, 'ForgetPassword', $arData)) {
						$status = 1;
						$mess = $arMessages["ForgetPassword"]["send"][MESSAGE_LANG];
					}else	
						$mess = $arMessages["general"]["mail_error"][MESSAGE_LANG];	
						
					if (!isset($_POST['forget_reserv'])) {
						$_SESSION['Shelter']['Info']['Message'] = $mess;
						header('location: '.change_address(Array(MODULE_SHEDULE=>PAGE_USERCABINET), true));			
						exit;
					}	
				}else
					$mess = $arMessages["ForgetPassword"]["error"][MESSAGE_LANG];			
			}else{
				$mess = $arMessages["CreateUser"]["count"][MESSAGE_LANG];		
				$status = 2;	
			}	
		}	
	} 
	
	$arResult['mess'] = getMessageForUser($mess);
	
	$arResult['captcha'] = setCaptcha();
	$arResult['captcha_user'] = CAPTCHA_USER;
	
	if (isset($_POST['forget_reserv'])) {
		echo $status.';'.$mess;
	}
	
	
?>
