<?php

error_reporting(0);
 
include_once('config.php');	
if(!defined("PATH_CORRECT") || PATH_CORRECT!==true)die();

if (isset($_POST['check_connect'])){
	$arData = getHotelList();
	echo '1';
}

if (isset($_POST['countsum'])) {
		
	$res_request_in =  explode(';',$_POST['res_request_in']);
	$res_request_out =  explode(';',$_POST['res_request_out']);
	
	for($i = 0; $i < sizeof($res_request_in); $i++) {
		$res_request_in[$i] = toDdate($res_request_in[$i]);
		$res_request_out[$i] = toDdate($res_request_out[$i]);
	}
	
	$_POST['res_request_in'] = implode(';',$res_request_in);
	$_POST['res_request_out'] = implode(';',$res_request_out);
	
	include_once(PATH_MODULES.PAGE_RESERV.'.php');	
	include_once(PATH_CONTOLLER.PAGE_RESERV.'.php');	
	include_once(PATH_TEMPLATE.PAGE_RESERV.'.php');	
	
}

if (isset($_POST['addreserv'])) {		
	$res_request_in =  explode(';',$_POST['res_request_in']);
	$res_request_out =  explode(';',$_POST['res_request_out']);
	
	for($i = 0; $i < sizeof($res_request_in); $i++) {
		$res_request_in[$i] = toDdate($res_request_in[$i]);
		$res_request_out[$i] = toDdate($res_request_out[$i]);
	}
	
	$_POST['res_request_in'] = implode(';',$res_request_in);
	$_POST['res_request_out'] = implode(';',$res_request_out);
	
	include_once(PATH_MODULES.PAGE_SENDRESERV.'.php');	
	include_once(PATH_CONTOLLER.PAGE_SENDRESERV.'.php');		
}

if (isset($_POST['about_reserv'])) {
	include_once(PATH_MODULES.PAGE_RESERV.'.php');	
	include_once(PATH_CONTOLLER.PAGE_ABOUTRESERV.'.php');	
	include_once(PATH_TEMPLATE.PAGE_ABOUTRESERV.'.php');
}

if (isset($_POST['login_reserv'])) {
	include_once(PATH_MODULES.PAGE_LOGINUSER.'.php');		
	include_once(PATH_CONTOLLER.PAGE_LOGINUSER.'.php');		
}

if (isset($_POST['reg_reserv'])) {
	include_once(PATH_MODULES.PAGE_CREATEUSER.'.php');		
	include_once(PATH_CONTOLLER.PAGE_CREATEUSER.'.php');		
}

if (isset($_POST['forget_reserv'])) {
	include_once(PATH_MODULES.PAGE_FORGETPASSWORD.'.php');		
	include_once(PATH_CONTOLLER.PAGE_FORGETPASSWORD.'.php');		
}

if (isset($_POST['reload_capcha'])) {			
	echo setCaptcha();
}

if (isset($_POST['reload_clientcapcha'])) {			
	echo '<img src="'.PATH_CAPTCHA.'captcha.php?width=100&height=35&characters=5&use='.time().'" />';	
}

if (isset($_POST['change_current'])) {
	$arCurrencyShow = unserialize(CURRENCY_SHOW_FROM_BANK);
	$current_name = $arMessages["current_choose"][CURRENCYSHOW][MESSAGE_LANG].';';
	$current_price = '1;';
	$current_code = $arMessages["current"][CURRENCYSHOW][MESSAGE_LANG].';';		
	$ex = false;
	
	if (CURRENCYSHOWCOUNTRY == 0) {		
		$file = simplexml_load_file("http://www.cbr.ru/scripts/XML_daily.asp?date_req=".date("d/m/Y"));
		foreach ($file AS $el){
			if (in_array(strval($el->CharCode), $arCurrencyShow)) {
				if ($ex) {
					$current_name .= ';';			
					$current_price .= ';';		
					$current_code .= ';';		
				}	
				$nominal = strval($el->Nominal);
				$price = floatval(str_replace(',', '.', $el->Value));			
				$price = $price/$nominal;
				
				$name = strval($el->Name);		
				if ($arMessages["current_choose"][strval($el->CharCode)][MESSAGE_LANG] != '') $name = $arMessages["current_choose"][strval($el->CharCode)][MESSAGE_LANG];
				$cc = strval($el->CharCode);
				if (isset($arMessages["current"][$cc][MESSAGE_LANG])) $cc = $arMessages["current"][$cc][MESSAGE_LANG];
				
				$current_name .= $name;					
				$current_price .= $price;									
				$current_code .= $cc;
				
				$ex = true;	
			}	
		}	
	}elseif (CURRENCYSHOWCOUNTRY == 1){
		
		$ch = curl_init("https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=3");
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);           
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		$result = curl_exec($ch);
		curl_close($ch);
		
		$file = simplexml_load_string(trim($result));
		
		foreach ($file->row as $row){
			if ($ex) {
				$current_name .= ';';			
				$current_price .= ';';		
				$current_code .= ';';		
			}	
			
			$el = $row->exchangerate->attributes();
			
			$price = floatval(str_replace(',', '.', $el->sale));						
			
			$name = strval($el->ccy);		
			if ($arMessages["current_choose"][$name][MESSAGE_LANG] != '') $name = $arMessages["current_choose"][$name][MESSAGE_LANG];
			$cc = strval($el->ccy);
			if (isset($arMessages["current"][$cc][MESSAGE_LANG])) $cc = $arMessages["current"][$cc][MESSAGE_LANG];
			
			$current_price .= $price;
			$current_name .= $name;					
			$current_code .= $cc;					
			
			$ex = true;	
		}
	}elseif (CURRENCYSHOWCOUNTRY == 2){		
		$file = simplexml_load_file("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
		
		foreach ($file->Cube->Cube->Cube AS $el){			
			$currency = strval($el->attributes()->currency);
			
			if (in_array($currency, $arCurrencyShow)) {
				if ($ex) {
					$current_name .= ';';			
					$current_price .= ';';		
					$current_code .= ';';		
				}	
				$price = floatval(str_replace(',', '.', $el->attributes()->rate));
				if ($price != 0){
					$price = 1/$price;
				}else{
					$price = 0;
				}
				
				$name = $currency;		
				if (isset($arMessages["current_choose"][$currency][MESSAGE_LANG]))	$name = $arMessages["current_choose"][$currency][MESSAGE_LANG];
				$cc = $currency;
				if (isset($arMessages["current"][$cc][MESSAGE_LANG])) $cc = $arMessages["current"][$cc][MESSAGE_LANG];
				
				if ($price != 0){
					$current_name .= $name;					
					$current_price .= $price;
					$current_code .= $cc;
					
					$ex = true;	
				}
			}	
		}	
	}	
	
	if ($ex) echo $current_name.'*'.$current_price.'*'.$current_code;
}

if (isset($_POST['discount'])){
	getDiscount(intval($_POST['number']), protection($_POST['name']));
}

if (isset($_POST['set_pack_service'])){
	$arData = Array();	
	$arInfo = explode(';', $_POST['data']);
	for($i = 0; $i < sizeof($arInfo); $i++){
		if (strlen($arInfo[$i]) > 0){
			$data = explode('_', $arInfo[$i]);
			if (!(isset($arData[$data[0]]))) $arData[$data[0]] = Array();
			$arData[$data[0]][] = $data[1];
		}
	}
	if ($_POST['v'] == 1) $arData[$_POST['i']] = Array();
	if ($_POST['v'] == 2) {
		$arDataNew = Array();
		for($i = 0; $i < sizeof($arData); $i++){
			if ($i != $_POST['i']) $arDataNew[] = $arData[$i];
		}
		$arData = $arDataNew;
	}
	
	$_SESSION['Shelter']['Data']['UsePackageService'] = $arData;
	
	if ($_POST['v'] == 2) echo '0';
		else echo '1';
}

?>