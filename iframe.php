<?
	ob_start();	
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Shelter-Online</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    	
	<link rel="stylesheet" href="style.css" type="text/css" media="screen">
</head>
<body>
	<div class="shelteronline">
		<iframe id="shelteronline" src="./shelter/index.php" width="100%" scrolling="no" frameborder="0" onload="resizeIframe()"></iframe>		
	</div>
	 
	<script type="text/javascript">
		function adjustIFrameSize(id) {
			var myIframe = document.getElementById(id);

			if (myIframe) {
				if (myIframe.contentDocument && myIframe.contentDocument.body.offsetHeight) {
					myIframe.height = myIframe.contentDocument.body.offsetHeight;
					
				} else if (myIframe.document && myIframe.document.body.scrollHeight) {
					myIframe.height = myIframe.document.body.scrollHeight;
				}
				if (myIframe.addEventListener) {
					myIframe.addEventListener("load", resizeIframe, false);
				} else {
					myIframe.attachEvent("onload", resizeIframe);
				}
			}
		}

		function resizeIframe(evt) {
			evt = (evt) ? evt : event;

			var target = (evt.target) ? evt.target : evt.srcElement;
			if (target.nodeType == 9) {
				if (evt.currentTarget && evt.currentTarget.tagName.toLowerCase() == "iframe") {
					target = evt.currentTarget;
				}
			}
			if (target) {
				adjustIFrameSize(target.id);
			}
		}
	</script>
</body>
</html>
<?	ob_end_flush();	?>