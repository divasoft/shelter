<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/*
 * Debug test msg
 * 0JTQvNC40YLRgNC40Lkobml4MUBpbmJveC5ydSkgLSDQvNC10LvQvtGH0L3Ri9C5INC4INGD0LPRgNGO0LzRi9C5INGH0LXQu9C+0LLQtdC6LiAK0JXRgdC70Lgg0LLRiyDRh9C40YLQsNC10YLQtSDRjdGC0L4g0YHQvtC+0LHRidC10L3QuNC1LCDQt9C90LDRh9C40YIg0YMg0LLQsNGBINGF0LLQsNGC0LjRgiDQvNC+0LfQs9C+0LIg0L3QtSDRgdCy0Y/Qt9GL0LLQsNGC0YzRgdGPINGBINC90LjQvC4=
 */

/**
 * Все вопросы на inbox@divasoft.ru
 * Можно все эти настройки вынести в Битрикс, или доработать эту админку. Но... этого никто не оценит, и уж тем более не оплатит.
 */
class DivasoftShelter {
    const ORIGINAL_SITE = "http://klen-rosha.ru";

    private static $room_id;

    public function __construct() {
        // Подсвечиваем выбранный номер
        
        $sroom_id = intval($_SESSION['room_id']);
        $room_id = intval($_REQUEST['room_id']);
        
        if ($_REQUEST['room_id']==-1) { // TODO - сделать чуть лучше
            self::clearRoomId();
        }

        if ($room_id > 0) {
            self::$room_id = $room_id;
            $_SESSION['room_id'] = $room_id;
        } elseif ($sroom_id > 0) {
            self::$room_id = $sroom_id;
        } 

        // TODO: Наверно не надо этого делать. Не просили, не будет.
        if ($_REQUEST['shelter'] == "ReservationNR" && $_REQUEST['id'] > 0 && $_REQUEST['email'] != "" && $_REQUEST['code'] != "") {
            $_SESSION['last_order'] = $_SERVER['REQUEST_URI'];
        }
        if ($_REQUEST['pay_status'] == "Y" && $_SESSION['last_order'] != "" && $_REQUEST['shelter'] != "ReservationNR") {
            $url = $_SESSION['last_order'];
            unset($_SESSION['last_order']);
            header('Location: ' . $url);
        }
        
        /*if ($_REQUEST['mail_test']=="Y") {
            self::mail("test@divasoft.ru", "test", "Проверка");
            die();
        }*/
    }
    
    public static function clearRoomId() {
        unset($_SESSION['room_id']);
    }

    public static function getRoomId() {
        return self::$room_id;
    }
    
    /**
     * Функция вывода в лог-файл
     */
    public static function a2l($sText) {
        $date = date("d/m/Y H:i:s");
        if (!is_string($sText)) {
            $sText = print_r($sText, true);
        }
        $sText = "********    " . $date . " BEGIN LOG    ********\n" . $sText;
        $sText .= "\n********    " . $date . " END LOG    ********\n";
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/shelter/dvs_log.txt", $sText, FILE_APPEND);
        return true;
    }

    /**
     * Нельзя просто так взять, и отправить почту в инбокс, а не в спам.
     * @param type $to
     * @param type $subject
     * @param type $body
     * @return boolean
     */
    public static function mail($to, $subject, $body) {
        require_once __DIR__ . '/mailer/vendor/autoload.php';
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->isSMTP();
            $mail->CharSet = PHPMailer::CHARSET_UTF8;
            $mail->Host = 'ssl://smtp.mail.ru';
            $mail->SMTPAuth = true;
            $mail->Username = 'korabl_kl@mail.ru';
            $mail->Password = '*****'; // Нене
            $mail->SMTPSecure = 'SSL';
            $mail->Port = 465;
            //
            //Recipients
            $mail->setFrom('korabl_kl@mail.ru', 'Administrator');
            $mail->addAddress($to);
            $mail->addReplyTo('korabl_kl@mail.ru');
            $mail->addBCC('klen@divasoft.ru');


            // Content
            $mail->isHTML(true);   // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $body;
            //$mail->AltBody = $body;

            $mail->send();
            return true;
        } catch (Exception $e) {
            self::a2l($mail->ErrorInfo);
            //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
        return false;
    }
}

new DivasoftShelter();