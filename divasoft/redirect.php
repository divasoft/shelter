<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Shelter-Online Payment Redirect</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    	
</head>
<body>
    <script>
    var url = '<?= urldecode($_REQUEST['url'])?>';
    if (window.parent.location) {
        window.parent.location = url;
        console.log("parent "+url);
    } else {
        window.location = url;
        console.log("inner "+url);
    }
    </script>
</body>
</html>